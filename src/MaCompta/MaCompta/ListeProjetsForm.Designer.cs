﻿namespace MaCompta
{
    partial class ListeProjetsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.myInfoBar1 = new My.Forms.Controls.Panels.MyInfoBar();
            this.tblListe = new My.Forms.Controls.ListViews.XPTable.Models.Table();
            this.cmListe = new My.Forms.Controls.ListViews.XPTable.Models.ColumnModel();
            this.tcId = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcDate = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcDétails = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcDébit = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcCrédit = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tmListe = new My.Forms.Controls.ListViews.XPTable.Models.TableModel();
            this.dtpDateDébut = new System.Windows.Forms.DateTimePicker();
            this.dtpDateFin = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tblListe)).BeginInit();
            this.SuspendLayout();
            // 
            // myInfoBar1
            // 
            this.myInfoBar1.BackColor = System.Drawing.Color.Transparent;
            this.myInfoBar1.BackStyle = My.Forms.Controls.Panels.BackStyle.Gradient;
            this.myInfoBar1.BorderSide = System.Windows.Forms.Border3DSide.Bottom;
            this.myInfoBar1.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.myInfoBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.myInfoBar1.GradientEndColor = System.Drawing.Color.Transparent;
            this.myInfoBar1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.myInfoBar1.GradientStartColor = System.Drawing.Color.White;
            this.myInfoBar1.Image = global::MaCompta.Properties.Resources.file_manager_New1_32;
            this.myInfoBar1.ImageAlign = My.Forms.Controls.Panels.ImageAlignment.TopLeft;
            this.myInfoBar1.ImageOffsetX = 2;
            this.myInfoBar1.ImageOffsetY = 10;
            this.myInfoBar1.Location = new System.Drawing.Point(0, 0);
            this.myInfoBar1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.myInfoBar1.Name = "myInfoBar1";
            this.myInfoBar1.Size = new System.Drawing.Size(917, 52);
            this.myInfoBar1.TabIndex = 1;
            this.myInfoBar1.Text1 = "Détails des Projets";
            this.myInfoBar1.Text1Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold);
            this.myInfoBar1.Text1ForeColor = System.Drawing.SystemColors.ControlText;
            this.myInfoBar1.Text1OffsetX = 0;
            this.myInfoBar1.Text1OffsetY = 0;
            this.myInfoBar1.Text2 = "Informations sur les différents projets.";
            this.myInfoBar1.Text2Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.myInfoBar1.Text2ForeColor = System.Drawing.SystemColors.ControlText;
            this.myInfoBar1.Text2OffsetX = 20;
            this.myInfoBar1.Text2OffsetY = 0;
            // 
            // tblListe
            // 
            this.tblListe.AlternatingRowColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tblListe.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tblListe.ColumnModel = this.cmListe;
            this.tblListe.CustomEditKey = System.Windows.Forms.Keys.None;
            this.tblListe.EditStartAction = My.Forms.Controls.ListViews.XPTable.Editors.EditStartAction.CustomKey;
            this.tblListe.EnableHeaderContextMenu = false;
            this.tblListe.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.tblListe.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tblListe.GridLines = My.Forms.Controls.ListViews.XPTable.Models.GridLines.Columns;
            this.tblListe.HeaderFont = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold);
            this.tblListe.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.tblListe.Location = new System.Drawing.Point(12, 80);
            this.tblListe.Name = "tblListe";
            this.tblListe.NoItemsText = "Aucun projet!";
            this.tblListe.Size = new System.Drawing.Size(893, 476);
            this.tblListe.TabIndex = 2;
            this.tblListe.TableModel = this.tmListe;
            this.tblListe.Text = "table1";
            this.tblListe.DoubleClick += new System.EventHandler(this.tblListe_DoubleClick);
            // 
            // cmListe
            // 
            this.cmListe.AutoAdustWidth = true;
            this.cmListe.Columns.AddRange(new My.Forms.Controls.ListViews.XPTable.Models.Column[] {
            this.tcId,
            this.tcDate,
            this.tcDétails,
            this.tcDébit,
            this.tcCrédit});
            this.cmListe.HeaderHeight = 29;
            // 
            // tcId
            // 
            this.tcId.Text = "Id";
            this.tcId.Visible = false;
            this.tcId.Width = 16;
            this.tcId.WidthPercentage = 1D;
            // 
            // tcDate
            // 
            this.tcDate.Text = "Date";
            this.tcDate.Width = 71;
            this.tcDate.WidthPercentage = 8D;
            // 
            // tcDétails
            // 
            this.tcDétails.Text = "Détails";
            this.tcDétails.Width = 651;
            this.tcDétails.WidthPercentage = 73D;
            // 
            // tcDébit
            // 
            this.tcDébit.Alignment = My.Forms.Controls.ListViews.XPTable.Models.ColumnAlignment.Right;
            this.tcDébit.Text = "Débit";
            this.tcDébit.Width = 80;
            this.tcDébit.WidthPercentage = 9D;
            // 
            // tcCrédit
            // 
            this.tcCrédit.Alignment = My.Forms.Controls.ListViews.XPTable.Models.ColumnAlignment.Right;
            this.tcCrédit.Text = "Crédit";
            this.tcCrédit.Width = 80;
            this.tcCrédit.WidthPercentage = 9D;
            // 
            // tmListe
            // 
            this.tmListe.RowHeight = 25;
            // 
            // dtpDateDébut
            // 
            this.dtpDateDébut.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpDateDébut.Checked = false;
            this.dtpDateDébut.Location = new System.Drawing.Point(204, 52);
            this.dtpDateDébut.Name = "dtpDateDébut";
            this.dtpDateDébut.ShowCheckBox = true;
            this.dtpDateDébut.Size = new System.Drawing.Size(268, 27);
            this.dtpDateDébut.TabIndex = 23;
            this.dtpDateDébut.ValueChanged += new System.EventHandler(this.dtpDateDébut_ValueChanged);
            this.dtpDateDébut.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dtpDateDébut_MouseUp);
            // 
            // dtpDateFin
            // 
            this.dtpDateFin.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpDateFin.Location = new System.Drawing.Point(582, 52);
            this.dtpDateFin.Name = "dtpDateFin";
            this.dtpDateFin.Size = new System.Drawing.Size(243, 27);
            this.dtpDateFin.TabIndex = 24;
            this.dtpDateFin.ValueChanged += new System.EventHandler(this.dtpDateFin_ValueChanged);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(92, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 20);
            this.label2.TabIndex = 21;
            this.label2.Text = "Date de début";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(492, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 20);
            this.label1.TabIndex = 22;
            this.label1.Text = "Date de fin";
            // 
            // ListeProjetsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MaCompta.Properties.Resources.lake_1024;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(917, 568);
            this.Controls.Add(this.dtpDateDébut);
            this.Controls.Add(this.dtpDateFin);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tblListe);
            this.Controls.Add(this.myInfoBar1);
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ListeProjetsForm";
            this.Text = "ListeProjetsForm";
            this.Shown += new System.EventHandler(this.ListeProjetsForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.tblListe)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private My.Forms.Controls.Panels.MyInfoBar myInfoBar1;
        private My.Forms.Controls.ListViews.XPTable.Models.Table tblListe;
        private My.Forms.Controls.ListViews.XPTable.Models.TableModel tmListe;
        private My.Forms.Controls.ListViews.XPTable.Models.ColumnModel cmListe;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcId;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcDétails;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcDébit;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcCrédit;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcDate;
        public System.Windows.Forms.DateTimePicker dtpDateDébut;
        public System.Windows.Forms.DateTimePicker dtpDateFin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}