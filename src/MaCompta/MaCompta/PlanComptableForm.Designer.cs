﻿namespace MaCompta
{
    partial class PlanComptableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.myInfoBar1 = new My.Forms.Controls.Panels.MyInfoBar();
            this.tblPlanComptable = new My.Forms.Controls.ListViews.XPTable.Models.Table();
            this.cmPlanComptable = new My.Forms.Controls.ListViews.XPTable.Models.ColumnModel();
            this.tcIdCompte = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcIdTypeCompte = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcCompte = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcType = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcSolde1 = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcSodle2 = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tmPlanComptable = new My.Forms.Controls.ListViews.XPTable.Models.TableModel();
            this.rmBtnComptes = new My.Forms.Controls.Ribbon.RibbonMenuButton();
            ((System.ComponentModel.ISupportInitialize)(this.tblPlanComptable)).BeginInit();
            this.SuspendLayout();
            // 
            // myInfoBar1
            // 
            this.myInfoBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.myInfoBar1.BackColor = System.Drawing.Color.Transparent;
            this.myInfoBar1.BackStyle = My.Forms.Controls.Panels.BackStyle.Gradient;
            this.myInfoBar1.BorderSide = System.Windows.Forms.Border3DSide.Bottom;
            this.myInfoBar1.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.myInfoBar1.GradientEndColor = System.Drawing.Color.Transparent;
            this.myInfoBar1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.myInfoBar1.GradientStartColor = System.Drawing.Color.White;
            this.myInfoBar1.Image = global::MaCompta.Properties.Resources.my_documents_32;
            this.myInfoBar1.ImageAlign = My.Forms.Controls.Panels.ImageAlignment.TopLeft;
            this.myInfoBar1.ImageOffsetX = 2;
            this.myInfoBar1.ImageOffsetY = 10;
            this.myInfoBar1.Location = new System.Drawing.Point(0, 0);
            this.myInfoBar1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.myInfoBar1.Name = "myInfoBar1";
            this.myInfoBar1.Size = new System.Drawing.Size(708, 54);
            this.myInfoBar1.TabIndex = 0;
            this.myInfoBar1.Text1 = "Plan Comptable";
            this.myInfoBar1.Text1Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold);
            this.myInfoBar1.Text1ForeColor = System.Drawing.SystemColors.ControlText;
            this.myInfoBar1.Text1OffsetX = 0;
            this.myInfoBar1.Text1OffsetY = 0;
            this.myInfoBar1.Text2 = "Gestion de la liste des comptes";
            this.myInfoBar1.Text2Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.myInfoBar1.Text2ForeColor = System.Drawing.SystemColors.ControlText;
            this.myInfoBar1.Text2OffsetX = 20;
            this.myInfoBar1.Text2OffsetY = 0;
            // 
            // tblPlanComptable
            // 
            this.tblPlanComptable.AlternatingRowColor = System.Drawing.Color.White;
            this.tblPlanComptable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tblPlanComptable.ColumnModel = this.cmPlanComptable;
            this.tblPlanComptable.CustomEditKey = System.Windows.Forms.Keys.None;
            this.tblPlanComptable.EditStartAction = My.Forms.Controls.ListViews.XPTable.Editors.EditStartAction.CustomKey;
            this.tblPlanComptable.EnableHeaderContextMenu = false;
            this.tblPlanComptable.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.tblPlanComptable.FullRowSelect = true;
            this.tblPlanComptable.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tblPlanComptable.GridLines = My.Forms.Controls.ListViews.XPTable.Models.GridLines.Columns;
            this.tblPlanComptable.HeaderFont = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold);
            this.tblPlanComptable.Location = new System.Drawing.Point(12, 63);
            this.tblPlanComptable.Name = "tblPlanComptable";
            this.tblPlanComptable.NoItemsText = "Aucun compte!";
            this.tblPlanComptable.Size = new System.Drawing.Size(692, 495);
            this.tblPlanComptable.TabIndex = 13;
            this.tblPlanComptable.TableModel = this.tmPlanComptable;
            this.tblPlanComptable.Text = "table1";
            this.tblPlanComptable.DoubleClick += new System.EventHandler(this.tblPlanComptable_DoubleClick);
            // 
            // cmPlanComptable
            // 
            this.cmPlanComptable.AutoAdustWidth = true;
            this.cmPlanComptable.Columns.AddRange(new My.Forms.Controls.ListViews.XPTable.Models.Column[] {
            this.tcIdCompte,
            this.tcIdTypeCompte,
            this.tcCompte,
            this.tcType,
            this.tcSolde1,
            this.tcSodle2});
            this.cmPlanComptable.HeaderHeight = 29;
            // 
            // tcIdCompte
            // 
            this.tcIdCompte.Text = "Id";
            this.tcIdCompte.Visible = false;
            this.tcIdCompte.Width = 16;
            this.tcIdCompte.WidthPercentage = 0D;
            // 
            // tcIdTypeCompte
            // 
            this.tcIdTypeCompte.Text = "IdTypeCompte";
            this.tcIdTypeCompte.Visible = false;
            this.tcIdTypeCompte.Width = 16;
            this.tcIdTypeCompte.WidthPercentage = 0D;
            // 
            // tcCompte
            // 
            this.tcCompte.Text = "Compte";
            this.tcCompte.Width = 415;
            this.tcCompte.WidthPercentage = 60D;
            // 
            // tcType
            // 
            this.tcType.Text = "Type";
            this.tcType.Width = 96;
            this.tcType.WidthPercentage = 14D;
            // 
            // tcSolde1
            // 
            this.tcSolde1.Alignment = My.Forms.Controls.ListViews.XPTable.Models.ColumnAlignment.Right;
            this.tcSolde1.Width = 83;
            this.tcSolde1.WidthPercentage = 12D;
            // 
            // tcSodle2
            // 
            this.tcSodle2.Alignment = My.Forms.Controls.ListViews.XPTable.Models.ColumnAlignment.Right;
            this.tcSodle2.Width = 83;
            this.tcSodle2.WidthPercentage = 12D;
            // 
            // tmPlanComptable
            // 
            this.tmPlanComptable.RowHeight = 25;
            // 
            // rmBtnComptes
            // 
            this.rmBtnComptes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rmBtnComptes.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
            this.rmBtnComptes.ArrowSize = 2F;
            this.rmBtnComptes.BackColor = System.Drawing.Color.Transparent;
            this.rmBtnComptes.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
            this.rmBtnComptes.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
            this.rmBtnComptes.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
            this.rmBtnComptes.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
            this.rmBtnComptes.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.rmBtnComptes.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.rmBtnComptes.FadingSpeed = 35;
            this.rmBtnComptes.FlatAppearance.BorderSize = 0;
            this.rmBtnComptes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rmBtnComptes.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rmBtnComptes.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
            this.rmBtnComptes.Image = global::MaCompta.Properties.Resources.my_documents_32;
            this.rmBtnComptes.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Left;
            this.rmBtnComptes.ImageOffset = 8;
            this.rmBtnComptes.IsPressed = false;
            this.rmBtnComptes.KeepPress = false;
            this.rmBtnComptes.Location = new System.Drawing.Point(440, 3);
            this.rmBtnComptes.Margin = new System.Windows.Forms.Padding(0);
            this.rmBtnComptes.MaxImageSize = new System.Drawing.Point(32, 32);
            this.rmBtnComptes.MenuPos = new System.Drawing.Point(0, 0);
            this.rmBtnComptes.Name = "rmBtnComptes";
            this.rmBtnComptes.Radius = 25;
            this.rmBtnComptes.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
            this.rmBtnComptes.Size = new System.Drawing.Size(268, 49);
            this.rmBtnComptes.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
            this.rmBtnComptes.SplitDistance = 20;
            this.rmBtnComptes.TabIndex = 14;
            this.rmBtnComptes.Text = "Modifier le Plan Comptable";
            this.rmBtnComptes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rmBtnComptes.Title = "Comptes";
            this.rmBtnComptes.UseVisualStyleBackColor = false;
            this.rmBtnComptes.Click += new System.EventHandler(this.rmBtnComptes_Click);
            // 
            // PlanComptableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MaCompta.Properties.Resources.Atra_Dot___Blue_1920x1200;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(717, 571);
            this.Controls.Add(this.rmBtnComptes);
            this.Controls.Add(this.tblPlanComptable);
            this.Controls.Add(this.myInfoBar1);
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "PlanComptableForm";
            this.Text = "PlanComptableForm";
            this.Shown += new System.EventHandler(this.PlanComptableForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.tblPlanComptable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private My.Forms.Controls.Panels.MyInfoBar myInfoBar1;
        private My.Forms.Controls.ListViews.XPTable.Models.Table tblPlanComptable;
        private My.Forms.Controls.ListViews.XPTable.Models.ColumnModel cmPlanComptable;
        private My.Forms.Controls.ListViews.XPTable.Models.TableModel tmPlanComptable;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcCompte;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcType;
        private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnComptes;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcSolde1;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcIdCompte;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcIdTypeCompte;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcSodle2;
    }
}