﻿using Controller;

using My.Forms.Controls.ListViews.XPTable.Models;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MaCompta
{
	public partial class BilanForm : Form
	{
		#region Fields

		private BilanController _controller;

		#endregion Fields

		#region Constructors

		public BilanForm()
		{
			InitializeComponent();
			Initialisation();
			ChargerBilan(DateTime.Now);
			ChargerGraphes();
		}

		#endregion Constructors

		#region Private Methods

		private void AfficherDétails()
		{
			if (tblBilan.TableModel.Selections.SelectedItems.Length != 0) {
			    Row ligne = tblBilan.TableModel.Selections.SelectedItems[0];
			    if (!string.IsNullOrEmpty(ligne.Cells[1].Text)) {
			        int idTypeCompte = int.Parse(ligne.Cells[1].Text);
			        if (idTypeCompte == 2 || idTypeCompte == 3) {
			            //Compte normal et compte de sous-groupe: 
			            //Afficher liste des transactions dans le compte
			            new ListeTransactionsForm(ligne.Cells[2].Text.Trim()).Show();
			        }//if
			        else {
			            //Titre, total de sous-groupe ou grand total: 
			            //Affiche les détails des comptes
			            new CompteForm(ligne.Cells[2].Text.Trim()).Show();
			        }//else
			    }//if
			}//if
		}

		void BilanForm_Shown(object sender, EventArgs e)
		{
			WindowState = FormWindowState.Maximized;
		}

		/// <summary>
		/// Affichage du bilan
		/// </summary>
		private void ChargerBilan(DateTime dateFin)
		{
			_controller.ChargerBilan(dateFin, tblBilan, ckMasquerComptes.Checked, ckAfficherZéro.Checked);
		}

		/// <summary>
		/// Affichage des graphes du bilan
		/// </summary>
		private void ChargerGraphes()
		{
			_controller.ChargerGraphes(tblBilan, pcActifs, pcPassifs, pcRevenus, pcDepenses);
		}

		private void Initialisation()
		{
			_controller = new BilanController(Program.BD, Program.CultureIG);
		}

		void ckAfficherZéro_Click(object sender, EventArgs e)
		{
			ChargerBilan(dtpDateFin.Value);
		}

		void ckMasquerComptes_Click(object sender, EventArgs e)
		{
			ckAfficherZéro.Enabled = !ckMasquerComptes.Checked;
			ChargerBilan(dtpDateFin.Value);
		}

		void dtpDateFin_ValueChanged(object sender, EventArgs e)
		{
			ChargerBilan(dtpDateFin.Value);
		}

		void rmBtnRaffraichir_Click(object sender, EventArgs e)
		{
			ChargerBilan(dtpDateFin.Value);
		}

		void tblBilan_DoubleClick(object sender, EventArgs e)
		{
			AfficherDétails();
		}

		#endregion Private Methods
	}
}