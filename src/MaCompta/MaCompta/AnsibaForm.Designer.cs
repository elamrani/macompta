﻿namespace MaCompta {
    partial class AnsibaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.myInfoBar1 = new My.Forms.Controls.Panels.MyInfoBar();
            this.tmAnsiba = new My.Forms.Controls.ListViews.XPTable.Models.TableModel();
            this.tblAnsiba = new My.Forms.Controls.ListViews.XPTable.Models.Table();
            this.cmAnsiba = new My.Forms.Controls.ListViews.XPTable.Models.ColumnModel();
            this.tcId = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcDate = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcMontantTotal = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcMontantNisab = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcMontantZakat = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.ckSupprimee = new My.Forms.Controls.ListViews.XPTable.Models.CheckBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.tblAnsiba)).BeginInit();
            this.SuspendLayout();
            // 
            // myInfoBar1
            // 
            this.myInfoBar1.BackColor = System.Drawing.Color.Transparent;
            this.myInfoBar1.BackStyle = My.Forms.Controls.Panels.BackStyle.Gradient;
            this.myInfoBar1.BorderSide = System.Windows.Forms.Border3DSide.Bottom;
            this.myInfoBar1.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.myInfoBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.myInfoBar1.GradientEndColor = System.Drawing.Color.Transparent;
            this.myInfoBar1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.myInfoBar1.GradientStartColor = System.Drawing.Color.White;
            this.myInfoBar1.Image = global::MaCompta.Properties.Resources.my_documents_32;
            this.myInfoBar1.ImageAlign = My.Forms.Controls.Panels.ImageAlignment.TopLeft;
            this.myInfoBar1.ImageOffsetX = 2;
            this.myInfoBar1.ImageOffsetY = 10;
            this.myInfoBar1.Location = new System.Drawing.Point(0, 0);
            this.myInfoBar1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.myInfoBar1.Name = "myInfoBar1";
            this.myInfoBar1.Size = new System.Drawing.Size(747, 52);
            this.myInfoBar1.TabIndex = 1;
            this.myInfoBar1.Text1 = "النصاب";
            this.myInfoBar1.Text1Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.myInfoBar1.Text1ForeColor = System.Drawing.SystemColors.ControlText;
            this.myInfoBar1.Text1OffsetX = 0;
            this.myInfoBar1.Text1OffsetY = 0;
            this.myInfoBar1.Text2 = "Montants a payer";
            this.myInfoBar1.Text2Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.myInfoBar1.Text2ForeColor = System.Drawing.SystemColors.ControlText;
            this.myInfoBar1.Text2OffsetX = 20;
            this.myInfoBar1.Text2OffsetY = 0;
            // 
            // tmAnsiba
            // 
            this.tmAnsiba.RowHeight = 25;
            // 
            // tblAnsiba
            // 
            this.tblAnsiba.AlternatingRowColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tblAnsiba.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tblAnsiba.ColumnModel = this.cmAnsiba;
            this.tblAnsiba.CustomEditKey = System.Windows.Forms.Keys.None;
            this.tblAnsiba.EditStartAction = My.Forms.Controls.ListViews.XPTable.Editors.EditStartAction.CustomKey;
            this.tblAnsiba.EnableHeaderContextMenu = false;
            this.tblAnsiba.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.tblAnsiba.FullRowSelect = true;
            this.tblAnsiba.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tblAnsiba.GridLines = My.Forms.Controls.ListViews.XPTable.Models.GridLines.Columns;
            this.tblAnsiba.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.tblAnsiba.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.tblAnsiba.Location = new System.Drawing.Point(12, 150);
            this.tblAnsiba.Name = "tblAnsiba";
            this.tblAnsiba.NoItemsText = "Aucune information à afficher";
            this.tblAnsiba.Size = new System.Drawing.Size(723, 377);
            this.tblAnsiba.TabIndex = 14;
            this.tblAnsiba.TableModel = this.tmAnsiba;
            this.tblAnsiba.Text = "أنصبة";
            // 
            // cmAnsiba
            // 
            this.cmAnsiba.AutoAdustWidth = true;
            this.cmAnsiba.Columns.AddRange(new My.Forms.Controls.ListViews.XPTable.Models.Column[] {
            this.tcId,
            this.tcDate,
            this.tcMontantTotal,
            this.tcMontantNisab,
            this.tcMontantZakat,
            this.ckSupprimee});
            this.cmAnsiba.HeaderHeight = 29;
            // 
            // tcId
            // 
            this.tcId.Text = "ID";
            this.tcId.Width = 36;
            this.tcId.WidthPercentage = 5D;
            // 
            // tcDate
            // 
            this.tcDate.Text = "Date";
            this.tcDate.Width = 108;
            this.tcDate.WidthPercentage = 15D;
            // 
            // tcMontantTotal
            // 
            this.tcMontantTotal.Alignment = My.Forms.Controls.ListViews.XPTable.Models.ColumnAlignment.Right;
            this.tcMontantTotal.Text = "Montant Total";
            this.tcMontantTotal.Width = 108;
            this.tcMontantTotal.WidthPercentage = 15D;
            // 
            // tcMontantNisab
            // 
            this.tcMontantNisab.Alignment = My.Forms.Controls.ListViews.XPTable.Models.ColumnAlignment.Right;
            this.tcMontantNisab.Text = "Montant Nisab";
            this.tcMontantNisab.Width = 108;
            this.tcMontantNisab.WidthPercentage = 15D;
            // 
            // tcMontantZakat
            // 
            this.tcMontantZakat.Alignment = My.Forms.Controls.ListViews.XPTable.Models.ColumnAlignment.Right;
            this.tcMontantZakat.Text = "Montant Zakat";
            this.tcMontantZakat.Width = 108;
            this.tcMontantZakat.WidthPercentage = 15D;
            // 
            // ckSupprimee
            // 
            this.ckSupprimee.Text = "Supprimee";
            this.ckSupprimee.Width = 93;
            this.ckSupprimee.WidthPercentage = 13D;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(12, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Date";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(104, 60);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(226, 24);
            this.dateTimePicker1.TabIndex = 17;
            // 
            // AnsibaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MaCompta.Properties.Resources.lake_1024;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(747, 539);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tblAnsiba);
            this.Controls.Add(this.myInfoBar1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AnsibaForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "الأنصبة";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.tblAnsiba)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private My.Forms.Controls.Panels.MyInfoBar myInfoBar1;
        private My.Forms.Controls.ListViews.XPTable.Models.TableModel tmAnsiba;
        private My.Forms.Controls.ListViews.XPTable.Models.Table tblAnsiba;
        private My.Forms.Controls.ListViews.XPTable.Models.ColumnModel cmAnsiba;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcId;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcDate;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcMontantTotal;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcMontantNisab;
        private My.Forms.Controls.ListViews.XPTable.Models.CheckBoxColumn ckSupprimee;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcMontantZakat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}