﻿using Controller;

using My.Forms.Controls.ListViews.XPTable.Models;
using My.Forms.MsgBoxes;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MaCompta
{
	public partial class ListeTransactionsForm : Form
	{
		#region Fields

		private string _compte_a_charger; //ID-Nom du compte
		private ListeTransactionsController _controller;
		private DateTime? _date_début;
		private DateTime _date_fin;

		#endregion Fields

		#region Constructors

		public ListeTransactionsForm()
		{
			InitializeComponent();
			Initialisation();
			ChargerListe();
		}

		public ListeTransactionsForm(string sCompteACharger)
		{
			InitializeComponent();
			Initialisation();
			_compte_a_charger = sCompteACharger;
			ChargerListe();
		}

		public ListeTransactionsForm(string sCompteACharger, DateTime? dtDateDébut, DateTime dtDateFin)
		{
			InitializeComponent();
			Initialisation();
			_compte_a_charger = sCompteACharger;
			_date_début = dtDateDébut;
			_date_fin = dtDateFin;
			ChargerListe();
		}

		#endregion Constructors

		#region Private Methods

		/// <summary>
		/// Charger la liste des transactions d'un compte donné à une date donnée.
		/// </summary>
		private void ChargerListe()
		{
			Exception eValeurRetournée = _controller.ChargerListeTransactions(tblListe, _compte_a_charger, _date_début, _date_fin);
			if ( eValeurRetournée != null )
			{
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement " +
			        "des listes des opérations";
			    msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "Les listes des opérations n'a pas pu être chargée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
				msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
		}

		private void Initialisation()
		{
			_controller = new ListeTransactionsController(Program.BD, Program.CultureIG);
			_compte_a_charger = null;
			_date_début = null;
			_date_fin = DateTime.Now;
		}

		void ListeTransactionsForm_Shown(object sender, EventArgs e)
		{
			WindowState = FormWindowState.Maximized;
		}

		void dtpDateDébut_MouseUp(object sender, MouseEventArgs e)
		{
			if ( dtpDateDébut.Checked )
			{
			    _date_début = dtpDateDébut.Value;
			}//if
			else
			{
			    _date_début = null;
			}//else
			ChargerListe();
		}

		void dtpDateDébut_ValueChanged(object sender, EventArgs e)
		{
			if ( dtpDateDébut.Checked )
			{
			    _date_début = dtpDateDébut.Value;
			    ChargerListe();
			}//if
		}

		void dtpDateFin_ValueChanged(object sender, EventArgs e)
		{
			_date_fin = dtpDateFin.Value;
			ChargerListe();
		}

		void tblListe_DoubleClick(object sender, EventArgs e)
		{
			if ( tblListe.TableModel.Selections.SelectedItems.Length != 0 )
			{
			    Row ligne = tblListe.TableModel.Selections.SelectedItems[0];
			    if ( !string.IsNullOrEmpty(ligne.Cells[1].Text) && !string.IsNullOrEmpty(ligne.Cells[2].Text) ) //Id et Nom du compte
			    {
			        string sNouveauCompteACharger = ligne.Cells[1].Text + "-" + ligne.Cells[2].Text;
			        if ( sNouveauCompteACharger != _compte_a_charger )
			        {
			            new ListeTransactionsForm(sNouveauCompteACharger, _date_début, _date_fin).Show();
			        }//if
			        else if ( !string.IsNullOrEmpty(ligne.Cells[0].Text) )
			        {
			            //TODO: Trouver un moyen de Charger la transaction dans la fenêtre principale
			        }//else if
			    }//if
			}//if
		}

		#endregion Private Methods
	}
}