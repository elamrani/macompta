﻿namespace MaCompta
{
	partial class BilanForm
	{
		#region " Fields "

		private System.Windows.Forms.CheckBox ckAfficherZéro; // End of Protected Method Dispose
		private System.Windows.Forms.CheckBox ckMasquerComptes;
		private My.Forms.Controls.ListViews.XPTable.Models.ColumnModel cmBilan;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.DateTimePicker dtpDateFin; // End of Protected Method Dispose
		private System.Windows.Forms.Label label1;
		private My.Forms.Controls.Panels.MyInfoBar myInfoBar1;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnRaffraichir;
		private My.Forms.Controls.ListViews.XPTable.Models.Table tblBilan;
		private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcCompte;
		private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcIdCompte;
		private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcIdTypeCompte;
		private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcSolde1;
		private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcSolde2;
		private My.Forms.Controls.ListViews.XPTable.Models.TableModel tmBilan;

		#endregion " Fields "

		#region " Private Methods "

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.myInfoBar1 = new My.Forms.Controls.Panels.MyInfoBar();
            this.tblBilan = new My.Forms.Controls.ListViews.XPTable.Models.Table();
            this.cmBilan = new My.Forms.Controls.ListViews.XPTable.Models.ColumnModel();
            this.tcIdCompte = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcIdTypeCompte = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcCompte = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcSolde1 = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcSolde2 = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tmBilan = new My.Forms.Controls.ListViews.XPTable.Models.TableModel();
            this.ckMasquerComptes = new System.Windows.Forms.CheckBox();
            this.ckAfficherZéro = new System.Windows.Forms.CheckBox();
            this.rmBtnRaffraichir = new My.Forms.Controls.Ribbon.RibbonMenuButton();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpDateFin = new System.Windows.Forms.DateTimePicker();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.label2 = new System.Windows.Forms.Label();
            this.pcActifs = new My.Forms.Controls.Charts.PieChart.PieChart();
            this.label3 = new System.Windows.Forms.Label();
            this.pcPassifs = new My.Forms.Controls.Charts.PieChart.PieChart();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.label4 = new System.Windows.Forms.Label();
            this.pcRevenus = new My.Forms.Controls.Charts.PieChart.PieChart();
            this.label5 = new System.Windows.Forms.Label();
            this.pcDepenses = new My.Forms.Controls.Charts.PieChart.PieChart();
            ((System.ComponentModel.ISupportInitialize)(this.tblBilan)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.SuspendLayout();
            // 
            // myInfoBar1
            // 
            this.myInfoBar1.BackColor = System.Drawing.Color.Transparent;
            this.myInfoBar1.BackStyle = My.Forms.Controls.Panels.BackStyle.Gradient;
            this.myInfoBar1.BorderSide = System.Windows.Forms.Border3DSide.Bottom;
            this.myInfoBar1.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.myInfoBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.myInfoBar1.GradientEndColor = System.Drawing.Color.Transparent;
            this.myInfoBar1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.myInfoBar1.GradientStartColor = System.Drawing.Color.White;
            this.myInfoBar1.Image = global::MaCompta.Properties.Resources.document_open_8_32;
            this.myInfoBar1.ImageAlign = My.Forms.Controls.Panels.ImageAlignment.TopLeft;
            this.myInfoBar1.ImageOffsetX = 2;
            this.myInfoBar1.ImageOffsetY = 10;
            this.myInfoBar1.Location = new System.Drawing.Point(0, 0);
            this.myInfoBar1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.myInfoBar1.Name = "myInfoBar1";
            this.myInfoBar1.Size = new System.Drawing.Size(872, 52);
            this.myInfoBar1.TabIndex = 0;
            this.myInfoBar1.Text1 = "Bilan Général";
            this.myInfoBar1.Text1Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.myInfoBar1.Text1ForeColor = System.Drawing.SystemColors.ControlText;
            this.myInfoBar1.Text1OffsetX = 0;
            this.myInfoBar1.Text1OffsetY = 0;
            this.myInfoBar1.Text2 = "Affichage des soldes des comptes";
            this.myInfoBar1.Text2Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.myInfoBar1.Text2ForeColor = System.Drawing.SystemColors.ControlText;
            this.myInfoBar1.Text2OffsetX = 20;
            this.myInfoBar1.Text2OffsetY = 0;
            // 
            // tblBilan
            // 
            this.tblBilan.AlternatingRowColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tblBilan.ColumnModel = this.cmBilan;
            this.tblBilan.CustomEditKey = System.Windows.Forms.Keys.None;
            this.tblBilan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblBilan.EditStartAction = My.Forms.Controls.ListViews.XPTable.Editors.EditStartAction.CustomKey;
            this.tblBilan.EnableHeaderContextMenu = false;
            this.tblBilan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.tblBilan.FullRowSelect = true;
            this.tblBilan.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tblBilan.GridLines = My.Forms.Controls.ListViews.XPTable.Models.GridLines.Columns;
            this.tblBilan.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.tblBilan.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.tblBilan.Location = new System.Drawing.Point(3, 3);
            this.tblBilan.Name = "tblBilan";
            this.tblBilan.NoItemsText = "Aucun compte!";
            this.tblBilan.Size = new System.Drawing.Size(834, 440);
            this.tblBilan.TabIndex = 13;
            this.tblBilan.TableModel = this.tmBilan;
            this.tblBilan.Text = "table1";
            this.tblBilan.DoubleClick += new System.EventHandler(this.tblBilan_DoubleClick);
            // 
            // cmBilan
            // 
            this.cmBilan.AutoAdustWidth = true;
            this.cmBilan.Columns.AddRange(new My.Forms.Controls.ListViews.XPTable.Models.Column[] {
            this.tcIdCompte,
            this.tcIdTypeCompte,
            this.tcCompte,
            this.tcSolde1,
            this.tcSolde2});
            this.cmBilan.HeaderHeight = 29;
            // 
            // tcIdCompte
            // 
            this.tcIdCompte.Text = "IdCompte";
            this.tcIdCompte.Visible = false;
            this.tcIdCompte.Width = 16;
            this.tcIdCompte.WidthPercentage = 0D;
            // 
            // tcIdTypeCompte
            // 
            this.tcIdTypeCompte.Text = "IdTypeCompte";
            this.tcIdTypeCompte.Visible = false;
            this.tcIdTypeCompte.Width = 16;
            this.tcIdTypeCompte.WidthPercentage = 0D;
            // 
            // tcCompte
            // 
            this.tcCompte.Text = "Compte";
            this.tcCompte.Width = 617;
            this.tcCompte.WidthPercentage = 74D;
            // 
            // tcSolde1
            // 
            this.tcSolde1.Alignment = My.Forms.Controls.ListViews.XPTable.Models.ColumnAlignment.Right;
            this.tcSolde1.Text = "";
            this.tcSolde1.Width = 100;
            this.tcSolde1.WidthPercentage = 12D;
            // 
            // tcSolde2
            // 
            this.tcSolde2.Alignment = My.Forms.Controls.ListViews.XPTable.Models.ColumnAlignment.Right;
            this.tcSolde2.Width = 100;
            this.tcSolde2.WidthPercentage = 12D;
            // 
            // tmBilan
            // 
            this.tmBilan.RowHeight = 25;
            // 
            // ckMasquerComptes
            // 
            this.ckMasquerComptes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckMasquerComptes.AutoSize = true;
            this.ckMasquerComptes.BackColor = System.Drawing.Color.Transparent;
            this.ckMasquerComptes.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckMasquerComptes.Location = new System.Drawing.Point(356, 53);
            this.ckMasquerComptes.Name = "ckMasquerComptes";
            this.ckMasquerComptes.Size = new System.Drawing.Size(280, 22);
            this.ckMasquerComptes.TabIndex = 14;
            this.ckMasquerComptes.Text = "Masquer les comptes non utilisés";
            this.ckMasquerComptes.UseVisualStyleBackColor = false;
            this.ckMasquerComptes.Click += new System.EventHandler(this.ckMasquerComptes_Click);
            // 
            // ckAfficherZéro
            // 
            this.ckAfficherZéro.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ckAfficherZéro.AutoSize = true;
            this.ckAfficherZéro.BackColor = System.Drawing.Color.Transparent;
            this.ckAfficherZéro.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckAfficherZéro.Location = new System.Drawing.Point(638, 53);
            this.ckAfficherZéro.Name = "ckAfficherZéro";
            this.ckAfficherZéro.Size = new System.Drawing.Size(222, 22);
            this.ckAfficherZéro.TabIndex = 14;
            this.ckAfficherZéro.Text = "Afficher les montants nuls";
            this.ckAfficherZéro.UseVisualStyleBackColor = false;
            this.ckAfficherZéro.Click += new System.EventHandler(this.ckAfficherZéro_Click);
            // 
            // rmBtnRaffraichir
            // 
            this.rmBtnRaffraichir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rmBtnRaffraichir.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
            this.rmBtnRaffraichir.ArrowSize = 2F;
            this.rmBtnRaffraichir.BackColor = System.Drawing.Color.Transparent;
            this.rmBtnRaffraichir.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
            this.rmBtnRaffraichir.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
            this.rmBtnRaffraichir.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
            this.rmBtnRaffraichir.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
            this.rmBtnRaffraichir.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.rmBtnRaffraichir.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.rmBtnRaffraichir.FadingSpeed = 35;
            this.rmBtnRaffraichir.FlatAppearance.BorderSize = 0;
            this.rmBtnRaffraichir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rmBtnRaffraichir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rmBtnRaffraichir.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
            this.rmBtnRaffraichir.Image = global::MaCompta.Properties.Resources.reload_32;
            this.rmBtnRaffraichir.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Left;
            this.rmBtnRaffraichir.ImageOffset = 8;
            this.rmBtnRaffraichir.IsPressed = false;
            this.rmBtnRaffraichir.KeepPress = false;
            this.rmBtnRaffraichir.Location = new System.Drawing.Point(742, 0);
            this.rmBtnRaffraichir.Margin = new System.Windows.Forms.Padding(0);
            this.rmBtnRaffraichir.MaxImageSize = new System.Drawing.Point(32, 32);
            this.rmBtnRaffraichir.MenuPos = new System.Drawing.Point(0, 0);
            this.rmBtnRaffraichir.Name = "rmBtnRaffraichir";
            this.rmBtnRaffraichir.Radius = 25;
            this.rmBtnRaffraichir.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
            this.rmBtnRaffraichir.Size = new System.Drawing.Size(130, 50);
            this.rmBtnRaffraichir.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
            this.rmBtnRaffraichir.SplitDistance = 20;
            this.rmBtnRaffraichir.TabIndex = 15;
            this.rmBtnRaffraichir.Text = "Raffraichir";
            this.rmBtnRaffraichir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rmBtnRaffraichir.Title = "";
            this.rmBtnRaffraichir.UseVisualStyleBackColor = false;
            this.rmBtnRaffraichir.Click += new System.EventHandler(this.rmBtnRaffraichir_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 18);
            this.label1.TabIndex = 16;
            this.label1.Text = "Bilan au";
            // 
            // dtpDateFin
            // 
            this.dtpDateFin.Location = new System.Drawing.Point(86, 53);
            this.dtpDateFin.Name = "dtpDateFin";
            this.dtpDateFin.Size = new System.Drawing.Size(243, 24);
            this.dtpDateFin.TabIndex = 17;
            this.dtpDateFin.ValueChanged += new System.EventHandler(this.dtpDateFin_ValueChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 82);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(848, 477);
            this.tabControl1.TabIndex = 18;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tblBilan);
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(840, 446);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Liste";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.splitContainer1);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(840, 446);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Graphes";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer1.Size = new System.Drawing.Size(834, 440);
            this.splitContainer1.SplitterDistance = 214;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.label2);
            this.splitContainer2.Panel1.Controls.Add(this.pcActifs);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.label3);
            this.splitContainer2.Panel2.Controls.Add(this.pcPassifs);
            this.splitContainer2.Size = new System.Drawing.Size(830, 210);
            this.splitContainer2.SplitterDistance = 404;
            this.splitContainer2.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(402, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "ACTIF";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pcActifs
            // 
            this.pcActifs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pcActifs.AutoSizePie = true;
            this.pcActifs.BackColor = System.Drawing.Color.Transparent;
            this.pcActifs.Inclination = 1F;
            this.pcActifs.Items.Add(new My.Forms.Controls.Charts.PieChart.PieChartItem(10D, System.Drawing.Color.Gray, "test", "", 0F));
            this.pcActifs.Items.Add(new My.Forms.Controls.Charts.PieChart.PieChartItem(5D, System.Drawing.Color.Red, "test 2", "", 0F));
            this.pcActifs.Location = new System.Drawing.Point(-3, 21);
            this.pcActifs.Name = "pcActifs";
            this.pcActifs.Radius = 200F;
            this.pcActifs.Size = new System.Drawing.Size(406, 187);
            this.pcActifs.TabIndex = 1;
            this.pcActifs.Text = "Actifs";
            this.pcActifs.Thickness = 50F;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(420, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "PASSIF";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pcPassifs
            // 
            this.pcPassifs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pcPassifs.AutoSizePie = true;
            this.pcPassifs.BackColor = System.Drawing.Color.Transparent;
            this.pcPassifs.Inclination = 1F;
            this.pcPassifs.Items.Add(new My.Forms.Controls.Charts.PieChart.PieChartItem(10D, System.Drawing.Color.Gray, "test", "", 0F));
            this.pcPassifs.Items.Add(new My.Forms.Controls.Charts.PieChart.PieChartItem(5D, System.Drawing.Color.Red, "test 2", "", 0F));
            this.pcPassifs.Location = new System.Drawing.Point(0, 21);
            this.pcPassifs.Name = "pcPassifs";
            this.pcPassifs.Radius = 200F;
            this.pcPassifs.Size = new System.Drawing.Size(419, 187);
            this.pcPassifs.TabIndex = 3;
            this.pcPassifs.Text = "Passifs";
            this.pcPassifs.Thickness = 50F;
            // 
            // splitContainer3
            // 
            this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.label4);
            this.splitContainer3.Panel1.Controls.Add(this.pcRevenus);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.label5);
            this.splitContainer3.Panel2.Controls.Add(this.pcDepenses);
            this.splitContainer3.Size = new System.Drawing.Size(830, 218);
            this.splitContainer3.SplitterDistance = 375;
            this.splitContainer3.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(373, 18);
            this.label4.TabIndex = 4;
            this.label4.Text = "REVENU";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pcRevenus
            // 
            this.pcRevenus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pcRevenus.AutoSizePie = true;
            this.pcRevenus.BackColor = System.Drawing.Color.Transparent;
            this.pcRevenus.Inclination = 1F;
            this.pcRevenus.Items.Add(new My.Forms.Controls.Charts.PieChart.PieChartItem(10D, System.Drawing.Color.Gray, "test", "", 0F));
            this.pcRevenus.Items.Add(new My.Forms.Controls.Charts.PieChart.PieChartItem(5D, System.Drawing.Color.Red, "test 2", "", 0F));
            this.pcRevenus.Location = new System.Drawing.Point(-3, 21);
            this.pcRevenus.Name = "pcRevenus";
            this.pcRevenus.Radius = 200F;
            this.pcRevenus.Size = new System.Drawing.Size(377, 195);
            this.pcRevenus.TabIndex = 3;
            this.pcRevenus.Text = "Revenus";
            this.pcRevenus.Thickness = 50F;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(449, 18);
            this.label5.TabIndex = 4;
            this.label5.Text = "DEPENSE";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pcDepenses
            // 
            this.pcDepenses.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pcDepenses.AutoSizePie = true;
            this.pcDepenses.BackColor = System.Drawing.Color.Transparent;
            this.pcDepenses.Inclination = 1F;
            this.pcDepenses.Items.Add(new My.Forms.Controls.Charts.PieChart.PieChartItem(10D, System.Drawing.Color.Gray, "test", "", 0F));
            this.pcDepenses.Items.Add(new My.Forms.Controls.Charts.PieChart.PieChartItem(5D, System.Drawing.Color.Red, "test 2", "", 0F));
            this.pcDepenses.Location = new System.Drawing.Point(0, 21);
            this.pcDepenses.Name = "pcDepenses";
            this.pcDepenses.Radius = 200F;
            this.pcDepenses.Size = new System.Drawing.Size(449, 195);
            this.pcDepenses.TabIndex = 3;
            this.pcDepenses.Text = "Depenses";
            this.pcDepenses.Thickness = 50F;
            // 
            // BilanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MaCompta.Properties.Resources.Atra_Dot___Blue_1920x1200;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(872, 571);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.dtpDateFin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rmBtnRaffraichir);
            this.Controls.Add(this.ckAfficherZéro);
            this.Controls.Add(this.ckMasquerComptes);
            this.Controls.Add(this.myInfoBar1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "BilanForm";
            this.Text = "BilanForm";
            this.Shown += new System.EventHandler(this.BilanForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.tblBilan)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		} // End of Private Method InitializeComponent

		#endregion " Private Methods "

		#region " Protected Methods "

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if ( disposing && (components != null) )
			{
			    components.Dispose();
			}
			base.Dispose(disposing);
		}

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Label label2;
        private My.Forms.Controls.Charts.PieChart.PieChart pcActifs;
        private System.Windows.Forms.Label label3;
        private My.Forms.Controls.Charts.PieChart.PieChart pcPassifs;
        private System.Windows.Forms.Label label4;
        private My.Forms.Controls.Charts.PieChart.PieChart pcRevenus;
        private System.Windows.Forms.Label label5;
        private My.Forms.Controls.Charts.PieChart.PieChart pcDepenses; // End of Protected Method Dispose

		#endregion " Protected Methods "
	} // End of None Type BilanForm
}