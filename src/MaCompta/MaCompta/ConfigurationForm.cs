﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 05/08/2011
 * Time: 10:28
 * 
 * © 1428-1432 (2007-2011) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using Controller;

using My.Forms.MsgBoxes;

using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace MaCompta
{
	/// <summary>
	/// Description of ConfigForm.
	/// </summary>
	public partial class ConfigurationForm : Form
	{
		#region Fields

		private ConfigurationController _controller;

		#endregion Fields

		#region Constructors

		public ConfigurationForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();

			Initialisation();
		}

		#endregion Constructors

		#region Private Methods

		void BtnParcourir1Click(object sender, EventArgs e)
		{
			FolderBrowserDialog f = new FolderBrowserDialog();
			f.ShowDialog();
			tbArchive1.Text = f.SelectedPath + (f.SelectedPath.Substring(f.SelectedPath.Length - 1).Equals("\\") ? string.Empty : "\\");
		}

		void BtnParcourir2Click(object sender, EventArgs e)
		{
			FolderBrowserDialog f = new FolderBrowserDialog();
			f.ShowDialog();
			tbArchive2.Text = f.SelectedPath + (f.SelectedPath.Substring(f.SelectedPath.Length - 1).Equals("\\") ? string.Empty : "\\");
		}

		private void CalculerTotalNisab()
		{
			try {
			    ddcNisab.DigitText = (double.Parse(tbPrixGrammeOr.Text) * (double)nudGrammesOr.Value).ToString("N2", Program.CultureIG);
			}//try
			catch {
			    ddcNisab.DigitText = (0).ToString("N2", Program.CultureIG);
			}//catch
		}

		private void Enregistrer()
		{
			try {
			    Exception eValeurRetournée = null;
			    Model.Configuration config = new Model.Configuration(Program.BD, ref eValeurRetournée);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			    config.MotPasseLogiciel = tbMotPasse.Text;
			    config.DatePrixGrammeOr = DateTime.Parse(lblDatePrixGrammeOr.Text);
			    config.PrixGrammeOr = double.Parse(tbPrixGrammeOr.Text);
			    config.GrammesOr = double.Parse(nudGrammesOr.Text);
			    config.Nisab = double.Parse(ddcNisab.DigitText);
			    config.CheminArchive = tbArchive1.Text;
			    config.CheminCopieArchive = tbArchive2.Text;
			    config.MaintenirSuppressionsTransactions = ckMaintenirSuppressionsTransactions.Checked;
			    eValeurRetournée = config.Modifier(config.Id);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			    this.Close();
			}//try
			catch (Exception ex) {
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative d'enregistrement de la nouvelle configuration";
			    msgbox.WindowTitle = "Erreur d'enregistrement des données";
			    msgbox.Content = "Les changements effectués n'ont pas pu être enregistrés.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
				msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = ex.Message;
			    msgbox.Show();
			}//catch
		}

		/// <summary>
		/// Chargement des infos de la BD
		/// </summary>
		private void Initialisation()
		{
			_controller = new ConfigurationController(Program.BD);

			Exception eValeurRetournée = null;

			try {
			    eValeurRetournée = _controller.ChargerComptes(cbCompteZakatDue, 2);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if

			    Model.Configuration config = new Model.Configuration(Program.BD, ref eValeurRetournée);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			    tbMotPasse.Text = config.MotPasseLogiciel;
			    ddcNisab.DigitText = config.Nisab.ToString("N2", Program.CultureIG);
			    nudGrammesOr.Text = config.GrammesOr.ToString("N1", Program.CultureIG);
			    tbPrixGrammeOr.Text = config.PrixGrammeOr.ToString("N2", Program.CultureIG);
			    lblDatePrixGrammeOr.Text = config.DatePrixGrammeOr.Year +
			        "/" + config.DatePrixGrammeOr.Month.ToString("D2") +
			        "/" + config.DatePrixGrammeOr.Day.ToString("D2");
			    tbArchive1.Text = config.CheminArchive;
			    tbArchive2.Text = config.CheminCopieArchive;
			    ckMaintenirSuppressionsTransactions.Checked = config.MaintenirSuppressionsTransactions;
			    if (config.CompteZakatDue == -1) {
			        cbCompteZakatDue.SelectedIndex = cbCompteZakatDue.Items.IndexOf(config.CompteZakatDue);
			    }//if
			}//try
			catch (Exception ex) {
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement de la configuration actuelle";
			    msgbox.WindowTitle = "Erreur de chargement des données";
			    msgbox.Content = "La configuration actuelle n'a pas pu être chargée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
				msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = ex.Message;
			    msgbox.Show();
			}//catch
		}

		void RmBtnEnregistrerClick(object sender, EventArgs e)
		{
			if (VérifierDonnées()) {
			    Enregistrer();
			}//if
		}

		private void ShowUpdateNisabManually()
		{
			tbMontantNisab.Text = ddcNisab.DigitText;
			tbMontantNisab.Visible = true;
			rmBtnValiderMontantNisab.Visible = true;
		}

		void TbMotPasseTextChanged(object sender, EventArgs e)
		{
			pscComplexité.SetPassword(tbMotPasse.Text);
		}

		void TbPrixGrammeOrTextChanged(object sender, EventArgs e)
		{
			CalculerTotalNisab();
		}

		private void UpdateNisabManually()
		{
			try {
			    tbMontantNisab.Text = double.Parse(tbMontantNisab.Text).ToString("N2", Program.CultureIG);
			}//try
			catch (Exception ex) {
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}//catch
		}

		private bool VérifierDonnées()
		{
			bool bValeurRetournée = true;
			epValidation.Clear();
			try {
			    tbPrixGrammeOr.Text = double.Parse(tbPrixGrammeOr.Text).ToString("N2", Program.CultureIG);
			}//try
			catch {
			    bValeurRetournée = false;
			    epValidation.SetError(tbPrixGrammeOr, "Le montant saisi est invalide!");
			    MyMsgBox.Show("Le montant saisi est invalide!");
			}//catch

			try {
			    nudGrammesOr.Text = double.Parse(nudGrammesOr.Text).ToString("N1", Program.CultureIG);
			}//try
			catch {
			    bValeurRetournée = false;
			    epValidation.SetError(nudGrammesOr, "Le montant saisi est invalide!");
			    MyMsgBox.Show("Le montant saisi est invalide!");
			}//catch

			if (string.IsNullOrEmpty(tbArchive1.Text)) {
			    bValeurRetournée = false;
			    epValidation.SetError(tbArchive1, "Le chemin de l'archive doit être précisé!");
			    MyMsgBox.Show("Le chemin de l'archive doit être précisé!");
			}//if

			if (!string.IsNullOrEmpty(tbArchive2.Text) && string.IsNullOrEmpty(tbArchive1.Text)) {
			    tbArchive1.Text = tbArchive2.Text;
			    tbArchive2.Text = string.Empty;
			}//if

			return bValeurRetournée;
		}

		private void ckLimitierNbFichiers_MouseUp(object sender, MouseEventArgs e)
		{
			gbLimiteFichiers.Enabled = ckLimitierNbFichiers.Checked;
		}

		private void ddcNisab_DoubleClick(object sender, EventArgs e)
		{
			ShowUpdateNisabManually();
		}

		private void ddcNisab_MouseEnter(object sender, EventArgs e)
		{
			ShowUpdateNisabManually();
		}

		private void gclObtenirPrix24hgold_Click(object sender, EventArgs e)
		{
			Process.Start("http://www.24hgold.com/english/gold_silver_prices_charts.aspx?money=MAD");
		}

		private void gclObtenirPrix_Click(object sender, EventArgs e)
		{
			double dPrixGrammeOr = 0;
			string sDateMiseAJour = string.Empty;
			Exception eValeurRetournée = _controller.ObtenirPrixGrammeOr(ref dPrixGrammeOr, ref sDateMiseAJour);
			if (eValeurRetournée != null) {
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(eValeurRetournée.Message);
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement du prix du gramme d'or";
			    msgbox.WindowTitle = "Erreur de chargement des données";
			    msgbox.Content = "Le chargement du prix du gramme d'or n'a pas pu être effectuée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
				msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
			else {
			    tbPrixGrammeOr.Text = dPrixGrammeOr.ToString("N2", Program.CultureIG);
			    lblDatePrixGrammeOr.Text = sDateMiseAJour;
			}//else
		}

		private void nudGrammesOr_ValueChanged(object sender, EventArgs e)
		{
			CalculerTotalNisab();
		}

		private void rmBtnValiderMontantNisab_Click(object sender, EventArgs e)
		{
			ddcNisab.DigitText = tbMontantNisab.Text;
			tbMontantNisab.Visible = false;
			rmBtnValiderMontantNisab.Visible = false;
		}

		private void tbMontantNisab_Leave(object sender, EventArgs e)
		{
			UpdateNisabManually();
		}

		private void tbMontantNisab_MouseLeave(object sender, EventArgs e)
		{
			UpdateNisabManually();
			tbMontantNisab.Visible = false;
			rmBtnValiderMontantNisab.Visible = false;
		}

		#endregion Private Methods
	}
}