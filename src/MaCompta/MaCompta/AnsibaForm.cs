﻿using Controller;

using My.Forms.MsgBoxes;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MaCompta
{
	public partial class AnsibaForm : Form
	{
		#region Fields

		private AnsibaController _controller;

		#endregion Fields

		#region Constructors

		public AnsibaForm()
		{
			InitializeComponent();
			Initialisation();
			ChargerAnsiba();
		}

		#endregion Constructors

		#region Private Methods

		/// <summary>
		/// Chargement de toutes les donnees
		/// </summary>
		private void ChargerAnsiba()
		{
			Exception eValeurRetournée = _controller.ChargerAnsiba(tblAnsiba);
			if (eValeurRetournée != null) {
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement " +
			        "de la liste des informations des montants de Zakat";
			    msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "La liste des montants de Zakat n'a pas pu être chargée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
			    msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
		}

		private void Initialisation()
		{
			_controller = new AnsibaController(Program.BD, Program.CultureIG);
		}

		#endregion Private Methods
	}
}