﻿using Controller;

using My.Forms.Controls.ListViews.XPTable.Models;
using My.Forms.MsgBoxes;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MaCompta
{
	public partial class ListePartenairesForm : Form
	{
		#region Fields

		private ListePartenairesController _controller;
		private DateTime? _date_début;
		private DateTime _date_fin;
		private string _nom_partenaire_a_charger; //Nom du partenaire

		#endregion Fields

		#region Constructors

		public ListePartenairesForm()
		{
			InitializeComponent();
			Initialisation();
			ChargerListe();
			ChargerGraphe();
		}

		public ListePartenairesForm(string sNomPartenaireACharger, DateTime? dtDateDébut, DateTime dtDateFin)
		{
			InitializeComponent();
			Initialisation();
			_nom_partenaire_a_charger = sNomPartenaireACharger;
			_date_début = dtDateDébut;
			_date_fin = dtDateFin;
			ChargerListeDétaillée();
		}

		#endregion Constructors

		#region Private Methods

		/// <summary>
		/// Charger la liste des partenaires dans un graphe.
		/// </summary>
		private void ChargerGraphe()
		{
			Exception eValeurRetournée = _controller.ChargerGraphePartenaires(pcListePartenaires, tblListe);
			if (eValeurRetournée != null) {
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement d'informations à partir de la base de données.";
			    msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "La liste des données n'a pas pu être chargée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
				msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
		}

		/// <summary>
		/// Charger la liste des partenaires.
		/// </summary>
		private void ChargerListe()
		{
			Exception eValeurRetournée = _controller.ChargerListePartenaires(tblListe);
			if (eValeurRetournée != null) {
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement d'informations à partir de la base de données.";
			    msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "La liste des données n'a pas pu être chargée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
				msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
		}

		private void ChargerListeDétaillée()
		{
			Exception eValeurRetournée = _controller.ChargerListeTransactions(tblListe, _nom_partenaire_a_charger, _date_début, _date_fin);
			if ( eValeurRetournée != null )
			{
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement d'informations à partir de la base de données.";
			    msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "La liste des données n'a pas pu être chargée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
				msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
		}

		private void Initialisation()
		{
			_controller = new ListePartenairesController(Program.BD, Program.CultureIG);
			_nom_partenaire_a_charger = null;
			_date_début = null;
			_date_fin = DateTime.Now;
		}

		void ListeProjetsForm_Shown(object sender, EventArgs e)
		{
			WindowState = FormWindowState.Maximized;
		}

		void dtpDateDébut_MouseUp(object sender, MouseEventArgs e)
		{
			if ( dtpDateDébut.Checked )
			{
			    _date_début = dtpDateDébut.Value;
			}//if
			else
			{
			    _date_début = null;
			}//else
			ChargerListeDétaillée();
		}

		void dtpDateDébut_ValueChanged(object sender, EventArgs e)
		{
			if ( dtpDateDébut.Checked )
			{
			    _date_début = dtpDateDébut.Value;
			    ChargerListeDétaillée();
			}//if
		}

		void dtpDateFin_ValueChanged(object sender, EventArgs e)
		{
			_date_fin = dtpDateFin.Value;
			ChargerListeDétaillée();
		}

		void tblListe_DoubleClick(object sender, EventArgs e)
		{
			if ( tblListe.TableModel.Selections.SelectedItems.Length != 0 )
			{
			    Row ligne = tblListe.TableModel.Selections.SelectedItems[0];
			    if ( !string.IsNullOrEmpty(ligne.Cells[2].Text) ) //Nom du projet
			    {
			        if ( ligne.Cells[2].Text != _nom_partenaire_a_charger )
			        {
			            long lValeurIdCompte;
			            if ( long.TryParse(ligne.Cells[2].Text.Substring(0, 4), out lValeurIdCompte) )
			            {
			                //Détails de la transaction
			                new ListeTransactionsForm(ligne.Cells[2].Text.Substring(0, ligne.Cells[2].Text.IndexOf(", ", 0)), _date_début, _date_fin).Show();
			            }//if
			            else
			            {
			                //Détails du partenaire
			                new ListePartenairesForm(ligne.Cells[2].Text, _date_début, _date_fin).Show();
			            }//else
			        }//if
			    }//if
			}//if
		}

		#endregion Private Methods
	}
}