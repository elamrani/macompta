﻿/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 07/09/1433
 * Time: 18:31
 * 
 * © 1428-1433 (2007-2012) All rights reserved to Mohamed Y. ElAmrani.
 */
namespace MaCompta
{
	partial class PartenaireForm
	{
		#region Fields

		private System.Windows.Forms.ComboBox cbListe;
		private System.Windows.Forms.CheckBox ckAfficher;

		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.ErrorProvider epValidation;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lbAdresse;
		private System.Windows.Forms.Label lbAssistant;
		private System.Windows.Forms.Label lbEmail;
		private System.Windows.Forms.Label lbFonction;
		private System.Windows.Forms.Label lbNom;
		private System.Windows.Forms.Label lbTéléphone1;
		private System.Windows.Forms.Label lbTéléphone2;
		private System.Windows.Forms.Label lbTéléphoneAssistant;
		private System.Windows.Forms.Label lbWeb;
		private My.Forms.Controls.TextBoxes.MyLinkTextBox ltbEmail;
		private My.Forms.Controls.TextBoxes.MyLinkTextBox ltbWeb;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnEnregistrer;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnRecommencer;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnSupprimer;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
		private System.Windows.Forms.TextBox tbAdresse;
		private System.Windows.Forms.TextBox tbAssistant;
		private System.Windows.Forms.TextBox tbFonction;
		private System.Windows.Forms.TextBox tbNom;
		private System.Windows.Forms.TextBox tbTéléphone1;
		private System.Windows.Forms.TextBox tbTéléphone2;
		private System.Windows.Forms.TextBox tbTéléphoneAssistant;

		#endregion Fields

		#region Private Methods

		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.cbListe = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.ltbEmail = new My.Forms.Controls.TextBoxes.MyLinkTextBox();
			this.tbTéléphone2 = new System.Windows.Forms.TextBox();
			this.tbTéléphone1 = new System.Windows.Forms.TextBox();
			this.tbAdresse = new System.Windows.Forms.TextBox();
			this.rmBtnSupprimer = new My.Forms.Controls.Ribbon.RibbonMenuButton();
			this.rmBtnEnregistrer = new My.Forms.Controls.Ribbon.RibbonMenuButton();
			this.rmBtnRecommencer = new My.Forms.Controls.Ribbon.RibbonMenuButton();
			this.lbNom = new System.Windows.Forms.Label();
			this.lbAdresse = new System.Windows.Forms.Label();
			this.lbTéléphone1 = new System.Windows.Forms.Label();
			this.lbTéléphone2 = new System.Windows.Forms.Label();
			this.lbWeb = new System.Windows.Forms.Label();
			this.lbEmail = new System.Windows.Forms.Label();
			this.lbAssistant = new System.Windows.Forms.Label();
			this.lbTéléphoneAssistant = new System.Windows.Forms.Label();
			this.lbFonction = new System.Windows.Forms.Label();
			this.tbNom = new System.Windows.Forms.TextBox();
			this.tbTéléphoneAssistant = new System.Windows.Forms.TextBox();
			this.tbAssistant = new System.Windows.Forms.TextBox();
			this.tbFonction = new System.Windows.Forms.TextBox();
			this.ltbWeb = new My.Forms.Controls.TextBoxes.MyLinkTextBox();
			this.ckAfficher = new System.Windows.Forms.CheckBox();
			this.epValidation = new System.Windows.Forms.ErrorProvider(this.components);
			this.tableLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.epValidation)).BeginInit();
			this.SuspendLayout();
			// 
			// cbListe
			// 
			this.cbListe.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.cbListe.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.cbListe.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.cbListe.FormattingEnabled = true;
			this.cbListe.Location = new System.Drawing.Point(273, 16);
			this.cbListe.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.cbListe.MaxLength = 45;
			this.cbListe.Name = "cbListe";
			this.cbListe.Size = new System.Drawing.Size(218, 26);
			this.cbListe.TabIndex = 2;
			this.cbListe.SelectedIndexChanged += new System.EventHandler(this.CbListeSelectedIndexChanged);
			this.cbListe.TextUpdate += new System.EventHandler(this.CbListeTextUpdate);
			// 
			// label1
			// 
			this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.label1.BackColor = System.Drawing.Color.Transparent;
			this.label1.Location = new System.Drawing.Point(222, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(45, 31);
			this.label1.TabIndex = 1;
			this.label1.Text = "Liste";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tableLayoutPanel
			// 
			this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Left)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
			this.tableLayoutPanel.ColumnCount = 4;
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel.Controls.Add(this.ltbEmail, 1, 5);
			this.tableLayoutPanel.Controls.Add(this.tbTéléphone2, 1, 3);
			this.tableLayoutPanel.Controls.Add(this.tbTéléphone1, 1, 2);
			this.tableLayoutPanel.Controls.Add(this.tbAdresse, 1, 1);
			this.tableLayoutPanel.Controls.Add(this.rmBtnSupprimer, 0, 10);
			this.tableLayoutPanel.Controls.Add(this.rmBtnEnregistrer, 3, 10);
			this.tableLayoutPanel.Controls.Add(this.rmBtnRecommencer, 2, 10);
			this.tableLayoutPanel.Controls.Add(this.lbNom, 0, 0);
			this.tableLayoutPanel.Controls.Add(this.lbAdresse, 0, 1);
			this.tableLayoutPanel.Controls.Add(this.lbTéléphone1, 0, 2);
			this.tableLayoutPanel.Controls.Add(this.lbTéléphone2, 0, 3);
			this.tableLayoutPanel.Controls.Add(this.lbWeb, 0, 4);
			this.tableLayoutPanel.Controls.Add(this.lbEmail, 0, 5);
			this.tableLayoutPanel.Controls.Add(this.lbAssistant, 0, 6);
			this.tableLayoutPanel.Controls.Add(this.lbTéléphoneAssistant, 0, 7);
			this.tableLayoutPanel.Controls.Add(this.lbFonction, 0, 8);
			this.tableLayoutPanel.Controls.Add(this.tbNom, 1, 0);
			this.tableLayoutPanel.Controls.Add(this.tbTéléphoneAssistant, 1, 7);
			this.tableLayoutPanel.Controls.Add(this.tbAssistant, 1, 6);
			this.tableLayoutPanel.Controls.Add(this.tbFonction, 1, 8);
			this.tableLayoutPanel.Controls.Add(this.ltbWeb, 1, 4);
			this.tableLayoutPanel.Controls.Add(this.ckAfficher, 0, 9);
			this.tableLayoutPanel.Location = new System.Drawing.Point(14, 55);
			this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tableLayoutPanel.Name = "tableLayoutPanel";
			this.tableLayoutPanel.RowCount = 11;
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 87F));
			this.tableLayoutPanel.Size = new System.Drawing.Size(686, 563);
			this.tableLayoutPanel.TabIndex = 0;
			// 
			// ltbEmail
			// 
			this.ltbEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.SetColumnSpan(this.ltbEmail, 3);
			this.ltbEmail.LinkType = My.Forms.Controls.TextBoxes.LinkTypes.Email;
			this.ltbEmail.Location = new System.Drawing.Point(174, 246);
			this.ltbEmail.Margin = new System.Windows.Forms.Padding(3, 4, 18, 4);
			this.ltbEmail.MaxLength = 150;
			this.ltbEmail.Name = "ltbEmail";
			this.ltbEmail.Size = new System.Drawing.Size(494, 24);
			this.ltbEmail.TabIndex = 11;
			// 
			// tbTéléphone2
			// 
			this.tbTéléphone2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.SetColumnSpan(this.tbTéléphone2, 3);
			this.tbTéléphone2.Location = new System.Drawing.Point(174, 152);
			this.tbTéléphone2.Margin = new System.Windows.Forms.Padding(3, 4, 18, 4);
			this.tbTéléphone2.MaxLength = 150;
			this.tbTéléphone2.Name = "tbTéléphone2";
			this.tbTéléphone2.Size = new System.Drawing.Size(494, 24);
			this.tbTéléphone2.TabIndex = 7;
			// 
			// tbTéléphone1
			// 
			this.tbTéléphone1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.SetColumnSpan(this.tbTéléphone1, 3);
			this.tbTéléphone1.Location = new System.Drawing.Point(174, 105);
			this.tbTéléphone1.Margin = new System.Windows.Forms.Padding(3, 4, 18, 4);
			this.tbTéléphone1.MaxLength = 150;
			this.tbTéléphone1.Name = "tbTéléphone1";
			this.tbTéléphone1.Size = new System.Drawing.Size(494, 24);
			this.tbTéléphone1.TabIndex = 5;
			// 
			// tbAdresse
			// 
			this.tbAdresse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.SetColumnSpan(this.tbAdresse, 3);
			this.tbAdresse.Location = new System.Drawing.Point(174, 58);
			this.tbAdresse.Margin = new System.Windows.Forms.Padding(3, 4, 18, 4);
			this.tbAdresse.MaxLength = 250;
			this.tbAdresse.Name = "tbAdresse";
			this.tbAdresse.Size = new System.Drawing.Size(494, 24);
			this.tbAdresse.TabIndex = 3;
			// 
			// rmBtnSupprimer
			// 
			this.rmBtnSupprimer.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.rmBtnSupprimer.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
			this.rmBtnSupprimer.BackColor = System.Drawing.Color.Transparent;
			this.rmBtnSupprimer.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
			this.rmBtnSupprimer.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
			this.rmBtnSupprimer.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
			this.rmBtnSupprimer.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
			this.rmBtnSupprimer.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.rmBtnSupprimer.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.rmBtnSupprimer.FadingSpeed = 35;
			this.rmBtnSupprimer.FlatAppearance.BorderSize = 0;
			this.rmBtnSupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.rmBtnSupprimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rmBtnSupprimer.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
			this.rmBtnSupprimer.Image = global::MaCompta.Properties.Resources.user_trash_full_64;
			this.rmBtnSupprimer.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Top;
			this.rmBtnSupprimer.ImageOffset = 1;
			this.rmBtnSupprimer.IsPressed = false;
			this.rmBtnSupprimer.KeepPress = false;
			this.rmBtnSupprimer.Location = new System.Drawing.Point(13, 474);
			this.rmBtnSupprimer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.rmBtnSupprimer.MaxImageSize = new System.Drawing.Point(50, 50);
			this.rmBtnSupprimer.MenuPos = new System.Drawing.Point(0, 0);
			this.rmBtnSupprimer.Name = "rmBtnSupprimer";
			this.rmBtnSupprimer.Radius = 11;
			this.rmBtnSupprimer.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
			this.rmBtnSupprimer.Size = new System.Drawing.Size(145, 85);
			this.rmBtnSupprimer.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
			this.rmBtnSupprimer.SplitDistance = 0;
			this.rmBtnSupprimer.TabIndex = 21;
			this.rmBtnSupprimer.Text = "Supprimer";
			this.rmBtnSupprimer.Title = "";
			this.rmBtnSupprimer.UseVisualStyleBackColor = false;
			this.rmBtnSupprimer.Click += new System.EventHandler(this.RmBtnSupprimerClick);
			// 
			// rmBtnEnregistrer
			// 
			this.rmBtnEnregistrer.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.rmBtnEnregistrer.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
			this.rmBtnEnregistrer.BackColor = System.Drawing.Color.Transparent;
			this.rmBtnEnregistrer.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
			this.rmBtnEnregistrer.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
			this.rmBtnEnregistrer.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
			this.rmBtnEnregistrer.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
			this.rmBtnEnregistrer.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.rmBtnEnregistrer.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.rmBtnEnregistrer.FadingSpeed = 35;
			this.rmBtnEnregistrer.FlatAppearance.BorderSize = 0;
			this.rmBtnEnregistrer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.rmBtnEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rmBtnEnregistrer.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
			this.rmBtnEnregistrer.Image = global::MaCompta.Properties.Resources.dialog_ok_2_64;
			this.rmBtnEnregistrer.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Top;
			this.rmBtnEnregistrer.ImageOffset = 0;
			this.rmBtnEnregistrer.IsPressed = false;
			this.rmBtnEnregistrer.KeepPress = false;
			this.rmBtnEnregistrer.Location = new System.Drawing.Point(527, 474);
			this.rmBtnEnregistrer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.rmBtnEnregistrer.MaxImageSize = new System.Drawing.Point(50, 50);
			this.rmBtnEnregistrer.MenuPos = new System.Drawing.Point(0, 0);
			this.rmBtnEnregistrer.Name = "rmBtnEnregistrer";
			this.rmBtnEnregistrer.Radius = 11;
			this.rmBtnEnregistrer.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
			this.rmBtnEnregistrer.Size = new System.Drawing.Size(145, 85);
			this.rmBtnEnregistrer.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
			this.rmBtnEnregistrer.SplitDistance = 0;
			this.rmBtnEnregistrer.TabIndex = 19;
			this.rmBtnEnregistrer.Text = "Enregistrer";
			this.rmBtnEnregistrer.Title = "";
			this.rmBtnEnregistrer.UseVisualStyleBackColor = false;
			this.rmBtnEnregistrer.Click += new System.EventHandler(this.RmBtnEnregistrerClick);
			// 
			// rmBtnRecommencer
			// 
			this.rmBtnRecommencer.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.rmBtnRecommencer.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
			this.rmBtnRecommencer.BackColor = System.Drawing.Color.Transparent;
			this.rmBtnRecommencer.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
			this.rmBtnRecommencer.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
			this.rmBtnRecommencer.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
			this.rmBtnRecommencer.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
			this.rmBtnRecommencer.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.rmBtnRecommencer.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.rmBtnRecommencer.FadingSpeed = 35;
			this.rmBtnRecommencer.FlatAppearance.BorderSize = 0;
			this.rmBtnRecommencer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.rmBtnRecommencer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rmBtnRecommencer.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
			this.rmBtnRecommencer.Image = global::MaCompta.Properties.Resources.edit_clear_2_64;
			this.rmBtnRecommencer.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Top;
			this.rmBtnRecommencer.ImageOffset = 0;
			this.rmBtnRecommencer.IsPressed = false;
			this.rmBtnRecommencer.KeepPress = false;
			this.rmBtnRecommencer.Location = new System.Drawing.Point(355, 474);
			this.rmBtnRecommencer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.rmBtnRecommencer.MaxImageSize = new System.Drawing.Point(50, 50);
			this.rmBtnRecommencer.MenuPos = new System.Drawing.Point(0, 0);
			this.rmBtnRecommencer.Name = "rmBtnRecommencer";
			this.rmBtnRecommencer.Radius = 11;
			this.rmBtnRecommencer.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
			this.rmBtnRecommencer.Size = new System.Drawing.Size(145, 85);
			this.rmBtnRecommencer.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
			this.rmBtnRecommencer.SplitDistance = 0;
			this.rmBtnRecommencer.TabIndex = 20;
			this.rmBtnRecommencer.Text = "Recommencer";
			this.rmBtnRecommencer.Title = "";
			this.rmBtnRecommencer.UseVisualStyleBackColor = false;
			this.rmBtnRecommencer.Click += new System.EventHandler(this.RmBtnRecommencerClick);
			// 
			// lbNom
			// 
			this.lbNom.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbNom.Location = new System.Drawing.Point(3, 0);
			this.lbNom.Name = "lbNom";
			this.lbNom.Size = new System.Drawing.Size(165, 47);
			this.lbNom.TabIndex = 0;
			this.lbNom.Text = "Nom / Raison Sociale";
			this.lbNom.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbAdresse
			// 
			this.lbAdresse.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbAdresse.Location = new System.Drawing.Point(3, 47);
			this.lbAdresse.Name = "lbAdresse";
			this.lbAdresse.Size = new System.Drawing.Size(165, 47);
			this.lbAdresse.TabIndex = 2;
			this.lbAdresse.Text = "Adresse";
			this.lbAdresse.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbTéléphone1
			// 
			this.lbTéléphone1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbTéléphone1.Location = new System.Drawing.Point(3, 94);
			this.lbTéléphone1.Name = "lbTéléphone1";
			this.lbTéléphone1.Size = new System.Drawing.Size(165, 47);
			this.lbTéléphone1.TabIndex = 4;
			this.lbTéléphone1.Text = "Téléphone Portable";
			this.lbTéléphone1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbTéléphone2
			// 
			this.lbTéléphone2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbTéléphone2.Location = new System.Drawing.Point(3, 141);
			this.lbTéléphone2.Name = "lbTéléphone2";
			this.lbTéléphone2.Size = new System.Drawing.Size(165, 47);
			this.lbTéléphone2.TabIndex = 6;
			this.lbTéléphone2.Text = "Téléphone Fixe";
			this.lbTéléphone2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbWeb
			// 
			this.lbWeb.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbWeb.Location = new System.Drawing.Point(3, 188);
			this.lbWeb.Name = "lbWeb";
			this.lbWeb.Size = new System.Drawing.Size(165, 47);
			this.lbWeb.TabIndex = 8;
			this.lbWeb.Text = "Site Web";
			this.lbWeb.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbEmail
			// 
			this.lbEmail.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbEmail.Location = new System.Drawing.Point(3, 235);
			this.lbEmail.Name = "lbEmail";
			this.lbEmail.Size = new System.Drawing.Size(165, 47);
			this.lbEmail.TabIndex = 10;
			this.lbEmail.Text = "Email";
			this.lbEmail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbAssistant
			// 
			this.lbAssistant.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbAssistant.Location = new System.Drawing.Point(3, 282);
			this.lbAssistant.Name = "lbAssistant";
			this.lbAssistant.Size = new System.Drawing.Size(165, 47);
			this.lbAssistant.TabIndex = 12;
			this.lbAssistant.Text = "Assistant";
			this.lbAssistant.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbTéléphoneAssistant
			// 
			this.lbTéléphoneAssistant.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbTéléphoneAssistant.Location = new System.Drawing.Point(3, 329);
			this.lbTéléphoneAssistant.Name = "lbTéléphoneAssistant";
			this.lbTéléphoneAssistant.Size = new System.Drawing.Size(165, 47);
			this.lbTéléphoneAssistant.TabIndex = 14;
			this.lbTéléphoneAssistant.Text = "Téléphone";
			this.lbTéléphoneAssistant.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbFonction
			// 
			this.lbFonction.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbFonction.Location = new System.Drawing.Point(3, 376);
			this.lbFonction.Name = "lbFonction";
			this.lbFonction.Size = new System.Drawing.Size(165, 47);
			this.lbFonction.TabIndex = 16;
			this.lbFonction.Text = "Fonction";
			this.lbFonction.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbNom
			// 
			this.tbNom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.SetColumnSpan(this.tbNom, 3);
			this.tbNom.Location = new System.Drawing.Point(174, 11);
			this.tbNom.Margin = new System.Windows.Forms.Padding(3, 4, 18, 4);
			this.tbNom.MaxLength = 150;
			this.tbNom.Name = "tbNom";
			this.tbNom.Size = new System.Drawing.Size(494, 24);
			this.tbNom.TabIndex = 1;
			// 
			// tbTéléphoneAssistant
			// 
			this.tbTéléphoneAssistant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.SetColumnSpan(this.tbTéléphoneAssistant, 3);
			this.tbTéléphoneAssistant.Location = new System.Drawing.Point(174, 340);
			this.tbTéléphoneAssistant.Margin = new System.Windows.Forms.Padding(3, 4, 18, 4);
			this.tbTéléphoneAssistant.MaxLength = 150;
			this.tbTéléphoneAssistant.Name = "tbTéléphoneAssistant";
			this.tbTéléphoneAssistant.Size = new System.Drawing.Size(494, 24);
			this.tbTéléphoneAssistant.TabIndex = 15;
			// 
			// tbAssistant
			// 
			this.tbAssistant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.SetColumnSpan(this.tbAssistant, 3);
			this.tbAssistant.Location = new System.Drawing.Point(174, 293);
			this.tbAssistant.Margin = new System.Windows.Forms.Padding(3, 4, 18, 4);
			this.tbAssistant.MaxLength = 150;
			this.tbAssistant.Name = "tbAssistant";
			this.tbAssistant.Size = new System.Drawing.Size(494, 24);
			this.tbAssistant.TabIndex = 13;
			// 
			// tbFonction
			// 
			this.tbFonction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.SetColumnSpan(this.tbFonction, 3);
			this.tbFonction.Location = new System.Drawing.Point(174, 387);
			this.tbFonction.Margin = new System.Windows.Forms.Padding(3, 4, 18, 4);
			this.tbFonction.MaxLength = 150;
			this.tbFonction.Name = "tbFonction";
			this.tbFonction.Size = new System.Drawing.Size(494, 24);
			this.tbFonction.TabIndex = 17;
			// 
			// ltbWeb
			// 
			this.ltbWeb.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.SetColumnSpan(this.ltbWeb, 3);
			this.ltbWeb.LinkType = My.Forms.Controls.TextBoxes.LinkTypes.Http;
			this.ltbWeb.Location = new System.Drawing.Point(174, 199);
			this.ltbWeb.Margin = new System.Windows.Forms.Padding(3, 4, 18, 4);
			this.ltbWeb.MaxLength = 150;
			this.ltbWeb.Name = "ltbWeb";
			this.ltbWeb.Size = new System.Drawing.Size(494, 24);
			this.ltbWeb.TabIndex = 9;
			// 
			// ckAfficher
			// 
			this.ckAfficher.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ckAfficher.Checked = true;
			this.ckAfficher.CheckState = System.Windows.Forms.CheckState.Checked;
			this.tableLayoutPanel.SetColumnSpan(this.ckAfficher, 3);
			this.ckAfficher.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ckAfficher.Location = new System.Drawing.Point(3, 427);
			this.ckAfficher.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.ckAfficher.Name = "ckAfficher";
			this.ckAfficher.Size = new System.Drawing.Size(507, 39);
			this.ckAfficher.TabIndex = 18;
			this.ckAfficher.Text = "Afficher ce partenaire dans les différentes listes";
			this.ckAfficher.UseVisualStyleBackColor = true;
			// 
			// epValidation
			// 
			this.epValidation.ContainerControl = this;
			// 
			// PartenaireForm
			// 
			this.AcceptButton = this.rmBtnEnregistrer;
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
			this.BackgroundImage = global::MaCompta.Properties.Resources.Atra_Dot___White_1920x1200;
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.ClientSize = new System.Drawing.Size(713, 633);
			this.Controls.Add(this.tableLayoutPanel);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cbListe);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = global::MaCompta.Properties.Resources.contact_new_2_32_1;
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "PartenaireForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Partenaires";
			this.tableLayoutPanel.ResumeLayout(false);
			this.tableLayoutPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.epValidation)).EndInit();
			this.ResumeLayout(false);
		}

		#endregion Private Methods

		#region Protected Methods

		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#endregion Protected Methods
	}
}