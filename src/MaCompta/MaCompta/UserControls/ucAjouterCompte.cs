﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MaCompta.UserControls
{
	public partial class ucAjouterCompte : UserControl
	{
		#region Fields

		Controller.MainController _controller;

		#endregion Fields

		#region Constructors

		public ucAjouterCompte()
		{
			InitializeComponent();

			_controller = new Controller.MainController(Program.BD);

			_controller.ChargerListeValeurs(cbTypeCompte, "type_compte", "id", "nom", "`id` BETWEEN 2 AND 3");
		}

		#endregion Constructors
	}
}