﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 15/11/2010
 * Time: 22:49
 * 
 * © 1428-1431 (2007-2010) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using Model;

using My.Forms.MsgBoxes;

using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace MaCompta
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		#region Constants

		public const string TOUS = "--- Tous ---";

		#endregion Constants

		#region Static Fields

		static BaseDonnées _bd;
		static CultureInfo _cultureIG;

		#endregion Static Fields

		#region Public Properties

		/// <summary>
		/// La base de données à utiliser
		/// </summary>
		public static BaseDonnées BD
		{
			get { return _bd; }
			set { _bd = value; }
		}

		/// <summary>
		/// Culture de l'utilisateur avec une modification du symbole décimal pour le point (.)
		/// et du groupement des chiffres pour un espace.
		/// </summary>
		public static CultureInfo CultureIG
		{
			get { return _cultureIG; }
			set { _cultureIG = value; }
		}

		#endregion Public Properties

		#region Private Methods

		/// <summary>
		/// Program entry point.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			try {
				_bd = new BaseDonnées();
				Exception eValeurRetournée = _bd.Ouvrir();
				if ( eValeurRetournée == null ) {
					_cultureIG = new CultureInfo(Application.CurrentCulture.Name);
					_cultureIG.NumberFormat.NumberDecimalSeparator = ".";
					_cultureIG.NumberFormat.CurrencyDecimalSeparator = ".";
					_cultureIG.NumberFormat.NumberGroupSeparator = " ";
					_cultureIG.NumberFormat.CurrencyGroupSeparator = " ";
					MainForm mf = new MainForm();
					if ( mf.MotPasseOK ) {
						//Démarrage de l'application
						Application.Run(mf);
					}//if
				}//if
				else {
					MyMsgBox msgbox = new MyMsgBox();
					msgbox.Buttons = new MyMsgBoxButton[] {
						new MyMsgBoxButton(MyMsgBoxResult.OK)
					};
					msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
					msgbox.MainInstruction = "Erreur lors de la tentative de connexion à la BD";
					msgbox.WindowTitle = "Erreur de connexion à la BD";
					msgbox.Content = "L'application va devoir fermer";
					#if DEBUG
					msgbox.ExpandedByDefault = true;
					#else
					msgbox.ExpandedByDefault = false;
					#endif
					msgbox.ExpandedInformation = eValeurRetournée.Message;
					msgbox.Show();
				}//else
			}//try
			catch (Exception e) {
					MyMsgBox msgbox = new MyMsgBox();
					msgbox.Buttons = new MyMsgBoxButton[] {
						new MyMsgBoxButton(MyMsgBoxResult.OK)
					};
			#if DEBUG
			        msgbox.VerificationFlagChecked = CheckState.Unchecked;
			#else
			        msgbox.VerificationFlagChecked = CheckState.Checked;
			#endif
			        msgbox.VerificationText = "Redémarrer l'application?";
					msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
					msgbox.MainInstruction = "Un comportement imprévu à été détecté.";
					msgbox.WindowTitle = "Comportement imprévu détecté!";
					msgbox.Content = "L'application va devoir fermer maintenant.";
			#if DEBUG
					msgbox.ExpandedByDefault = true;
			#else
					msgbox.ExpandedByDefault = false;
			#endif
					msgbox.ExpandedInformation = e.Message;
					msgbox.Show();
					if ( msgbox.VerificationFlagChecked == CheckState.Checked ) {
						Application.Restart();
					}//if
					
			}//catch
		}

		#endregion Private Methods
	}
}