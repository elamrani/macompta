﻿namespace MaCompta
{
    partial class RechercherTransationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.myInfoBar1 = new My.Forms.Controls.Panels.MyInfoBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpDateDébut = new System.Windows.Forms.DateTimePicker();
            this.dtpDateFin = new System.Windows.Forms.DateTimePicker();
            this.cbPartenaire = new System.Windows.Forms.ComboBox();
            this.rmBtnRechercher = new My.Forms.Controls.Ribbon.RibbonMenuButton();
            this.tblListe = new My.Forms.Controls.ListViews.XPTable.Models.Table();
            this.cmListe = new My.Forms.Controls.ListViews.XPTable.Models.ColumnModel();
            this.tcId = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcDate = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcPartenaire = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcDesignation = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcCompte = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcDébit = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcCrédit = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tmListe = new My.Forms.Controls.ListViews.XPTable.Models.TableModel();
            this.label4 = new System.Windows.Forms.Label();
            this.tbMontant = new System.Windows.Forms.TextBox();
            this.cbTypeMontant = new System.Windows.Forms.ComboBox();
            this.epValidation = new System.Windows.Forms.ErrorProvider(this.components);
            this.pcbCompte = new My.Forms.Controls.ComboBoxes.PowerComboBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tblListe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epValidation)).BeginInit();
            this.SuspendLayout();
            // 
            // myInfoBar1
            // 
            this.myInfoBar1.BackColor = System.Drawing.Color.Transparent;
            this.myInfoBar1.BackStyle = My.Forms.Controls.Panels.BackStyle.Gradient;
            this.myInfoBar1.BorderSide = System.Windows.Forms.Border3DSide.Bottom;
            this.myInfoBar1.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.myInfoBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.myInfoBar1.GradientEndColor = System.Drawing.Color.Transparent;
            this.myInfoBar1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.myInfoBar1.GradientStartColor = System.Drawing.Color.White;
            this.myInfoBar1.Image = global::MaCompta.Properties.Resources.edit_find_project_32;
            this.myInfoBar1.ImageAlign = My.Forms.Controls.Panels.ImageAlignment.TopLeft;
            this.myInfoBar1.ImageOffsetX = 5;
            this.myInfoBar1.ImageOffsetY = 20;
            this.myInfoBar1.Location = new System.Drawing.Point(0, 0);
            this.myInfoBar1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.myInfoBar1.Name = "myInfoBar1";
            this.myInfoBar1.Size = new System.Drawing.Size(1004, 73);
            this.myInfoBar1.TabIndex = 1;
            this.myInfoBar1.Text1 = "Rechercher une Opération";
            this.myInfoBar1.Text1Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold);
            this.myInfoBar1.Text1ForeColor = System.Drawing.SystemColors.ControlText;
            this.myInfoBar1.Text1OffsetX = 10;
            this.myInfoBar1.Text1OffsetY = 0;
            this.myInfoBar1.Text2 = "Selectionner les critères de recherche pour afficher ou modifier une opération dé" +
    "jà enregistrée.";
            this.myInfoBar1.Text2Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.myInfoBar1.Text2ForeColor = System.Drawing.SystemColors.ControlText;
            this.myInfoBar1.Text2OffsetX = 15;
            this.myInfoBar1.Text2OffsetY = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(76, 86);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date de Début";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(457, 86);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Date de Fin";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(76, 119);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Partenaire";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtpDateDébut
            // 
            this.dtpDateDébut.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpDateDébut.Location = new System.Drawing.Point(190, 81);
            this.dtpDateDébut.Name = "dtpDateDébut";
            this.dtpDateDébut.Size = new System.Drawing.Size(253, 27);
            this.dtpDateDébut.TabIndex = 1;
            // 
            // dtpDateFin
            // 
            this.dtpDateFin.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpDateFin.Location = new System.Drawing.Point(549, 79);
            this.dtpDateFin.Name = "dtpDateFin";
            this.dtpDateFin.Size = new System.Drawing.Size(253, 27);
            this.dtpDateFin.TabIndex = 1;
            // 
            // cbPartenaire
            // 
            this.cbPartenaire.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbPartenaire.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbPartenaire.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbPartenaire.FormattingEnabled = true;
            this.cbPartenaire.Location = new System.Drawing.Point(190, 114);
            this.cbPartenaire.Name = "cbPartenaire";
            this.cbPartenaire.Size = new System.Drawing.Size(253, 28);
            this.cbPartenaire.TabIndex = 2;
            // 
            // rmBtnRechercher
            // 
            this.rmBtnRechercher.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.rmBtnRechercher.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
            this.rmBtnRechercher.BackColor = System.Drawing.Color.Transparent;
            this.rmBtnRechercher.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
            this.rmBtnRechercher.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
            this.rmBtnRechercher.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
            this.rmBtnRechercher.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
            this.rmBtnRechercher.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.rmBtnRechercher.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.rmBtnRechercher.FadingSpeed = 35;
            this.rmBtnRechercher.FlatAppearance.BorderSize = 0;
            this.rmBtnRechercher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rmBtnRechercher.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rmBtnRechercher.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
            this.rmBtnRechercher.Image = global::MaCompta.Properties.Resources.edit_find_7_32;
            this.rmBtnRechercher.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Top;
            this.rmBtnRechercher.ImageOffset = 0;
            this.rmBtnRechercher.IsPressed = false;
            this.rmBtnRechercher.KeepPress = false;
            this.rmBtnRechercher.Location = new System.Drawing.Point(823, 96);
            this.rmBtnRechercher.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rmBtnRechercher.MaxImageSize = new System.Drawing.Point(40, 40);
            this.rmBtnRechercher.MenuPos = new System.Drawing.Point(0, 0);
            this.rmBtnRechercher.Name = "rmBtnRechercher";
            this.rmBtnRechercher.Radius = 11;
            this.rmBtnRechercher.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
            this.rmBtnRechercher.Size = new System.Drawing.Size(104, 63);
            this.rmBtnRechercher.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
            this.rmBtnRechercher.SplitDistance = 0;
            this.rmBtnRechercher.TabIndex = 20;
            this.rmBtnRechercher.Text = "Rechercher";
            this.rmBtnRechercher.Title = "";
            this.rmBtnRechercher.UseVisualStyleBackColor = false;
            this.rmBtnRechercher.Click += new System.EventHandler(this.rmBtnRechercher_Click);
            // 
            // tblListe
            // 
            this.tblListe.AlternatingRowColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tblListe.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tblListe.ColumnModel = this.cmListe;
            this.tblListe.CustomEditKey = System.Windows.Forms.Keys.None;
            this.tblListe.EditStartAction = My.Forms.Controls.ListViews.XPTable.Editors.EditStartAction.CustomKey;
            this.tblListe.EnableHeaderContextMenu = false;
            this.tblListe.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.tblListe.FullRowSelect = true;
            this.tblListe.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tblListe.GridLines = My.Forms.Controls.ListViews.XPTable.Models.GridLines.Columns;
            this.tblListe.HeaderFont = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold);
            this.tblListe.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.tblListe.Location = new System.Drawing.Point(12, 182);
            this.tblListe.Name = "tblListe";
            this.tblListe.NoItemsText = "Aucune information à afficher!";
            this.tblListe.Size = new System.Drawing.Size(980, 335);
            this.tblListe.TabIndex = 3;
            this.tblListe.TableModel = this.tmListe;
            this.tblListe.Text = "table1";
            this.tblListe.DoubleClick += new System.EventHandler(this.tblListe_DoubleClick);
            // 
            // cmListe
            // 
            this.cmListe.AutoAdustWidth = true;
            this.cmListe.Columns.AddRange(new My.Forms.Controls.ListViews.XPTable.Models.Column[] {
            this.tcId,
            this.tcDate,
            this.tcPartenaire,
            this.tcDesignation,
            this.tcCompte,
            this.tcDébit,
            this.tcCrédit});
            this.cmListe.HeaderHeight = 29;
            // 
            // tcId
            // 
            this.tcId.Text = "Id Transaction";
            this.tcId.Visible = false;
            this.tcId.Width = 16;
            this.tcId.WidthPercentage = 0D;
            // 
            // tcDate
            // 
            this.tcDate.Text = "Date";
            this.tcDate.Width = 98;
            this.tcDate.WidthPercentage = 10D;
            // 
            // tcPartenaire
            // 
            this.tcPartenaire.Text = "Partenaire";
            this.tcPartenaire.Width = 137;
            this.tcPartenaire.WidthPercentage = 14D;
            // 
            // tcDesignation
            // 
            this.tcDesignation.Text = "Designation";
            this.tcDesignation.Width = 176;
            this.tcDesignation.WidthPercentage = 18D;
            // 
            // tcCompte
            // 
            this.tcCompte.Text = "Compte";
            this.tcCompte.Width = 362;
            this.tcCompte.WidthPercentage = 37D;
            // 
            // tcDébit
            // 
            this.tcDébit.Alignment = My.Forms.Controls.ListViews.XPTable.Models.ColumnAlignment.Right;
            this.tcDébit.Text = "Débit";
            this.tcDébit.Width = 88;
            this.tcDébit.WidthPercentage = 9D;
            // 
            // tcCrédit
            // 
            this.tcCrédit.Alignment = My.Forms.Controls.ListViews.XPTable.Models.ColumnAlignment.Right;
            this.tcCrédit.Text = "Crédit";
            this.tcCrédit.Width = 88;
            this.tcCrédit.WidthPercentage = 9D;
            // 
            // tmListe
            // 
            this.tmListe.RowHeight = 25;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(457, 117);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Montant";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbMontant
            // 
            this.tbMontant.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tbMontant.Location = new System.Drawing.Point(730, 115);
            this.tbMontant.Name = "tbMontant";
            this.tbMontant.Size = new System.Drawing.Size(72, 27);
            this.tbMontant.TabIndex = 21;
            this.tbMontant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cbTypeMontant
            // 
            this.cbTypeMontant.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbTypeMontant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeMontant.FormattingEnabled = true;
            this.cbTypeMontant.Location = new System.Drawing.Point(549, 114);
            this.cbTypeMontant.Name = "cbTypeMontant";
            this.cbTypeMontant.Size = new System.Drawing.Size(175, 28);
            this.cbTypeMontant.TabIndex = 2;
            // 
            // epValidation
            // 
            this.epValidation.ContainerControl = this;
            // 
            // pcbCompte
            // 
            this.pcbCompte.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pcbCompte.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.pcbCompte.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.pcbCompte.CheckBoxes = false;
            this.pcbCompte.Checked = System.Windows.Forms.CheckState.Unchecked;
            this.pcbCompte.DividerFormat = "---";
            this.pcbCompte.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.pcbCompte.DropDownWidth = 500;
            this.pcbCompte.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pcbCompte.FormattingEnabled = true;
            this.pcbCompte.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.pcbCompte.GroupColor = System.Drawing.SystemColors.WindowText;
            this.pcbCompte.ItemSeparator1 = ',';
            this.pcbCompte.ItemSeparator2 = '&';
            this.pcbCompte.Location = new System.Drawing.Point(190, 149);
            this.pcbCompte.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pcbCompte.MaxLength = 150;
            this.pcbCompte.Name = "pcbCompte";
            this.pcbCompte.Size = new System.Drawing.Size(612, 26);
            this.pcbCompte.TabIndex = 22;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(76, 150);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Compte";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // RechercherTransationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MaCompta.Properties.Resources.Atra_Dot___White_1920x1200;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1004, 529);
            this.Controls.Add(this.pcbCompte);
            this.Controls.Add(this.rmBtnRechercher);
            this.Controls.Add(this.tblListe);
            this.Controls.Add(this.dtpDateDébut);
            this.Controls.Add(this.dtpDateFin);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbTypeMontant);
            this.Controls.Add(this.cbPartenaire);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbMontant);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.myInfoBar1);
            this.Controls.Add(this.label4);
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "RechercherTransationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rechercher une Opération";
            ((System.ComponentModel.ISupportInitialize)(this.tblListe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epValidation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private My.Forms.Controls.Panels.MyInfoBar myInfoBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnRechercher;
        private My.Forms.Controls.ListViews.XPTable.Models.Table tblListe;
        private My.Forms.Controls.ListViews.XPTable.Models.TableModel tmListe;
        private My.Forms.Controls.ListViews.XPTable.Models.ColumnModel cmListe;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbMontant;
        private System.Windows.Forms.DateTimePicker dtpDateDébut;
        private System.Windows.Forms.DateTimePicker dtpDateFin;
        private System.Windows.Forms.ComboBox cbPartenaire;
        private System.Windows.Forms.ComboBox cbTypeMontant;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcId;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcDate;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcPartenaire;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcDesignation;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcDébit;
        private System.Windows.Forms.ErrorProvider epValidation;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcCompte;
        private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcCrédit;
        private My.Forms.Controls.ComboBoxes.PowerComboBox pcbCompte;
        private System.Windows.Forms.Label label5;
    }
}