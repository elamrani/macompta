﻿using Controller;

using My.Forms.Controls.ListViews.XPTable.Models;
using My.Forms.MsgBoxes;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MaCompta
{
	public partial class PlanComptableForm : Form
	{
		#region Fields

		private PlanComptableController _controller;

		#endregion Fields

		#region Constructors

		public PlanComptableForm()
		{
			InitializeComponent();
			Initialisation();
			ChargerPlanComptable();
		}

		#endregion Constructors

		#region Private Methods

		private void AfficherDétails()
		{
			if ( tblPlanComptable.TableModel.Selections.SelectedItems.Length != 0 )
			{
			    Row ligne = tblPlanComptable.TableModel.Selections.SelectedItems[0];
			    if ( !string.IsNullOrEmpty(ligne.Cells[2].Text) )
			    {
			        new CompteForm(ligne.Cells[2].Text.Trim()).ShowDialog();
			        ChargerPlanComptable();
			    }//if
			}//if
		}

		/// <summary>
		/// Affichage du plan comptable
		/// </summary>
		private void ChargerPlanComptable()
		{
			tblPlanComptable.TableModel.Rows.Clear();
			Exception eValeurRetournée = _controller.ChargerPlanComptable(tblPlanComptable);
			if ( eValeurRetournée != null )
			{
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement d'informations à partir de la base de données.";
			    msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "La liste des données n'a pas pu être chargée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
				msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
		}

		private void Initialisation()
		{
			_controller = new PlanComptableController(Program.BD, Program.CultureIG);
		}

		void PlanComptableForm_Shown(object sender, EventArgs e)
		{
			WindowState = FormWindowState.Maximized;
		}

		private void rmBtnComptes_Click(object sender, EventArgs e)
		{
			new CompteForm().ShowDialog();
			ChargerPlanComptable();
		}

		void tblPlanComptable_DoubleClick(object sender, EventArgs e)
		{
			AfficherDétails();
		}

		#endregion Private Methods
	}
}