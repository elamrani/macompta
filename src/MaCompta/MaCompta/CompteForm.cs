﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 20/11/2010
 * Time: 14:31
 * 
 * © 1428-1431 (2007-2010) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using Controller;

using Model;

using My.Forms.MsgBoxes;

using System;
using System.Drawing;
using System.Windows.Forms;

namespace MaCompta
{
	/// <summary>
	/// Description of CompteForm.
	/// </summary>
	public partial class CompteForm : Form
	{
		#region Fields

		private bool _activerEvenement;
		private Compte _compteChargé;
		private CompteController _controller;
		private DateTime _dateSoldeChargé;

		#endregion Fields

		#region Constructors

		public CompteForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();

			Initialisation();
		}

		public CompteForm(string sCompteACharger)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();

			Initialisation();

			pcbListe.Text = sCompteACharger;
		}

		#endregion Constructors

		#region Public Properties

		public Compte CompteChargé
		{
			get { return _compteChargé; }
		}

		#endregion Public Properties

		#region Private Methods

		private void AffichageSelonBudget()
		{
			try
			{
			    if ( string.IsNullOrEmpty(tbBudgetQuotidien.Text) )
			    {
			        gbDifférence.Visible = false;
			        gbDurée.Visible = false;
			    }//if
			    else if ( double.Parse(tbBudgetQuotidien.Text) != 0 )
			    {
			        CalculerDifférencesBudget();
			        gbDifférence.Visible = true;
			        gbDurée.Visible = true;
			    }//else if
			}//try
			catch
			{
			    gbDifférence.Visible = false;
			    gbDurée.Visible = false;
			}//catch
		}

		/// <summary>
		/// Active ou désactives les autres informations en fonction du type de compte
		/// </summary>
		/// <param name="id">Id du type de compte</param>
		private void AffichageSelonTypeCompte(long id)
		{
			bool bAfficherDétails = (id == 2 || id == 3);

			tbNuméro.Enabled = bAfficherDétails;
			tbInstitution.Enabled = bAfficherDétails;
			tbAgence.Enabled = bAfficherDétails;
			tbBudgetQuotidien.Enabled = bAfficherDétails;
			AffichageSelonBudget();
			lbSolde.Visible = bAfficherDétails;
			apSolde.Visible = bAfficherDétails;
		}

		/// <summary>
		/// Permet de calculer la différence entre moyenne sur les jours restants avec le budget
		/// </summary>
		private void CalculerDifférencesBudget()
		{
			try
			{
			    int iNbJours = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
			    if ( rbMoisSuivant.Checked ) {
			        iNbJours += DateTime.DaysInMonth(
			            DateTime.Now.Month + 1 > 12 ? DateTime.Now.Year + 1 : DateTime.Now.Year,
			            DateTime.Now.Month + 1 > 12 ? 1 : DateTime.Now.Month + 1
			            );
			    }//if

			    int iNbJoursRestants = iNbJours - DateTime.Now.Day;
			    ddcNombreJours.DigitText = iNbJoursRestants.ToString();
			    double dDifférence = (double.Parse(ddcSolde.DigitText) / iNbJoursRestants) -
				    double.Parse(tbBudgetQuotidien.Text);

			    ddcDifférenceJournalière.DigitText = dDifférence.ToString("N2", Program.CultureIG);
			    ddcSommeAllouée.DigitText = (double.Parse(tbBudgetQuotidien.Text) * iNbJoursRestants).ToString("N2", Program.CultureIG);
			    ddcDifférenceTotale.DigitText = (dDifférence * iNbJoursRestants).ToString("N2", Program.CultureIG);
			    if ( dDifférence < 0 ) {
				    ddcDifférenceJournalière.DigitColor = Color.PeachPuff;
				    ddcDifférenceTotale.DigitColor = Color.PeachPuff;
			    }//if
			    else {
				    ddcDifférenceJournalière.DigitColor = Color.GreenYellow;
				    ddcDifférenceTotale.DigitColor = Color.GreenYellow;
			    }//else
			}//try
			catch ( Exception ex )
			{
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}//catch
		}

		/// <summary>
		/// Charger les infos de la base de données
		/// </summary>
		/// <param name="id_nom">Nom du compte à charger</param>
		/// <param name="afficherErreur">Vrai pour afficher un message d'erreur lorsque
		/// le code ne permet pas de charger des infos,
		/// faux pour ne rien afficher</param>
		private void ChargerInformations(string id_nom, bool afficherErreur)
		{
			if ( string.IsNullOrEmpty(id_nom) ) {
				//Rien à charger
				return;
			}//if
			else if (id_nom.Length < 4)
			{
			    //Rien à charger car le compte n'est pas valide!
			    return;
			}//else if

			_compteChargé = new Model.Compte(Program.BD);
			Exception eValeurRetournée = _compteChargé.Charger(long.Parse(id_nom.Substring(0,4)));
			if ( eValeurRetournée == null ) {
			    cbTypeCompte.SelectedValue = _compteChargé.IdTypeCompte;
			    mtbNuméro.Text = _compteChargé.Id.ToString();
				tbNom.Text = _compteChargé.Nom;
				tbNuméro.Text = _compteChargé.Numéro;
				tbInstitution.Text = _compteChargé.Institution;
				tbAgence.Text = _compteChargé.Agence;
			    if (_compteChargé.Solde.HasValue)
			    {
			        ddcSolde.DigitText = _compteChargé.Solde.Value.ToString("N2", Program.CultureIG);
			    }//if
			    else
			    {
			        ddcSolde.DigitText = (0).ToString("N2", Program.CultureIG);
			    }//else
				ckAfficher.Checked = _compteChargé.Afficher;
			    if (_compteChargé.BudgetQuotidien.HasValue)
			    {
			        tbBudgetQuotidien.Text = _compteChargé.BudgetQuotidien.Value.ToString("N2", Program.CultureIG);
			    }//if
			    else
			    {
			        tbBudgetQuotidien.Text = string.Empty;
			    }//else
			    rbMoisCourant.Checked = _compteChargé.TypeNombreJours.HasValue && _compteChargé.TypeNombreJours == 0 ? true : false;
				rbMoisSuivant.Checked = !rbMoisCourant.Checked;
				DateTime? dtDateSolde = null;
				eValeurRetournée = _controller.ChargerDateSoldeCompte(_compteChargé.Id, ref dtDateSolde);
				if ( eValeurRetournée != null ) {
					_dateSoldeChargé = DateTime.Now;
				    System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				    System.Diagnostics.Debug.WriteLine( eValeurRetournée );
					MyMsgBox.Show(eValeurRetournée.Message,
					              "Erreur de chargement de la date du solde du compte",
					              MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				}//if
				else {
					_dateSoldeChargé = dtDateSolde.Value;
				}//else
				lbSolde.Text = "Solde au " +  _dateSoldeChargé.ToShortDateString();
				//Calculer la différence par rapport au budget
				if ( gbDifférence.Visible ) {
					//tbBudgetQuotidien.Text a un évènement qui modifie cette visibilité
					CalculerDifférencesBudget();
				}//if
			}//if
			else {
				//Une erreur est survenue
				MessageBox.Show("Une erreur est survenue lors du chargement des informations",
				                "Une erreur est survenue", MessageBoxButtons.OK,
				                MessageBoxIcon.Exclamation);
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( eValeurRetournée.Message );
				Recommencer(false, false);
			}//else
		}

		/// <summary>
		/// Permet de charger la liste des comptes ordonnées selon la classe de compte
		/// </summary>
		private void ChargerListeComptes()
		{
			Exception eValeurRetournée = _controller.ChargerListeComptes(pcbListe, true);
			if ( eValeurRetournée != null )
			{
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement " +
			        "de la liste des comptes";
			    msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "La liste des comptes n'a pas pu être chargée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
				msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
		}

		private void Enregistrer()
		{
			if ( VérifierInfos() ) {
				//Confirmation
				if ( MyMsgBox.Show("Confirmez-vous l'enregistrement de ces informations?",
				                   "Confirmation de l'enregistrement", MessageBoxButtons.YesNo,
				                   MessageBoxIcon.Question) != DialogResult.Yes
				   ) {
					return;
				}//if

				if ( _compteChargé != null ) {
					Modification();
				}//if
				else {
					//C'est un nouvel enregistrement à ajouter
					Nouveau();
				}//else
			}//if
		}

		/// <summary>
		/// Prépare l'IG
		/// </summary>
		private void Initialisation()
		{
			Exception eValeurRetournée = null;
			_controller = new CompteController(Program.BD);
			_compteChargé = null;

			#region " Chargement des types de comptes "
			_activerEvenement = false;
			eValeurRetournée = _controller.ChargerListeValeurs(cbTypeCompte, "type_compte", "id", "nom", "1 = 1 ORDER BY `id`");
			_activerEvenement = true;
			cbTypeCompte_SelectedIndexChanged(this, null);
			if ( eValeurRetournée != null )
			{
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement " +
			        "de la liste des types de comptes";
			    msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "La liste des types de comptes n'a pas pu être chargée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
				msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
			#endregion " Chargement des types de comptes "

			ChargerListeComptes();

			_dateSoldeChargé = DateTime.Now;
		}

		private void Modification()
		{
			try {
				Model.Compte compte = new Model.Compte(Program.BD);
				compte.Id = long.Parse(mtbNuméro.Text);
			    compte.IdTypeCompte = (long) cbTypeCompte.SelectedValue;
				compte.Nom = tbNom.Text.Trim();
				compte.Numéro = tbNuméro.Text.Trim();
				compte.Institution = tbInstitution.Text.Trim();
				compte.Agence = tbAgence.Text.Trim();
				compte.Afficher = ckAfficher.Checked;
			    if ( string.IsNullOrEmpty(tbBudgetQuotidien.Text) )
			    {
			        compte.BudgetQuotidien = null;
			    }//if
			    else
			    {
			        compte.BudgetQuotidien = double.Parse(tbBudgetQuotidien.Text);
			    }//else
			    compte.TypeNombreJours = rbMoisCourant.Checked ? 0 : 1;
				Exception eValeurRetournée = compte.Modifier(_compteChargé.Id);
				if ( eValeurRetournée != null ) {
					throw eValeurRetournée;
				}//if
				else {
					Recommencer(true, false);
				}//else
			}//try
			catch ( Exception ex ) {
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( ex.Message );
				MyMsgBox msgbox = new MyMsgBox();
				msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
				msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
				msgbox.MainInstruction = "Erreur lors de la tentative " +
					"d'enregistrement du compte";
				msgbox.WindowTitle = "Erreur d'enregistrement des données";
				msgbox.Content = "Ce compte n'a pas pu être enregistré.";
				#if DEBUG
				msgbox.ExpandedByDefault = true;
				#else
				msgbox.ExpandedByDefault = false;
				#endif
				msgbox.ExpandedInformation = ex.Message;
				msgbox.Show();
			}//catch
		}

		private void Nouveau()
		{
			try {
				Model.Compte compte = new Model.Compte(Program.BD);
			    compte.Id = long.Parse(mtbNuméro.Text);
			    compte.IdTypeCompte = (long) cbTypeCompte.SelectedValue;
			    compte.Nom = tbNom.Text.Trim();
				compte.Numéro = tbNuméro.Text.Trim();
				compte.Institution = tbInstitution.Text.Trim();
				compte.Agence = tbAgence.Text.Trim();
				compte.Afficher = ckAfficher.Checked;
			    if ( string.IsNullOrEmpty(tbBudgetQuotidien.Text) )
			    {
			        compte.BudgetQuotidien = null;
			    }//if
			    else
			    {
			        compte.BudgetQuotidien = double.Parse(tbBudgetQuotidien.Text);
			    }//else
				compte.TypeNombreJours = rbMoisCourant.Checked ? 0 : 1;
				Exception eValeurRetournée = null;
				eValeurRetournée = compte.Ajouter();
				if ( eValeurRetournée != null ) {
					throw eValeurRetournée;
				}//if
				else {
			        Recommencer(true, false);
				}//else
				
			}//try
			catch ( Exception ex ) {
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( ex.Message );
				MyMsgBox msgbox = new MyMsgBox();
				msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
				msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
				msgbox.MainInstruction = "Erreur lors de la tentative d'enregistrement du compte";
				msgbox.WindowTitle = "Erreur d'enregistrement des données";
				msgbox.Content = "Ce compte n'a pas pu être enregistré.";
				#if DEBUG
				msgbox.ExpandedByDefault = true;
				#else
				msgbox.ExpandedByDefault = false;
				#endif
				msgbox.ExpandedInformation = ex.Message;
				msgbox.Show();
			}//catch
		}

		void PcbListeSelectedIndexChanged(object sender, EventArgs e)
		{
			ChargerInformations(pcbListe.Text, true);
		}

		void PcbListeTextUpdate(object sender, EventArgs e)
		{
			ChargerInformations(pcbListe.Text, false);
		}

		void RbMoisCourantClick(object sender, EventArgs e)
		{
			//if ( !string.IsNullOrEmpty(cbListe.Text) ) {
			CalculerDifférencesBudget();
			//}//if
		}

		void RbMoisSuivantClick(object sender, EventArgs e)
		{
			//if ( !string.IsNullOrEmpty(cbListe.Text) ) {
			CalculerDifférencesBudget();
			//}//if
		}

		/// <summary>
		/// Permet d'effacer tout le form
		/// </summary>
		/// <param name="effacerComboBox">true : effacer également le combobox;
		/// false: ne pas l'effacer (ne pas effacer la saisit de l'usager)</param>
		/// <param name="confirmer">Demander la confirmation?</param>
		private void Recommencer(bool effacerComboBox, bool confirmer)
		{
			if ( confirmer ) {
				//Confirmation
				if ( MyMsgBox.Show("Confirmez-vous l'effacement de toutes les informations " +
				                   "de cette fenêtre?",
				                   "Confirmation de l'effacement", MessageBoxButtons.YesNo,
				                   MessageBoxIcon.Question) != DialogResult.Yes
				   ) {
					return;
				}//
			}//if

			if ( effacerComboBox ) {
				pcbListe.Text = string.Empty;
			}//if

			_compteChargé = null;
			epValidation.Clear();
			ChargerListeComptes();
			_activerEvenement = true;
			cbTypeCompte.SelectedIndex = 0;
			mtbNuméro.Text = string.Empty;
			mtbNuméro.Mask = "0000";
			tbNom.Text = string.Empty;
			tbNuméro.Text =string.Empty;
			tbInstitution.Text =string.Empty;
			tbAgence.Text =string.Empty;
			rbMoisCourant.Checked = true;
			tbBudgetQuotidien.Text = string.Empty;
			lbSolde.Text = "Solde";
			ddcSolde.DigitText = (0).ToString("N2", Program.CultureIG);
			ddcDifférenceJournalière.DigitText = (0).ToString("N2", Program.CultureIG);
			ddcDifférenceTotale.DigitText = (0).ToString("N2", Program.CultureIG);
			_dateSoldeChargé = DateTime.Now;
			ckAfficher.Checked = true;
		}

		void RmBtnEnregistrerClick(object sender, EventArgs e)
		{
			Enregistrer();
		}

		void RmBtnRecommencerClick(object sender, EventArgs e)
		{
			Recommencer(true, true);
		}

		void RmBtnSupprimerClick(object sender, EventArgs e)
		{
			Supprimer();
		}

		private void Supprimer()
		{
			//Vérifier si les infos ont été chargées
			if ( _compteChargé == null )
				return; //ne rien faire

			//Demander la confirmation
			MyMsgBoxResult mrRésultat;
			MyMsgBox msgBox = new MyMsgBox();
			{
				msgBox.Owner = this;
				msgBox.Buttons = new MyMsgBoxButton[] { new MyMsgBoxButton(MyMsgBoxResult.Yes),
					new MyMsgBoxButton(MyMsgBoxResult.No) };
				msgBox.Content = "Cette suppression entraînera la suppression de toutes les informations associées à ce compte!";
				msgBox.MainIcon = MyMsgBoxIcon.SecurityQuestion;
				msgBox.MainInstruction = "Etes-vous sûr de toujours vouloir supprimer de manière permanente ce compte?";
				msgBox.WindowTitle = "Suppression du compte";
				msgBox.DefaultButton = MyMsgBoxDefaultButton.Button2;
				msgBox.FooterIcon = MyMsgBoxIcon.SecurityWarning;
				msgBox.FooterText = "Cette opération ne peut pas être annulée.";
				msgBox.LockSystem = true;
				mrRésultat = msgBox.Show();
			}
			if ( mrRésultat == MyMsgBoxResult.No ) {
				return;
			}//if

			try {
				//Suppression
			    Exception eValeurRetournée = _compteChargé.Supprimer();
				if ( eValeurRetournée != null ) {
					throw eValeurRetournée;
				}//if
				else {
					Recommencer(true, false);
				}//else
			}//try
			catch ( Exception ex ) {
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( ex.Message );
				MyMsgBox msgbox = new MyMsgBox();
				msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
				msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
				msgbox.MainInstruction = "Erreur lors de la tentative " +
					"de suppression du compte";
				msgbox.WindowTitle = "Erreur de suppression des données";
				msgbox.Content = "Ce compte n'a pas pu être supprimé correctement.";
				#if DEBUG
				msgbox.ExpandedByDefault = true;
				#else
				msgbox.ExpandedByDefault = false;
				#endif
				msgbox.ExpandedInformation = ex.Message;
				msgbox.Show();
			}//catch
		}

		void TbBudgetQuotidienTextChanged(object sender, EventArgs e)
		{
			AffichageSelonBudget();
		}

		/// <summary>
		/// Permet de vérifier si toutes les données saisies sont correctes
		/// </summary>
		/// <returns>true : tout est bon, false : un problème est survenu</returns>
		private bool VérifierInfos()
		{
			bool bValeurRetournée = true;
			epValidation.Clear();

			if ( string.IsNullOrEmpty(tbNom.Text.Trim()) ) {
				epValidation.SetError( tbNom, "Le nom du compte ne peut pas être vide");
				bValeurRetournée = false;
			}//if

			if ( !string.IsNullOrEmpty(tbBudgetQuotidien.Text) )
			{
			    try {
				    double.Parse(tbBudgetQuotidien.Text);
			    }//try
			    catch {
				    epValidation.SetError( tbBudgetQuotidien, "Le montant saisit est invalide.");
				    bValeurRetournée = false;
			    }//catch
			}//if

			return bValeurRetournée;
		}

		void cbTypeCompte_SelectedIndexChanged(object sender, EventArgs e)
		{
			if ( _activerEvenement )
			{
			    AffichageSelonTypeCompte(long.Parse(cbTypeCompte.SelectedValue.ToString())); 
			}//if
		}

		#endregion Private Methods
	}
}