﻿namespace MaCompta
{
    partial class GraphesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pcGroupes = new My.Forms.Controls.Charts.PieChart.PieChart();
            this.label1 = new System.Windows.Forms.Label();
            this.pcbListe = new My.Forms.Controls.ComboBoxes.PowerComboBox();
            this.SuspendLayout();
            // 
            // pcGroupes
            // 
            this.pcGroupes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pcGroupes.AutoSizePie = true;
            this.pcGroupes.BackColor = System.Drawing.Color.Transparent;
            this.pcGroupes.Inclination = 1F;
            this.pcGroupes.Items.Add(new My.Forms.Controls.Charts.PieChart.PieChartItem(10D, System.Drawing.Color.Gray, "test", "", 0F));
            this.pcGroupes.Items.Add(new My.Forms.Controls.Charts.PieChart.PieChartItem(5D, System.Drawing.Color.Red, "test 2", "", 0F));
            this.pcGroupes.Location = new System.Drawing.Point(12, 45);
            this.pcGroupes.Name = "pcGroupes";
            this.pcGroupes.Radius = 219.2499F;
            this.pcGroupes.Size = new System.Drawing.Size(678, 396);
            this.pcGroupes.TabIndex = 4;
            this.pcGroupes.Text = "Depenses";
            this.pcGroupes.Thickness = 50F;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "Groupes";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pcbListe
            // 
            this.pcbListe.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pcbListe.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.pcbListe.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.pcbListe.CheckBoxes = false;
            this.pcbListe.Checked = System.Windows.Forms.CheckState.Unchecked;
            this.pcbListe.DividerFormat = "---";
            this.pcbListe.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.pcbListe.FormattingEnabled = true;
            this.pcbListe.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
            this.pcbListe.GroupColor = System.Drawing.SystemColors.WindowText;
            this.pcbListe.ItemSeparator1 = ',';
            this.pcbListe.ItemSeparator2 = '&';
            this.pcbListe.Location = new System.Drawing.Point(85, 13);
            this.pcbListe.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pcbListe.MaxLength = 45;
            this.pcbListe.Name = "pcbListe";
            this.pcbListe.Size = new System.Drawing.Size(605, 25);
            this.pcbListe.TabIndex = 6;
            this.pcbListe.SelectedIndexChanged += new System.EventHandler(this.pcbListe_SelectedIndexChanged);
            // 
            // GraphesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(702, 453);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pcbListe);
            this.Controls.Add(this.pcGroupes);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "GraphesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Graphs pour les dépenses";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private My.Forms.Controls.Charts.PieChart.PieChart pcGroupes;
        private System.Windows.Forms.Label label1;
        private My.Forms.Controls.ComboBoxes.PowerComboBox pcbListe;
    }
}