﻿/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 20/11/2010
 * Time: 14:31
 * 
 * © 1428-1431 (2007-2010) All rights reserved to Mohamed Y. ElAmrani.
 */
namespace MaCompta
{
	partial class CompteForm
	{
		#region Fields

		private My.Forms.Controls.Panels.A1Panel.A1Panel a1Panel1;
		private My.Forms.Controls.Panels.A1Panel.A1Panel a1Panel2;
		private My.Forms.Controls.Panels.A1Panel.A1Panel a1Panel3;
		private My.Forms.Controls.Panels.A1Panel.A1Panel a1Panel4;
		private My.Forms.Controls.Panels.A1Panel.A1Panel apSolde;
		private System.Windows.Forms.ComboBox cbTypeCompte;
		private System.Windows.Forms.CheckBox ckAfficher;

		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private My.Forms.Controls.Misc.DigitalDisplayControl ddcDifférenceJournalière;
		private My.Forms.Controls.Misc.DigitalDisplayControl ddcDifférenceTotale;
		private My.Forms.Controls.Misc.DigitalDisplayControl ddcNombreJours;
		private My.Forms.Controls.Misc.DigitalDisplayControl ddcSolde;
		private My.Forms.Controls.Misc.DigitalDisplayControl ddcSommeAllouée;
		private System.Windows.Forms.ErrorProvider epValidation;
		private System.Windows.Forms.GroupBox gbDifférence;
		private System.Windows.Forms.GroupBox gbDurée;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label lbAgence;
		private System.Windows.Forms.Label lbInstitution;
		private System.Windows.Forms.Label lbNom;
		private System.Windows.Forms.Label lbNuméro;
		private System.Windows.Forms.Label lbSolde;
		private System.Windows.Forms.Label lbSymbolMonétaire2;
		private System.Windows.Forms.MaskedTextBox mtbNuméro;
		private My.Forms.Controls.ComboBoxes.PowerComboBox pcbListe;
		private System.Windows.Forms.RadioButton rbMoisCourant;
		private System.Windows.Forms.RadioButton rbMoisSuivant;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnEnregistrer;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnRecommencer;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnSupprimer;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
		private System.Windows.Forms.TextBox tbAgence;
		private System.Windows.Forms.TextBox tbBudgetQuotidien;
		private System.Windows.Forms.TextBox tbInstitution;
		private System.Windows.Forms.TextBox tbNom;
		private System.Windows.Forms.TextBox tbNuméro;

		#endregion Fields

		#region Private Methods

		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.pcbListe = new My.Forms.Controls.ComboBoxes.PowerComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.tbAgence = new System.Windows.Forms.TextBox();
			this.tbInstitution = new System.Windows.Forms.TextBox();
			this.tbNuméro = new System.Windows.Forms.TextBox();
			this.rmBtnSupprimer = new My.Forms.Controls.Ribbon.RibbonMenuButton();
			this.rmBtnEnregistrer = new My.Forms.Controls.Ribbon.RibbonMenuButton();
			this.rmBtnRecommencer = new My.Forms.Controls.Ribbon.RibbonMenuButton();
			this.lbNom = new System.Windows.Forms.Label();
			this.lbNuméro = new System.Windows.Forms.Label();
			this.lbInstitution = new System.Windows.Forms.Label();
			this.lbAgence = new System.Windows.Forms.Label();
			this.lbSolde = new System.Windows.Forms.Label();
			this.tbNom = new System.Windows.Forms.TextBox();
			this.ckAfficher = new System.Windows.Forms.CheckBox();
			this.apSolde = new My.Forms.Controls.Panels.A1Panel.A1Panel();
			this.ddcSolde = new My.Forms.Controls.Misc.DigitalDisplayControl();
			this.lbSymbolMonétaire2 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.tbBudgetQuotidien = new System.Windows.Forms.TextBox();
			this.gbDifférence = new System.Windows.Forms.GroupBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.a1Panel4 = new My.Forms.Controls.Panels.A1Panel.A1Panel();
			this.ddcSommeAllouée = new My.Forms.Controls.Misc.DigitalDisplayControl();
			this.label8 = new System.Windows.Forms.Label();
			this.a1Panel2 = new My.Forms.Controls.Panels.A1Panel.A1Panel();
			this.ddcDifférenceTotale = new My.Forms.Controls.Misc.DigitalDisplayControl();
			this.label5 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.a1Panel3 = new My.Forms.Controls.Panels.A1Panel.A1Panel();
			this.ddcNombreJours = new My.Forms.Controls.Misc.DigitalDisplayControl();
			this.a1Panel1 = new My.Forms.Controls.Panels.A1Panel.A1Panel();
			this.ddcDifférenceJournalière = new My.Forms.Controls.Misc.DigitalDisplayControl();
			this.label2 = new System.Windows.Forms.Label();
			this.gbDurée = new System.Windows.Forms.GroupBox();
			this.rbMoisSuivant = new System.Windows.Forms.RadioButton();
			this.rbMoisCourant = new System.Windows.Forms.RadioButton();
			this.mtbNuméro = new System.Windows.Forms.MaskedTextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.cbTypeCompte = new System.Windows.Forms.ComboBox();
			this.epValidation = new System.Windows.Forms.ErrorProvider(this.components);
			this.tableLayoutPanel.SuspendLayout();
			this.apSolde.SuspendLayout();
			this.gbDifférence.SuspendLayout();
			this.a1Panel4.SuspendLayout();
			this.a1Panel2.SuspendLayout();
			this.a1Panel3.SuspendLayout();
			this.a1Panel1.SuspendLayout();
			this.gbDurée.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.epValidation)).BeginInit();
			this.SuspendLayout();
			// 
			// pcbListe
			// 
			this.pcbListe.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.pcbListe.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.pcbListe.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.pcbListe.CheckBoxes = false;
			this.pcbListe.Checked = System.Windows.Forms.CheckState.Unchecked;
			this.pcbListe.DividerFormat = "---";
			this.pcbListe.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
			this.pcbListe.FormattingEnabled = true;
			this.pcbListe.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(248)))), ((int)(((byte)(255)))));
			this.pcbListe.GroupColor = System.Drawing.SystemColors.WindowText;
			this.pcbListe.ItemSeparator1 = ',';
			this.pcbListe.ItemSeparator2 = '&';
			this.pcbListe.Location = new System.Drawing.Point(208, 16);
			this.pcbListe.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.pcbListe.MaxLength = 45;
			this.pcbListe.Name = "pcbListe";
			this.pcbListe.Size = new System.Drawing.Size(394, 25);
			this.pcbListe.TabIndex = 1;
			this.pcbListe.SelectedIndexChanged += new System.EventHandler(this.PcbListeSelectedIndexChanged);
			this.pcbListe.TextUpdate += new System.EventHandler(this.PcbListeTextUpdate);
			// 
			// label1
			// 
			this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.label1.BackColor = System.Drawing.Color.Transparent;
			this.label1.Location = new System.Drawing.Point(157, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(45, 31);
			this.label1.TabIndex = 0;
			this.label1.Text = "Liste";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tableLayoutPanel
			// 
			this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Left)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
			this.tableLayoutPanel.ColumnCount = 4;
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.20367F));
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.71286F));
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.04174F));
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.04174F));
			this.tableLayoutPanel.Controls.Add(this.tbAgence, 1, 4);
			this.tableLayoutPanel.Controls.Add(this.tbInstitution, 1, 3);
			this.tableLayoutPanel.Controls.Add(this.tbNuméro, 1, 2);
			this.tableLayoutPanel.Controls.Add(this.rmBtnSupprimer, 0, 9);
			this.tableLayoutPanel.Controls.Add(this.rmBtnEnregistrer, 3, 9);
			this.tableLayoutPanel.Controls.Add(this.rmBtnRecommencer, 1, 9);
			this.tableLayoutPanel.Controls.Add(this.lbNom, 0, 1);
			this.tableLayoutPanel.Controls.Add(this.lbNuméro, 0, 2);
			this.tableLayoutPanel.Controls.Add(this.lbInstitution, 0, 3);
			this.tableLayoutPanel.Controls.Add(this.lbAgence, 0, 4);
			this.tableLayoutPanel.Controls.Add(this.lbSolde, 0, 8);
			this.tableLayoutPanel.Controls.Add(this.tbNom, 1, 1);
			this.tableLayoutPanel.Controls.Add(this.ckAfficher, 0, 7);
			this.tableLayoutPanel.Controls.Add(this.apSolde, 2, 8);
			this.tableLayoutPanel.Controls.Add(this.label4, 0, 5);
			this.tableLayoutPanel.Controls.Add(this.tbBudgetQuotidien, 1, 5);
			this.tableLayoutPanel.Controls.Add(this.gbDifférence, 2, 5);
			this.tableLayoutPanel.Controls.Add(this.gbDurée, 0, 6);
			this.tableLayoutPanel.Controls.Add(this.mtbNuméro, 3, 0);
			this.tableLayoutPanel.Controls.Add(this.label10, 2, 0);
			this.tableLayoutPanel.Controls.Add(this.label11, 0, 0);
			this.tableLayoutPanel.Controls.Add(this.cbTypeCompte, 1, 0);
			this.tableLayoutPanel.Location = new System.Drawing.Point(14, 55);
			this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tableLayoutPanel.Name = "tableLayoutPanel";
			this.tableLayoutPanel.RowCount = 10;
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.67222F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66555F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66555F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66555F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66555F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66555F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67F));
			this.tableLayoutPanel.Size = new System.Drawing.Size(731, 643);
			this.tableLayoutPanel.TabIndex = 2;
			// 
			// tbAgence
			// 
			this.tbAgence.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.SetColumnSpan(this.tbAgence, 3);
			this.tbAgence.Location = new System.Drawing.Point(165, 213);
			this.tbAgence.Margin = new System.Windows.Forms.Padding(3, 4, 18, 4);
			this.tbAgence.MaxLength = 150;
			this.tbAgence.Name = "tbAgence";
			this.tbAgence.Size = new System.Drawing.Size(548, 24);
			this.tbAgence.TabIndex = 11;
			// 
			// tbInstitution
			// 
			this.tbInstitution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.SetColumnSpan(this.tbInstitution, 3);
			this.tbInstitution.Location = new System.Drawing.Point(165, 163);
			this.tbInstitution.Margin = new System.Windows.Forms.Padding(3, 4, 18, 4);
			this.tbInstitution.MaxLength = 150;
			this.tbInstitution.Name = "tbInstitution";
			this.tbInstitution.Size = new System.Drawing.Size(548, 24);
			this.tbInstitution.TabIndex = 9;
			// 
			// tbNuméro
			// 
			this.tbNuméro.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.SetColumnSpan(this.tbNuméro, 3);
			this.tbNuméro.Location = new System.Drawing.Point(165, 113);
			this.tbNuméro.Margin = new System.Windows.Forms.Padding(3, 4, 18, 4);
			this.tbNuméro.MaxLength = 150;
			this.tbNuméro.Name = "tbNuméro";
			this.tbNuméro.Size = new System.Drawing.Size(548, 24);
			this.tbNuméro.TabIndex = 7;
			// 
			// rmBtnSupprimer
			// 
			this.rmBtnSupprimer.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.rmBtnSupprimer.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
			this.rmBtnSupprimer.BackColor = System.Drawing.Color.Transparent;
			this.rmBtnSupprimer.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
			this.rmBtnSupprimer.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
			this.rmBtnSupprimer.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
			this.rmBtnSupprimer.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
			this.rmBtnSupprimer.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.rmBtnSupprimer.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.rmBtnSupprimer.FadingSpeed = 35;
			this.rmBtnSupprimer.FlatAppearance.BorderSize = 0;
			this.rmBtnSupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.rmBtnSupprimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rmBtnSupprimer.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
			this.rmBtnSupprimer.Image = global::MaCompta.Properties.Resources.user_trash_full_64;
			this.rmBtnSupprimer.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Left;
			this.rmBtnSupprimer.ImageOffset = 1;
			this.rmBtnSupprimer.IsPressed = false;
			this.rmBtnSupprimer.KeepPress = false;
			this.rmBtnSupprimer.Location = new System.Drawing.Point(3, 584);
			this.rmBtnSupprimer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.rmBtnSupprimer.MaxImageSize = new System.Drawing.Point(50, 50);
			this.rmBtnSupprimer.MenuPos = new System.Drawing.Point(0, 0);
			this.rmBtnSupprimer.Name = "rmBtnSupprimer";
			this.rmBtnSupprimer.Radius = 25;
			this.rmBtnSupprimer.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
			this.rmBtnSupprimer.Size = new System.Drawing.Size(155, 49);
			this.rmBtnSupprimer.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
			this.rmBtnSupprimer.SplitDistance = 0;
			this.rmBtnSupprimer.TabIndex = 21;
			this.rmBtnSupprimer.Text = "Supprimer";
			this.rmBtnSupprimer.Title = "";
			this.rmBtnSupprimer.UseVisualStyleBackColor = false;
			this.rmBtnSupprimer.Click += new System.EventHandler(this.RmBtnSupprimerClick);
			// 
			// rmBtnEnregistrer
			// 
			this.rmBtnEnregistrer.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.rmBtnEnregistrer.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
			this.rmBtnEnregistrer.BackColor = System.Drawing.Color.Transparent;
			this.rmBtnEnregistrer.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
			this.rmBtnEnregistrer.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
			this.rmBtnEnregistrer.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
			this.rmBtnEnregistrer.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
			this.rmBtnEnregistrer.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.rmBtnEnregistrer.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.rmBtnEnregistrer.FadingSpeed = 35;
			this.rmBtnEnregistrer.FlatAppearance.BorderSize = 0;
			this.rmBtnEnregistrer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.rmBtnEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rmBtnEnregistrer.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
			this.rmBtnEnregistrer.Image = global::MaCompta.Properties.Resources.dialog_ok_2_64;
			this.rmBtnEnregistrer.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Left;
			this.rmBtnEnregistrer.ImageOffset = 0;
			this.rmBtnEnregistrer.IsPressed = false;
			this.rmBtnEnregistrer.KeepPress = false;
			this.rmBtnEnregistrer.Location = new System.Drawing.Point(557, 584);
			this.rmBtnEnregistrer.Margin = new System.Windows.Forms.Padding(3, 4, 6, 4);
			this.rmBtnEnregistrer.MaxImageSize = new System.Drawing.Point(50, 50);
			this.rmBtnEnregistrer.MenuPos = new System.Drawing.Point(0, 0);
			this.rmBtnEnregistrer.Name = "rmBtnEnregistrer";
			this.rmBtnEnregistrer.Radius = 25;
			this.rmBtnEnregistrer.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
			this.rmBtnEnregistrer.Size = new System.Drawing.Size(160, 49);
			this.rmBtnEnregistrer.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
			this.rmBtnEnregistrer.SplitDistance = 0;
			this.rmBtnEnregistrer.TabIndex = 19;
			this.rmBtnEnregistrer.Text = "Enregistrer";
			this.rmBtnEnregistrer.Title = "";
			this.rmBtnEnregistrer.UseVisualStyleBackColor = false;
			this.rmBtnEnregistrer.Click += new System.EventHandler(this.RmBtnEnregistrerClick);
			// 
			// rmBtnRecommencer
			// 
			this.rmBtnRecommencer.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.rmBtnRecommencer.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
			this.rmBtnRecommencer.BackColor = System.Drawing.Color.Transparent;
			this.rmBtnRecommencer.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
			this.rmBtnRecommencer.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
			this.rmBtnRecommencer.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
			this.rmBtnRecommencer.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
			this.rmBtnRecommencer.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.rmBtnRecommencer.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.tableLayoutPanel.SetColumnSpan(this.rmBtnRecommencer, 2);
			this.rmBtnRecommencer.FadingSpeed = 35;
			this.rmBtnRecommencer.FlatAppearance.BorderSize = 0;
			this.rmBtnRecommencer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.rmBtnRecommencer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rmBtnRecommencer.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
			this.rmBtnRecommencer.Image = global::MaCompta.Properties.Resources.edit_clear_2_64;
			this.rmBtnRecommencer.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Left;
			this.rmBtnRecommencer.ImageOffset = 0;
			this.rmBtnRecommencer.IsPressed = false;
			this.rmBtnRecommencer.KeepPress = false;
			this.rmBtnRecommencer.Location = new System.Drawing.Point(270, 584);
			this.rmBtnRecommencer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.rmBtnRecommencer.MaxImageSize = new System.Drawing.Point(50, 50);
			this.rmBtnRecommencer.MenuPos = new System.Drawing.Point(0, 0);
			this.rmBtnRecommencer.Name = "rmBtnRecommencer";
			this.rmBtnRecommencer.Radius = 25;
			this.rmBtnRecommencer.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
			this.rmBtnRecommencer.Size = new System.Drawing.Size(169, 49);
			this.rmBtnRecommencer.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
			this.rmBtnRecommencer.SplitDistance = 0;
			this.rmBtnRecommencer.TabIndex = 20;
			this.rmBtnRecommencer.Text = "Recommencer";
			this.rmBtnRecommencer.Title = "";
			this.rmBtnRecommencer.UseVisualStyleBackColor = false;
			this.rmBtnRecommencer.Click += new System.EventHandler(this.RmBtnRecommencerClick);
			// 
			// lbNom
			// 
			this.lbNom.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbNom.Location = new System.Drawing.Point(3, 50);
			this.lbNom.Name = "lbNom";
			this.lbNom.Size = new System.Drawing.Size(156, 50);
			this.lbNom.TabIndex = 4;
			this.lbNom.Text = "Nom / Raison sociale";
			this.lbNom.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbNuméro
			// 
			this.lbNuméro.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbNuméro.Location = new System.Drawing.Point(3, 100);
			this.lbNuméro.Name = "lbNuméro";
			this.lbNuméro.Size = new System.Drawing.Size(156, 50);
			this.lbNuméro.TabIndex = 6;
			this.lbNuméro.Text = "Numéro";
			this.lbNuméro.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbInstitution
			// 
			this.lbInstitution.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbInstitution.Location = new System.Drawing.Point(3, 150);
			this.lbInstitution.Name = "lbInstitution";
			this.lbInstitution.Size = new System.Drawing.Size(156, 50);
			this.lbInstitution.TabIndex = 8;
			this.lbInstitution.Text = "Institution";
			this.lbInstitution.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbAgence
			// 
			this.lbAgence.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbAgence.Location = new System.Drawing.Point(3, 200);
			this.lbAgence.Name = "lbAgence";
			this.lbAgence.Size = new System.Drawing.Size(156, 50);
			this.lbAgence.TabIndex = 10;
			this.lbAgence.Text = "Agence";
			this.lbAgence.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbSolde
			// 
			this.tableLayoutPanel.SetColumnSpan(this.lbSolde, 2);
			this.lbSolde.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbSolde.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbSolde.Location = new System.Drawing.Point(3, 500);
			this.lbSolde.Name = "lbSolde";
			this.lbSolde.Size = new System.Drawing.Size(358, 75);
			this.lbSolde.TabIndex = 17;
			this.lbSolde.Text = "Solde";
			this.lbSolde.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbNom
			// 
			this.tbNom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.SetColumnSpan(this.tbNom, 3);
			this.tbNom.Location = new System.Drawing.Point(165, 63);
			this.tbNom.Margin = new System.Windows.Forms.Padding(3, 4, 18, 4);
			this.tbNom.MaxLength = 150;
			this.tbNom.Name = "tbNom";
			this.tbNom.Size = new System.Drawing.Size(548, 24);
			this.tbNom.TabIndex = 5;
			// 
			// ckAfficher
			// 
			this.ckAfficher.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ckAfficher.Checked = true;
			this.ckAfficher.CheckState = System.Windows.Forms.CheckState.Checked;
			this.tableLayoutPanel.SetColumnSpan(this.ckAfficher, 2);
			this.ckAfficher.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ckAfficher.Location = new System.Drawing.Point(3, 404);
			this.ckAfficher.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.ckAfficher.Name = "ckAfficher";
			this.ckAfficher.Size = new System.Drawing.Size(358, 92);
			this.ckAfficher.TabIndex = 16;
			this.ckAfficher.Text = "Afficher ce compte dans les différentes listes";
			this.ckAfficher.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ckAfficher.UseVisualStyleBackColor = true;
			// 
			// apSolde
			// 
			this.apSolde.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.apSolde.BackColor = System.Drawing.Color.Transparent;
			this.apSolde.BorderColor = System.Drawing.Color.Transparent;
			this.apSolde.BorderWidth = 2;
			this.tableLayoutPanel.SetColumnSpan(this.apSolde, 2);
			this.apSolde.Controls.Add(this.ddcSolde);
			this.apSolde.Controls.Add(this.lbSymbolMonétaire2);
			this.apSolde.GradientEndColor = System.Drawing.Color.DimGray;
			this.apSolde.GradientStartColor = System.Drawing.Color.Black;
			this.apSolde.Image = null;
			this.apSolde.ImageLocation = new System.Drawing.Point(4, 4);
			this.apSolde.Location = new System.Drawing.Point(399, 507);
			this.apSolde.Margin = new System.Windows.Forms.Padding(3, 4, 0, 4);
			this.apSolde.Name = "apSolde";
			this.apSolde.RoundCornerRadius = 29;
			this.apSolde.Size = new System.Drawing.Size(299, 60);
			this.apSolde.TabIndex = 18;
			// 
			// ddcSolde
			// 
			this.ddcSolde.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Left)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.ddcSolde.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ddcSolde.BackColor = System.Drawing.Color.Transparent;
			this.ddcSolde.DigitColor = System.Drawing.Color.GreenYellow;
			this.ddcSolde.DigitText = "0.00";
			this.ddcSolde.Location = new System.Drawing.Point(10, 5);
			this.ddcSolde.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.ddcSolde.Name = "ddcSolde";
			this.ddcSolde.Size = new System.Drawing.Size(241, 43);
			this.ddcSolde.TabIndex = 0;
			// 
			// lbSymbolMonétaire2
			// 
			this.lbSymbolMonétaire2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.lbSymbolMonétaire2.BackColor = System.Drawing.Color.Transparent;
			this.lbSymbolMonétaire2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
			this.lbSymbolMonétaire2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.lbSymbolMonétaire2.Location = new System.Drawing.Point(248, 5);
			this.lbSymbolMonétaire2.Name = "lbSymbolMonétaire2";
			this.lbSymbolMonétaire2.Size = new System.Drawing.Size(38, 23);
			this.lbSymbolMonétaire2.TabIndex = 1;
			this.lbSymbolMonétaire2.Text = "DH";
			// 
			// label4
			// 
			this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label4.Location = new System.Drawing.Point(3, 250);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(156, 50);
			this.label4.TabIndex = 12;
			this.label4.Text = "Budget Quotidien";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbBudgetQuotidien
			// 
			this.tbBudgetQuotidien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tbBudgetQuotidien.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tbBudgetQuotidien.Location = new System.Drawing.Point(165, 262);
			this.tbBudgetQuotidien.Margin = new System.Windows.Forms.Padding(3, 4, 18, 4);
			this.tbBudgetQuotidien.MaxLength = 150;
			this.tbBudgetQuotidien.Name = "tbBudgetQuotidien";
			this.tbBudgetQuotidien.Size = new System.Drawing.Size(181, 26);
			this.tbBudgetQuotidien.TabIndex = 13;
			this.tbBudgetQuotidien.Text = "0";
			this.tbBudgetQuotidien.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.tbBudgetQuotidien.TextChanged += new System.EventHandler(this.TbBudgetQuotidienTextChanged);
			// 
			// gbDifférence
			// 
			this.tableLayoutPanel.SetColumnSpan(this.gbDifférence, 2);
			this.gbDifférence.Controls.Add(this.label9);
			this.gbDifférence.Controls.Add(this.label6);
			this.gbDifférence.Controls.Add(this.a1Panel4);
			this.gbDifférence.Controls.Add(this.a1Panel2);
			this.gbDifférence.Controls.Add(this.label7);
			this.gbDifférence.Controls.Add(this.label3);
			this.gbDifférence.Controls.Add(this.a1Panel3);
			this.gbDifférence.Controls.Add(this.a1Panel1);
			this.gbDifférence.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gbDifférence.Location = new System.Drawing.Point(367, 254);
			this.gbDifférence.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.gbDifférence.Name = "gbDifférence";
			this.gbDifférence.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tableLayoutPanel.SetRowSpan(this.gbDifférence, 3);
			this.gbDifférence.Size = new System.Drawing.Size(361, 242);
			this.gbDifférence.TabIndex = 15;
			this.gbDifférence.TabStop = false;
			this.gbDifférence.Text = "Différences selon le Budget Quotidien";
			this.gbDifférence.Visible = false;
			// 
			// label9
			// 
			this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label9.BackColor = System.Drawing.Color.Transparent;
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.Location = new System.Drawing.Point(34, 131);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(126, 51);
			this.label9.TabIndex = 4;
			this.label9.Text = "Somme Allouée";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label6
			// 
			this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label6.BackColor = System.Drawing.Color.Transparent;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(34, 192);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(126, 51);
			this.label6.TabIndex = 6;
			this.label6.Text = "Différence Totale";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// a1Panel4
			// 
			this.a1Panel4.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.a1Panel4.BackColor = System.Drawing.Color.Transparent;
			this.a1Panel4.BorderColor = System.Drawing.Color.Transparent;
			this.a1Panel4.BorderWidth = 2;
			this.a1Panel4.Controls.Add(this.ddcSommeAllouée);
			this.a1Panel4.Controls.Add(this.label8);
			this.a1Panel4.GradientEndColor = System.Drawing.Color.DimGray;
			this.a1Panel4.GradientStartColor = System.Drawing.Color.Black;
			this.a1Panel4.Image = null;
			this.a1Panel4.ImageLocation = new System.Drawing.Point(4, 4);
			this.a1Panel4.Location = new System.Drawing.Point(167, 125);
			this.a1Panel4.Margin = new System.Windows.Forms.Padding(3, 4, 0, 4);
			this.a1Panel4.Name = "a1Panel4";
			this.a1Panel4.RoundCornerRadius = 29;
			this.a1Panel4.Size = new System.Drawing.Size(178, 56);
			this.a1Panel4.TabIndex = 5;
			// 
			// ddcSommeAllouée
			// 
			this.ddcSommeAllouée.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Left)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.ddcSommeAllouée.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ddcSommeAllouée.BackColor = System.Drawing.Color.Transparent;
			this.ddcSommeAllouée.DigitColor = System.Drawing.Color.GreenYellow;
			this.ddcSommeAllouée.DigitText = "0.00";
			this.ddcSommeAllouée.Location = new System.Drawing.Point(10, 4);
			this.ddcSommeAllouée.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.ddcSommeAllouée.Name = "ddcSommeAllouée";
			this.ddcSommeAllouée.Size = new System.Drawing.Size(120, 39);
			this.ddcSommeAllouée.TabIndex = 0;
			// 
			// label8
			// 
			this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.label8.BackColor = System.Drawing.Color.Transparent;
			this.label8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
			this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.label8.Location = new System.Drawing.Point(127, 5);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(38, 25);
			this.label8.TabIndex = 1;
			this.label8.Text = "DH";
			// 
			// a1Panel2
			// 
			this.a1Panel2.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.a1Panel2.BackColor = System.Drawing.Color.Transparent;
			this.a1Panel2.BorderColor = System.Drawing.Color.Transparent;
			this.a1Panel2.BorderWidth = 2;
			this.a1Panel2.Controls.Add(this.ddcDifférenceTotale);
			this.a1Panel2.Controls.Add(this.label5);
			this.a1Panel2.GradientEndColor = System.Drawing.Color.DimGray;
			this.a1Panel2.GradientStartColor = System.Drawing.Color.Black;
			this.a1Panel2.Image = null;
			this.a1Panel2.ImageLocation = new System.Drawing.Point(4, 4);
			this.a1Panel2.Location = new System.Drawing.Point(167, 187);
			this.a1Panel2.Margin = new System.Windows.Forms.Padding(3, 4, 0, 4);
			this.a1Panel2.Name = "a1Panel2";
			this.a1Panel2.RoundCornerRadius = 29;
			this.a1Panel2.Size = new System.Drawing.Size(178, 56);
			this.a1Panel2.TabIndex = 7;
			// 
			// ddcDifférenceTotale
			// 
			this.ddcDifférenceTotale.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Left)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.ddcDifférenceTotale.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ddcDifférenceTotale.BackColor = System.Drawing.Color.Transparent;
			this.ddcDifférenceTotale.DigitColor = System.Drawing.Color.GreenYellow;
			this.ddcDifférenceTotale.DigitText = "0.00";
			this.ddcDifférenceTotale.Location = new System.Drawing.Point(10, 4);
			this.ddcDifférenceTotale.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.ddcDifférenceTotale.Name = "ddcDifférenceTotale";
			this.ddcDifférenceTotale.Size = new System.Drawing.Size(120, 39);
			this.ddcDifférenceTotale.TabIndex = 0;
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.label5.BackColor = System.Drawing.Color.Transparent;
			this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
			this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.label5.Location = new System.Drawing.Point(127, 5);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(38, 25);
			this.label5.TabIndex = 1;
			this.label5.Text = "DH";
			// 
			// label7
			// 
			this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label7.BackColor = System.Drawing.Color.Transparent;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(34, 25);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(157, 51);
			this.label7.TabIndex = 0;
			this.label7.Text = "Nombre de jours";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label3
			// 
			this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label3.BackColor = System.Drawing.Color.Transparent;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(34, 76);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(157, 51);
			this.label3.TabIndex = 2;
			this.label3.Text = "Différence Journalière";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// a1Panel3
			// 
			this.a1Panel3.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.a1Panel3.BackColor = System.Drawing.Color.Transparent;
			this.a1Panel3.BorderColor = System.Drawing.Color.Transparent;
			this.a1Panel3.BorderWidth = 2;
			this.a1Panel3.Controls.Add(this.ddcNombreJours);
			this.a1Panel3.GradientEndColor = System.Drawing.Color.DimGray;
			this.a1Panel3.GradientStartColor = System.Drawing.Color.Black;
			this.a1Panel3.Image = null;
			this.a1Panel3.ImageLocation = new System.Drawing.Point(4, 4);
			this.a1Panel3.Location = new System.Drawing.Point(198, 20);
			this.a1Panel3.Margin = new System.Windows.Forms.Padding(3, 4, 0, 4);
			this.a1Panel3.Name = "a1Panel3";
			this.a1Panel3.RoundCornerRadius = 29;
			this.a1Panel3.Size = new System.Drawing.Size(147, 47);
			this.a1Panel3.TabIndex = 1;
			// 
			// ddcNombreJours
			// 
			this.ddcNombreJours.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Left)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.ddcNombreJours.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ddcNombreJours.BackColor = System.Drawing.Color.Transparent;
			this.ddcNombreJours.DigitColor = System.Drawing.Color.GreenYellow;
			this.ddcNombreJours.DigitText = "0";
			this.ddcNombreJours.Location = new System.Drawing.Point(10, 4);
			this.ddcNombreJours.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.ddcNombreJours.Name = "ddcNombreJours";
			this.ddcNombreJours.Size = new System.Drawing.Size(123, 29);
			this.ddcNombreJours.TabIndex = 0;
			// 
			// a1Panel1
			// 
			this.a1Panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.a1Panel1.BackColor = System.Drawing.Color.Transparent;
			this.a1Panel1.BorderColor = System.Drawing.Color.Transparent;
			this.a1Panel1.BorderWidth = 2;
			this.a1Panel1.Controls.Add(this.ddcDifférenceJournalière);
			this.a1Panel1.Controls.Add(this.label2);
			this.a1Panel1.GradientEndColor = System.Drawing.Color.DimGray;
			this.a1Panel1.GradientStartColor = System.Drawing.Color.Black;
			this.a1Panel1.Image = null;
			this.a1Panel1.ImageLocation = new System.Drawing.Point(4, 4);
			this.a1Panel1.Location = new System.Drawing.Point(198, 71);
			this.a1Panel1.Margin = new System.Windows.Forms.Padding(3, 4, 0, 4);
			this.a1Panel1.Name = "a1Panel1";
			this.a1Panel1.RoundCornerRadius = 29;
			this.a1Panel1.Size = new System.Drawing.Size(147, 47);
			this.a1Panel1.TabIndex = 3;
			// 
			// ddcDifférenceJournalière
			// 
			this.ddcDifférenceJournalière.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Left)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.ddcDifférenceJournalière.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ddcDifférenceJournalière.BackColor = System.Drawing.Color.Transparent;
			this.ddcDifférenceJournalière.DigitColor = System.Drawing.Color.GreenYellow;
			this.ddcDifférenceJournalière.DigitText = "0.00";
			this.ddcDifférenceJournalière.Location = new System.Drawing.Point(10, 4);
			this.ddcDifférenceJournalière.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.ddcDifférenceJournalière.Name = "ddcDifférenceJournalière";
			this.ddcDifférenceJournalière.Size = new System.Drawing.Size(89, 29);
			this.ddcDifférenceJournalière.TabIndex = 0;
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.BackColor = System.Drawing.Color.Transparent;
			this.label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
			this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.label2.Location = new System.Drawing.Point(96, 1);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(38, 24);
			this.label2.TabIndex = 1;
			this.label2.Text = "DH";
			// 
			// gbDurée
			// 
			this.gbDurée.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Left)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.SetColumnSpan(this.gbDurée, 2);
			this.gbDurée.Controls.Add(this.rbMoisSuivant);
			this.gbDurée.Controls.Add(this.rbMoisCourant);
			this.gbDurée.Location = new System.Drawing.Point(0, 300);
			this.gbDurée.Margin = new System.Windows.Forms.Padding(0);
			this.gbDurée.Name = "gbDurée";
			this.gbDurée.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.gbDurée.Size = new System.Drawing.Size(364, 100);
			this.gbDurée.TabIndex = 14;
			this.gbDurée.TabStop = false;
			this.gbDurée.Text = "Durée";
			this.gbDurée.Visible = false;
			// 
			// rbMoisSuivant
			// 
			this.rbMoisSuivant.Location = new System.Drawing.Point(179, 13);
			this.rbMoisSuivant.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.rbMoisSuivant.Name = "rbMoisSuivant";
			this.rbMoisSuivant.Size = new System.Drawing.Size(182, 47);
			this.rbMoisSuivant.TabIndex = 1;
			this.rbMoisSuivant.Text = "Reste du mois courant + mois suivant";
			this.rbMoisSuivant.UseVisualStyleBackColor = true;
			this.rbMoisSuivant.Click += new System.EventHandler(this.RbMoisSuivantClick);
			// 
			// rbMoisCourant
			// 
			this.rbMoisCourant.Checked = true;
			this.rbMoisCourant.Location = new System.Drawing.Point(7, 13);
			this.rbMoisCourant.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.rbMoisCourant.Name = "rbMoisCourant";
			this.rbMoisCourant.Size = new System.Drawing.Size(166, 47);
			this.rbMoisCourant.TabIndex = 0;
			this.rbMoisCourant.TabStop = true;
			this.rbMoisCourant.Text = "Reste du mois courant";
			this.rbMoisCourant.UseVisualStyleBackColor = true;
			this.rbMoisCourant.Click += new System.EventHandler(this.RbMoisCourantClick);
			// 
			// mtbNuméro
			// 
			this.mtbNuméro.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.mtbNuméro.Location = new System.Drawing.Point(550, 13);
			this.mtbNuméro.Mask = "0000";
			this.mtbNuméro.Name = "mtbNuméro";
			this.mtbNuméro.PromptChar = '#';
			this.mtbNuméro.Size = new System.Drawing.Size(50, 24);
			this.mtbNuméro.TabIndex = 3;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label10.Location = new System.Drawing.Point(367, 0);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(177, 50);
			this.label10.TabIndex = 2;
			this.label10.Text = "Numéro";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label11.Location = new System.Drawing.Point(3, 0);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(156, 50);
			this.label11.TabIndex = 0;
			this.label11.Text = "Type";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cbTypeCompte
			// 
			this.cbTypeCompte.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.cbTypeCompte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbTypeCompte.FormattingEnabled = true;
			this.cbTypeCompte.Location = new System.Drawing.Point(165, 14);
			this.cbTypeCompte.Name = "cbTypeCompte";
			this.cbTypeCompte.Size = new System.Drawing.Size(196, 26);
			this.cbTypeCompte.TabIndex = 1;
			this.cbTypeCompte.SelectedIndexChanged += new System.EventHandler(this.cbTypeCompte_SelectedIndexChanged);
			// 
			// epValidation
			// 
			this.epValidation.ContainerControl = this;
			// 
			// CompteForm
			// 
			this.AcceptButton = this.rmBtnEnregistrer;
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
			this.BackgroundImage = global::MaCompta.Properties.Resources.Atra_Dot___Olive_1920x1200;
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.ClientSize = new System.Drawing.Size(759, 714);
			this.Controls.Add(this.tableLayoutPanel);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.pcbListe);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = global::MaCompta.Properties.Resources.my_documents_32_1;
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "CompteForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Compte";
			this.tableLayoutPanel.ResumeLayout(false);
			this.tableLayoutPanel.PerformLayout();
			this.apSolde.ResumeLayout(false);
			this.gbDifférence.ResumeLayout(false);
			this.a1Panel4.ResumeLayout(false);
			this.a1Panel2.ResumeLayout(false);
			this.a1Panel3.ResumeLayout(false);
			this.a1Panel1.ResumeLayout(false);
			this.gbDurée.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.epValidation)).EndInit();
			this.ResumeLayout(false);
		}

		#endregion Private Methods

		#region Protected Methods

		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#endregion Protected Methods
	}
}