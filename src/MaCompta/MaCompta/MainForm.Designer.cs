﻿/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 15/11/2010
 * Time: 22:49
 * 
 * © 1428-1431 (2007-2010) All rights reserved to Mohamed Y. ElAmrani.
 */
namespace MaCompta
{
	partial class MainForm
	{
		#region " Fields "

		private My.Forms.Controls.Panels.A1Panel.A1Panel a1pContenuTransaction;
		private My.Forms.Controls.Panels.A1Panel.A1Panel apSoldeTotal;
		private My.Forms.Controls.Labels.Blured.BluredLabel blSoldeCaisse;
		private System.Windows.Forms.ComboBox cbPartenaire;
		private System.Windows.Forms.ComboBox cbProjet;
		private My.Forms.Controls.ListViews.XPTable.Models.ColumnModel cmContenuTransaction;

		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private My.Forms.Controls.Misc.DigitalDisplayControl ddcSoldeTotal;
		private System.Windows.Forms.DateTimePicker dtpDate;
		private System.Windows.Forms.ErrorProvider epValidation;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lbDate;
		private System.Windows.Forms.Label lbDescription;
		private System.Windows.Forms.Label lbDesignation;
		private System.Windows.Forms.Label lbDébit;
		private System.Windows.Forms.Label lbNuméroChèque;
		private System.Windows.Forms.Label lbPartenaire;
		private System.Windows.Forms.Label lbRubrique;
		private System.Windows.Forms.Label lbSymbolMonétaire;
		private My.Forms.Controls.ComboBoxes.PowerComboBox pcbCompte;
		private System.Windows.Forms.RibbonButton rBtnBilan;
		private System.Windows.Forms.RibbonButton rBtnCalculatrice; // End of Protected Method Dispose
		private System.Windows.Forms.RibbonButton rBtnComptes;
		private System.Windows.Forms.RibbonButton rBtnDétailsProjets;
		private System.Windows.Forms.RibbonButton rBtnListeOpérations; // End of Protected Method Dispose
		private System.Windows.Forms.RibbonButton rBtnListePartenaires; // End of Protected Method Dispose
		private System.Windows.Forms.RibbonButton rBtnMinimiserRibbon;
		private System.Windows.Forms.RibbonButton rBtnPartenaires;
		private System.Windows.Forms.RibbonButton rBtnPlanComptable;
		private System.Windows.Forms.RibbonButton rBtnProjet;
		private System.Windows.Forms.RibbonButton rBtnRechercherTransaction;
		private System.Windows.Forms.RibbonButton rbAuto;
		private System.Windows.Forms.RibbonColorChooser rccBleu;
		private System.Windows.Forms.RibbonColorChooser rccNoir;
		private System.Windows.Forms.Ribbon ribbon;
		private System.Windows.Forms.RibbonSeparator ribbonSeparator1; // End of Protected Method Dispose // End of Protected Method Dispose
		private System.Windows.Forms.RibbonSeparator ribbonSeparator3;
		private System.Windows.Forms.RibbonSeparator ribbonSeparator4;
		private System.Windows.Forms.RibbonSeparator ribbonSeparator5;
		private System.Windows.Forms.RibbonSeparator ribbonSeparator6; // End of Protected Method Dispose
		private System.Windows.Forms.RibbonOrbMenuItem rimoMinimiser;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnAjouter;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnEnregistrer;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnRecommencer;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnSupprimer;
		private System.Windows.Forms.RibbonOrbMenuItem romiConfiguration;
		private System.Windows.Forms.RibbonOrbMenuItem romiRestaurer;
		private System.Windows.Forms.RibbonOrbMenuItem romiSauvegarder;
		private System.Windows.Forms.RibbonOrbOptionButton rooBtnQuitter;
		private System.Windows.Forms.RibbonPanel rpCopiesSecours;
		private System.Windows.Forms.RibbonPanel rpModules;
		private System.Windows.Forms.RibbonPanel rpOutils;
		private System.Windows.Forms.RibbonPanel rpAfficher;
		private System.Windows.Forms.RibbonPanel rpThème;
		private System.Windows.Forms.RibbonTab rtAccueil;
		private System.Windows.Forms.RibbonTab rtAvancé;
		private System.Windows.Forms.TextBox tbCrédit;
		private System.Windows.Forms.TextBox tbDescription;
		private System.Windows.Forms.TextBox tbDescriptionTransaction;
		private System.Windows.Forms.TextBox tbDébit;
		private System.Windows.Forms.TextBox tbNuméroChèque; // End of Protected Method Dispose
		private My.Forms.Controls.ListViews.XPTable.Models.Table tblContenu;
		private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcCompte;
		private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcCrédit;
		private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcDescription;
		private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcDébit;
		private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcProjet;
		private My.Forms.Controls.ListViews.XPTable.Models.TableModel tmContenuTransaction;
		private System.Windows.Forms.ToolTip ttCompte; // End of Protected Method Dispose

		#endregion " Fields "

		#region " Private Methods "

		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ribbon = new System.Windows.Forms.Ribbon();
            this.romiConfiguration = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonSeparator3 = new System.Windows.Forms.RibbonSeparator();
            this.romiRestaurer = new System.Windows.Forms.RibbonOrbMenuItem();
            this.romiSauvegarder = new System.Windows.Forms.RibbonOrbMenuItem();
            this.ribbonSeparator4 = new System.Windows.Forms.RibbonSeparator();
            this.rimoMinimiser = new System.Windows.Forms.RibbonOrbMenuItem();
            this.rooBtnQuitter = new System.Windows.Forms.RibbonOrbOptionButton();
            this.rBtnMinimiserRibbon = new System.Windows.Forms.RibbonButton();
            this.rtAccueil = new System.Windows.Forms.RibbonTab();
            this.rpModules = new System.Windows.Forms.RibbonPanel();
            this.rBtnComptes = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator1 = new System.Windows.Forms.RibbonSeparator();
            this.rBtnPartenaires = new System.Windows.Forms.RibbonButton();
            this.rBtnProjet = new System.Windows.Forms.RibbonButton();
            this.rpAfficher = new System.Windows.Forms.RibbonPanel();
            this.rBtnRechercherTransaction = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator6 = new System.Windows.Forms.RibbonSeparator();
            this.rBtnPlanComptable = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator7 = new System.Windows.Forms.RibbonSeparator();
            this.rBtnGraphs = new System.Windows.Forms.RibbonButton();
            this.rpBilan = new System.Windows.Forms.RibbonPanel();
            this.rBtnBilan = new System.Windows.Forms.RibbonButton();
            this.rBtnListeOpérations = new System.Windows.Forms.RibbonButton();
            this.rBtnListePartenaires = new System.Windows.Forms.RibbonButton();
            this.rBtnDétailsProjets = new System.Windows.Forms.RibbonButton();
            this.ribbonSeparator2 = new System.Windows.Forms.RibbonSeparator();
            this.rbtnZakat = new System.Windows.Forms.RibbonButton();
            this.rpOutils = new System.Windows.Forms.RibbonPanel();
            this.rBtnCalculatrice = new System.Windows.Forms.RibbonButton();
            this.rtAvancé = new System.Windows.Forms.RibbonTab();
            this.rpThème = new System.Windows.Forms.RibbonPanel();
            this.rccNoir = new System.Windows.Forms.RibbonColorChooser();
            this.ribbonSeparator5 = new System.Windows.Forms.RibbonSeparator();
            this.rccBleu = new System.Windows.Forms.RibbonColorChooser();
            this.rpCopiesSecours = new System.Windows.Forms.RibbonPanel();
            this.rbAuto = new System.Windows.Forms.RibbonButton();
            this.rmBtnSupprimer = new My.Forms.Controls.Ribbon.RibbonMenuButton();
            this.lbDate = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.lbPartenaire = new System.Windows.Forms.Label();
            this.lbDesignation = new System.Windows.Forms.Label();
            this.cbPartenaire = new System.Windows.Forms.ComboBox();
            this.tbDescriptionTransaction = new System.Windows.Forms.TextBox();
            this.tblContenu = new My.Forms.Controls.ListViews.XPTable.Models.Table();
            this.cmContenuTransaction = new My.Forms.Controls.ListViews.XPTable.Models.ColumnModel();
            this.tcCompte = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcProjet = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcDescription = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcDébit = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcCrédit = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tmContenuTransaction = new My.Forms.Controls.ListViews.XPTable.Models.TableModel();
            this.a1pContenuTransaction = new My.Forms.Controls.Panels.A1Panel.A1Panel();
            this.pcbCompte = new My.Forms.Controls.ComboBoxes.PowerComboBox();
            this.rmBtnAjouter = new My.Forms.Controls.Ribbon.RibbonMenuButton();
            this.label2 = new System.Windows.Forms.Label();
            this.tbCrédit = new System.Windows.Forms.TextBox();
            this.lbRubrique = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbDébit = new System.Windows.Forms.TextBox();
            this.lbDébit = new System.Windows.Forms.Label();
            this.lbDescription = new System.Windows.Forms.Label();
            this.cbProjet = new System.Windows.Forms.ComboBox();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.blSoldeCaisse = new My.Forms.Controls.Labels.Blured.BluredLabel();
            this.apSoldeTotal = new My.Forms.Controls.Panels.A1Panel.A1Panel();
            this.ddcSoldeTotal = new My.Forms.Controls.Misc.DigitalDisplayControl();
            this.lbSymbolMonétaire = new System.Windows.Forms.Label();
            this.rmBtnEnregistrer = new My.Forms.Controls.Ribbon.RibbonMenuButton();
            this.rmBtnRecommencer = new My.Forms.Controls.Ribbon.RibbonMenuButton();
            this.epValidation = new System.Windows.Forms.ErrorProvider(this.components);
            this.ttCompte = new System.Windows.Forms.ToolTip(this.components);
            this.tbNuméroChèque = new System.Windows.Forms.TextBox();
            this.lbNuméroChèque = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tblContenu)).BeginInit();
            this.a1pContenuTransaction.SuspendLayout();
            this.apSoldeTotal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epValidation)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ribbon.Minimized = false;
            this.ribbon.Name = "ribbon";
            // 
            // 
            // 
            this.ribbon.OrbDropDown.Location = new System.Drawing.Point(0, 0);
            this.ribbon.OrbDropDown.MenuItems.Add(this.romiConfiguration);
            this.ribbon.OrbDropDown.MenuItems.Add(this.ribbonSeparator3);
            this.ribbon.OrbDropDown.MenuItems.Add(this.romiRestaurer);
            this.ribbon.OrbDropDown.MenuItems.Add(this.romiSauvegarder);
            this.ribbon.OrbDropDown.MenuItems.Add(this.ribbonSeparator4);
            this.ribbon.OrbDropDown.MenuItems.Add(this.rimoMinimiser);
            this.ribbon.OrbDropDown.Name = "";
            this.ribbon.OrbDropDown.NextPopup = null;
            this.ribbon.OrbDropDown.OptionItems.Add(this.rooBtnQuitter);
            this.ribbon.OrbDropDown.PreviousPopup = null;
            this.ribbon.OrbDropDown.Size = new System.Drawing.Size(527, 254);
            this.ribbon.OrbDropDown.TabIndex = 0;
            this.ribbon.OrbDropDown.ToolStripDropDown = null;
            this.ribbon.OrbImage = global::MaCompta.Properties.Resources.project_development_24;
            // 
            // 
            // 
            this.ribbon.QuickAcessToolbar.AltKey = null;
            this.ribbon.QuickAcessToolbar.Image = null;
            this.ribbon.QuickAcessToolbar.Items.Add(this.rBtnMinimiserRibbon);
            this.ribbon.QuickAcessToolbar.Tag = null;
            this.ribbon.QuickAcessToolbar.Text = null;
            this.ribbon.QuickAcessToolbar.ToolTip = null;
            this.ribbon.QuickAcessToolbar.ToolTipImage = null;
            this.ribbon.QuickAcessToolbar.ToolTipTitle = null;
            this.ribbon.Size = new System.Drawing.Size(1004, 138);
            this.ribbon.TabIndex = 0;
            this.ribbon.Tabs.Add(this.rtAccueil);
            this.ribbon.Tabs.Add(this.rtAvancé);
            this.ribbon.TabSpacing = 6;
            this.ribbon.Text = "ribbon";
            this.ribbon.DoubleClick += new System.EventHandler(this.RibbonDoubleClick);
            // 
            // romiConfiguration
            // 
            this.romiConfiguration.AltKey = null;
            this.romiConfiguration.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.romiConfiguration.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.romiConfiguration.Image = global::MaCompta.Properties.Resources.advanced_32;
            this.romiConfiguration.SmallImage = ((System.Drawing.Image)(resources.GetObject("romiConfiguration.SmallImage")));
            this.romiConfiguration.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.romiConfiguration.Tag = null;
            this.romiConfiguration.Text = "Configuration";
            this.romiConfiguration.ToolTip = null;
            this.romiConfiguration.ToolTipImage = null;
            this.romiConfiguration.ToolTipTitle = null;
            this.romiConfiguration.Click += new System.EventHandler(this.RomiConfigurationClick);
            // 
            // ribbonSeparator3
            // 
            this.ribbonSeparator3.AltKey = null;
            this.ribbonSeparator3.Image = null;
            this.ribbonSeparator3.Tag = null;
            this.ribbonSeparator3.Text = null;
            this.ribbonSeparator3.ToolTip = null;
            this.ribbonSeparator3.ToolTipImage = null;
            this.ribbonSeparator3.ToolTipTitle = null;
            // 
            // romiRestaurer
            // 
            this.romiRestaurer.AltKey = null;
            this.romiRestaurer.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.romiRestaurer.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.romiRestaurer.Enabled = false;
            this.romiRestaurer.Image = global::MaCompta.Properties.Resources.db_comit_32;
            this.romiRestaurer.SmallImage = ((System.Drawing.Image)(resources.GetObject("romiRestaurer.SmallImage")));
            this.romiRestaurer.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.romiRestaurer.Tag = null;
            this.romiRestaurer.Text = "Restaurer une sauvegarde";
            this.romiRestaurer.ToolTip = null;
            this.romiRestaurer.ToolTipImage = null;
            this.romiRestaurer.ToolTipTitle = null;
            // 
            // romiSauvegarder
            // 
            this.romiSauvegarder.AltKey = null;
            this.romiSauvegarder.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.romiSauvegarder.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.romiSauvegarder.Image = global::MaCompta.Properties.Resources.db_update_32;
            this.romiSauvegarder.SmallImage = ((System.Drawing.Image)(resources.GetObject("romiSauvegarder.SmallImage")));
            this.romiSauvegarder.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.romiSauvegarder.Tag = null;
            this.romiSauvegarder.Text = "Effectuer une sauvegarde";
            this.romiSauvegarder.ToolTip = null;
            this.romiSauvegarder.ToolTipImage = null;
            this.romiSauvegarder.ToolTipTitle = null;
            this.romiSauvegarder.Click += new System.EventHandler(this.RomiSauvegarderClick);
            // 
            // ribbonSeparator4
            // 
            this.ribbonSeparator4.AltKey = null;
            this.ribbonSeparator4.Image = null;
            this.ribbonSeparator4.Tag = null;
            this.ribbonSeparator4.Text = null;
            this.ribbonSeparator4.ToolTip = null;
            this.ribbonSeparator4.ToolTipImage = null;
            this.ribbonSeparator4.ToolTipTitle = null;
            // 
            // rimoMinimiser
            // 
            this.rimoMinimiser.AltKey = null;
            this.rimoMinimiser.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Left;
            this.rimoMinimiser.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rimoMinimiser.Image = global::MaCompta.Properties.Resources.arrow_up_double_3_20;
            this.rimoMinimiser.SmallImage = ((System.Drawing.Image)(resources.GetObject("rimoMinimiser.SmallImage")));
            this.rimoMinimiser.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rimoMinimiser.Tag = null;
            this.rimoMinimiser.Text = "Minimiser le menu";
            this.rimoMinimiser.ToolTip = null;
            this.rimoMinimiser.ToolTipImage = null;
            this.rimoMinimiser.ToolTipTitle = null;
            this.rimoMinimiser.Click += new System.EventHandler(this.RimoMinimiserClick);
            // 
            // rooBtnQuitter
            // 
            this.rooBtnQuitter.AltKey = null;
            this.rooBtnQuitter.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rooBtnQuitter.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rooBtnQuitter.Image = global::MaCompta.Properties.Resources.system_log_out_20;
            this.rooBtnQuitter.SmallImage = ((System.Drawing.Image)(resources.GetObject("rooBtnQuitter.SmallImage")));
            this.rooBtnQuitter.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rooBtnQuitter.Tag = null;
            this.rooBtnQuitter.Text = "Quitter";
            this.rooBtnQuitter.ToolTip = "Ferme toutes les fenêtres de l\'application";
            this.rooBtnQuitter.ToolTipImage = global::MaCompta.Properties.Resources.system_log_out_20;
            this.rooBtnQuitter.ToolTipTitle = "Quitter l\'application";
            this.rooBtnQuitter.Click += new System.EventHandler(this.RooBtnQuitterClick);
            // 
            // rBtnMinimiserRibbon
            // 
            this.rBtnMinimiserRibbon.AltKey = null;
            this.rBtnMinimiserRibbon.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rBtnMinimiserRibbon.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rBtnMinimiserRibbon.Image = ((System.Drawing.Image)(resources.GetObject("rBtnMinimiserRibbon.Image")));
            this.rBtnMinimiserRibbon.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Compact;
            this.rBtnMinimiserRibbon.SmallImage = global::MaCompta.Properties.Resources.arrow_up_double_3_20;
            this.rBtnMinimiserRibbon.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rBtnMinimiserRibbon.Tag = null;
            this.rBtnMinimiserRibbon.Text = "Minimiser le ribbon";
            this.rBtnMinimiserRibbon.ToolTip = null;
            this.rBtnMinimiserRibbon.ToolTipImage = null;
            this.rBtnMinimiserRibbon.ToolTipTitle = null;
            this.rBtnMinimiserRibbon.Click += new System.EventHandler(this.RBtnMinimiserRibbonClick);
            // 
            // rtAccueil
            // 
            this.rtAccueil.Panels.Add(this.rpModules);
            this.rtAccueil.Panels.Add(this.rpAfficher);
            this.rtAccueil.Panels.Add(this.rpBilan);
            this.rtAccueil.Panels.Add(this.rpOutils);
            this.rtAccueil.Tag = null;
            this.rtAccueil.Text = "Accueil";
            // 
            // rpModules
            // 
            this.rpModules.ButtonMoreVisible = false;
            this.rpModules.Items.Add(this.rBtnComptes);
            this.rpModules.Items.Add(this.ribbonSeparator1);
            this.rpModules.Items.Add(this.rBtnPartenaires);
            this.rpModules.Items.Add(this.rBtnProjet);
            this.rpModules.Tag = null;
            this.rpModules.Text = "Modules";
            // 
            // rBtnComptes
            // 
            this.rBtnComptes.AltKey = null;
            this.rBtnComptes.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rBtnComptes.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rBtnComptes.Image = global::MaCompta.Properties.Resources.my_documents_32;
            this.rBtnComptes.SmallImage = global::MaCompta.Properties.Resources.my_documents_24;
            this.rBtnComptes.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rBtnComptes.Tag = null;
            this.rBtnComptes.Text = "Comptes";
            this.rBtnComptes.ToolTip = null;
            this.rBtnComptes.ToolTipImage = null;
            this.rBtnComptes.ToolTipTitle = null;
            this.rBtnComptes.Click += new System.EventHandler(this.rBtnComptes_Click);
            // 
            // ribbonSeparator1
            // 
            this.ribbonSeparator1.AltKey = null;
            this.ribbonSeparator1.Image = null;
            this.ribbonSeparator1.Tag = null;
            this.ribbonSeparator1.Text = null;
            this.ribbonSeparator1.ToolTip = null;
            this.ribbonSeparator1.ToolTipImage = null;
            this.ribbonSeparator1.ToolTipTitle = null;
            // 
            // rBtnPartenaires
            // 
            this.rBtnPartenaires.AltKey = null;
            this.rBtnPartenaires.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rBtnPartenaires.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rBtnPartenaires.Image = global::MaCompta.Properties.Resources.contact_new_2_32;
            this.rBtnPartenaires.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.rBtnPartenaires.SmallImage = global::MaCompta.Properties.Resources.contact_new_2_24;
            this.rBtnPartenaires.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rBtnPartenaires.Tag = null;
            this.rBtnPartenaires.Text = "Partenaires";
            this.rBtnPartenaires.ToolTip = null;
            this.rBtnPartenaires.ToolTipImage = null;
            this.rBtnPartenaires.ToolTipTitle = null;
            this.rBtnPartenaires.Click += new System.EventHandler(this.rBtnPartenaires_Click);
            // 
            // rBtnProjet
            // 
            this.rBtnProjet.AltKey = null;
            this.rBtnProjet.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rBtnProjet.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rBtnProjet.Image = global::MaCompta.Properties.Resources.file_manager_New1_32;
            this.rBtnProjet.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.rBtnProjet.SmallImage = global::MaCompta.Properties.Resources.file_manager_New1_24;
            this.rBtnProjet.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rBtnProjet.Tag = null;
            this.rBtnProjet.Text = "Projet";
            this.rBtnProjet.ToolTip = null;
            this.rBtnProjet.ToolTipImage = null;
            this.rBtnProjet.ToolTipTitle = null;
            this.rBtnProjet.Click += new System.EventHandler(this.rBtnProjet_Click);
            // 
            // rpAfficher
            // 
            this.rpAfficher.ButtonMoreVisible = false;
            this.rpAfficher.Image = global::MaCompta.Properties.Resources.edit_find_7_32;
            this.rpAfficher.Items.Add(this.rBtnRechercherTransaction);
            this.rpAfficher.Items.Add(this.ribbonSeparator6);
            this.rpAfficher.Items.Add(this.rBtnPlanComptable);
            this.rpAfficher.Items.Add(this.ribbonSeparator7);
            this.rpAfficher.Items.Add(this.rBtnGraphs);
            this.rpAfficher.Tag = null;
            this.rpAfficher.Text = "Afficher";
            // 
            // rBtnRechercherTransaction
            // 
            this.rBtnRechercherTransaction.AltKey = null;
            this.rBtnRechercherTransaction.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rBtnRechercherTransaction.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rBtnRechercherTransaction.Image = global::MaCompta.Properties.Resources.edit_find_project_32;
            this.rBtnRechercherTransaction.SmallImage = global::MaCompta.Properties.Resources.edit_find_project_24;
            this.rBtnRechercherTransaction.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rBtnRechercherTransaction.Tag = null;
            this.rBtnRechercherTransaction.Text = "Rechercher";
            this.rBtnRechercherTransaction.ToolTip = null;
            this.rBtnRechercherTransaction.ToolTipImage = null;
            this.rBtnRechercherTransaction.ToolTipTitle = null;
            this.rBtnRechercherTransaction.Click += new System.EventHandler(this.rBtnRechercherTransaction_Click);
            // 
            // ribbonSeparator6
            // 
            this.ribbonSeparator6.AltKey = null;
            this.ribbonSeparator6.Image = null;
            this.ribbonSeparator6.Tag = null;
            this.ribbonSeparator6.Text = null;
            this.ribbonSeparator6.ToolTip = null;
            this.ribbonSeparator6.ToolTipImage = null;
            this.ribbonSeparator6.ToolTipTitle = null;
            // 
            // rBtnPlanComptable
            // 
            this.rBtnPlanComptable.AltKey = null;
            this.rBtnPlanComptable.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rBtnPlanComptable.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rBtnPlanComptable.Image = ((System.Drawing.Image)(resources.GetObject("rBtnPlanComptable.Image")));
            this.rBtnPlanComptable.SmallImage = ((System.Drawing.Image)(resources.GetObject("rBtnPlanComptable.SmallImage")));
            this.rBtnPlanComptable.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rBtnPlanComptable.Tag = null;
            this.rBtnPlanComptable.Text = "Plan Comptable";
            this.rBtnPlanComptable.ToolTip = null;
            this.rBtnPlanComptable.ToolTipImage = null;
            this.rBtnPlanComptable.ToolTipTitle = null;
            this.rBtnPlanComptable.Click += new System.EventHandler(this.rBtnPlanComptable_Click);
            // 
            // ribbonSeparator7
            // 
            this.ribbonSeparator7.AltKey = null;
            this.ribbonSeparator7.Image = null;
            this.ribbonSeparator7.Tag = null;
            this.ribbonSeparator7.Text = null;
            this.ribbonSeparator7.ToolTip = null;
            this.ribbonSeparator7.ToolTipImage = null;
            this.ribbonSeparator7.ToolTipTitle = null;
            // 
            // rBtnGraphs
            // 
            this.rBtnGraphs.AltKey = null;
            this.rBtnGraphs.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rBtnGraphs.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rBtnGraphs.Image = global::MaCompta.Properties.Resources.dictionnarie1_32;
            this.rBtnGraphs.SmallImage = global::MaCompta.Properties.Resources.dictionnarie1_24;
            this.rBtnGraphs.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rBtnGraphs.Tag = null;
            this.rBtnGraphs.Text = "Graphs";
            this.rBtnGraphs.ToolTip = null;
            this.rBtnGraphs.ToolTipImage = null;
            this.rBtnGraphs.ToolTipTitle = null;
            this.rBtnGraphs.Click += new System.EventHandler(this.rBtnGraphs_Click);
            // 
            // rpBilan
            // 
            this.rpBilan.ButtonMoreVisible = false;
            this.rpBilan.Items.Add(this.rBtnBilan);
            this.rpBilan.Items.Add(this.rBtnListeOpérations);
            this.rpBilan.Items.Add(this.rBtnListePartenaires);
            this.rpBilan.Items.Add(this.rBtnDétailsProjets);
            this.rpBilan.Items.Add(this.ribbonSeparator2);
            this.rpBilan.Items.Add(this.rbtnZakat);
            this.rpBilan.Tag = null;
            this.rpBilan.Text = "Bilan";
            // 
            // rBtnBilan
            // 
            this.rBtnBilan.AltKey = null;
            this.rBtnBilan.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rBtnBilan.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rBtnBilan.Image = global::MaCompta.Properties.Resources.document_open_8_32;
            this.rBtnBilan.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.rBtnBilan.SmallImage = global::MaCompta.Properties.Resources.document_open_8_24;
            this.rBtnBilan.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rBtnBilan.Tag = null;
            this.rBtnBilan.Text = "Général";
            this.rBtnBilan.ToolTip = null;
            this.rBtnBilan.ToolTipImage = null;
            this.rBtnBilan.ToolTipTitle = null;
            this.rBtnBilan.Click += new System.EventHandler(this.rBtnBilan_Click);
            // 
            // rBtnListeOpérations
            // 
            this.rBtnListeOpérations.AltKey = null;
            this.rBtnListeOpérations.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rBtnListeOpérations.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rBtnListeOpérations.Image = global::MaCompta.Properties.Resources.document_open_8_32;
            this.rBtnListeOpérations.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.rBtnListeOpérations.SmallImage = global::MaCompta.Properties.Resources.document_open_8_24;
            this.rBtnListeOpérations.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rBtnListeOpérations.Tag = null;
            this.rBtnListeOpérations.Text = "Opérations";
            this.rBtnListeOpérations.ToolTip = null;
            this.rBtnListeOpérations.ToolTipImage = null;
            this.rBtnListeOpérations.ToolTipTitle = null;
            this.rBtnListeOpérations.Click += new System.EventHandler(this.rBtnListeOpérations_Click);
            // 
            // rBtnListePartenaires
            // 
            this.rBtnListePartenaires.AltKey = null;
            this.rBtnListePartenaires.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rBtnListePartenaires.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rBtnListePartenaires.Image = global::MaCompta.Properties.Resources.contact_new_2_32;
            this.rBtnListePartenaires.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.rBtnListePartenaires.SmallImage = global::MaCompta.Properties.Resources.contact_new_2_24;
            this.rBtnListePartenaires.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rBtnListePartenaires.Tag = null;
            this.rBtnListePartenaires.Text = "Partenaires";
            this.rBtnListePartenaires.ToolTip = null;
            this.rBtnListePartenaires.ToolTipImage = null;
            this.rBtnListePartenaires.ToolTipTitle = null;
            this.rBtnListePartenaires.Click += new System.EventHandler(this.rBtnListePartenaires_Click);
            // 
            // rBtnDétailsProjets
            // 
            this.rBtnDétailsProjets.AltKey = null;
            this.rBtnDétailsProjets.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rBtnDétailsProjets.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rBtnDétailsProjets.Image = global::MaCompta.Properties.Resources.file_manager_New1_32;
            this.rBtnDétailsProjets.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium;
            this.rBtnDétailsProjets.SmallImage = global::MaCompta.Properties.Resources.file_manager_New1_24;
            this.rBtnDétailsProjets.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rBtnDétailsProjets.Tag = null;
            this.rBtnDétailsProjets.Text = "Projets";
            this.rBtnDétailsProjets.ToolTip = null;
            this.rBtnDétailsProjets.ToolTipImage = null;
            this.rBtnDétailsProjets.ToolTipTitle = null;
            this.rBtnDétailsProjets.Click += new System.EventHandler(this.rBtnDétailsProjets_Click);
            // 
            // ribbonSeparator2
            // 
            this.ribbonSeparator2.AltKey = null;
            this.ribbonSeparator2.Image = null;
            this.ribbonSeparator2.Tag = null;
            this.ribbonSeparator2.Text = null;
            this.ribbonSeparator2.ToolTip = null;
            this.ribbonSeparator2.ToolTipImage = null;
            this.ribbonSeparator2.ToolTipTitle = null;
            // 
            // rbtnZakat
            // 
            this.rbtnZakat.AltKey = null;
            this.rbtnZakat.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rbtnZakat.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rbtnZakat.Image = ((System.Drawing.Image)(resources.GetObject("rbtnZakat.Image")));
            this.rbtnZakat.SmallImage = ((System.Drawing.Image)(resources.GetObject("rbtnZakat.SmallImage")));
            this.rbtnZakat.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rbtnZakat.Tag = null;
            this.rbtnZakat.Text = "Zakat";
            this.rbtnZakat.ToolTip = null;
            this.rbtnZakat.ToolTipImage = null;
            this.rbtnZakat.ToolTipTitle = null;
            this.rbtnZakat.Click += new System.EventHandler(this.rbtnZakat_Click);
            // 
            // rpOutils
            // 
            this.rpOutils.ButtonMoreVisible = false;
            this.rpOutils.Items.Add(this.rBtnCalculatrice);
            this.rpOutils.Tag = null;
            this.rpOutils.Text = "Outils";
            // 
            // rBtnCalculatrice
            // 
            this.rBtnCalculatrice.AltKey = null;
            this.rBtnCalculatrice.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rBtnCalculatrice.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rBtnCalculatrice.Image = ((System.Drawing.Image)(resources.GetObject("rBtnCalculatrice.Image")));
            this.rBtnCalculatrice.SmallImage = ((System.Drawing.Image)(resources.GetObject("rBtnCalculatrice.SmallImage")));
            this.rBtnCalculatrice.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rBtnCalculatrice.Tag = null;
            this.rBtnCalculatrice.Text = null;
            this.rBtnCalculatrice.ToolTip = null;
            this.rBtnCalculatrice.ToolTipImage = null;
            this.rBtnCalculatrice.ToolTipTitle = null;
            this.rBtnCalculatrice.Click += new System.EventHandler(this.rBtnCalculatrice_Click);
            // 
            // rtAvancé
            // 
            this.rtAvancé.Panels.Add(this.rpThème);
            this.rtAvancé.Panels.Add(this.rpCopiesSecours);
            this.rtAvancé.Tag = null;
            this.rtAvancé.Text = "Avancé";
            // 
            // rpThème
            // 
            this.rpThème.ButtonMoreVisible = false;
            this.rpThème.Items.Add(this.rccNoir);
            this.rpThème.Items.Add(this.ribbonSeparator5);
            this.rpThème.Items.Add(this.rccBleu);
            this.rpThème.Tag = null;
            this.rpThème.Text = "Thème";
            // 
            // rccNoir
            // 
            this.rccNoir.AltKey = null;
            this.rccNoir.CheckOnClick = true;
            this.rccNoir.Color = System.Drawing.Color.Black;
            this.rccNoir.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rccNoir.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rccNoir.Image = ((System.Drawing.Image)(resources.GetObject("rccNoir.Image")));
            this.rccNoir.SmallImage = ((System.Drawing.Image)(resources.GetObject("rccNoir.SmallImage")));
            this.rccNoir.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rccNoir.Tag = null;
            this.rccNoir.Text = "Noir";
            this.rccNoir.ToolTip = null;
            this.rccNoir.ToolTipImage = null;
            this.rccNoir.ToolTipTitle = null;
            this.rccNoir.Click += new System.EventHandler(this.RccNoirClick);
            // 
            // ribbonSeparator5
            // 
            this.ribbonSeparator5.AltKey = null;
            this.ribbonSeparator5.Image = null;
            this.ribbonSeparator5.Tag = null;
            this.ribbonSeparator5.Text = null;
            this.ribbonSeparator5.ToolTip = null;
            this.ribbonSeparator5.ToolTipImage = null;
            this.ribbonSeparator5.ToolTipTitle = null;
            // 
            // rccBleu
            // 
            this.rccBleu.AltKey = null;
            this.rccBleu.Checked = true;
            this.rccBleu.CheckOnClick = true;
            this.rccBleu.Color = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.rccBleu.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rccBleu.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rccBleu.Image = ((System.Drawing.Image)(resources.GetObject("rccBleu.Image")));
            this.rccBleu.SmallImage = ((System.Drawing.Image)(resources.GetObject("rccBleu.SmallImage")));
            this.rccBleu.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rccBleu.Tag = null;
            this.rccBleu.Text = "Bleu";
            this.rccBleu.ToolTip = null;
            this.rccBleu.ToolTipImage = null;
            this.rccBleu.ToolTipTitle = null;
            this.rccBleu.Click += new System.EventHandler(this.RccBleuClick);
            // 
            // rpCopiesSecours
            // 
            this.rpCopiesSecours.Items.Add(this.rbAuto);
            this.rpCopiesSecours.Tag = null;
            this.rpCopiesSecours.Text = "Copies de Secours";
            this.rpCopiesSecours.ButtonMoreClick += new System.EventHandler(this.RpCopiesSecoursButtonMoreClick);
            // 
            // rbAuto
            // 
            this.rbAuto.AltKey = null;
            this.rbAuto.Checked = true;
            this.rbAuto.CheckOnClick = true;
            this.rbAuto.DropDownArrowDirection = System.Windows.Forms.RibbonArrowDirection.Down;
            this.rbAuto.DropDownArrowSize = new System.Drawing.Size(5, 3);
            this.rbAuto.Image = global::MaCompta.Properties.Resources.advanced_32;
            this.rbAuto.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large;
            this.rbAuto.SmallImage = global::MaCompta.Properties.Resources.advanced_32;
            this.rbAuto.Style = System.Windows.Forms.RibbonButtonStyle.Normal;
            this.rbAuto.Tag = null;
            this.rbAuto.Text = "Automatique";
            this.rbAuto.ToolTip = null;
            this.rbAuto.ToolTipImage = null;
            this.rbAuto.ToolTipTitle = null;
            this.rbAuto.Click += new System.EventHandler(this.RbAutoClick);
            // 
            // rmBtnSupprimer
            // 
            this.rmBtnSupprimer.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.rmBtnSupprimer.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
            this.rmBtnSupprimer.BackColor = System.Drawing.Color.Transparent;
            this.rmBtnSupprimer.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
            this.rmBtnSupprimer.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
            this.rmBtnSupprimer.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
            this.rmBtnSupprimer.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
            this.rmBtnSupprimer.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.rmBtnSupprimer.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.rmBtnSupprimer.FadingSpeed = 35;
            this.rmBtnSupprimer.FlatAppearance.BorderSize = 0;
            this.rmBtnSupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rmBtnSupprimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rmBtnSupprimer.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
            this.rmBtnSupprimer.Image = global::MaCompta.Properties.Resources.user_trash_full_64;
            this.rmBtnSupprimer.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Left;
            this.rmBtnSupprimer.ImageOffset = 1;
            this.rmBtnSupprimer.IsPressed = false;
            this.rmBtnSupprimer.KeepPress = false;
            this.rmBtnSupprimer.Location = new System.Drawing.Point(19, 579);
            this.rmBtnSupprimer.Margin = new System.Windows.Forms.Padding(0);
            this.rmBtnSupprimer.MaxImageSize = new System.Drawing.Point(50, 50);
            this.rmBtnSupprimer.MenuPos = new System.Drawing.Point(0, 0);
            this.rmBtnSupprimer.Name = "rmBtnSupprimer";
            this.rmBtnSupprimer.Radius = 11;
            this.rmBtnSupprimer.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
            this.rmBtnSupprimer.Size = new System.Drawing.Size(160, 49);
            this.rmBtnSupprimer.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
            this.rmBtnSupprimer.SplitDistance = 0;
            this.rmBtnSupprimer.TabIndex = 15;
            this.rmBtnSupprimer.Text = "Supprimer";
            this.rmBtnSupprimer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rmBtnSupprimer.Title = "";
            this.rmBtnSupprimer.UseVisualStyleBackColor = false;
            this.rmBtnSupprimer.Click += new System.EventHandler(this.RmBtnSupprimerClick);
            // 
            // lbDate
            // 
            this.lbDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbDate.BackColor = System.Drawing.Color.Transparent;
            this.lbDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDate.Location = new System.Drawing.Point(10, 144);
            this.lbDate.Name = "lbDate";
            this.lbDate.Size = new System.Drawing.Size(106, 24);
            this.lbDate.TabIndex = 1;
            this.lbDate.Text = "Date";
            this.lbDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpDate
            // 
            this.dtpDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpDate.Location = new System.Drawing.Point(122, 141);
            this.dtpDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(246, 24);
            this.dtpDate.TabIndex = 2;
            this.dtpDate.Leave += new System.EventHandler(this.dtpDate_Leave);
            // 
            // lbPartenaire
            // 
            this.lbPartenaire.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbPartenaire.BackColor = System.Drawing.Color.Transparent;
            this.lbPartenaire.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPartenaire.Location = new System.Drawing.Point(10, 171);
            this.lbPartenaire.Name = "lbPartenaire";
            this.lbPartenaire.Size = new System.Drawing.Size(106, 24);
            this.lbPartenaire.TabIndex = 3;
            this.lbPartenaire.Text = "Partenaire";
            this.lbPartenaire.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbDesignation
            // 
            this.lbDesignation.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbDesignation.BackColor = System.Drawing.Color.Transparent;
            this.lbDesignation.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDesignation.Location = new System.Drawing.Point(10, 202);
            this.lbDesignation.Name = "lbDesignation";
            this.lbDesignation.Size = new System.Drawing.Size(106, 24);
            this.lbDesignation.TabIndex = 5;
            this.lbDesignation.Text = "Designation";
            this.lbDesignation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbPartenaire
            // 
            this.cbPartenaire.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbPartenaire.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbPartenaire.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbPartenaire.FormattingEnabled = true;
            this.cbPartenaire.Location = new System.Drawing.Point(122, 170);
            this.cbPartenaire.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbPartenaire.MaxLength = 150;
            this.cbPartenaire.Name = "cbPartenaire";
            this.cbPartenaire.Size = new System.Drawing.Size(328, 26);
            this.cbPartenaire.Sorted = true;
            this.cbPartenaire.TabIndex = 4;
            this.cbPartenaire.Enter += new System.EventHandler(this.CbPartenaireEnter);
            this.cbPartenaire.Leave += new System.EventHandler(this.CbPartenaireLeave);
            // 
            // tbDescriptionTransaction
            // 
            this.tbDescriptionTransaction.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tbDescriptionTransaction.Location = new System.Drawing.Point(122, 201);
            this.tbDescriptionTransaction.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbDescriptionTransaction.MaxLength = 250;
            this.tbDescriptionTransaction.Name = "tbDescriptionTransaction";
            this.tbDescriptionTransaction.Size = new System.Drawing.Size(328, 24);
            this.tbDescriptionTransaction.TabIndex = 6;
            // 
            // tblContenu
            // 
            this.tblContenu.AlternatingRowColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tblContenu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tblContenu.ColumnModel = this.cmContenuTransaction;
            this.tblContenu.CustomEditKey = System.Windows.Forms.Keys.None;
            this.tblContenu.EditStartAction = My.Forms.Controls.ListViews.XPTable.Editors.EditStartAction.SingleClick;
            this.tblContenu.EnableHeaderContextMenu = false;
            this.tblContenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.tblContenu.FullRowSelect = true;
            this.tblContenu.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tblContenu.GridLines = My.Forms.Controls.ListViews.XPTable.Models.GridLines.Columns;
            this.tblContenu.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.tblContenu.Location = new System.Drawing.Point(10, 345);
            this.tblContenu.Margin = new System.Windows.Forms.Padding(3, 4, 17, 4);
            this.tblContenu.Name = "tblContenu";
            this.tblContenu.NoItemsText = "Aucune opération!";
            this.tblContenu.Size = new System.Drawing.Size(984, 230);
            this.tblContenu.TabIndex = 12;
            this.tblContenu.TableModel = this.tmContenuTransaction;
            this.tblContenu.Text = "table1";
            this.tblContenu.CellPropertyChanged += new My.Forms.Controls.ListViews.XPTable.Events.CellEventHandler(this.TblContenuCellPropertyChanged);
            this.tblContenu.DoubleClick += new System.EventHandler(this.TblContenuDoubleClick);
            this.tblContenu.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TblContenuTransactionKeyDown);
            // 
            // cmContenuTransaction
            // 
            this.cmContenuTransaction.AutoAdustWidth = true;
            this.cmContenuTransaction.Columns.AddRange(new My.Forms.Controls.ListViews.XPTable.Models.Column[] {
            this.tcCompte,
            this.tcProjet,
            this.tcDescription,
            this.tcDébit,
            this.tcCrédit});
            this.cmContenuTransaction.HeaderHeight = 29;
            // 
            // tcCompte
            // 
            this.tcCompte.Editable = false;
            this.tcCompte.Text = "Compte";
            this.tcCompte.Width = 334;
            this.tcCompte.WidthPercentage = 34D;
            // 
            // tcProjet
            // 
            this.tcProjet.Editable = false;
            this.tcProjet.Text = "Projet";
            this.tcProjet.Width = 196;
            this.tcProjet.WidthPercentage = 20D;
            // 
            // tcDescription
            // 
            this.tcDescription.Text = "Libellé opération";
            this.tcDescription.Width = 246;
            this.tcDescription.WidthPercentage = 25D;
            // 
            // tcDébit
            // 
            this.tcDébit.Alignment = My.Forms.Controls.ListViews.XPTable.Models.ColumnAlignment.Right;
            this.tcDébit.Text = "Débit";
            this.tcDébit.Width = 98;
            this.tcDébit.WidthPercentage = 10D;
            // 
            // tcCrédit
            // 
            this.tcCrédit.Alignment = My.Forms.Controls.ListViews.XPTable.Models.ColumnAlignment.Right;
            this.tcCrédit.Text = "Crédit";
            this.tcCrédit.Width = 98;
            this.tcCrédit.WidthPercentage = 10D;
            // 
            // tmContenuTransaction
            // 
            this.tmContenuTransaction.RowHeight = 25;
            // 
            // a1pContenuTransaction
            // 
            this.a1pContenuTransaction.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.a1pContenuTransaction.BackColor = System.Drawing.Color.Transparent;
            this.a1pContenuTransaction.BorderColor = System.Drawing.Color.Gray;
            this.a1pContenuTransaction.BorderWidth = 2;
            this.a1pContenuTransaction.Controls.Add(this.pcbCompte);
            this.a1pContenuTransaction.Controls.Add(this.rmBtnAjouter);
            this.a1pContenuTransaction.Controls.Add(this.label2);
            this.a1pContenuTransaction.Controls.Add(this.tbCrédit);
            this.a1pContenuTransaction.Controls.Add(this.lbRubrique);
            this.a1pContenuTransaction.Controls.Add(this.label1);
            this.a1pContenuTransaction.Controls.Add(this.tbDébit);
            this.a1pContenuTransaction.Controls.Add(this.lbDébit);
            this.a1pContenuTransaction.Controls.Add(this.lbDescription);
            this.a1pContenuTransaction.Controls.Add(this.cbProjet);
            this.a1pContenuTransaction.Controls.Add(this.tbDescription);
            this.a1pContenuTransaction.GradientEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.a1pContenuTransaction.GradientStartColor = System.Drawing.Color.White;
            this.a1pContenuTransaction.Image = null;
            this.a1pContenuTransaction.ImageLocation = new System.Drawing.Point(4, 4);
            this.a1pContenuTransaction.Location = new System.Drawing.Point(10, 267);
            this.a1pContenuTransaction.Margin = new System.Windows.Forms.Padding(3, 4, 3, 0);
            this.a1pContenuTransaction.Name = "a1pContenuTransaction";
            this.a1pContenuTransaction.RoundCornerRadius = 20;
            this.a1pContenuTransaction.ShadowOffSet = 7;
            this.a1pContenuTransaction.Size = new System.Drawing.Size(984, 78);
            this.a1pContenuTransaction.TabIndex = 11;
            // 
            // pcbCompte
            // 
            this.pcbCompte.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pcbCompte.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.pcbCompte.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.pcbCompte.CheckBoxes = false;
            this.pcbCompte.Checked = System.Windows.Forms.CheckState.Unchecked;
            this.pcbCompte.DividerFormat = "---";
            this.pcbCompte.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.pcbCompte.DropDownWidth = 500;
            this.pcbCompte.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pcbCompte.FormattingEnabled = true;
            this.pcbCompte.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.pcbCompte.GroupColor = System.Drawing.SystemColors.WindowText;
            this.pcbCompte.ItemSeparator1 = ',';
            this.pcbCompte.ItemSeparator2 = '&';
            this.pcbCompte.Location = new System.Drawing.Point(9, 27);
            this.pcbCompte.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pcbCompte.MaxLength = 150;
            this.pcbCompte.Name = "pcbCompte";
            this.pcbCompte.Size = new System.Drawing.Size(304, 23);
            this.pcbCompte.TabIndex = 1;
            this.pcbCompte.SelectedIndexChanged += new System.EventHandler(this.PcbCompteSelectedIndexChanged);
            this.pcbCompte.Enter += new System.EventHandler(this.PcbCompteEnter);
            this.pcbCompte.Leave += new System.EventHandler(this.PcbCompteLeave);
            // 
            // rmBtnAjouter
            // 
            this.rmBtnAjouter.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rmBtnAjouter.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
            this.rmBtnAjouter.BackColor = System.Drawing.Color.Transparent;
            this.rmBtnAjouter.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
            this.rmBtnAjouter.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
            this.rmBtnAjouter.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
            this.rmBtnAjouter.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
            this.rmBtnAjouter.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.rmBtnAjouter.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.rmBtnAjouter.FadingSpeed = 35;
            this.rmBtnAjouter.FlatAppearance.BorderSize = 0;
            this.rmBtnAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rmBtnAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rmBtnAjouter.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
            this.rmBtnAjouter.Image = global::MaCompta.Properties.Resources.arrow_down_24;
            this.rmBtnAjouter.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Right;
            this.rmBtnAjouter.ImageOffset = 5;
            this.rmBtnAjouter.IsPressed = false;
            this.rmBtnAjouter.KeepPress = false;
            this.rmBtnAjouter.Location = new System.Drawing.Point(885, 22);
            this.rmBtnAjouter.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rmBtnAjouter.MaxImageSize = new System.Drawing.Point(20, 20);
            this.rmBtnAjouter.MenuPos = new System.Drawing.Point(0, 0);
            this.rmBtnAjouter.Name = "rmBtnAjouter";
            this.rmBtnAjouter.Radius = 11;
            this.rmBtnAjouter.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
            this.rmBtnAjouter.Size = new System.Drawing.Size(85, 33);
            this.rmBtnAjouter.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
            this.rmBtnAjouter.SplitDistance = 0;
            this.rmBtnAjouter.TabIndex = 10;
            this.rmBtnAjouter.Text = "Ajouter";
            this.rmBtnAjouter.Title = "";
            this.rmBtnAjouter.UseVisualStyleBackColor = false;
            this.rmBtnAjouter.Click += new System.EventHandler(this.RmBtnAjouterClick);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(320, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(212, 19);
            this.label2.TabIndex = 2;
            this.label2.Text = "Projet";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // tbCrédit
            // 
            this.tbCrédit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbCrédit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCrédit.Location = new System.Drawing.Point(804, 28);
            this.tbCrédit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbCrédit.MaxLength = 25;
            this.tbCrédit.Name = "tbCrédit";
            this.tbCrédit.Size = new System.Drawing.Size(75, 22);
            this.tbCrédit.TabIndex = 9;
            this.tbCrédit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbCrédit.TextChanged += new System.EventHandler(this.TbCrédit_TextChanged);
            this.tbCrédit.Enter += new System.EventHandler(this.tbCrédit_Enter);
            // 
            // lbRubrique
            // 
            this.lbRubrique.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbRubrique.BackColor = System.Drawing.Color.Transparent;
            this.lbRubrique.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRubrique.Location = new System.Drawing.Point(9, 5);
            this.lbRubrique.Name = "lbRubrique";
            this.lbRubrique.Size = new System.Drawing.Size(305, 19);
            this.lbRubrique.TabIndex = 0;
            this.lbRubrique.Text = "Compte";
            this.lbRubrique.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(804, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 19);
            this.label1.TabIndex = 8;
            this.label1.Text = "Crédit";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // tbDébit
            // 
            this.tbDébit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbDébit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDébit.Location = new System.Drawing.Point(723, 28);
            this.tbDébit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbDébit.MaxLength = 25;
            this.tbDébit.Name = "tbDébit";
            this.tbDébit.Size = new System.Drawing.Size(75, 22);
            this.tbDébit.TabIndex = 7;
            this.tbDébit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbDébit.TextChanged += new System.EventHandler(this.TbDébit_TextChanged);
            this.tbDébit.Enter += new System.EventHandler(this.tbDébit_Enter);
            // 
            // lbDébit
            // 
            this.lbDébit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbDébit.BackColor = System.Drawing.Color.Transparent;
            this.lbDébit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDébit.Location = new System.Drawing.Point(723, 5);
            this.lbDébit.Name = "lbDébit";
            this.lbDébit.Size = new System.Drawing.Size(75, 19);
            this.lbDébit.TabIndex = 6;
            this.lbDébit.Text = "Débit";
            this.lbDébit.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // lbDescription
            // 
            this.lbDescription.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbDescription.BackColor = System.Drawing.Color.Transparent;
            this.lbDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDescription.Location = new System.Drawing.Point(538, 5);
            this.lbDescription.Name = "lbDescription";
            this.lbDescription.Size = new System.Drawing.Size(180, 19);
            this.lbDescription.TabIndex = 4;
            this.lbDescription.Text = "Libellé opération";
            this.lbDescription.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // cbProjet
            // 
            this.cbProjet.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbProjet.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbProjet.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbProjet.DropDownWidth = 412;
            this.cbProjet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbProjet.FormattingEnabled = true;
            this.cbProjet.Location = new System.Drawing.Point(319, 28);
            this.cbProjet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbProjet.MaxLength = 150;
            this.cbProjet.Name = "cbProjet";
            this.cbProjet.Size = new System.Drawing.Size(212, 24);
            this.cbProjet.Sorted = true;
            this.cbProjet.TabIndex = 3;
            this.cbProjet.Enter += new System.EventHandler(this.CbProjetEnter);
            this.cbProjet.Leave += new System.EventHandler(this.CbProjetLeave);
            // 
            // tbDescription
            // 
            this.tbDescription.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDescription.Location = new System.Drawing.Point(538, 28);
            this.tbDescription.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbDescription.MaxLength = 150;
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(179, 22);
            this.tbDescription.TabIndex = 5;
            // 
            // blSoldeCaisse
            // 
            this.blSoldeCaisse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.blSoldeCaisse.BackColor = System.Drawing.Color.Transparent;
            this.blSoldeCaisse.Blur = new System.Drawing.Size(3, 4);
            this.blSoldeCaisse.BlurColor = System.Drawing.Color.Black;
            this.blSoldeCaisse.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.blSoldeCaisse.Location = new System.Drawing.Point(456, 136);
            this.blSoldeCaisse.Name = "blSoldeCaisse";
            this.blSoldeCaisse.Size = new System.Drawing.Size(538, 28);
            this.blSoldeCaisse.TabIndex = 9;
            this.blSoldeCaisse.Text = "Solde au ";
            this.blSoldeCaisse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // apSoldeTotal
            // 
            this.apSoldeTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.apSoldeTotal.BackColor = System.Drawing.Color.Transparent;
            this.apSoldeTotal.BorderColor = System.Drawing.Color.Transparent;
            this.apSoldeTotal.BorderWidth = 2;
            this.apSoldeTotal.Controls.Add(this.ddcSoldeTotal);
            this.apSoldeTotal.Controls.Add(this.lbSymbolMonétaire);
            this.apSoldeTotal.GradientEndColor = System.Drawing.Color.DimGray;
            this.apSoldeTotal.GradientStartColor = System.Drawing.Color.Black;
            this.apSoldeTotal.Image = null;
            this.apSoldeTotal.ImageLocation = new System.Drawing.Point(4, 4);
            this.apSoldeTotal.Location = new System.Drawing.Point(456, 164);
            this.apSoldeTotal.Margin = new System.Windows.Forms.Padding(3, 4, 0, 4);
            this.apSoldeTotal.Name = "apSoldeTotal";
            this.apSoldeTotal.RoundCornerRadius = 29;
            this.apSoldeTotal.ShadowOffSet = 12;
            this.apSoldeTotal.Size = new System.Drawing.Size(538, 103);
            this.apSoldeTotal.TabIndex = 10;
            // 
            // ddcSoldeTotal
            // 
            this.ddcSoldeTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ddcSoldeTotal.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ddcSoldeTotal.BackColor = System.Drawing.Color.Transparent;
            this.ddcSoldeTotal.DigitColor = System.Drawing.Color.GreenYellow;
            this.ddcSoldeTotal.DigitText = "0.00";
            this.ddcSoldeTotal.Location = new System.Drawing.Point(11, 5);
            this.ddcSoldeTotal.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.ddcSoldeTotal.Name = "ddcSoldeTotal";
            this.ddcSoldeTotal.Size = new System.Drawing.Size(479, 75);
            this.ddcSoldeTotal.TabIndex = 0;
            // 
            // lbSymbolMonétaire
            // 
            this.lbSymbolMonétaire.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSymbolMonétaire.BackColor = System.Drawing.Color.Transparent;
            this.lbSymbolMonétaire.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lbSymbolMonétaire.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lbSymbolMonétaire.Location = new System.Drawing.Point(486, 11);
            this.lbSymbolMonétaire.Name = "lbSymbolMonétaire";
            this.lbSymbolMonétaire.Size = new System.Drawing.Size(38, 66);
            this.lbSymbolMonétaire.TabIndex = 1;
            this.lbSymbolMonétaire.Text = "DH";
            // 
            // rmBtnEnregistrer
            // 
            this.rmBtnEnregistrer.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.rmBtnEnregistrer.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
            this.rmBtnEnregistrer.ArrowSize = 2F;
            this.rmBtnEnregistrer.BackColor = System.Drawing.Color.Transparent;
            this.rmBtnEnregistrer.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
            this.rmBtnEnregistrer.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
            this.rmBtnEnregistrer.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
            this.rmBtnEnregistrer.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
            this.rmBtnEnregistrer.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.rmBtnEnregistrer.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.rmBtnEnregistrer.FadingSpeed = 35;
            this.rmBtnEnregistrer.FlatAppearance.BorderSize = 0;
            this.rmBtnEnregistrer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rmBtnEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rmBtnEnregistrer.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
            this.rmBtnEnregistrer.Image = global::MaCompta.Properties.Resources.dialog_ok_2_64;
            this.rmBtnEnregistrer.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Left;
            this.rmBtnEnregistrer.ImageOffset = 0;
            this.rmBtnEnregistrer.IsPressed = false;
            this.rmBtnEnregistrer.KeepPress = false;
            this.rmBtnEnregistrer.Location = new System.Drawing.Point(827, 579);
            this.rmBtnEnregistrer.Margin = new System.Windows.Forms.Padding(0);
            this.rmBtnEnregistrer.MaxImageSize = new System.Drawing.Point(50, 50);
            this.rmBtnEnregistrer.MenuPos = new System.Drawing.Point(0, 0);
            this.rmBtnEnregistrer.Name = "rmBtnEnregistrer";
            this.rmBtnEnregistrer.Radius = 11;
            this.rmBtnEnregistrer.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
            this.rmBtnEnregistrer.Size = new System.Drawing.Size(160, 49);
            this.rmBtnEnregistrer.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
            this.rmBtnEnregistrer.SplitDistance = 20;
            this.rmBtnEnregistrer.TabIndex = 13;
            this.rmBtnEnregistrer.Text = "Enregistrer";
            this.rmBtnEnregistrer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rmBtnEnregistrer.Title = "";
            this.rmBtnEnregistrer.UseVisualStyleBackColor = false;
            this.rmBtnEnregistrer.Click += new System.EventHandler(this.RmBtnEnregistrerClick);
            // 
            // rmBtnRecommencer
            // 
            this.rmBtnRecommencer.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.rmBtnRecommencer.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
            this.rmBtnRecommencer.BackColor = System.Drawing.Color.Transparent;
            this.rmBtnRecommencer.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
            this.rmBtnRecommencer.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
            this.rmBtnRecommencer.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
            this.rmBtnRecommencer.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
            this.rmBtnRecommencer.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.rmBtnRecommencer.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.rmBtnRecommencer.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.rmBtnRecommencer.FadingSpeed = 35;
            this.rmBtnRecommencer.FlatAppearance.BorderSize = 0;
            this.rmBtnRecommencer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rmBtnRecommencer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rmBtnRecommencer.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
            this.rmBtnRecommencer.Image = global::MaCompta.Properties.Resources.edit_clear_2_64;
            this.rmBtnRecommencer.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Left;
            this.rmBtnRecommencer.ImageOffset = 0;
            this.rmBtnRecommencer.IsPressed = false;
            this.rmBtnRecommencer.KeepPress = false;
            this.rmBtnRecommencer.Location = new System.Drawing.Point(422, 579);
            this.rmBtnRecommencer.Margin = new System.Windows.Forms.Padding(0);
            this.rmBtnRecommencer.MaxImageSize = new System.Drawing.Point(50, 50);
            this.rmBtnRecommencer.MenuPos = new System.Drawing.Point(0, 0);
            this.rmBtnRecommencer.Name = "rmBtnRecommencer";
            this.rmBtnRecommencer.Radius = 11;
            this.rmBtnRecommencer.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
            this.rmBtnRecommencer.Size = new System.Drawing.Size(160, 49);
            this.rmBtnRecommencer.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
            this.rmBtnRecommencer.SplitDistance = 0;
            this.rmBtnRecommencer.TabIndex = 14;
            this.rmBtnRecommencer.Text = "Recommencer";
            this.rmBtnRecommencer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rmBtnRecommencer.Title = "";
            this.rmBtnRecommencer.UseVisualStyleBackColor = false;
            this.rmBtnRecommencer.Click += new System.EventHandler(this.RmBtnRecommencerClick);
            // 
            // epValidation
            // 
            this.epValidation.ContainerControl = this;
            // 
            // ttCompte
            // 
            this.ttCompte.ShowAlways = true;
            this.ttCompte.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.ttCompte.ToolTipTitle = "Solde du Compte";
            // 
            // tbNuméroChèque
            // 
            this.tbNuméroChèque.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tbNuméroChèque.Location = new System.Drawing.Point(122, 232);
            this.tbNuméroChèque.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbNuméroChèque.MaxLength = 50;
            this.tbNuméroChèque.Name = "tbNuméroChèque";
            this.tbNuméroChèque.Size = new System.Drawing.Size(134, 24);
            this.tbNuméroChèque.TabIndex = 8;
            // 
            // lbNuméroChèque
            // 
            this.lbNuméroChèque.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbNuméroChèque.BackColor = System.Drawing.Color.Transparent;
            this.lbNuméroChèque.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNuméroChèque.Location = new System.Drawing.Point(10, 233);
            this.lbNuméroChèque.Name = "lbNuméroChèque";
            this.lbNuméroChèque.Size = new System.Drawing.Size(112, 24);
            this.lbNuméroChèque.TabIndex = 7;
            this.lbNuméroChèque.Text = "N° du Chèque";
            this.lbNuméroChèque.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MainForm
            // 
            this.AcceptButton = this.rmBtnAjouter;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CancelButton = this.rmBtnRecommencer;
            this.ClientSize = new System.Drawing.Size(1004, 643);
            this.Controls.Add(this.tblContenu);
            this.Controls.Add(this.rmBtnSupprimer);
            this.Controls.Add(this.lbNuméroChèque);
            this.Controls.Add(this.lbDesignation);
            this.Controls.Add(this.a1pContenuTransaction);
            this.Controls.Add(this.rmBtnRecommencer);
            this.Controls.Add(this.rmBtnEnregistrer);
            this.Controls.Add(this.ribbon);
            this.Controls.Add(this.tbNuméroChèque);
            this.Controls.Add(this.tbDescriptionTransaction);
            this.Controls.Add(this.lbPartenaire);
            this.Controls.Add(this.lbDate);
            this.Controls.Add(this.cbPartenaire);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.blSoldeCaisse);
            this.Controls.Add(this.apSoldeTotal);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MinimumSize = new System.Drawing.Size(1000, 674);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestion des Comptes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainFormFormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Leave += new System.EventHandler(this.MainFormLeave);
            ((System.ComponentModel.ISupportInitialize)(this.tblContenu)).EndInit();
            this.a1pContenuTransaction.ResumeLayout(false);
            this.a1pContenuTransaction.PerformLayout();
            this.apSoldeTotal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.epValidation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		} // End of Private Method InitializeComponent

		#endregion " Private Methods "

		#region " Protected Methods "

		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        private System.Windows.Forms.RibbonPanel rpBilan;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator2;
        private System.Windows.Forms.RibbonButton rbtnZakat;
        private System.Windows.Forms.RibbonSeparator ribbonSeparator7;
        private System.Windows.Forms.RibbonButton rBtnGraphs; // End of Protected Method Dispose

		#endregion " Protected Methods "
	} // End of None Type MainForm
}