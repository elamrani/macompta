﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 07/09/1433
 * Time: 18:31
 * 
 * © 1428-1433 (2007-2012) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using Controller;

using Model;

using My.Forms.MsgBoxes;

using System;
using System.Drawing;
using System.Windows.Forms;

namespace MaCompta
{
	/// <summary>
	/// Description of PartenaireForm.
	/// </summary>
	public partial class ProjetForm : Form
	{
		#region Fields

		long? _idChargé;

		private ProjetController _controller;

		#endregion Fields

		#region Constructors

		public ProjetForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();

			Initialisation();
		}

		#endregion Constructors

		#region Private Methods

		void CbListeSelectedIndexChanged(object sender, EventArgs e)
		{
			ChargerInformations(cbListe.Text, true);
		}

		void CbListeTextUpdate(object sender, EventArgs e)
		{
			ChargerInformations(cbListe.Text, false);
		}

		/// <summary>
		/// Charger les infos de la base de données
		/// </summary>
		/// <param name="nom">Nom du projet à charger</param>
		/// <param name="afficherErreur">Vrai pour afficher un message d'erreur lorsque
		/// le code ne permet pas de charger des infos,
		/// faux pour ne rien afficher</param>
		private void ChargerInformations(string nom, bool afficherErreur)
		{
			if ( string.IsNullOrEmpty(nom) ) {
				//Rien à charger
				return;
			}//if

			Model.Projet projet = new Model.Projet(Program.BD);
			Exception eValeurRetournée = projet.Charger(nom);
			if ( eValeurRetournée == null ) {
				_idChargé = projet.Id;
				tbNom.Text = projet.Nom;
			    if ( projet.DateDébut.HasValue )
			    {
			        dtpDateDébut.Checked = true;
			        dtpDateDébut.Value = projet.DateDébut.Value;
			    }//if
			    else
			    {
			        dtpDateDébut.Value = DateTime.Now;
			        dtpDateDébut.Checked = false;
			    }//else
			    if ( projet.DateFin.HasValue )
			    {
			        dtpDateFin.Checked = true;
			        dtpDateFin.Value = projet.DateFin.Value;
			    }//if
			    else
			    {
			        dtpDateFin.Value = DateTime.Now;
			        dtpDateFin.Checked = false;
			    }//else
			    ckAfficher.Checked = projet.Afficher;
			}//if
			else {
				//Une erreur est survenue
				MessageBox.Show("Une erreur est survenue lors du chargement des informations",
				                "Une erreur est survenue", MessageBoxButtons.OK,
				                MessageBoxIcon.Exclamation);
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( eValeurRetournée.Message );
				Recommencer(false, false);
			}//else
		}

		private void Enregistrer()
		{
			if ( VérifierInfos() ) {
				//Confirmation
				if ( MyMsgBox.Show("Confirmez-vous l'enregistrement de ces informations?",
				                   "Confirmation de l'enregistrement", MessageBoxButtons.YesNo,
				                   MessageBoxIcon.Question) != DialogResult.Yes
				   ) {
					return;
				}//if

				if ( _idChargé.HasValue ) {
					Modification();
				}//if
				else {
					//C'est un nouvel enregistrement à ajouter
					Nouveau();
				}//else
			}//if
		}

		/// <summary>
		/// Prépare l'IG
		/// </summary>
		private void Initialisation()
		{
			Exception eValeurRetournée = null;
			_controller = new ProjetController(Program.BD);

			#region " Chargement des projets "
			eValeurRetournée = _controller.ChargerListe(cbListe, "projet", "nom");
			if ( eValeurRetournée != null ) {
				MyMsgBox msgbox = new MyMsgBox();
				msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
				msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
				msgbox.MainInstruction = "Erreur lors de la tentative de chargement de la liste des projets";
				msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "La liste des projets n'a pas pu être chargée.";
				#if DEBUGr
				msgbox.ExpandedByDefault = true;
				#else
				msgbox.ExpandedByDefault = false;
				#endif
				msgbox.ExpandedInformation = eValeurRetournée.Message;
				msgbox.Show();
			}//if
			#endregion " Chargement des partenaires "

			_idChargé = null;
		}

		private void Modification()
		{
			try {
				Model.Projet projet = new Model.Projet(Program.BD);
				projet.Id = _idChargé.Value;
				projet.Nom = tbNom.Text.Trim();
			    if ( dtpDateDébut.Checked )
			    {
			        projet.DateDébut = dtpDateDébut.Value;
			    }//if
			    else
			    {
			        projet.DateDébut = null;
			    }//else
			    if ( dtpDateFin.Checked )
			    {
			        projet.DateFin = dtpDateFin.Value;
			    }//if
			    else
			    {
			        projet.DateFin = null;
			    }//else
			    projet.Afficher = ckAfficher.Checked;
			    Exception eValeurRetournée = projet.Modifier(projet.Id);
				if ( eValeurRetournée != null ) {
					throw eValeurRetournée;
				}//if
				else {
					Initialisation();
					Recommencer(true, false);
				}//else
			}//try
			catch ( Exception ex ) {
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( ex.Message );
				MyMsgBox msgbox = new MyMsgBox();
				msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
				msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
				msgbox.MainInstruction = "Erreur lors de la tentative " +
					"d'enregistrement du projet";
				msgbox.WindowTitle = "Erreur d'enregistrement des données";
				msgbox.Content = "Ce projet n'a pas pu être enregistré.";
				#if DEBUG
				msgbox.ExpandedByDefault = true;
				#else
				msgbox.ExpandedByDefault = false;
				#endif
				msgbox.ExpandedInformation = ex.Message;
				msgbox.Show();
			}//catch
		}

		private void Nouveau()
		{
			try {
				Model.Projet projet = new Model.Projet(Program.BD);
				projet.Nom = tbNom.Text.Trim();
			    if ( dtpDateDébut.Checked )
			    {
			        projet.DateDébut = dtpDateDébut.Value;
			    }//if
			    else
			    {
			        projet.DateDébut = null;
			    }//else
			    if ( dtpDateFin.Checked )
			    {
			        projet.DateFin = dtpDateFin.Value;
			    }//if
			    else
			    {
			        projet.DateFin = null;
			    }//else
			    projet.Afficher = ckAfficher.Checked;
				Exception eValeurRetournée = null;
				eValeurRetournée = projet.Ajouter();
				if ( eValeurRetournée != null ) {
					throw eValeurRetournée;
				}//if
				else {
					//Ajouter dans la liste
					cbListe.Items.Add(tbNom.Text);
					Recommencer(true, false);
				}//else
			}//try
			catch ( Exception ex ) {
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( ex.Message );
				MyMsgBox msgbox = new MyMsgBox();
				msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
				msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
				msgbox.MainInstruction = "Erreur lors de la tentative d'enregistrement du projet";
				msgbox.WindowTitle = "Erreur d'enregistrement des données";
				msgbox.Content = "Ce projet n'a pas pu être enregistré.";
				#if DEBUG
				msgbox.ExpandedByDefault = true;
				#else
				msgbox.ExpandedByDefault = false;
				#endif
				msgbox.ExpandedInformation = ex.Message;
				msgbox.Show();
			}//catch
		}

		/// <summary>
		/// Permet d'effacer tout le form
		/// </summary>
		/// <param name="effacerComboBox">true : effacer également le combobox;
		/// false: ne pas l'effacer (ne pas effacer la saisit de l'usager)</param>
		/// <param name="confirmer">Demander la confirmation?</param>
		private void Recommencer(bool effacerComboBox, bool confirmer)
		{
			if ( confirmer ) {
				//Confirmation
				if ( MyMsgBox.Show("Confirmez-vous l'effacement de toutes les informations " +
				                   "de cette fenêtre?",
				                   "Confirmation de l'effacement", MessageBoxButtons.YesNo,
				                   MessageBoxIcon.Question) != DialogResult.Yes
				   ) {
					return;
				}//
			}//if

			if ( effacerComboBox ) {
				cbListe.Text = string.Empty;
			}//if

			_idChargé = null;
			epValidation.Clear();

			tbNom.Text = string.Empty;
			dtpDateDébut.Value = DateTime.Now;
			dtpDateDébut.Checked = false;
			dtpDateFin.Value = DateTime.Now;
			dtpDateFin.Checked = false;
			ckAfficher.Checked = true;
		}

		void RmBtnEnregistrerClick(object sender, EventArgs e)
		{
			Enregistrer();
		}

		void RmBtnRecommencerClick(object sender, EventArgs e)
		{
			Recommencer(true, true);
		}

		void RmBtnSupprimerClick(object sender, EventArgs e)
		{
			Supprimer();
		}

		private void Supprimer()
		{
			//Vérifier si les infos ont été chargées
			if ( !_idChargé.HasValue )
				return; //ne rien faire

			//Demander la confirmation
			MyMsgBoxResult mrRésultat;
			MyMsgBox msgBox = new MyMsgBox();
			{
				msgBox.Owner = this;
				msgBox.Buttons = new MyMsgBoxButton[] { new MyMsgBoxButton(MyMsgBoxResult.Yes),
					new MyMsgBoxButton(MyMsgBoxResult.No) };
				msgBox.Content = "Cette suppression entraînera la suppression de toutes les informations associées à ce projet!";
				msgBox.MainIcon = MyMsgBoxIcon.SecurityQuestion;
				msgBox.MainInstruction = "Etes-vous sûr de toujours vouloir supprimer de manière permanente ce projet?";
				msgBox.WindowTitle = "Suppression du projet";
				msgBox.DefaultButton = MyMsgBoxDefaultButton.Button2;
				msgBox.FooterIcon = MyMsgBoxIcon.SecurityWarning;
				msgBox.FooterText = "Cette opération ne peut pas être annulée.";
				msgBox.LockSystem = true;
				mrRésultat = msgBox.Show();
			}
			if ( mrRésultat == MyMsgBoxResult.No ) {
				return;
			}//if

			try {
				//Suppression
				Model.Projet projet = new Model.Projet(Program.BD);
				projet.Id = _idChargé.Value;
				Exception eValeurRetournée = projet.Supprimer();
				if ( eValeurRetournée != null ) {
					throw eValeurRetournée;
				}//if
				else {
					Initialisation();
					Recommencer(true, false);
				}//else
			}//try
			catch ( Exception ex ) {
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( ex.Message );
				MyMsgBox msgbox = new MyMsgBox();
				msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
				msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
				msgbox.MainInstruction = "Erreur lors de la tentative " +
					"de suppression du projet";
				msgbox.WindowTitle = "Erreur de suppression des données";
				msgbox.Content = "Ce projet n'a pas pu être supprimé correctement.";
				#if DEBUG
				msgbox.ExpandedByDefault = true;
				#else
				msgbox.ExpandedByDefault = false;
				#endif
				msgbox.ExpandedInformation = ex.Message;
				msgbox.Show();
			}//catch
		}

		/// <summary>
		/// Permet de vérifier si toutes les données saisies sont correctes
		/// </summary>
		/// <returns>true : tout est bon, false : un problème est survenu</returns>
		private bool VérifierInfos()
		{
			bool bValeurRetournée = true;
			epValidation.Clear();

			if ( string.IsNullOrEmpty(tbNom.Text.Trim()) ) {
				epValidation.SetError( tbNom, "Le nom du projet ne peut pas être vide");
				bValeurRetournée = false;
			}//if

			return bValeurRetournée;
		}

		#endregion Private Methods
	}
}