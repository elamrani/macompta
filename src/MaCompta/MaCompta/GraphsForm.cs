﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Controller;

using My.Forms.MsgBoxes;

namespace MaCompta
{
	public partial class GraphesForm : Form
	{
		#region Fields

		private GraphesController _controller;

		#endregion Fields

		#region Constructors

		public GraphesForm()
		{
			InitializeComponent();
			Initialisation();
		}

		#endregion Constructors

		#region Private Methods

		/// <summary>
		/// Prépare l'IG
		/// </summary>
		private void Initialisation()
		{
			pcGroupes.Items.Clear();
			_controller = new GraphesController(Program.BD, Program.CultureIG);
			Exception eValeurRetournée = _controller.ChargerComptesGroupes(pcbListe, 5);
			if (eValeurRetournée != null)
			{
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement " +
			        "de la liste des comptes";
			    msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "La liste des comptes n'a pas pu être chargée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
				msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
		}

		private void pcbListe_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(pcbListe.Text))
			{
			    Exception eValeurRetournée = _controller.ChargerGraphe(pcGroupes, int.Parse(pcbListe.Text.Substring(0,4)));
			    if (eValeurRetournée != null)
			    {
			        MyMsgBox msgbox = new MyMsgBox();
			        msgbox.Buttons = new MyMsgBoxButton[] {
					    new MyMsgBoxButton(MyMsgBoxResult.OK)
				    };
			        msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			        msgbox.MainInstruction = "Erreur lors de la tentative de chargement " +
			            "des informations des comptes";
			        msgbox.WindowTitle = "Erreur d'extraction de données";
			        msgbox.Content = "Les informations des comptes n'ont pas pu être chargés.";
			#if DEBUG
			        msgbox.ExpandedByDefault = true;
			#else
				    msgbox.ExpandedByDefault = false;
			#endif
			        msgbox.ExpandedInformation = eValeurRetournée.Message;
			        msgbox.Show();
			    }//if
			}//if
		}

		#endregion Private Methods
	}
}