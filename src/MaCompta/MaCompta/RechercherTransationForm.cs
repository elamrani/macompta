﻿using Controller;

using Model;

using My.Forms.MsgBoxes;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MaCompta
{
	public partial class RechercherTransationForm : Form
	{
		#region Fields

		private RechercherTransactionController _controller;
		private Transaction _transaction;

		#endregion Fields

		#region Constructors

		public RechercherTransationForm()
		{
			InitializeComponent();
			Initialisations();
		}

		#endregion Constructors

		#region Public Properties

		public Transaction Transaction
		{
			get {
			    return _transaction;
			}
		}

		#endregion Public Properties

		#region Private Methods

		/// <summary>
		/// Permet d'afficher un message d'erreur générique
		/// </summary>
		/// <param name="eValeurRetournée">l'exception à afficher dans ce message.</param>
		private void AfficherMessageErreur(Exception eValeurRetournée)
		{
			MyMsgBox msgbox = new MyMsgBox();
			msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			msgbox.MainInstruction = "Erreur lors de la tentative de chargement d'informations à partir de la base de données.";
			msgbox.WindowTitle = "Erreur d'extraction de données";
			msgbox.Content = "La liste des données n'a pas pu être chargée.";
			#if DEBUG
			msgbox.ExpandedByDefault = true;
			#else
				msgbox.ExpandedByDefault = false;
			#endif
			msgbox.ExpandedInformation = eValeurRetournée.Message;
			msgbox.Show();
		}

		/// <summary>
		/// Permet de charger la transaction
		/// </summary>
		private void ChargerTransaction()
		{
			int index = tblListe.TableModel.Selections.SelectedIndicies[0];
			string sIdTransaction = tblListe.TableModel.Rows[index].Cells[0].Text;
			if ( !string.IsNullOrEmpty(sIdTransaction) )
			{
			    Exception eValeurRetournée = null;
			    _transaction = new Transaction(Program.BD);
			    eValeurRetournée = _transaction.Charger(long.Parse(sIdTransaction));
			    if ( eValeurRetournée != null )
			    {
			        AfficherMessageErreur(eValeurRetournée);
			        _transaction = null;
			    }//if
			    else
			    {
			        this.Close();
			    }//else
			}//if
		}

		/// <summary>
		/// Initialisations de l'IG
		/// </summary>
		private void Initialisations()
		{
			Exception eValeurRetournée = null;
			try
			{
			    _controller = new RechercherTransactionController(Program.BD, Program.CultureIG);
			    _transaction = null;
			    //Reculer d'un mois pour la date de début.
			    dtpDateDébut.Value = dtpDateDébut.Value.AddMonths(-1);

			    Dictionary<long, string> dic = new Dictionary<long, string>();
			    dic.Add(1, "Montant Inférieur à");
			    dic.Add(2, "Montant Egale à");
			    dic.Add(3, "Montant Supérieur à");
			    cbTypeMontant.DataSource = new BindingSource(dic, null);
			    cbTypeMontant.DisplayMember = "Value";
			    cbTypeMontant.ValueMember = "Key";

			    eValeurRetournée = _controller.ChargerListeValeurs(cbPartenaire, "partenaire", "id", "nom", "`afficher` = TRUE ORDER BY `nom`");
			    if ( eValeurRetournée != null )
			    {
			        throw eValeurRetournée;
			    }//if
			    else if ( cbPartenaire.Items.Count > 0 )
			    {
			        cbPartenaire.SelectedIndex = -1;
			    }//else if

			    eValeurRetournée = _controller.ChargerListeComptes(pcbCompte);
			    if ( eValeurRetournée != null )
			    {
			        throw eValeurRetournée;
			    }//if			    
			}//try
			catch ( Exception ex )
			{
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement d'informations à partir de la base de données.";
			    msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "La liste des données n'a pas pu être chargée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
				msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = ex.Message;
			    msgbox.Show();
			}//catch
		}

		/// <summary>
		/// Permet de peupler la liste avec les résultats de la recherche selon les paramètres spécifiés par l'utilisateur
		/// </summary>
		private void Rechercher()
		{
			string id_compte = string.Empty;
			if ( !string.IsNullOrEmpty(pcbCompte.Text) )
			{
			    id_compte = pcbCompte.Text.Substring(0, 4);
			}//if
			Exception eValeurRetournée = _controller.Rechercher(
			    tblListe, dtpDateDébut.Value, dtpDateFin.Value, cbPartenaire.Text, tbMontant.Text,
			    int.Parse(cbTypeMontant.SelectedValue.ToString()), id_compte);
			if ( eValeurRetournée != null )
			{
			    AfficherMessageErreur(eValeurRetournée);
			}//if
		}

		/// <summary>
		/// Vérifie si le montant saisi est valide ou non
		/// </summary>
		/// <returns>true = c bon, false sinon.</returns>
		private bool VérifierInformations()
		{
			bool bValeurRetour = true;
			if ( !string.IsNullOrEmpty(tbMontant.Text) )
			{
			    try
			    {
			        tbMontant.Text = double.Parse(tbMontant.Text).ToString("N2", Program.CultureIG);
			    }//try
			    catch
			    {
			        epValidation.SetError(tbMontant, "Le montant saisit est invalide!");
			        bValeurRetour = false;
			    }//catch
			}//if
			if ( !string.IsNullOrEmpty(pcbCompte.Text) )
			{
			    try
			    {
			        int.Parse(pcbCompte.Text.Substring(0, 4));
			    }//try
			    catch
			    {
			        epValidation.SetError(pcbCompte, "Le compte sélectionné est invalide!");
			        bValeurRetour = false;
			    }//catch
			}//if
			return bValeurRetour;
		}

		void rmBtnRechercher_Click(object sender, EventArgs e)
		{
			if ( VérifierInformations() )
			{
			    Rechercher();
			}//if
		}

		void tblListe_DoubleClick(object sender, EventArgs e)
		{
			if ( tblListe.TableModel.Selections.SelectedIndicies.Length != 0 )
			{
			    ChargerTransaction(); 
			}//if
		}

		#endregion Private Methods
	}
}