﻿using Controller;

using My.Forms.Controls.ListViews.XPTable.Models;
using My.Forms.MsgBoxes;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MaCompta
{
	public partial class ListeProjetsForm : Form
	{
		#region Fields

		private ListeProjetsController _controller;
		private DateTime? _date_début;
		private DateTime _date_fin;
		private string _nom_projet_a_charger; //Nom du projet

		#endregion Fields

		#region Constructors

		public ListeProjetsForm()
		{
			InitializeComponent();
			Initialisation();
			ChargerListe();
		}

		public ListeProjetsForm(string sNomProjetACharger, DateTime? dtDateDébut, DateTime dtDateFin)
		{
			InitializeComponent();
			Initialisation();
			_nom_projet_a_charger = sNomProjetACharger;
			_date_début = dtDateDébut;
			_date_fin = dtDateFin;
			ChargerListeDétaillée();
		}

		#endregion Constructors

		#region Private Methods

		/// <summary>
		/// Charger la liste des transactions d'un compte donné à une date donnée.
		/// </summary>
		private void ChargerListe()
		{
			Exception eValeurRetournée = _controller.ChargerListeProjets(tblListe);
			if ( eValeurRetournée != null )
			{
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement d'informations à partir de la base de données.";
			    msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "La liste des données n'a pas pu être chargée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
				msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
		}

		private void ChargerListeDétaillée()
		{
			Exception eValeurRetournée = _controller.ChargerListeTransactions(tblListe, _nom_projet_a_charger, _date_début, _date_fin);
			if ( eValeurRetournée != null )
			{
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement d'informations à partir de la base de données.";
			    msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "La liste des données n'a pas pu être chargée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
				msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
		}

		private void Initialisation()
		{
			_controller = new ListeProjetsController(Program.BD, Program.CultureIG);
			_nom_projet_a_charger = null;
			_date_début = null;
			_date_fin = DateTime.Now;
		}

		void ListeProjetsForm_Shown(object sender, EventArgs e)
		{
			WindowState = FormWindowState.Maximized;
		}

		void dtpDateDébut_MouseUp(object sender, MouseEventArgs e)
		{
			if ( dtpDateDébut.Checked )
			{
			    _date_début = dtpDateDébut.Value;
			}//if
			else
			{
			    _date_début = null;
			}//else
			ChargerListeDétaillée();
		}

		void dtpDateDébut_ValueChanged(object sender, EventArgs e)
		{
			if ( dtpDateDébut.Checked )
			{
			    _date_début = dtpDateDébut.Value;
			    ChargerListeDétaillée();
			}//if
		}

		void dtpDateFin_ValueChanged(object sender, EventArgs e)
		{
			_date_fin = dtpDateFin.Value;
			ChargerListeDétaillée();
		}

		void tblListe_DoubleClick(object sender, EventArgs e)
		{
			if ( tblListe.TableModel.Selections.SelectedItems.Length != 0 )
			{
			    Row ligne = tblListe.TableModel.Selections.SelectedItems[0];
			    if ( !string.IsNullOrEmpty(ligne.Cells[2].Text) ) //Nom du projet
			    {
			        if ( ligne.Cells[2].Text != _nom_projet_a_charger )
			        {
			            long lValeurIdCompte;
			            if ( long.TryParse(ligne.Cells[2].Text.Substring(0, 4), out lValeurIdCompte) )
			            {
			                //Détails de la transaction
			                new ListeTransactionsForm(ligne.Cells[2].Text.Substring(0, ligne.Cells[2].Text.IndexOf(", ", 0)), _date_début, _date_fin).Show();
			            }//if
			            else
			            {
			                //Détails du projet
			                new ListeProjetsForm(ligne.Cells[2].Text, _date_début, _date_fin).Show();
			            }//else
			        }//if
			    }//if
			}//if
		}

		#endregion Private Methods
	}
}