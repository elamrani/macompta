﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 15/11/2010
 * Time: 22:49
 * 
 * © 1428-1431 (2007-2010) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using Controller;

using My.Forms.Controls.ListViews.XPTable.Models;
using My.Forms.MsgBoxes;

using System;
using System.Windows.Forms;

namespace MaCompta
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		#region Fields

		private bool _activerEvènements;
		private Model.Configuration _configuration;
		private MainController _controller;
		private long? _idChargé;
		private bool _motPasseOK;
		private double _total_crédits;
		private double _total_débits;

		#endregion Fields

		#region Constructors

		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();

			Initialisation();
		}

		#endregion Constructors

		#region Public Properties

		public MainController Controller
		{
			get { return _controller; }
		}

		public bool MotPasseOK
		{
			get { return _motPasseOK; }
		}

		#endregion Public Properties

		#region Private Methods

		/// <summary>
		/// Permet d'ajouter un nouveau compte
		/// </summary>
		private void AjouterCompte()
		{
			//Confirmation
			MyMsgBox msgbox = new MyMsgBox();
			msgbox.WindowTitle = "Ajouter un nouveau compte?";
			UserControls.ucAjouterCompte ajouterCompte = new UserControls.ucAjouterCompte();
			if (pcbCompte.Text.Length > 4) {
			    try {
			        ajouterCompte.mtbId.Text = double.Parse(pcbCompte.Text.Substring(0, 4)).ToString();
			        ajouterCompte.tbNom.Text = pcbCompte.Text.Substring(5);
			    }//try
			    catch (Exception ex) {
			        System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine(ex.Message);
			    }//catch
			}//if
			else {
			    ajouterCompte.tbNom.Text = pcbCompte.Text;
			}//else
			msgbox.CustomControl = ajouterCompte;
			msgbox.MainIcon = MyMsgBoxIcon.Question;
			msgbox.MainInstruction = "Etes-vous sûr de vouloir ajouter ce nouveau compte ?";
			msgbox.Buttons = new MyMsgBoxButton[] {
				new MyMsgBoxButton(MyMsgBoxResult.Yes),
				new MyMsgBoxButton(MyMsgBoxResult.No)
			};
			if (msgbox.Show() != MyMsgBoxResult.Yes) {
			    //annuler tout
			    return;
			}//if

			//Enregistrer dans la BD
			Model.Compte compte = new Model.Compte(Program.BD);
			try {
			    compte.Id = long.Parse(ajouterCompte.mtbId.Text);
			}//try
			catch {
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative d'enregistrement du compte";
			    msgbox.WindowTitle = "Erreur d'enregistrement des données";
			    msgbox.Content = "Ce compte n'a pas pu être enregistré car le numéro saisi est incorrect.";
			    msgbox.Show();
			}//catch
			compte.Nom = ajouterCompte.tbNom.Text;
			compte.IdTypeCompte = (long)ajouterCompte.cbTypeCompte.SelectedValue;
			Exception eValeurRetournée = null;
			eValeurRetournée = compte.Ajouter();
			if (eValeurRetournée != null) {
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative d'enregistrement du compte";
			    msgbox.WindowTitle = "Erreur d'enregistrement des données";
			    msgbox.Content = "Ce compte n'a pas pu être enregistré.";
			#if DEBUG
				msgbox.ExpandedByDefault = true;
			#else
			    msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
			else {
			    //Ajouter dans la liste
			    pcbCompte.Items.Add(pcbCompte.Text);
			}//else
		}

		private void AjouterLigne()
		{
			if (string.IsNullOrEmpty(pcbCompte.Text)
			    && string.IsNullOrEmpty(cbProjet.Text)
			    && string.IsNullOrEmpty(tbDescription.Text)
			    && string.IsNullOrEmpty(tbDébit.Text)
			    && string.IsNullOrEmpty(tbCrédit.Text)
			    ) {
			    Enregistrer();
			}//if
			else {
			    if (!VérifierInfosContenu()) {
			        return;
			    }//if
			    string sDébit = tbDébit.Text;
			    string sCrédit = tbCrédit.Text;
			    if (!string.IsNullOrEmpty(sDébit)) {
			        _total_débits += double.Parse(sDébit);
			        sDébit = double.Parse(sDébit).ToString("N2", Program.CultureIG);
			    }//if
			    if (!string.IsNullOrEmpty(sCrédit)) {
			        _total_crédits += double.Parse(sCrédit);
			        sCrédit = double.Parse(sCrédit).ToString("N2", Program.CultureIG);
			    }//if
			    // Ajouter une nouvelle ligne vide
			    Row rLigneVide = new Row(new Cell[] {
			                         	    new Cell(pcbCompte.Text), //Compte
			                         	    new Cell(cbProjet.Text), //Projet
			                         	    new Cell(tbDescription.Text), //Libellé
			                         	    new Cell(sDébit), //Débit
			                         	    new Cell(sCrédit) //Crédit
			                             });
			    tblContenu.TableModel.Rows.Add(rLigneVide);

			    EffacerContenuTransaction();
			}//else
		}

		/// <summary>
		/// Ajoute un nouveau partenaire à la BD
		/// </summary>
		private void AjouterPartenaire()
		{
			//Confirmation
			MyMsgBox msgbox = new MyMsgBox();
			msgbox.WindowTitle = "Ajouter un nouveau partenaire?";
			msgbox.MainIcon = MyMsgBoxIcon.Question;
			msgbox.MainInstruction = "Etes-vous sûr de vouloir ajouter ce nouveau partenaire ?";
			msgbox.Buttons = new MyMsgBoxButton[] {
				new MyMsgBoxButton(MyMsgBoxResult.Yes),
				new MyMsgBoxButton(MyMsgBoxResult.No)
			};
			if (msgbox.Show() != MyMsgBoxResult.Yes) {
			    //annuler tout
			    return;
			}//if

			//Enregistrer dans la BD
			Model.Partenaire partenaire = new Model.Partenaire(Program.BD);
			partenaire.Nom = cbPartenaire.Text;
			Exception eValeurRetournée = null;
			eValeurRetournée = partenaire.Ajouter();
			if (eValeurRetournée != null) {
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative d'enregistrement du partenaire";
			    msgbox.WindowTitle = "Erreur d'enregistrement des données";
			    msgbox.Content = "Ce partenaire n'a pas pu être enregistré.";
			#if DEBUG
				msgbox.ExpandedByDefault = true;
			#else
			    msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
			else {
			    //Ajouter dans la liste
			    cbPartenaire.Items.Add(cbPartenaire.Text);
			}//else
		}

		/// <summary>
		/// Ajoute un nouveau projet à la BD
		/// </summary>
		private void AjouterProjet()
		{
			//Confirmation
			MyMsgBox msgbox = new MyMsgBox();
			UserControls.ucAjouterProjet AjouterProjet = new UserControls.ucAjouterProjet();
			AjouterProjet.tbNom.Text = cbProjet.Text;
			msgbox.CustomControl = AjouterProjet;
			msgbox.WindowTitle = "Ajouter un nouveau projet?";
			msgbox.MainIcon = MyMsgBoxIcon.Question;
			msgbox.MainInstruction = "Etes-vous sûr de vouloir ajouter ce nouveau projet ?";
			msgbox.Buttons = new MyMsgBoxButton[] {
				new MyMsgBoxButton(MyMsgBoxResult.Yes),
				new MyMsgBoxButton(MyMsgBoxResult.No)
			};
			if (msgbox.Show() != MyMsgBoxResult.Yes) {
			    //annuler tout
			    return;
			}//if

			//Enregistrer dans la BD
			Model.Projet projet = new Model.Projet(Program.BD);
			projet.Nom = cbProjet.Text;
			if (AjouterProjet.dtpDateDébut.Checked) {
			    projet.DateDébut = AjouterProjet.dtpDateDébut.Value;
			}//if
			else {
			    projet.DateDébut = null;
			}//else
			if (AjouterProjet.dtpDateFin.Checked) {
			    projet.DateFin = AjouterProjet.dtpDateFin.Value;
			}//if
			else {
			    projet.DateFin = null;
			}//else
			Exception eValeurRetournée = null;
			eValeurRetournée = projet.Ajouter();
			if (eValeurRetournée != null) {
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative d'enregistrement du projet";
			    msgbox.WindowTitle = "Erreur d'enregistrement des données";
			    msgbox.Content = "Ce projet n'a pas pu être enregistré.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
			    msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
			else {
			    //Ajouter dans la liste
			    cbProjet.Items.Add(cbProjet.Text);
			}//else
		}

		private void AppliquerThèmeBleu()
		{
			(ribbon.Renderer as RibbonProfessionalRenderer).ColorTable =
			    new RibbonProfesionalRendererColorTable();
			rccNoir.Checked = false;
			rccBleu.Checked = true;
		}

		private void AppliquerThèmeNoir()
		{
			(ribbon.Renderer as RibbonProfessionalRenderer).ColorTable =
			    new RibbonProfesionalRendererColorTableBlack();
			rccNoir.Checked = true;
			rccBleu.Checked = false;
		}

		void CbPartenaireEnter(object sender, EventArgs e)
		{
			if (_activerEvènements) {
			    _activerEvènements = false;
			    ChargerListePartenaires();
			    _activerEvènements = true;
			}//if
		}

		void CbPartenaireLeave(object sender, EventArgs e)
		{
			if (_activerEvènements) {
			    if (!string.IsNullOrEmpty(cbPartenaire.Text)
			        && cbPartenaire.Items.IndexOf(cbPartenaire.Text) == -1) {
			        AjouterPartenaire();
			    }//if
			}//if
		}

		void CbProjetEnter(object sender, EventArgs e)
		{
			if (_activerEvènements) {
			    _activerEvènements = false;
			    try {
			        Exception eValeurRetournée = null;
			        Model.Projet projet = new Model.Projet(Program.BD, cbProjet.Text, ref eValeurRetournée);
			        if (eValeurRetournée != null) {
			            throw eValeurRetournée;
			        }//if
			        ChargerListeProjets();
			    }//try
			    catch (Exception ex) {
			        System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine(ex.Message);
			        MyMsgBox msgbox = new MyMsgBox();
			        msgbox.Buttons = new MyMsgBoxButton[] {
						new MyMsgBoxButton(MyMsgBoxResult.OK)
					};
			        msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			        msgbox.MainInstruction = "Erreur lors de la tentative " +
			            "de chargement du projet";
			        msgbox.WindowTitle = "Erreur de chargement des données";
			        msgbox.Content = "Ce projet n'a pas pu être chargé correctement.";
			#if DEBUG
					msgbox.ExpandedByDefault = true;
			#else
			        msgbox.ExpandedByDefault = false;
			#endif
			        msgbox.ExpandedInformation = ex.Message;
			        msgbox.Show();
			    }//catch
			    _activerEvènements = true;
			}//if
		}

		void CbProjetLeave(object sender, EventArgs e)
		{
			if (_activerEvènements) {
			    if (string.IsNullOrEmpty(pcbCompte.Text)) {
			        cbProjet.Text = string.Empty;
			    }//if
			    else if (!string.IsNullOrEmpty(cbProjet.Text)
			             && cbProjet.Items.IndexOf(cbProjet.Text) == -1) {
			        AjouterProjet();
			    }//else if
			}//if
		}

		/// <summary>
		/// Charger la configuration concernant le maintient de la suppression des transactions
		/// </summary>
		private void ChargerConfiguration()
		{
			try {
			    Exception eValeurRetournée = _controller.ChargerConfiguration(ref _configuration);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			}//try
			catch (Exception ex) {
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors du chargement de la configuration.";
			    msgbox.WindowTitle = "Erreur de chargement des données";
			    msgbox.Content = "La configuration n'a pas pu être extraite correctement!";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
			    msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = ex.Message;
			    msgbox.Show();
			}//catch
		}

		/// <summary>
		/// Charger la date de la dernière opération
		/// </summary>
		private void ChargerDateSoldeTotal()
		{
			try {
			    DateTime? dtDateSoldeTotal = null;
			    Exception eValeurRetournée = _controller.ChargerDateSoldeTotal(ref dtDateSoldeTotal);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			    else {
			        blSoldeCaisse.Text = "Solde au ";
			        if (dtDateSoldeTotal.HasValue) {
			            blSoldeCaisse.Text = "Solde au " + dtDateSoldeTotal.Value.ToShortDateString();
			        }//if
			        else {
			            blSoldeCaisse.Text = "Solde au " + DateTime.Now.ToShortDateString();
			        }//else
			    }//else
			}//try
			catch (Exception ex) {
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de l'affichage de la date du solde total";
			    msgbox.WindowTitle = "Erreur d'affichage des données";
			    msgbox.Content = "La date du solde total n'a pas pu être extraite correctement!";
			#if DEBUG
				msgbox.ExpandedByDefault = true;
			#else
			    msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = ex.Message;
			    msgbox.Show();
			}//catch
		}

		/// <summary>
		/// Chargement de la liste des Comptes
		/// </summary>
		private void ChargerListeComptes()
		{
			try {
			    Exception eValeurRetournée = _controller.ChargerListeComptes(pcbCompte);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			}//try
			catch (Exception e) {
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement " +
			        "des listes des comptes de dépenses et de recettes";
			    msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "Les listes des comptes de dépenses et de recettes n'a pas pu être chargée.";
			#if DEBUG
				msgbox.ExpandedByDefault = true;
			#else
			    msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = e.Message;
			    msgbox.Show();
			}//catch
		}

		/// <summary>
		/// Chargement de la liste des partenaires
		/// </summary>
		private void ChargerListePartenaires()
		{
			Exception eValeurRetournée = _controller.ChargerListe(cbPartenaire,
			                                                     "partenaire", "nom", "`afficher` = true ORDER BY `nom`");
			if (eValeurRetournée != null) {
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement de la liste des partenaires";
			    msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "La liste des patenaires n'a pas pu être chargée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
			    msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
		}

		/// <summary>
		/// Chargement de la liste des projets
		/// </summary>
		private void ChargerListeProjets()
		{
			Exception eValeurRetournée = _controller.ChargerListe(cbProjet,
			                                                     "projet", "nom", "`afficher` = true ORDER BY `nom`");
			if (eValeurRetournée != null) {
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative de chargement de la liste des partenaires";
			    msgbox.WindowTitle = "Erreur d'extraction de données";
			    msgbox.Content = "La liste des partenaires n'a pas pu être chargée.";
			#if DEBUG
			    msgbox.ExpandedByDefault = true;
			#else
			    msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = eValeurRetournée.Message;
			    msgbox.Show();
			}//if
		}

		/// <summary>
		/// Permet de charger le solde total des comptes
		/// </summary>
		private void ChargerSoldeTotal()
		{
			try {
			    double? dSoldeTotal = null;
			    Exception eValeurRetournée = _controller.ChargerSoldeTotal(ref dSoldeTotal);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			    else {
			        if (dSoldeTotal.HasValue) {
			            ddcSoldeTotal.DigitText = dSoldeTotal.Value.ToString("N2", Program.CultureIG);
			        }//if
			        else {
			            ddcSoldeTotal.DigitText = (0).ToString("N2", Program.CultureIG);
			        }//else
			    }//else
			    ChargerDateSoldeTotal();

			}//try
			catch (Exception ex) {
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de l'affichage du solde total";
			    msgbox.WindowTitle = "Erreur d'affichage des données";
			    msgbox.Content = "Le solde total n'a pas pu être extrait correctement!";
			#if DEBUG
				msgbox.ExpandedByDefault = true;
			#else
			    msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = ex.Message;
			    msgbox.Show();
			}//catch
		}

		private void ChargerTransaction(Model.Transaction transaction)
		{
			Recommencer(false);

			_activerEvènements = false;
			_idChargé = transaction.Id;
			dtpDate.Value = transaction.DateTransaction;
			if (transaction.Partenaire != null) {
			    cbPartenaire.Text = transaction.Partenaire.Nom;
			}//if
			else {
			    cbPartenaire.Text = string.Empty;
			}//else
			tbDescriptionTransaction.Text = transaction.Description;
			tbNuméroChèque.Text = transaction.NuméroChèque;
			for (int i = 0; i < transaction.ContenuTransaction.Count; i++) {
			    string sDébit = string.Empty;
			    string sCrédit = string.Empty;
			    if (transaction.ContenuTransaction[i].Débit.HasValue) {
			        _total_débits += transaction.ContenuTransaction[i].Débit.Value;
			        sDébit = transaction.ContenuTransaction[i].Débit.Value.ToString("N2", Program.CultureIG);
			    }//if
			    if (transaction.ContenuTransaction[i].Crédit.HasValue) {
			        _total_crédits += transaction.ContenuTransaction[i].Crédit.Value;
			        sCrédit = transaction.ContenuTransaction[i].Crédit.Value.ToString("N2", Program.CultureIG);
			    }//if
			    Row ligne = new Row(
			        new Cell[] {
			            new Cell(transaction.ContenuTransaction[i].Compte.Id.ToString() + "-" + transaction.ContenuTransaction[i].Compte.Nom),
			            new Cell(transaction.ContenuTransaction[i].Projet != null ? transaction.ContenuTransaction[i].Projet.Nom : string.Empty),
			            new Cell(transaction.ContenuTransaction[i].Description),
			            new Cell(sDébit),
			            new Cell(sCrédit)
			        });
			    tblContenu.TableModel.Rows.Add(ligne);
			}//for i
			_activerEvènements = true;
		}

		/// <summary>
		/// Permet d'effacer les champs ajoutant les informations dans le contenu de la transaction
		/// </summary>
		private void EffacerContenuTransaction()
		{
			pcbCompte.Text = string.Empty;
			cbProjet.Items.Clear();
			cbProjet.Text = string.Empty;
			tbDescription.Text = string.Empty;
			tbDébit.Text = string.Empty;
			tbCrédit.Text = string.Empty;
			pcbCompte.Focus();
		}

		/// <summary>
		/// Copie de sauvegarde
		/// </summary>
		/// <param name="afficherMessageSuccès">true : afficher un message de félicitations, false, ne rien afficher</param>
		private void EffectuerCopieSauvegarde(bool afficherMessageSuccès)
		{
			Exception eValeurRetournée = null;
			Model.Configuration config = new Model.Configuration(Program.BD, ref eValeurRetournée);
			MyMsgBox msgbox = new MyMsgBox();
			msgbox.Buttons = new MyMsgBoxButton[] {
				new MyMsgBoxButton(MyMsgBoxResult.OK)
			};
			if (eValeurRetournée != null) {
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Configuration non chargée.";
			    msgbox.WindowTitle = "Configuration non chargée.";
			    msgbox.Content = "Le chargement des informations de la configuration n'a pas pu être effectuée!";
			    msgbox.Show();
			}//if
			else {
			    eValeurRetournée = _controller.CopieSecours(string.Empty,
			                                               config.CheminArchive, config.CheminCopieArchive);
			    if (eValeurRetournée != null) {
			        msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			        msgbox.MainInstruction = "Erreur lors de la tentative " +
			            "d'enregistrement des copies de secours";
			        msgbox.WindowTitle = "Erreur d'enregistrement des copies de secours";
			        msgbox.Content =
			            "La création des copies de secours des données ne s'est pas déroulée correctement.\n" +
			            "Il n'y a pas de problèmes grâves pour le moment mais si cela se répète souvent,\n" +
			            "alors il faut contacter votre fiston pour corriger le problème.";
			#if DEBUG
					msgbox.ExpandedByDefault = true;
			#else
			        msgbox.ExpandedByDefault = false;
			#endif
			        msgbox.ExpandedInformation = eValeurRetournée.Message;
			        msgbox.Show();
			    }//if
			    else if (afficherMessageSuccès) {
			        msgbox.MainIcon = MyMsgBoxIcon.SecuritySuccess;
			        msgbox.MainInstruction = "Copies de secours effectuées avec succès";
			        msgbox.WindowTitle = "Copies de secours";
			        msgbox.Content = "La création des copies de secours des données s'est déroulée correctement!";
			        msgbox.Show();
			    }//else if
			}//else
		}

		/// <summary>
		/// Permet d'enregistrer une nouvelle transaction ou la modification d'une transaction existante
		/// </summary>
		private void Enregistrer()
		{
			if (VérifierInfos()) {
			    //Confirmation
			    if (MyMsgBox.Show("Confirmez-vous l'enregistrement de ces informations?",
			                       "Confirmation de l'enregistrement", MessageBoxButtons.YesNo,
			                       MessageBoxIcon.Question) != DialogResult.Yes
			       ) {
			        return;
			    }//if

			    if (_idChargé.HasValue) {
			        Modification();
			    }//if
			    else {
			        //C'est un nouvel enregistrement à ajouter
			        Nouveau();
			    }//else
			    
                //TODO: uncomment when testing the nisab
                //VérifierNisab();

			}//if
		}

		/// <summary>
		/// Préparation de l'interface graphique
		/// </summary>
		private void Initialisation()
		{
			_controller = new MainController(Program.BD);

			_motPasseOK = ValiderMotPasse();
			if (_motPasseOK) {
			    ChargerListePartenaires();
			    ChargerListeComptes();
			    ChargerListeProjets();
			    ChargerSoldeTotal();
			    ChargerConfiguration();

			    _idChargé = null;
			    _activerEvènements = true;
			    _total_crédits = _total_débits = 0;

			    //Afficher le thème par défaut
			    rccBleu.Checked = true;
			    rccNoir.Checked = false;
			}//if
		}

		void MainFormFormClosing(object sender, FormClosingEventArgs e)
		{
			if (rbAuto.Checked) {
			    EffectuerCopieSauvegarde(false);
			}//if
			try {
			    Program.BD.Connection.Close();
			}//try
			catch (Exception ex) {
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}//catch
		}

		void MainFormLeave(object sender, EventArgs e)
		{
			dtpDate.Focus();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
		}

		/// <summary>
		/// Permet de minimiser ou de maximiser le ribbon selon le cas.
		/// L'icône du bouton de lancement rapide est également mis à jour.
		/// </summary>
		private void MinimiserMaximiserRibbon()
		{
			ribbon.Minimized = !ribbon.Minimized;
			if (ribbon.Minimized) {
			    rBtnMinimiserRibbon.SmallImage = Properties.Resources.arrow_down_double_3_20;
			}//if
			else {
			    rBtnMinimiserRibbon.SmallImage = Properties.Resources.arrow_up_double_3_20;
			}//else
		}

		private void Modification()
		{
			try {
			    Exception eValeurRetournée = null;
			    #region " Transaction "
			    Model.Transaction nouvelle_transaction = new Model.Transaction(Program.BD);
			    nouvelle_transaction.DateTransaction = dtpDate.Value;
			    if (string.IsNullOrEmpty(cbPartenaire.Text)) {
			        nouvelle_transaction.Partenaire = null;
			    }//if
			    else {
			        nouvelle_transaction.Partenaire = new Model.Partenaire(Program.BD,
			                                                       cbPartenaire.Text, ref eValeurRetournée);
			        if (eValeurRetournée != null) {
			            throw eValeurRetournée;
			        }//if
			    }//else
			    nouvelle_transaction.Description = tbDescriptionTransaction.Text.Trim();
			    nouvelle_transaction.NuméroChèque = tbNuméroChèque.Text.Trim();

			    #endregion " Transaction "

			    PréparerContenuTransaction(nouvelle_transaction);

			    eValeurRetournée = nouvelle_transaction.Modifier(_idChargé.Value, _configuration.MaintenirSuppressionsTransactions);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			    else {
			        //Charger le nouveau solde général
			        ChargerSoldeTotal();
			        Recommencer(false);
			    }//else

			}//try
			catch (Exception ex) {
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative d'enregistrement de la transaction";
			    msgbox.WindowTitle = "Erreur d'enregistrement des données";
			    msgbox.Content = "Cette transaction n'a pas pu être modifiée.";
			#if DEBUG
				msgbox.ExpandedByDefault = true;
			#else
			    msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = ex.Message;
			    msgbox.Show();
			}//catch
		}

		/// <summary>
		/// Permet de charger les infos de la ligne sélectionnée dans les combobox et textbox.
		/// </summary>
		private void ModifierLigne()
		{
			if (tblContenu.TableModel.Selections.SelectedIndicies.Length == 1) {
			    CellCollection ccSélection =
			        tblContenu.TableModel.Rows[
			            tblContenu.TableModel.Selections.SelectedIndicies[0]].Cells;
			    pcbCompte.Text = ccSélection[0].Text;
			    cbProjet.Text = ccSélection[1].Text;
			    tbDescription.Text = ccSélection[2].Text;
			    tbDébit.Text = ccSélection[3].Text;
			    tbCrédit.Text = ccSélection[4].Text;
			    try {
			        if (!string.IsNullOrEmpty(tbDébit.Text)) {
			            _total_débits -= double.Parse(tbDébit.Text);
			        }//if
			        if (!string.IsNullOrEmpty(tbCrédit.Text)) {
			            _total_crédits -= double.Parse(tbCrédit.Text);
			        }//if
			    }//try
			    catch (Exception ex) {
			        System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine(ex.Message);
			    }//catch
			    //Supprimer de la liste
			    tblContenu.TableModel.Rows.RemoveAt(
			        tblContenu.TableModel.Selections.SelectedIndicies[0]);

			}//if
		}

		private void Nouveau()
		{
			try {
			    Exception eValeurRetournée = null;
			    #region " Transaction "
			    Model.Transaction transaction = new Model.Transaction(Program.BD);
			    transaction.DateTransaction = dtpDate.Value;
			    if (string.IsNullOrEmpty(cbPartenaire.Text)) {
			        transaction.Partenaire = null;
			    }//if
			    else {
			        transaction.Partenaire = new Model.Partenaire(Program.BD,
			                                                       cbPartenaire.Text, ref eValeurRetournée);
			        if (eValeurRetournée != null) {
			            throw eValeurRetournée;
			        }//if
			    }//else
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			    transaction.Description = tbDescriptionTransaction.Text.Trim();
			    transaction.NuméroChèque = tbNuméroChèque.Text.Trim();

			    #endregion " Transaction "

			    PréparerContenuTransaction(transaction);

			    eValeurRetournée = transaction.Ajouter();
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			    else {
			        //Ajouter le montant total au solde général
			        ChargerSoldeTotal();
			        Recommencer(false);
			    }//else

			}//try
			catch (Exception ex) {
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative d'enregistrement de la transaction";
			    msgbox.WindowTitle = "Erreur d'enregistrement des données";
			    msgbox.Content = "Cette transaction n'a pas pu être enregistrée.";
			#if DEBUG
				msgbox.ExpandedByDefault = true;
			#else
			    msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = ex.Message;
			    msgbox.Show();
			}//catch
		}

		void PcbCompteEnter(object sender, EventArgs e)
		{
			if (_activerEvènements) {
			    _activerEvènements = false;
			    ChargerListeComptes();
			    _activerEvènements = true;
			}//if
		}

		void PcbCompteLeave(object sender, EventArgs e)
		{
			if (_activerEvènements) {
			    if (!string.IsNullOrEmpty(pcbCompte.Text)
			        && pcbCompte.Items.IndexOf(pcbCompte.Text) == -1) {
			        AjouterCompte();
			    }//if
			}//if
		}

		void PcbCompteSelectedIndexChanged(object sender, EventArgs e)
		{
			if (_activerEvènements) {
			    Model.Compte compte = new Model.Compte(Program.BD);
			    Exception eValeurRetournée = compte.Charger(long.Parse(pcbCompte.Text.Substring(0, 4)));
			    if (eValeurRetournée == null && compte.Solde.HasValue) {
			        double dTmp = Math.Abs(compte.Solde.Value);
			        ttCompte.SetToolTip(pcbCompte, dTmp.ToString("N2", Program.CultureIG));
			        ttCompte.Show(dTmp.ToString("N2", Program.CultureIG), this,
			                      pcbCompte.Location.X + pcbCompte.Size.Width + 10,
			                      pcbCompte.Location.Y + pcbCompte.Size.Height,
			                      10000);
			    }//if
			    if (_total_débits > _total_crédits) {
			        tbDébit.Text = string.Empty;
			        tbCrédit.Text = (_total_débits - _total_crédits).ToString("N2", Program.CultureIG);
			    }//if
			    else if (_total_débits < _total_crédits) {
			        tbDébit.Text = (_total_crédits - _total_débits).ToString("N2", Program.CultureIG);
			        tbCrédit.Text = string.Empty;
			    }//else if
			}//if
		}

		/// <summary>
		/// Prépare le contenu de la transaction à être ajouté
		/// </summary>
		/// <param name="transaction">la transaction dans laquel le contenu sera ajouté</param>
		private void PréparerContenuTransaction(Model.Transaction transaction)
		{
			Exception eValeurRetournée = null;
			foreach (Row r in tblContenu.TableModel.Rows) {
			    Model.Compte compte = new Model.Compte(
			        Program.BD, long.Parse(r.Cells[0].Text.Substring(0, 4)), ref eValeurRetournée);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			    Model.Projet projet = new Model.Projet(
			        Program.BD, r.Cells[1].Text, ref eValeurRetournée);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			    double? dDébit = null;
			    if (!string.IsNullOrEmpty(r.Cells[3].Text)) {
			        dDébit = double.Parse(r.Cells[3].Text);
			    }//if
			    double? dCrédit = null;
			    if (!string.IsNullOrEmpty(r.Cells[4].Text)) {
			        dCrédit = double.Parse(r.Cells[4].Text);
			    }//if
			    transaction.ContenuTransaction.Add(
			        new Model.ContenuTransaction(
			            Program.BD, transaction,
			            r.Cells[2].Text, //Description
			            compte, projet, dDébit, dCrédit
			        )
			    );
			}//foreach
		}

		void RBtnMinimiserRibbonClick(object sender, EventArgs e)
		{
			MinimiserMaximiserRibbon();
		}

		void RbAutoClick(object sender, EventArgs e)
		{
			if (rbAuto.Checked) {
			    rbAuto.Text = "Automatique";
			    rbAuto.Checked = true;
			}//if
			else {
			    rbAuto.Text = "Manuel";
			    rbAuto.Checked = false;
			}//else
		}

		void RccBleuClick(object sender, EventArgs e)
		{
			AppliquerThèmeBleu();
		}

		void RccNoirClick(object sender, EventArgs e)
		{
			AppliquerThèmeNoir();
		}

		/// <summary>
		/// Permet d'effacer toutes les informations pour recommencer
		/// </summary>
		/// <param name="confirmer">demander une confirmation</param>
		private void Recommencer(bool confirmer)
		{
			if (confirmer) {
			    //Confirmation
			    if (MyMsgBox.Show("Confirmez-vous l'effacement de toutes les informations " +
			                       "de cette fenêtre?",
			                       "Confirmation de l'effacement", MessageBoxButtons.YesNo,
			                       MessageBoxIcon.Question) != DialogResult.Yes
			       ) {
			        return;
			    }//if
			}//if

			_idChargé = null;
			_activerEvènements = true;
			epValidation.Clear();
			_total_crédits = _total_débits = 0;

			dtpDate.Value = DateTime.Now;
			cbPartenaire.Text = string.Empty;
			tbDescriptionTransaction.Text = string.Empty;
			tbNuméroChèque.Text = string.Empty;

			EffacerContenuTransaction();

			tblContenu.TableModel.Rows.Clear();

			ChargerSoldeTotal();

			dtpDate.Focus();
		}

		/// <summary>
		/// Permet de supprimer la ligne sélectionnée du tableau
		/// </summary>
		private void RetirerLigneSélectionnée()
		{
			ModifierLigne();
			EffacerContenuTransaction();
		}

		void RibbonDoubleClick(object sender, EventArgs e)
		{
			MinimiserMaximiserRibbon();
		}

		void RimoMinimiserClick(object sender, EventArgs e)
		{
			MinimiserMaximiserRibbon();
		}

		void RmBtnAjouterClick(object sender, EventArgs e)
		{
			AjouterLigne();
		}

		void RmBtnEnregistrerClick(object sender, EventArgs e)
		{
			Enregistrer();
		}

		void RmBtnRecommencerClick(object sender, EventArgs e)
		{
			Recommencer(true);
		}

		void RmBtnSupprimerClick(object sender, EventArgs e)
		{
			Supprimer();
		}

		void RomiConfigurationClick(object sender, EventArgs e)
		{
			new ConfigurationForm().ShowDialog();
			ChargerConfiguration();
		}

		void RomiSauvegarderClick(object sender, EventArgs e)
		{
			EffectuerCopieSauvegarde(true);
		}

		void RooBtnQuitterClick(object sender, EventArgs e)
		{
			this.Close();
		}

		void RpCopiesSecoursButtonMoreClick(object sender, EventArgs e)
		{
			new ConfigurationForm().ShowDialog();
		}

		private void Supprimer()
		{
			//Vérifier si les infos ont été chargées
			if (!_idChargé.HasValue)
			    return; //ne rien faire

			//Demander la confirmation
			if (MyMsgBox.Show("Confirmez-vous la suppression de cette opération?",
			                   "Confirmation de la suppression", MessageBoxButtons.YesNo,
			                   MessageBoxIcon.Question) != DialogResult.Yes
			   ) {
			    return;
			}//if

			try {
			    //Suppression
			    Exception eValeurRetournée = null;
			    Model.Transaction transaction = new Model.Transaction(Program.BD);
			    eValeurRetournée = transaction.Charger(_idChargé.Value);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			    eValeurRetournée = transaction.Supprimer(_configuration.MaintenirSuppressionsTransactions);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			    else {
			        Recommencer(false);
			    }//else
			}//try
			catch (Exception ex) {
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    MyMsgBox msgbox = new MyMsgBox();
			    msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
			    msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msgbox.MainInstruction = "Erreur lors de la tentative " +
			        "de suppression de la rubrique";
			    msgbox.WindowTitle = "Erreur de suppression des données";
			    msgbox.Content = "Cette rubrique n'a pas pu être supprimé correctement.";
			#if DEBUG
				msgbox.ExpandedByDefault = true;
			#else
			    msgbox.ExpandedByDefault = false;
			#endif
			    msgbox.ExpandedInformation = ex.Message;
			    msgbox.Show();
			}//catch
		}

		void TbCrédit_TextChanged(object sender, EventArgs e)
		{
			if (_activerEvènements) {
			    _activerEvènements = false;
			    tbDébit.Text = string.Empty;
			    _activerEvènements = true;
			}//if
		}

		void TbDébit_TextChanged(object sender, EventArgs e)
		{
			if (_activerEvènements) {
			    _activerEvènements = false;
			    tbCrédit.Text = string.Empty;
			    _activerEvènements = true;
			}//if
		}

		void TblContenuCellPropertyChanged(object sender, My.Forms.Controls.ListViews.XPTable.Events.CellEventArgs e)
		{
			if (_activerEvènements) {
			    _activerEvènements = false;
			    //Précison le traitement de chaque colonne modifiée
			    CellCollection cellules = tblContenu.TableModel.Rows[e.Row].Cells;
			    bool bRecalculerTotaux = false;
			    if (e.Column == 3) {
			        //Débit
			        bRecalculerTotaux = true;
			        try {
			            cellules[3].Text = double.Parse(cellules[3].Text).ToString("N2", Program.CultureIG);
			            cellules[4].Text = string.Empty;//crédit
			        }//try
			        catch {
			            cellules[3].Text = string.Empty;
			        }//catch
			    }//if
			    else if (e.Column == 4) {
			        //Crédit
			        bRecalculerTotaux = true;
			        try {
			            cellules[4].Text = double.Parse(cellules[4].Text).ToString("N2", Program.CultureIG);
			            cellules[3].Text = string.Empty;//débit
			        }//try
			        catch {
			            cellules[4].Text = string.Empty;
			        }//catch
			    }//else if

			    if (bRecalculerTotaux) {
			        _total_débits = _total_crédits = 0;
			        foreach (Row ligne in tblContenu.TableModel.Rows) {
			            if (!string.IsNullOrEmpty(ligne.Cells[3].Text)) {
			                try {
			                    _total_débits += double.Parse(ligne.Cells[3].Text);
			                }//try
			                catch (Exception ex) {
			                    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			                    System.Diagnostics.Debug.WriteLine(ex.Message);
			                }//catch
			            }//if
			            if (!string.IsNullOrEmpty(ligne.Cells[4].Text)) {
			                try {
			                    _total_crédits += double.Parse(ligne.Cells[4].Text);
			                }//try
			                catch (Exception ex) {
			                    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			                    System.Diagnostics.Debug.WriteLine(ex.Message);
			                }//catch
			            }//if
			        }//foreach
			    }//if
			    _activerEvènements = true;
			}//if
		}

		void TblContenuDoubleClick(object sender, EventArgs e)
		{
			ModifierLigne();
		}

		void TblContenuTransactionKeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete) {
			    RetirerLigneSélectionnée();
			}//if
		}

		/// <summary>
		/// Vérifier le mot de passe. Si le mot de passe est vide, ne pas demander de mot de passe
		/// </summary>
		/// <returns>true : le mot de passe est correct. false: incorrect </returns>
		private bool ValiderMotPasse()
		{
			MyMsgBox msg = new MyMsgBox();
			string mot_passe_bd = string.Empty;
			bool bValeurRetournée = false;
			Exception eValeurRetournée = _controller.ChargerConfiguration(ref mot_passe_bd);
			if (eValeurRetournée != null) {
			    msg.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msg.MainInstruction = "Erreur lors de la tentative de chargement " +
			        "des informations de configuration";
			    msg.WindowTitle = "Erreur d'extraction de données";
			    msg.Content = "Le mot de passe n'a pas pu être extrait correctement.";
			#if DEBUG
			    msg.ExpandedByDefault = true;
			#else
			    msg.ExpandedByDefault = false;
			#endif
			    msg.ExpandedInformation = eValeurRetournée.Message;
			    msg.Show();
			}//if

			if (!string.IsNullOrEmpty(mot_passe_bd)) {
			    int iCompteur = 0;
			    const int MAX_TENTATIVES = 3;

			    System.Windows.Forms.TextBox tb = new System.Windows.Forms.TextBox();
			    tb.Font = new System.Drawing.Font(this.Font.FontFamily, this.Font.Size + 2);
			    tb.PasswordChar = '*';
			    tb.TextAlign = HorizontalAlignment.Center;
			    msg.Buttons = new MyMsgBoxButton[] {
				new MyMsgBoxButton(MyMsgBoxResult.OK, "Connexion"),
				new MyMsgBoxButton(MyMsgBoxResult.Cancel, "Quitter")
			};
			    msg.MainIcon = MyMsgBoxIcon.SecurityQuestion;
			    msg.WindowTitle = "Autorisation d'accès";
			    msg.MainInstruction = "Veuillez saisir votre mot de passe";
			    msg.DefaultButton = MyMsgBoxDefaultButton.Button1;
			    msg.CustomControl = tb;
			    msg.Owner = this;
			    MyMsgBoxResult result = MyMsgBoxResult.OK;

			    result = msg.Show();

			    while ((result != MyMsgBoxResult.Cancel) && (iCompteur++ < MAX_TENTATIVES)) {
			        //ne pas afficher deux fois la première fois
			        if (iCompteur > 1) {
			            result = msg.Show();
			        }//if
			        try {
			            bool bMotPasseOK = false;
			            eValeurRetournée = _controller.VérifierMotPasse(tb.Text, ref bMotPasseOK);
			            if (eValeurRetournée != null) {
			                throw eValeurRetournée;
			            }//if

			            if (bMotPasseOK) {
			                //Mot de passe correct, forcer la sortie de la boucle
			                bValeurRetournée = true;
			                break;
			            }//if
			            else {
			                //Vérifier si c'est la dernière tentative
			                if (iCompteur == MAX_TENTATIVES - 1) {
			                    msg.MainIcon = MyMsgBoxIcon.SecurityWarning;
			                    msg.MainInstruction = "Veuillez saisir votre mot de passe." +
			                        Environment.NewLine + "Attention dernière chance!";
			                }//if
			                tb.Text = string.Empty;
			            }//else
			        }//try
			        catch {
			            bValeurRetournée = false;
			            iCompteur = MAX_TENTATIVES;
			            result = MyMsgBoxResult.Cancel;
			        }//catch

			    }//while

			    if ((result == MyMsgBoxResult.Cancel) || (iCompteur > MAX_TENTATIVES)) {
			        bValeurRetournée = false;
			        msg.CustomControl = null;
			        msg.MainIcon = MyMsgBoxIcon.SecurityError;
			        msg.MainInstruction = "Authentification échouée!" +
			            Environment.NewLine + "Impossible de démarrer l'application";
			        msg.Buttons = new MyMsgBoxButton[] { new MyMsgBoxButton(MyMsgBoxResult.OK) };
			        msg.Show();
			    }//if
			    else {
			        bValeurRetournée = true;
			    }//else
			}//if
			else {
			    //mot de passe vide trouve dans la BD.
			    bValeurRetournée = true;
			}//else

			return bValeurRetournée;
		}

		/// <summary>
		/// Permet de vérifier si toutes les données saisies sont correctes
		/// </summary>
		/// <returns>true : tout est bon, false : un problème est survenu</returns>
		private bool VérifierInfos()
		{
			bool bValeurRetournée = true;
			epValidation.Clear();

			if (tblContenu.TableModel.Rows.Count == 0) {
			    epValidation.SetError(tblContenu, "Vous devez inclure au moins une opération!");
			    bValeurRetournée = false;
			}//if

			double dTotalCrédits = 0, dTotalDébits = 0;
			foreach (Row ligne in tblContenu.TableModel.Rows) {
			    if (string.IsNullOrEmpty(ligne.Cells[3].Text)) {
			        try {
			            dTotalCrédits += double.Parse(ligne.Cells[4].Text);
			        }//try
			        catch (Exception ex) {
			            System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			            System.Diagnostics.Debug.WriteLine(ex.Message);
			            epValidation.SetError(tblContenu, "Le crédit de l'opération n°" + (ligne.Index + 1) + " est invalide.");
			            bValeurRetournée = false;
			        }//catch
			    }//if
			    else if (string.IsNullOrEmpty(ligne.Cells[4].Text)) {
			        try {
			            dTotalDébits += double.Parse(ligne.Cells[3].Text);
			        }//try
			        catch (Exception ex) {
			            System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			            System.Diagnostics.Debug.WriteLine(ex.Message);
			            epValidation.SetError(tblContenu, "Le débit de l'opération n°" + (ligne.Index + 1) + " est invalide.");
			            bValeurRetournée = false;
			        }//catch
			    }//else if
			}//foreach

			if (Math.Round(dTotalDébits, 2) != Math.Round(dTotalCrédits, 2)) {
			    epValidation.SetError(tblContenu, "Le total des crédits n'est pas égal au total des débits.");
			    bValeurRetournée = false;
			}//if

			if (Math.Round(dTotalDébits, 2) == 0 || Math.Round(dTotalCrédits, 2) == 0) {
			    epValidation.SetError(tblContenu, "Le total des crédits/débits ne peux être nul.");
			    bValeurRetournée = false;
			}//if

			return bValeurRetournée;
		}

		/// <summary>
		/// Permet de vérifier que les données de l'opération sont bien saisies
		/// </summary>
		/// <returns>true : tout est bon; false sinon.</returns>
		private bool VérifierInfosContenu()
		{
			bool bValeurRetour = true;
			epValidation.Clear();

			if (string.IsNullOrEmpty(pcbCompte.Text.Trim())) {
			    epValidation.SetError(pcbCompte,
			                          "Le compte ne peut être vide.");
			    bValeurRetour = false;
			}//if
			else if (pcbCompte.Items.IndexOf(pcbCompte.Text) == -1) {
			    epValidation.SetError(pcbCompte,
			                          "Le compte saisit ne figure pas dans la liste.");
			    bValeurRetour = false;
			}//else if

			if (string.IsNullOrEmpty(tbDébit.Text)) {
			    try {
			        double.Parse(tbCrédit.Text);
			    }//try
			    catch {
			        epValidation.SetError(tbCrédit,
			                              "Le crédit saisit est invalide.");
			        bValeurRetour = false;
			    }//catch
			}//if

			if (string.IsNullOrEmpty(tbCrédit.Text)) {
			    try {
			        double.Parse(tbDébit.Text);
			    }//try
			    catch {
			        epValidation.SetError(tbDébit,
			                              "Le débit saisit est invalide.");
			        bValeurRetour = false;
			    }//catch
			}//if

			if (string.IsNullOrEmpty(tbDébit.Text)
			    && string.IsNullOrEmpty(tbCrédit.Text)) {
			    epValidation.SetError(tbDébit,
			                          "Il faut préciser le débit ou bien le crédit. " + Environment.NewLine +
			                          "Ils ne peuvent pas être vides tous les deux.");
			    epValidation.SetError(tbCrédit,
			                          "Il faut préciser le débit ou bien le crédit. " + Environment.NewLine +
			                          "Ils ne peuvent pas être vides tous les deux.");
			    bValeurRetour = false;
			}//if

			return bValeurRetour;
		}

		/// <summary>
		/// Verifie s'il y a un nisab qui a ete atteint pour le noter
		/// </summary>
		private void VérifierNisab()
		{
			Exception eValeurRetournée = 
			    _controller.VérifierNisab(double.Parse(ddcSoldeTotal.DigitText), _configuration.MontantNisab);
			if (eValeurRetournée != null) {
			    MyMsgBox msg = new MyMsgBox();
			    msg.MainIcon = MyMsgBoxIcon.SecurityWarning;
			    msg.MainInstruction = "Erreur lors de la tentative de vérification " +
			        "du calcul du nisab";
			    msg.WindowTitle = "Erreur d'extraction de données";
			    msg.Content = "Le mot de passe n'a pas pu être extrait correctement.";
			#if DEBUG
			    msg.ExpandedByDefault = true;
			#else
			    msg.ExpandedByDefault = false;
			#endif
			    msg.ExpandedInformation = eValeurRetournée.Message;
			    msg.Show();
			}//if
		}

		void dtpDate_Leave(object sender, EventArgs e)
		{
			if (_activerEvènements) {
			    if (dtpDate.Value > DateTime.Now) {
			        if (MyMsgBox.Show("Êtes-vous sûr de vouloir utiliser une date ultérieure?", "Date ultérieure à la date du jour!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.No) {
			            _activerEvènements = false;
			            dtpDate.Value = DateTime.Now;
			            dtpDate.Focus();
			            _activerEvènements = true;
			        }//if
			    }//if
			}//if
		}

		void rBtnBilan_Click(object sender, EventArgs e)
		{
			new BilanForm().Show();
		}

		void rBtnCalculatrice_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start("calc");
		}

		void rBtnComptes_Click(object sender, EventArgs e)
		{
			new CompteForm().Show();
		}

		void rBtnDétailsProjets_Click(object sender, EventArgs e)
		{
			new ListeProjetsForm().Show();
		}

		private void rBtnGraphs_Click(object sender, EventArgs e)
		{
			new GraphesForm().Show();
		}

		void rBtnListeOpérations_Click(object sender, EventArgs e)
		{
			new ListeTransactionsForm().Show();
		}

		void rBtnListePartenaires_Click(object sender, EventArgs e)
		{
			new ListePartenairesForm().Show();
		}

		void rBtnPartenaires_Click(object sender, EventArgs e)
		{
			new PartenaireForm().Show();
		}

		void rBtnPlanComptable_Click(object sender, EventArgs e)
		{
			new PlanComptableForm().Show();
		}

		void rBtnProjet_Click(object sender, EventArgs e)
		{
			new ProjetForm().Show();
		}

		void rBtnRechercherTransaction_Click(object sender, EventArgs e)
		{
			RechercherTransationForm frm = new RechercherTransationForm();
			frm.ShowDialog();
			if (frm.Transaction != null) {
			    ChargerTransaction(frm.Transaction);
			}//if
		}

		private void rbtnZakat_Click(object sender, EventArgs e)
		{
			new AnsibaForm().Show();
		}

		void tbCrédit_Enter(object sender, EventArgs e)
		{
			if (_activerEvènements) {
			    _activerEvènements = false;
			    if (string.IsNullOrEmpty(tbCrédit.Text) && _total_débits > _total_crédits) {
			        tbCrédit.Text = (_total_débits - _total_crédits).ToString("N2", Program.CultureIG);
			    }//if
			    _activerEvènements = true;
			}//if
		}

		void tbDébit_Enter(object sender, EventArgs e)
		{
			if (_activerEvènements) {
			    _activerEvènements = false;
			    if (string.IsNullOrEmpty(tbDébit.Text) && _total_crédits > _total_débits) {
			        tbDébit.Text = (_total_crédits - _total_débits).ToString("N2", Program.CultureIG);
			    }//if
			    _activerEvènements = true;
			}//if
		}

		#endregion Private Methods
	}
}