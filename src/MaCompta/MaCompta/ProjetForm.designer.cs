﻿/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 07/09/1433
 * Time: 18:31
 * 
 * © 1428-1433 (2007-2012) All rights reserved to Mohamed Y. ElAmrani.
 */
namespace MaCompta
{
	partial class ProjetForm
	{
		#region Fields

		private System.Windows.Forms.ComboBox cbListe;
		private System.Windows.Forms.CheckBox ckAfficher;

		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.DateTimePicker dtpDateDébut;
		private System.Windows.Forms.DateTimePicker dtpDateFin; // End of Protected Method Dispose
		private System.Windows.Forms.ErrorProvider epValidation;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lbDateDébut;
		private System.Windows.Forms.Label lbDateFin;
		private System.Windows.Forms.Label lbNom;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnEnregistrer;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnRecommencer;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnSupprimer;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
		private System.Windows.Forms.TextBox tbNom;

		#endregion Fields

		#region Private Methods

		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.cbListe = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.rmBtnSupprimer = new My.Forms.Controls.Ribbon.RibbonMenuButton();
			this.rmBtnEnregistrer = new My.Forms.Controls.Ribbon.RibbonMenuButton();
			this.rmBtnRecommencer = new My.Forms.Controls.Ribbon.RibbonMenuButton();
			this.lbNom = new System.Windows.Forms.Label();
			this.lbDateDébut = new System.Windows.Forms.Label();
			this.lbDateFin = new System.Windows.Forms.Label();
			this.tbNom = new System.Windows.Forms.TextBox();
			this.ckAfficher = new System.Windows.Forms.CheckBox();
			this.dtpDateDébut = new System.Windows.Forms.DateTimePicker();
			this.dtpDateFin = new System.Windows.Forms.DateTimePicker();
			this.epValidation = new System.Windows.Forms.ErrorProvider(this.components);
			this.tableLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.epValidation)).BeginInit();
			this.SuspendLayout();
			// 
			// cbListe
			// 
			this.cbListe.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.cbListe.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
			this.cbListe.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
			this.cbListe.FormattingEnabled = true;
			this.cbListe.Location = new System.Drawing.Point(192, 16);
			this.cbListe.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.cbListe.MaxLength = 45;
			this.cbListe.Name = "cbListe";
			this.cbListe.Size = new System.Drawing.Size(218, 28);
			this.cbListe.TabIndex = 2;
			this.cbListe.SelectedIndexChanged += new System.EventHandler(this.CbListeSelectedIndexChanged);
			this.cbListe.TextUpdate += new System.EventHandler(this.CbListeTextUpdate);
			// 
			// label1
			// 
			this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.label1.BackColor = System.Drawing.Color.Transparent;
			this.label1.Location = new System.Drawing.Point(141, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(45, 31);
			this.label1.TabIndex = 1;
			this.label1.Text = "Liste";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tableLayoutPanel
			// 
			this.tableLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
			| System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
			this.tableLayoutPanel.ColumnCount = 4;
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel.Controls.Add(this.rmBtnSupprimer, 0, 4);
			this.tableLayoutPanel.Controls.Add(this.rmBtnEnregistrer, 3, 4);
			this.tableLayoutPanel.Controls.Add(this.rmBtnRecommencer, 2, 4);
			this.tableLayoutPanel.Controls.Add(this.lbNom, 0, 0);
			this.tableLayoutPanel.Controls.Add(this.lbDateDébut, 0, 1);
			this.tableLayoutPanel.Controls.Add(this.lbDateFin, 0, 2);
			this.tableLayoutPanel.Controls.Add(this.tbNom, 1, 0);
			this.tableLayoutPanel.Controls.Add(this.ckAfficher, 0, 3);
			this.tableLayoutPanel.Controls.Add(this.dtpDateDébut, 1, 1);
			this.tableLayoutPanel.Controls.Add(this.dtpDateFin, 1, 2);
			this.tableLayoutPanel.Location = new System.Drawing.Point(14, 55);
			this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tableLayoutPanel.Name = "tableLayoutPanel";
			this.tableLayoutPanel.RowCount = 5;
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 87F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel.Size = new System.Drawing.Size(524, 268);
			this.tableLayoutPanel.TabIndex = 0;
			// 
			// rmBtnSupprimer
			// 
			this.rmBtnSupprimer.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.rmBtnSupprimer.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
			this.rmBtnSupprimer.BackColor = System.Drawing.Color.Transparent;
			this.rmBtnSupprimer.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
			this.rmBtnSupprimer.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
			this.rmBtnSupprimer.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
			this.rmBtnSupprimer.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
			this.rmBtnSupprimer.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.rmBtnSupprimer.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.rmBtnSupprimer.FadingSpeed = 35;
			this.rmBtnSupprimer.FlatAppearance.BorderSize = 0;
			this.rmBtnSupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.rmBtnSupprimer.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rmBtnSupprimer.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
			this.rmBtnSupprimer.Image = global::MaCompta.Properties.Resources.user_trash_full_64;
			this.rmBtnSupprimer.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Top;
			this.rmBtnSupprimer.ImageOffset = 1;
			this.rmBtnSupprimer.IsPressed = false;
			this.rmBtnSupprimer.KeepPress = false;
			this.rmBtnSupprimer.Location = new System.Drawing.Point(11, 184);
			this.rmBtnSupprimer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.rmBtnSupprimer.MaxImageSize = new System.Drawing.Point(50, 50);
			this.rmBtnSupprimer.MenuPos = new System.Drawing.Point(0, 0);
			this.rmBtnSupprimer.Name = "rmBtnSupprimer";
			this.rmBtnSupprimer.Radius = 11;
			this.rmBtnSupprimer.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
			this.rmBtnSupprimer.Size = new System.Drawing.Size(109, 80);
			this.rmBtnSupprimer.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
			this.rmBtnSupprimer.SplitDistance = 0;
			this.rmBtnSupprimer.TabIndex = 21;
			this.rmBtnSupprimer.Text = "Supprimer";
			this.rmBtnSupprimer.Title = "";
			this.rmBtnSupprimer.UseVisualStyleBackColor = false;
			this.rmBtnSupprimer.Click += new System.EventHandler(this.RmBtnSupprimerClick);
			// 
			// rmBtnEnregistrer
			// 
			this.rmBtnEnregistrer.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.rmBtnEnregistrer.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
			this.rmBtnEnregistrer.BackColor = System.Drawing.Color.Transparent;
			this.rmBtnEnregistrer.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
			this.rmBtnEnregistrer.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
			this.rmBtnEnregistrer.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
			this.rmBtnEnregistrer.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
			this.rmBtnEnregistrer.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.rmBtnEnregistrer.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.rmBtnEnregistrer.FadingSpeed = 35;
			this.rmBtnEnregistrer.FlatAppearance.BorderSize = 0;
			this.rmBtnEnregistrer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.rmBtnEnregistrer.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rmBtnEnregistrer.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
			this.rmBtnEnregistrer.Image = global::MaCompta.Properties.Resources.dialog_ok_2_64;
			this.rmBtnEnregistrer.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Top;
			this.rmBtnEnregistrer.ImageOffset = 0;
			this.rmBtnEnregistrer.IsPressed = false;
			this.rmBtnEnregistrer.KeepPress = false;
			this.rmBtnEnregistrer.Location = new System.Drawing.Point(403, 184);
			this.rmBtnEnregistrer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.rmBtnEnregistrer.MaxImageSize = new System.Drawing.Point(50, 50);
			this.rmBtnEnregistrer.MenuPos = new System.Drawing.Point(0, 0);
			this.rmBtnEnregistrer.Name = "rmBtnEnregistrer";
			this.rmBtnEnregistrer.Radius = 11;
			this.rmBtnEnregistrer.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
			this.rmBtnEnregistrer.Size = new System.Drawing.Size(111, 80);
			this.rmBtnEnregistrer.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
			this.rmBtnEnregistrer.SplitDistance = 0;
			this.rmBtnEnregistrer.TabIndex = 19;
			this.rmBtnEnregistrer.Text = "Enregistrer";
			this.rmBtnEnregistrer.Title = "";
			this.rmBtnEnregistrer.UseVisualStyleBackColor = false;
			this.rmBtnEnregistrer.Click += new System.EventHandler(this.RmBtnEnregistrerClick);
			// 
			// rmBtnRecommencer
			// 
			this.rmBtnRecommencer.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.rmBtnRecommencer.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
			this.rmBtnRecommencer.BackColor = System.Drawing.Color.Transparent;
			this.rmBtnRecommencer.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
			this.rmBtnRecommencer.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
			this.rmBtnRecommencer.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
			this.rmBtnRecommencer.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
			this.rmBtnRecommencer.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.rmBtnRecommencer.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.rmBtnRecommencer.FadingSpeed = 35;
			this.rmBtnRecommencer.FlatAppearance.BorderSize = 0;
			this.rmBtnRecommencer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.rmBtnRecommencer.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rmBtnRecommencer.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
			this.rmBtnRecommencer.Image = global::MaCompta.Properties.Resources.edit_clear_2_64;
			this.rmBtnRecommencer.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Top;
			this.rmBtnRecommencer.ImageOffset = 0;
			this.rmBtnRecommencer.IsPressed = false;
			this.rmBtnRecommencer.KeepPress = false;
			this.rmBtnRecommencer.Location = new System.Drawing.Point(273, 184);
			this.rmBtnRecommencer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.rmBtnRecommencer.MaxImageSize = new System.Drawing.Point(50, 50);
			this.rmBtnRecommencer.MenuPos = new System.Drawing.Point(0, 0);
			this.rmBtnRecommencer.Name = "rmBtnRecommencer";
			this.rmBtnRecommencer.Radius = 11;
			this.rmBtnRecommencer.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
			this.rmBtnRecommencer.Size = new System.Drawing.Size(109, 80);
			this.rmBtnRecommencer.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
			this.rmBtnRecommencer.SplitDistance = 0;
			this.rmBtnRecommencer.TabIndex = 20;
			this.rmBtnRecommencer.Text = "Recommencer";
			this.rmBtnRecommencer.Title = "";
			this.rmBtnRecommencer.UseVisualStyleBackColor = false;
			this.rmBtnRecommencer.Click += new System.EventHandler(this.RmBtnRecommencerClick);
			// 
			// lbNom
			// 
			this.lbNom.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbNom.Location = new System.Drawing.Point(3, 0);
			this.lbNom.Name = "lbNom";
			this.lbNom.Size = new System.Drawing.Size(125, 45);
			this.lbNom.TabIndex = 0;
			this.lbNom.Text = "Nom";
			this.lbNom.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbDateDébut
			// 
			this.lbDateDébut.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbDateDébut.Location = new System.Drawing.Point(3, 45);
			this.lbDateDébut.Name = "lbDateDébut";
			this.lbDateDébut.Size = new System.Drawing.Size(125, 45);
			this.lbDateDébut.TabIndex = 2;
			this.lbDateDébut.Text = "Date de début";
			this.lbDateDébut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// lbDateFin
			// 
			this.lbDateFin.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lbDateFin.Location = new System.Drawing.Point(3, 90);
			this.lbDateFin.Name = "lbDateFin";
			this.lbDateFin.Size = new System.Drawing.Size(125, 45);
			this.lbDateFin.TabIndex = 4;
			this.lbDateFin.Text = "Date de Fin";
			this.lbDateFin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// tbNom
			// 
			this.tbNom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel.SetColumnSpan(this.tbNom, 3);
			this.tbNom.Location = new System.Drawing.Point(134, 9);
			this.tbNom.Margin = new System.Windows.Forms.Padding(3, 4, 18, 4);
			this.tbNom.MaxLength = 150;
			this.tbNom.Name = "tbNom";
			this.tbNom.Size = new System.Drawing.Size(372, 27);
			this.tbNom.TabIndex = 1;
			// 
			// ckAfficher
			// 
			this.ckAfficher.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.ckAfficher.Checked = true;
			this.ckAfficher.CheckState = System.Windows.Forms.CheckState.Checked;
			this.tableLayoutPanel.SetColumnSpan(this.ckAfficher, 3);
			this.ckAfficher.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ckAfficher.Location = new System.Drawing.Point(3, 139);
			this.ckAfficher.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.ckAfficher.Name = "ckAfficher";
			this.ckAfficher.Size = new System.Drawing.Size(387, 37);
			this.ckAfficher.TabIndex = 18;
			this.ckAfficher.Text = "Afficher ce projet dans les différentes listes";
			this.ckAfficher.UseVisualStyleBackColor = true;
			// 
			// dtpDateDébut
			// 
			this.dtpDateDébut.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.dtpDateDébut.Checked = false;
			this.tableLayoutPanel.SetColumnSpan(this.dtpDateDébut, 2);
			this.dtpDateDébut.Location = new System.Drawing.Point(134, 54);
			this.dtpDateDébut.Name = "dtpDateDébut";
			this.dtpDateDébut.ShowCheckBox = true;
			this.dtpDateDébut.Size = new System.Drawing.Size(256, 27);
			this.dtpDateDébut.TabIndex = 22;
			// 
			// dtpDateFin
			// 
			this.dtpDateFin.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.dtpDateFin.Checked = false;
			this.tableLayoutPanel.SetColumnSpan(this.dtpDateFin, 2);
			this.dtpDateFin.Location = new System.Drawing.Point(134, 99);
			this.dtpDateFin.Name = "dtpDateFin";
			this.dtpDateFin.ShowCheckBox = true;
			this.dtpDateFin.Size = new System.Drawing.Size(256, 27);
			this.dtpDateFin.TabIndex = 22;
			// 
			// epValidation
			// 
			this.epValidation.ContainerControl = this;
			// 
			// ProjetForm
			// 
			this.AcceptButton = this.rmBtnEnregistrer;
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
			this.BackgroundImage = global::MaCompta.Properties.Resources.Atra_Dot___Blue_1920x1200;
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.ClientSize = new System.Drawing.Size(551, 338);
			this.Controls.Add(this.tableLayoutPanel);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cbListe);
			this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Icon = global::MaCompta.Properties.Resources.file_manager_New1_50;
			this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.Name = "ProjetForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Projets";
			this.tableLayoutPanel.ResumeLayout(false);
			this.tableLayoutPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.epValidation)).EndInit();
			this.ResumeLayout(false);
		}

		#endregion Private Methods

		#region Protected Methods

		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#endregion Protected Methods
	}
}