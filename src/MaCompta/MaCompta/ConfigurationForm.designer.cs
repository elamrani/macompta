﻿/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 05/08/2011
 * Time: 10:28
 * 
 * © 1428-1432 (2007-2011) All rights reserved to Mohamed Y. ElAmrani.
 */
namespace MaCompta
{
	partial class ConfigurationForm
	{
		#region Fields

		private My.Forms.Controls.Panels.A1Panel.A1Panel a1Panel1;
		private System.Windows.Forms.Button btnParcourir1;
		private System.Windows.Forms.Button btnParcourir2;
		private System.Windows.Forms.ComboBox cbCompteZakatDue;
		private System.Windows.Forms.CheckBox ckLimitierNbFichiers;
		private System.Windows.Forms.CheckBox ckMaintenirSuppressionsTransactions; // End of Protected Method Dispose

		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private My.Forms.Controls.Misc.DigitalDisplayControl ddcNisab;
		private System.Windows.Forms.ErrorProvider epValidation;
		private System.Windows.Forms.GroupBox gbLimiteFichiers;
		private My.Forms.Controls.Buttons.GenericCommandLink gclObtenirPrix;
		private My.Forms.Controls.Buttons.GenericCommandLink gclObtenirPrix24hgold; // End of Protected Method Dispose
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14; // End of Protected Method Dispose
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label lblDatePrixGrammeOr;
		private My.Forms.Controls.Panels.MyInfoBar myInfoBar1;
		private System.Windows.Forms.NumericUpDown nudGrammesOr;
		private System.Windows.Forms.NumericUpDown nudLimiteNbFichiersArchives;
		private My.Forms.Controls.Misc.PasswordMeter.PasswordStrengthControl pscComplexité;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnEnregistrer;
		private My.Forms.Controls.Ribbon.RibbonMenuButton rmBtnValiderMontantNisab; // End of Protected Method Dispose
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TextBox tbArchive1;
		private System.Windows.Forms.TextBox tbArchive2;
		private System.Windows.Forms.TextBox tbMontantNisab;
		private System.Windows.Forms.TextBox tbMotPasse;
		private System.Windows.Forms.TextBox tbPrixGrammeOr;
		private System.Windows.Forms.TabPage tpArchives;
		private System.Windows.Forms.TabPage tpAvance; // End of Protected Method Dispose
		private System.Windows.Forms.TabPage tpSecurite;
		private System.Windows.Forms.TabPage tpZakat;

		#endregion Fields

		#region Private Methods

		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationForm));
			this.myInfoBar1 = new My.Forms.Controls.Panels.MyInfoBar();
			this.rmBtnEnregistrer = new My.Forms.Controls.Ribbon.RibbonMenuButton();
			this.label1 = new System.Windows.Forms.Label();
			this.tbMotPasse = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.tbPrixGrammeOr = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.epValidation = new System.Windows.Forms.ErrorProvider(this.components);
			this.label4 = new System.Windows.Forms.Label();
			this.tbArchive1 = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.tbArchive2 = new System.Windows.Forms.TextBox();
			this.btnParcourir1 = new System.Windows.Forms.Button();
			this.btnParcourir2 = new System.Windows.Forms.Button();
			this.a1Panel1 = new My.Forms.Controls.Panels.A1Panel.A1Panel();
			this.rmBtnValiderMontantNisab = new My.Forms.Controls.Ribbon.RibbonMenuButton();
			this.tbMontantNisab = new System.Windows.Forms.TextBox();
			this.ddcNisab = new My.Forms.Controls.Misc.DigitalDisplayControl();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.pscComplexité = new My.Forms.Controls.Misc.PasswordMeter.PasswordStrengthControl();
			this.label8 = new System.Windows.Forms.Label();
			this.ckMaintenirSuppressionsTransactions = new System.Windows.Forms.CheckBox();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tpArchives = new System.Windows.Forms.TabPage();
			this.ckLimitierNbFichiers = new System.Windows.Forms.CheckBox();
			this.gbLimiteFichiers = new System.Windows.Forms.GroupBox();
			this.nudLimiteNbFichiersArchives = new System.Windows.Forms.NumericUpDown();
			this.label13 = new System.Windows.Forms.Label();
			this.tpZakat = new System.Windows.Forms.TabPage();
			this.cbCompteZakatDue = new System.Windows.Forms.ComboBox();
			this.lblDatePrixGrammeOr = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.gclObtenirPrix24hgold = new My.Forms.Controls.Buttons.GenericCommandLink();
			this.gclObtenirPrix = new My.Forms.Controls.Buttons.GenericCommandLink();
			this.nudGrammesOr = new System.Windows.Forms.NumericUpDown();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.tpSecurite = new System.Windows.Forms.TabPage();
			this.tpAvance = new System.Windows.Forms.TabPage();
			((System.ComponentModel.ISupportInitialize)(this.epValidation)).BeginInit();
			this.a1Panel1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tpArchives.SuspendLayout();
			this.gbLimiteFichiers.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudLimiteNbFichiersArchives)).BeginInit();
			this.tpZakat.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudGrammesOr)).BeginInit();
			this.tpSecurite.SuspendLayout();
			this.tpAvance.SuspendLayout();
			this.SuspendLayout();
			// 
			// myInfoBar1
			// 
			this.myInfoBar1.BackColor = System.Drawing.Color.Transparent;
			this.myInfoBar1.BackStyle = My.Forms.Controls.Panels.BackStyle.Gradient;
			this.myInfoBar1.BorderSide = System.Windows.Forms.Border3DSide.Bottom;
			this.myInfoBar1.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
			this.myInfoBar1.Dock = System.Windows.Forms.DockStyle.Top;
			this.myInfoBar1.GradientEndColor = System.Drawing.Color.Transparent;
			this.myInfoBar1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
			this.myInfoBar1.GradientStartColor = System.Drawing.Color.White;
			this.myInfoBar1.Image = global::MaCompta.Properties.Resources.advanced_32;
			this.myInfoBar1.ImageAlign = My.Forms.Controls.Panels.ImageAlignment.TopLeft;
			this.myInfoBar1.ImageOffsetX = 2;
			this.myInfoBar1.ImageOffsetY = 20;
			this.myInfoBar1.Location = new System.Drawing.Point(0, 0);
			this.myInfoBar1.Name = "myInfoBar1";
			this.myInfoBar1.Size = new System.Drawing.Size(674, 75);
			this.myInfoBar1.TabIndex = 0;
			this.myInfoBar1.Text1 = "Configuration";
			this.myInfoBar1.Text1Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.myInfoBar1.Text1ForeColor = System.Drawing.SystemColors.ControlText;
			this.myInfoBar1.Text1OffsetX = 20;
			this.myInfoBar1.Text1OffsetY = 0;
			this.myInfoBar1.Text2 = "Modifier le options du programme";
			this.myInfoBar1.Text2Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
			this.myInfoBar1.Text2ForeColor = System.Drawing.SystemColors.ControlText;
			this.myInfoBar1.Text2OffsetX = 40;
			this.myInfoBar1.Text2OffsetY = 0;
			// 
			// rmBtnEnregistrer
			// 
			this.rmBtnEnregistrer.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.rmBtnEnregistrer.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
			this.rmBtnEnregistrer.BackColor = System.Drawing.Color.Transparent;
			this.rmBtnEnregistrer.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
			this.rmBtnEnregistrer.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
			this.rmBtnEnregistrer.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
			this.rmBtnEnregistrer.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
			this.rmBtnEnregistrer.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.rmBtnEnregistrer.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.rmBtnEnregistrer.FadingSpeed = 35;
			this.rmBtnEnregistrer.FlatAppearance.BorderSize = 0;
			this.rmBtnEnregistrer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.rmBtnEnregistrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rmBtnEnregistrer.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
			this.rmBtnEnregistrer.Image = global::MaCompta.Properties.Resources.dialog_ok_2_64;
			this.rmBtnEnregistrer.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Left;
			this.rmBtnEnregistrer.ImageOffset = 0;
			this.rmBtnEnregistrer.IsPressed = false;
			this.rmBtnEnregistrer.KeepPress = false;
			this.rmBtnEnregistrer.Location = new System.Drawing.Point(261, 300);
			this.rmBtnEnregistrer.Margin = new System.Windows.Forms.Padding(0);
			this.rmBtnEnregistrer.MaxImageSize = new System.Drawing.Point(50, 50);
			this.rmBtnEnregistrer.MenuPos = new System.Drawing.Point(0, 0);
			this.rmBtnEnregistrer.Name = "rmBtnEnregistrer";
			this.rmBtnEnregistrer.Radius = 15;
			this.rmBtnEnregistrer.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
			this.rmBtnEnregistrer.Size = new System.Drawing.Size(160, 49);
			this.rmBtnEnregistrer.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
			this.rmBtnEnregistrer.SplitDistance = 0;
			this.rmBtnEnregistrer.TabIndex = 16;
			this.rmBtnEnregistrer.Text = "Enregistrer";
			this.rmBtnEnregistrer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.rmBtnEnregistrer.Title = "";
			this.rmBtnEnregistrer.UseVisualStyleBackColor = false;
			this.rmBtnEnregistrer.Click += new System.EventHandler(this.RmBtnEnregistrerClick);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(50, 61);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(104, 23);
			this.label1.TabIndex = 17;
			this.label1.Text = "Mot de passe";
			// 
			// tbMotPasse
			// 
			this.tbMotPasse.Location = new System.Drawing.Point(160, 59);
			this.tbMotPasse.MaxLength = 50;
			this.tbMotPasse.Name = "tbMotPasse";
			this.tbMotPasse.Size = new System.Drawing.Size(225, 24);
			this.tbMotPasse.TabIndex = 18;
			this.tbMotPasse.TextChanged += new System.EventHandler(this.TbMotPasseTextChanged);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(589, 33);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(49, 23);
			this.label2.TabIndex = 17;
			this.label2.Text = "النصاب";
			// 
			// tbPrixGrammeOr
			// 
			this.tbPrixGrammeOr.Location = new System.Drawing.Point(129, 37);
			this.tbPrixGrammeOr.MaxLength = 50;
			this.tbPrixGrammeOr.Name = "tbPrixGrammeOr";
			this.tbPrixGrammeOr.Size = new System.Drawing.Size(77, 24);
			this.tbPrixGrammeOr.TabIndex = 18;
			this.tbPrixGrammeOr.Text = "0.00";
			this.tbPrixGrammeOr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.tbPrixGrammeOr.TextChanged += new System.EventHandler(this.TbPrixGrammeOrTextChanged);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 39);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(107, 23);
			this.label3.TabIndex = 17;
			this.label3.Text = "Prix d\'or (1 gr.)";
			// 
			// epValidation
			// 
			this.epValidation.ContainerControl = this;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(6, 6);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(218, 23);
			this.label4.TabIndex = 17;
			this.label4.Text = "Chemin de l\'Archive";
			// 
			// tbArchive1
			// 
			this.tbArchive1.Location = new System.Drawing.Point(242, 6);
			this.tbArchive1.MaxLength = 255;
			this.tbArchive1.Name = "tbArchive1";
			this.tbArchive1.Size = new System.Drawing.Size(357, 24);
			this.tbArchive1.TabIndex = 18;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(6, 42);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(218, 23);
			this.label5.TabIndex = 17;
			this.label5.Text = "Chemin de la copie de l\'archive";
			// 
			// tbArchive2
			// 
			this.tbArchive2.Location = new System.Drawing.Point(242, 41);
			this.tbArchive2.MaxLength = 255;
			this.tbArchive2.Name = "tbArchive2";
			this.tbArchive2.Size = new System.Drawing.Size(357, 24);
			this.tbArchive2.TabIndex = 18;
			// 
			// btnParcourir1
			// 
			this.btnParcourir1.Location = new System.Drawing.Point(605, 6);
			this.btnParcourir1.Name = "btnParcourir1";
			this.btnParcourir1.Size = new System.Drawing.Size(33, 27);
			this.btnParcourir1.TabIndex = 19;
			this.btnParcourir1.Text = "...";
			this.btnParcourir1.UseVisualStyleBackColor = true;
			this.btnParcourir1.Click += new System.EventHandler(this.BtnParcourir1Click);
			// 
			// btnParcourir2
			// 
			this.btnParcourir2.Location = new System.Drawing.Point(605, 38);
			this.btnParcourir2.Name = "btnParcourir2";
			this.btnParcourir2.Size = new System.Drawing.Size(33, 27);
			this.btnParcourir2.TabIndex = 19;
			this.btnParcourir2.Text = "...";
			this.btnParcourir2.UseVisualStyleBackColor = true;
			this.btnParcourir2.Click += new System.EventHandler(this.BtnParcourir2Click);
			// 
			// a1Panel1
			// 
			this.a1Panel1.BackColor = System.Drawing.Color.Transparent;
			this.a1Panel1.BorderColor = System.Drawing.Color.Transparent;
			this.a1Panel1.BorderWidth = 2;
			this.a1Panel1.Controls.Add(this.rmBtnValiderMontantNisab);
			this.a1Panel1.Controls.Add(this.tbMontantNisab);
			this.a1Panel1.Controls.Add(this.ddcNisab);
			this.a1Panel1.Controls.Add(this.label6);
			this.a1Panel1.GradientEndColor = System.Drawing.Color.DimGray;
			this.a1Panel1.GradientStartColor = System.Drawing.Color.Black;
			this.a1Panel1.Image = null;
			this.a1Panel1.ImageLocation = new System.Drawing.Point(4, 4);
			this.a1Panel1.Location = new System.Drawing.Point(347, 16);
			this.a1Panel1.Margin = new System.Windows.Forms.Padding(3, 4, 0, 4);
			this.a1Panel1.Name = "a1Panel1";
			this.a1Panel1.RoundCornerRadius = 19;
			this.a1Panel1.Size = new System.Drawing.Size(232, 54);
			this.a1Panel1.TabIndex = 23;
			// 
			// rmBtnValiderMontantNisab
			// 
			this.rmBtnValiderMontantNisab.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.rmBtnValiderMontantNisab.Arrow = My.Forms.Controls.Ribbon.RibbonMenuButton.e_arrow.None;
			this.rmBtnValiderMontantNisab.BackColor = System.Drawing.Color.Transparent;
			this.rmBtnValiderMontantNisab.ColorBase = System.Drawing.Color.FromArgb(((int)(((byte)(186)))), ((int)(((byte)(209)))), ((int)(((byte)(240)))));
			this.rmBtnValiderMontantNisab.ColorBaseStroke = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(59)))), ((int)(((byte)(66)))), ((int)(((byte)(76)))));
			this.rmBtnValiderMontantNisab.ColorOn = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(214)))), ((int)(((byte)(78)))));
			this.rmBtnValiderMontantNisab.ColorOnStroke = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(177)))), ((int)(((byte)(118)))));
			this.rmBtnValiderMontantNisab.ColorPress = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
			this.rmBtnValiderMontantNisab.ColorPressStroke = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
			this.rmBtnValiderMontantNisab.FadingSpeed = 35;
			this.rmBtnValiderMontantNisab.FlatAppearance.BorderSize = 0;
			this.rmBtnValiderMontantNisab.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.rmBtnValiderMontantNisab.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rmBtnValiderMontantNisab.GroupPos = My.Forms.Controls.Ribbon.RibbonMenuButton.e_groupPos.None;
			this.rmBtnValiderMontantNisab.Image = global::MaCompta.Properties.Resources.dialog_ok_2_64;
			this.rmBtnValiderMontantNisab.ImageLocation = My.Forms.Controls.Ribbon.RibbonMenuButton.e_imagelocation.Left;
			this.rmBtnValiderMontantNisab.ImageOffset = 0;
			this.rmBtnValiderMontantNisab.IsPressed = false;
			this.rmBtnValiderMontantNisab.KeepPress = false;
			this.rmBtnValiderMontantNisab.Location = new System.Drawing.Point(193, 20);
			this.rmBtnValiderMontantNisab.Margin = new System.Windows.Forms.Padding(0);
			this.rmBtnValiderMontantNisab.MaxImageSize = new System.Drawing.Point(50, 50);
			this.rmBtnValiderMontantNisab.MenuPos = new System.Drawing.Point(0, 0);
			this.rmBtnValiderMontantNisab.Name = "rmBtnValiderMontantNisab";
			this.rmBtnValiderMontantNisab.Radius = 15;
			this.rmBtnValiderMontantNisab.ShowBase = My.Forms.Controls.Ribbon.RibbonMenuButton.e_showbase.Yes;
			this.rmBtnValiderMontantNisab.Size = new System.Drawing.Size(25, 26);
			this.rmBtnValiderMontantNisab.SplitButton = My.Forms.Controls.Ribbon.RibbonMenuButton.e_splitbutton.No;
			this.rmBtnValiderMontantNisab.SplitDistance = 0;
			this.rmBtnValiderMontantNisab.TabIndex = 16;
			this.rmBtnValiderMontantNisab.Text = "Valider";
			this.rmBtnValiderMontantNisab.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.rmBtnValiderMontantNisab.Title = "";
			this.rmBtnValiderMontantNisab.UseVisualStyleBackColor = false;
			this.rmBtnValiderMontantNisab.Visible = false;
			this.rmBtnValiderMontantNisab.Click += new System.EventHandler(this.rmBtnValiderMontantNisab_Click);
			// 
			// tbMontantNisab
			// 
			this.tbMontantNisab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.tbMontantNisab.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.tbMontantNisab.Location = new System.Drawing.Point(5, 2);
			this.tbMontantNisab.Name = "tbMontantNisab";
			this.tbMontantNisab.Size = new System.Drawing.Size(185, 44);
			this.tbMontantNisab.TabIndex = 2;
			this.tbMontantNisab.Text = "0.00";
			this.tbMontantNisab.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.tbMontantNisab.Visible = false;
			this.tbMontantNisab.Leave += new System.EventHandler(this.tbMontantNisab_Leave);
			this.tbMontantNisab.MouseLeave += new System.EventHandler(this.tbMontantNisab_MouseLeave);
			// 
			// ddcNisab
			// 
			this.ddcNisab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Left)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.ddcNisab.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ddcNisab.BackColor = System.Drawing.Color.Transparent;
			this.ddcNisab.DigitColor = System.Drawing.Color.GreenYellow;
			this.ddcNisab.DigitText = "0.00";
			this.ddcNisab.Location = new System.Drawing.Point(10, 2);
			this.ddcNisab.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.ddcNisab.Name = "ddcNisab";
			this.ddcNisab.Size = new System.Drawing.Size(179, 44);
			this.ddcNisab.TabIndex = 0;
			this.ddcNisab.DoubleClick += new System.EventHandler(this.ddcNisab_DoubleClick);
			this.ddcNisab.MouseEnter += new System.EventHandler(this.ddcNisab_MouseEnter);
			// 
			// label6
			// 
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.label6.BackColor = System.Drawing.Color.Transparent;
			this.label6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
			this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.label6.Location = new System.Drawing.Point(189, 1);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(35, 40);
			this.label6.TabIndex = 1;
			this.label6.Text = "DH";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(16, 9);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(109, 23);
			this.label7.TabIndex = 17;
			this.label7.Text = "Nisab d\'or";
			// 
			// pscComplexité
			// 
			this.pscComplexité.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pscComplexité.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.pscComplexité.Location = new System.Drawing.Point(391, 59);
			this.pscComplexité.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.pscComplexité.Name = "pscComplexité";
			this.pscComplexité.Size = new System.Drawing.Size(203, 25);
			this.pscComplexité.TabIndex = 24;
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(391, 35);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(203, 21);
			this.label8.TabIndex = 17;
			this.label8.Text = "Complexité du mot de passe";
			// 
			// ckMaintenirSuppressionsTransactions
			// 
			this.ckMaintenirSuppressionsTransactions.AutoSize = true;
			this.ckMaintenirSuppressionsTransactions.Location = new System.Drawing.Point(47, 40);
			this.ckMaintenirSuppressionsTransactions.Name = "ckMaintenirSuppressionsTransactions";
			this.ckMaintenirSuppressionsTransactions.Size = new System.Drawing.Size(354, 17);
			this.ckMaintenirSuppressionsTransactions.TabIndex = 25;
			this.ckMaintenirSuppressionsTransactions.Text = "Maintenir la trace des suppressions et modifications des transactions";
			this.ckMaintenirSuppressionsTransactions.UseVisualStyleBackColor = true;
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Left)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Controls.Add(this.tpArchives);
			this.tabControl1.Controls.Add(this.tpZakat);
			this.tabControl1.Controls.Add(this.tpSecurite);
			this.tabControl1.Controls.Add(this.tpAvance);
			this.tabControl1.Location = new System.Drawing.Point(10, 81);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(652, 216);
			this.tabControl1.TabIndex = 26;
			// 
			// tpArchives
			// 
			this.tpArchives.Controls.Add(this.ckLimitierNbFichiers);
			this.tpArchives.Controls.Add(this.gbLimiteFichiers);
			this.tpArchives.Controls.Add(this.label4);
			this.tpArchives.Controls.Add(this.btnParcourir1);
			this.tpArchives.Controls.Add(this.label5);
			this.tpArchives.Controls.Add(this.btnParcourir2);
			this.tpArchives.Controls.Add(this.tbArchive1);
			this.tpArchives.Controls.Add(this.tbArchive2);
			this.tpArchives.Location = new System.Drawing.Point(4, 27);
			this.tpArchives.Name = "tpArchives";
			this.tpArchives.Padding = new System.Windows.Forms.Padding(3);
			this.tpArchives.Size = new System.Drawing.Size(644, 185);
			this.tpArchives.TabIndex = 2;
			this.tpArchives.Text = "Archives";
			this.tpArchives.UseVisualStyleBackColor = true;
			// 
			// ckLimitierNbFichiers
			// 
			this.ckLimitierNbFichiers.AutoSize = true;
			this.ckLimitierNbFichiers.Location = new System.Drawing.Point(16, 70);
			this.ckLimitierNbFichiers.Name = "ckLimitierNbFichiers";
			this.ckLimitierNbFichiers.Size = new System.Drawing.Size(15, 14);
			this.ckLimitierNbFichiers.TabIndex = 0;
			this.ckLimitierNbFichiers.UseVisualStyleBackColor = true;
			this.ckLimitierNbFichiers.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ckLimitierNbFichiers_MouseUp);
			// 
			// gbLimiteFichiers
			// 
			this.gbLimiteFichiers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
			            | System.Windows.Forms.AnchorStyles.Left)
			            | System.Windows.Forms.AnchorStyles.Right)));
			this.gbLimiteFichiers.Controls.Add(this.nudLimiteNbFichiersArchives);
			this.gbLimiteFichiers.Controls.Add(this.label13);
			this.gbLimiteFichiers.Enabled = false;
			this.gbLimiteFichiers.Location = new System.Drawing.Point(9, 68);
			this.gbLimiteFichiers.Name = "gbLimiteFichiers";
			this.gbLimiteFichiers.Size = new System.Drawing.Size(608, 56);
			this.gbLimiteFichiers.TabIndex = 20;
			this.gbLimiteFichiers.TabStop = false;
			this.gbLimiteFichiers.Text = "    Limiter le nombre de fichiers";
			// 
			// nudLimiteNbFichiersArchives
			// 
			this.nudLimiteNbFichiersArchives.Location = new System.Drawing.Point(509, 22);
			this.nudLimiteNbFichiersArchives.Minimum = new decimal(new int[] {
			1,
			0,
			0,
			0});
			this.nudLimiteNbFichiersArchives.Name = "nudLimiteNbFichiersArchives";
			this.nudLimiteNbFichiersArchives.Size = new System.Drawing.Size(60, 24);
			this.nudLimiteNbFichiersArchives.TabIndex = 26;
			this.nudLimiteNbFichiersArchives.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.nudLimiteNbFichiersArchives.Value = new decimal(new int[] {
			10,
			0,
			0,
			0});
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(15, 24);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(491, 18);
			this.label13.TabIndex = 1;
			this.label13.Text = "Limiter le nombre de fichiers à maintenir dans le(s) dossier(s) d\'archives à";
			// 
			// tpZakat
			// 
			this.tpZakat.Controls.Add(this.cbCompteZakatDue);
			this.tpZakat.Controls.Add(this.lblDatePrixGrammeOr);
			this.tpZakat.Controls.Add(this.label12);
			this.tpZakat.Controls.Add(this.gclObtenirPrix24hgold);
			this.tpZakat.Controls.Add(this.gclObtenirPrix);
			this.tpZakat.Controls.Add(this.nudGrammesOr);
			this.tpZakat.Controls.Add(this.label11);
			this.tpZakat.Controls.Add(this.label3);
			this.tpZakat.Controls.Add(this.label2);
			this.tpZakat.Controls.Add(this.tbPrixGrammeOr);
			this.tpZakat.Controls.Add(this.label10);
			this.tpZakat.Controls.Add(this.label9);
			this.tpZakat.Controls.Add(this.label14);
			this.tpZakat.Controls.Add(this.label7);
			this.tpZakat.Controls.Add(this.a1Panel1);
			this.tpZakat.Location = new System.Drawing.Point(4, 27);
			this.tpZakat.Name = "tpZakat";
			this.tpZakat.Padding = new System.Windows.Forms.Padding(3);
			this.tpZakat.Size = new System.Drawing.Size(644, 185);
			this.tpZakat.TabIndex = 1;
			this.tpZakat.Text = "الزكاة";
			this.tpZakat.UseVisualStyleBackColor = true;
			// 
			// cbCompteZakatDue
			// 
			this.cbCompteZakatDue.FormattingEnabled = true;
			this.cbCompteZakatDue.Location = new System.Drawing.Point(157, 138);
			this.cbCompteZakatDue.Name = "cbCompteZakatDue";
			this.cbCompteZakatDue.Size = new System.Drawing.Size(422, 26);
			this.cbCompteZakatDue.TabIndex = 28;
			// 
			// lblDatePrixGrammeOr
			// 
			this.lblDatePrixGrammeOr.AutoSize = true;
			this.lblDatePrixGrammeOr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDatePrixGrammeOr.Location = new System.Drawing.Point(168, 67);
			this.lblDatePrixGrammeOr.Name = "lblDatePrixGrammeOr";
			this.lblDatePrixGrammeOr.Size = new System.Drawing.Size(72, 16);
			this.lblDatePrixGrammeOr.TabIndex = 27;
			this.lblDatePrixGrammeOr.Text = "??/??/????";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.Location = new System.Drawing.Point(26, 67);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(142, 16);
			this.label12.TabIndex = 27;
			this.label12.Text = "Dernière mise à jour le";
			// 
			// gclObtenirPrix24hgold
			// 
			this.gclObtenirPrix24hgold.DescriptionText = "";
			this.gclObtenirPrix24hgold.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.gclObtenirPrix24hgold.ForeColor = System.Drawing.Color.Blue;
			this.gclObtenirPrix24hgold.HeaderText = "www.24hgold.com";
			this.gclObtenirPrix24hgold.Image = ((System.Drawing.Bitmap)(resources.GetObject("gclObtenirPrix24hgold.Image")));
			this.gclObtenirPrix24hgold.ImageScalingSize = new System.Drawing.Size(16, 16);
			this.gclObtenirPrix24hgold.Location = new System.Drawing.Point(246, 93);
			this.gclObtenirPrix24hgold.Name = "gclObtenirPrix24hgold";
			this.gclObtenirPrix24hgold.Size = new System.Drawing.Size(150, 21);
			this.gclObtenirPrix24hgold.TabIndex = 26;
			this.gclObtenirPrix24hgold.Click += new System.EventHandler(this.gclObtenirPrix24hgold_Click);
			// 
			// gclObtenirPrix
			// 
			this.gclObtenirPrix.DescriptionText = "Extraire le prix du site";
			this.gclObtenirPrix.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.gclObtenirPrix.HeaderText = "Obtenir le prix de l\'or";
			this.gclObtenirPrix.Image = global::MaCompta.Properties.Resources.rightarrow_36;
			this.gclObtenirPrix.ImageScalingSize = new System.Drawing.Size(32, 32);
			this.gclObtenirPrix.Location = new System.Drawing.Point(19, 86);
			this.gclObtenirPrix.Name = "gclObtenirPrix";
			this.gclObtenirPrix.Size = new System.Drawing.Size(415, 37);
			this.gclObtenirPrix.TabIndex = 26;
			this.gclObtenirPrix.Click += new System.EventHandler(this.gclObtenirPrix_Click);
			// 
			// nudGrammesOr
			// 
			this.nudGrammesOr.DecimalPlaces = 1;
			this.nudGrammesOr.Increment = new decimal(new int[] {
			5,
			0,
			0,
			65536});
			this.nudGrammesOr.Location = new System.Drawing.Point(129, 7);
			this.nudGrammesOr.Maximum = new decimal(new int[] {
			500,
			0,
			0,
			0});
			this.nudGrammesOr.Name = "nudGrammesOr";
			this.nudGrammesOr.Size = new System.Drawing.Size(77, 24);
			this.nudGrammesOr.TabIndex = 25;
			this.nudGrammesOr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.nudGrammesOr.ValueChanged += new System.EventHandler(this.nudGrammesOr_ValueChanged);
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(16, 40);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(107, 23);
			this.label11.TabIndex = 17;
			this.label11.Text = "Prix d\'1 gr. d\'or";
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(212, 40);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(71, 23);
			this.label10.TabIndex = 17;
			this.label10.Text = "dirhams";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(212, 9);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(71, 23);
			this.label9.TabIndex = 17;
			this.label9.Text = "grammes";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(16, 141);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(135, 23);
			this.label14.TabIndex = 17;
			this.label14.Text = "Compte Zakat due";
			// 
			// tpSecurite
			// 
			this.tpSecurite.Controls.Add(this.label8);
			this.tpSecurite.Controls.Add(this.tbMotPasse);
			this.tpSecurite.Controls.Add(this.pscComplexité);
			this.tpSecurite.Controls.Add(this.label1);
			this.tpSecurite.Location = new System.Drawing.Point(4, 27);
			this.tpSecurite.Name = "tpSecurite";
			this.tpSecurite.Padding = new System.Windows.Forms.Padding(3);
			this.tpSecurite.Size = new System.Drawing.Size(644, 185);
			this.tpSecurite.TabIndex = 0;
			this.tpSecurite.Text = "Sécurité";
			this.tpSecurite.UseVisualStyleBackColor = true;
			// 
			// tpAvance
			// 
			this.tpAvance.Controls.Add(this.ckMaintenirSuppressionsTransactions);
			this.tpAvance.Location = new System.Drawing.Point(4, 27);
			this.tpAvance.Name = "tpAvance";
			this.tpAvance.Padding = new System.Windows.Forms.Padding(3);
			this.tpAvance.Size = new System.Drawing.Size(644, 185);
			this.tpAvance.TabIndex = 3;
			this.tpAvance.Text = "Avancé";
			this.tpAvance.UseVisualStyleBackColor = true;
			// 
			// ConfigurationForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
			this.ClientSize = new System.Drawing.Size(674, 358);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.rmBtnEnregistrer);
			this.Controls.Add(this.myInfoBar1);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
			this.Icon = global::MaCompta.Properties.Resources.advanced_32_1;
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "ConfigurationForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Configuration";
			((System.ComponentModel.ISupportInitialize)(this.epValidation)).EndInit();
			this.a1Panel1.ResumeLayout(false);
			this.a1Panel1.PerformLayout();
			this.tabControl1.ResumeLayout(false);
			this.tpArchives.ResumeLayout(false);
			this.tpArchives.PerformLayout();
			this.gbLimiteFichiers.ResumeLayout(false);
			this.gbLimiteFichiers.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudLimiteNbFichiersArchives)).EndInit();
			this.tpZakat.ResumeLayout(false);
			this.tpZakat.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nudGrammesOr)).EndInit();
			this.tpSecurite.ResumeLayout(false);
			this.tpSecurite.PerformLayout();
			this.tpAvance.ResumeLayout(false);
			this.tpAvance.PerformLayout();
			this.ResumeLayout(false);
		}

		#endregion Private Methods

		#region Protected Methods

		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#endregion Protected Methods
	}
}