﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 07/09/1433
 * Time: 18:31
 * 
 * © 1428-1433 (2007-2012) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using Controller;

using Model;

using My.Forms.MsgBoxes;

using System;
using System.Drawing;
using System.Windows.Forms;

namespace MaCompta
{
	/// <summary>
	/// Description of PartenaireForm.
	/// </summary>
	public partial class PartenaireForm : Form
	{
		#region Fields

		long? _idChargé;

		private PartenaireController _controller;

		#endregion Fields

		#region Constructors

		public PartenaireForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();

			Initialisation();
		}

		#endregion Constructors

		#region Private Methods

		void CbListeSelectedIndexChanged(object sender, EventArgs e)
		{
			ChargerInformations(cbListe.Text, true);
		}

		void CbListeTextUpdate(object sender, EventArgs e)
		{
			ChargerInformations(cbListe.Text, false);
		}

		/// <summary>
		/// Charger les infos de la base de données
		/// </summary>
		/// <param name="nom">Nom du partenaire à charger</param>
		/// <param name="afficherErreur">Vrai pour afficher un message d'erreur lorsque
		/// le code ne permet pas de charger des infos,
		/// faux pour ne rien afficher</param>
		private void ChargerInformations(string nom, bool afficherErreur)
		{
			if ( string.IsNullOrEmpty(nom) ) {
				//Rien à charger
				return;
			}//if

			Model.Partenaire partenaire = new Model.Partenaire(Program.BD);
			Exception eValeurRetournée = partenaire.Charger(nom);
			if ( eValeurRetournée == null ) {
				_idChargé = partenaire.Id;
				tbNom.Text = partenaire.Nom;
				tbAdresse.Text = partenaire.Adresse;
				tbTéléphone1.Text = partenaire.Téléphone1;
				tbTéléphone2.Text = partenaire.Téléphone2;
				ltbWeb.Text = partenaire.Web;
				ltbEmail.Text = partenaire.Email;
				tbAssistant.Text = partenaire.Assistant;
				tbTéléphoneAssistant.Text = partenaire.TéléphoneAssistant;
				tbFonction.Text = partenaire.Fonction;
				ckAfficher.Checked = partenaire.Afficher;
			}//if
			else {
				//Une erreur est survenue
				MessageBox.Show("Une erreur est survenue lors du chargement des informations",
				                "Une erreur est survenue", MessageBoxButtons.OK,
				                MessageBoxIcon.Exclamation);
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( eValeurRetournée.Message );
				Recommencer(false, false);
			}//else
		}

		private void Enregistrer()
		{
			if ( VérifierInfos() ) {
				//Confirmation
				if ( MyMsgBox.Show("Confirmez-vous l'enregistrement de ces informations?",
				                   "Confirmation de l'enregistrement", MessageBoxButtons.YesNo,
				                   MessageBoxIcon.Question) != DialogResult.Yes
				   ) {
					return;
				}//if

				if ( _idChargé.HasValue ) {
					Modification();
				}//if
				else {
					//C'est un nouvel enregistrement à ajouter
					Nouveau();
				}//else
			}//if
		}

		/// <summary>
		/// Prépare l'IG
		/// </summary>
		private void Initialisation()
		{
			Exception eValeurRetournée = null;
			_controller = new PartenaireController(Program.BD);

			#region " Chargement des partenaires "
			eValeurRetournée = _controller.ChargerListe(cbListe, "partenaire", "nom");
			if ( eValeurRetournée != null ) {
				MyMsgBox msgbox = new MyMsgBox();
				msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
				msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
				msgbox.MainInstruction = "Erreur lors de la tentative de chargement de la liste des partenaires";
				msgbox.WindowTitle = "Erreur d'extraction de données";
				msgbox.Content = "La liste des patenaires n'a pas pu être chargée.";
				#if DEBUGr
				msgbox.ExpandedByDefault = true;
				#else
				msgbox.ExpandedByDefault = false;
				#endif
				msgbox.ExpandedInformation = eValeurRetournée.Message;
				msgbox.Show();
			}//if
			#endregion " Chargement des partenaires "

			_idChargé = null;
		}

		private void Modification()
		{
			try {
				Model.Partenaire partenaire = new Model.Partenaire(Program.BD);
				partenaire.Id = _idChargé.Value;
				partenaire.Nom = tbNom.Text.Trim();
				partenaire.Adresse = tbAdresse.Text.Trim();
				partenaire.Téléphone1 = tbTéléphone1.Text.Trim();
				partenaire.Téléphone2 = tbTéléphone2.Text.Trim();
				partenaire.Web = ltbWeb.Text.Trim();
				partenaire.Email = ltbEmail.Text.Trim();
				partenaire.Assistant = tbAssistant.Text.Trim();
				partenaire.TéléphoneAssistant= tbTéléphoneAssistant.Text.Trim();
				partenaire.Fonction = tbFonction.Text.Trim();
				partenaire.Afficher = ckAfficher.Checked;
			    Exception eValeurRetournée = partenaire.Modifier(partenaire.Id);
				if ( eValeurRetournée != null ) {
					throw eValeurRetournée;
				}//if
				else {
					Initialisation();
					Recommencer(true, false);
				}//else
			}//try
			catch ( Exception ex ) {
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( ex.Message );
				MyMsgBox msgbox = new MyMsgBox();
				msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
				msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
				msgbox.MainInstruction = "Erreur lors de la tentative " +
					"d'enregistrement du partenaire";
				msgbox.WindowTitle = "Erreur d'enregistrement des données";
				msgbox.Content = "Ce partenaire n'a pas pu être enregistré.";
				#if DEBUG
				msgbox.ExpandedByDefault = true;
				#else
				msgbox.ExpandedByDefault = false;
				#endif
				msgbox.ExpandedInformation = ex.Message;
				msgbox.Show();
			}//catch
		}

		private void Nouveau()
		{
			try {
				Model.Partenaire partenaire = new Model.Partenaire(Program.BD);
				partenaire.Nom = tbNom.Text.Trim();
				partenaire.Adresse = tbAdresse.Text.Trim();
				partenaire.Téléphone1 = tbTéléphone1.Text.Trim();
				partenaire.Téléphone2 = tbTéléphone2.Text.Trim();
				partenaire.Web = ltbWeb.Text.Trim();
				partenaire.Email = ltbEmail.Text.Trim();
				partenaire.Assistant = tbAssistant.Text.Trim();
				partenaire.TéléphoneAssistant = tbTéléphoneAssistant.Text.Trim();
				partenaire.Fonction = tbFonction.Text.Trim();
				partenaire.Afficher = ckAfficher.Checked;
				Exception eValeurRetournée = null;
				eValeurRetournée = partenaire.Ajouter();
				if ( eValeurRetournée != null ) {
					throw eValeurRetournée;
				}//if
				else {
					//Ajouter dans la liste
					cbListe.Items.Add(tbNom.Text);
					Recommencer(true, false);
				}//else
			}//try
			catch ( Exception ex ) {
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( ex.Message );
				MyMsgBox msgbox = new MyMsgBox();
				msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
				msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
				msgbox.MainInstruction = "Erreur lors de la tentative d'enregistrement du partenaire";
				msgbox.WindowTitle = "Erreur d'enregistrement des données";
				msgbox.Content = "Ce partenaire n'a pas pu être enregistré.";
				#if DEBUG
				msgbox.ExpandedByDefault = true;
				#else
				msgbox.ExpandedByDefault = false;
				#endif
				msgbox.ExpandedInformation = ex.Message;
				msgbox.Show();
			}//catch
		}

		/// <summary>
		/// Permet d'effacer tout le form
		/// </summary>
		/// <param name="effacerComboBox">true : effacer également le combobox;
		/// false: ne pas l'effacer (ne pas effacer la saisit de l'usager)</param>
		/// <param name="confirmer">Demander la confirmation?</param>
		private void Recommencer(bool effacerComboBox, bool confirmer)
		{
			if ( confirmer ) {
				//Confirmation
				if ( MyMsgBox.Show("Confirmez-vous l'effacement de toutes les informations " +
				                   "de cette fenêtre?",
				                   "Confirmation de l'effacement", MessageBoxButtons.YesNo,
				                   MessageBoxIcon.Question) != DialogResult.Yes
				   ) {
					return;
				}//
			}//if

			if ( effacerComboBox ) {
				cbListe.Text = string.Empty;
			}//if

			_idChargé = null;
			epValidation.Clear();

			tbNom.Text = string.Empty;
			tbAdresse.Text =string.Empty;
			tbTéléphone1.Text =string.Empty;
			tbTéléphone2.Text =string.Empty;
			ltbWeb.Text = string.Empty;
			ltbEmail.Text =string.Empty;
			tbAssistant.Text =string.Empty;
			tbTéléphoneAssistant.Text = string.Empty;
			tbFonction.Text = string.Empty;
			ckAfficher.Checked = true;
		}

		void RmBtnEnregistrerClick(object sender, EventArgs e)
		{
			Enregistrer();
		}

		void RmBtnRecommencerClick(object sender, EventArgs e)
		{
			Recommencer(true, true);
		}

		void RmBtnSupprimerClick(object sender, EventArgs e)
		{
			Supprimer();
		}

		private void Supprimer()
		{
			//Vérifier si les infos ont été chargées
			if ( !_idChargé.HasValue )
				return; //ne rien faire

			//Demander la confirmation
			MyMsgBoxResult mrRésultat;
			MyMsgBox msgBox = new MyMsgBox();
			{
				msgBox.Owner = this;
				msgBox.Buttons = new MyMsgBoxButton[] { new MyMsgBoxButton(MyMsgBoxResult.Yes),
					new MyMsgBoxButton(MyMsgBoxResult.No) };
				msgBox.Content = "Cette suppression entraînera la suppression de toutes les informations associées à ce partenaire!";
				msgBox.MainIcon = MyMsgBoxIcon.SecurityQuestion;
				msgBox.MainInstruction = "Etes-vous sûr de toujours vouloir supprimer de manière permanente ce partenaire?";
				msgBox.WindowTitle = "Suppression du partenaire";
				msgBox.DefaultButton = MyMsgBoxDefaultButton.Button2;
				msgBox.FooterIcon = MyMsgBoxIcon.SecurityWarning;
				msgBox.FooterText = "Cette opération ne peut pas être annulée.";
				msgBox.LockSystem = true;
				mrRésultat = msgBox.Show();
			}
			if ( mrRésultat == MyMsgBoxResult.No ) {
				return;
			}//if

			try {
				//Suppression
				Model.Partenaire partenaire = new Model.Partenaire(Program.BD);
				partenaire.Id = _idChargé.Value;
				Exception eValeurRetournée = partenaire.Supprimer();
				if ( eValeurRetournée != null ) {
					throw eValeurRetournée;
				}//if
				else {
					Initialisation();
					Recommencer(true, false);
				}//else
			}//try
			catch ( Exception ex ) {
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( ex.Message );
				MyMsgBox msgbox = new MyMsgBox();
				msgbox.Buttons = new MyMsgBoxButton[] {
					new MyMsgBoxButton(MyMsgBoxResult.OK)
				};
				msgbox.MainIcon = MyMsgBoxIcon.SecurityWarning;
				msgbox.MainInstruction = "Erreur lors de la tentative " +
					"de suppression du partenaire";
				msgbox.WindowTitle = "Erreur de suppression des données";
				msgbox.Content = "Ce partenaire n'a pas pu être supprimé correctement.";
				#if DEBUG
				msgbox.ExpandedByDefault = true;
				#else
				msgbox.ExpandedByDefault = false;
				#endif
				msgbox.ExpandedInformation = ex.Message;
				msgbox.Show();
			}//catch
		}

		/// <summary>
		/// Permet de vérifier si toutes les données saisies sont correctes
		/// </summary>
		/// <returns>true : tout est bon, false : un problème est survenu</returns>
		private bool VérifierInfos()
		{
			bool bValeurRetournée = true;
			epValidation.Clear();

			if ( string.IsNullOrEmpty(tbNom.Text.Trim()) ) {
				epValidation.SetError( tbNom, "Le nom du partenaire ne peut pas être vide");
				bValeurRetournée = false;
			}//if

			return bValeurRetournée;
		}

		#endregion Private Methods
	}
}