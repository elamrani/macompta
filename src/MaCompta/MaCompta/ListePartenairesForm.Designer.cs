﻿namespace MaCompta
{
	partial class ListePartenairesForm
	{
		#region " Fields "

		private My.Forms.Controls.ListViews.XPTable.Models.ColumnModel cmListe;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private My.Forms.Controls.Panels.MyInfoBar myInfoBar1;
		private My.Forms.Controls.ListViews.XPTable.Models.Table tblListe;
		private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcCrédit;
		private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcDate;
		private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcDébit;
		private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcDétails;
		private My.Forms.Controls.ListViews.XPTable.Models.TextColumn tcId;
		private My.Forms.Controls.ListViews.XPTable.Models.TableModel tmListe;

		public System.Windows.Forms.DateTimePicker dtpDateDébut;
		public System.Windows.Forms.DateTimePicker dtpDateFin;

		#endregion " Fields "

		#region " Private Methods "

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.myInfoBar1 = new My.Forms.Controls.Panels.MyInfoBar();
            this.tblListe = new My.Forms.Controls.ListViews.XPTable.Models.Table();
            this.cmListe = new My.Forms.Controls.ListViews.XPTable.Models.ColumnModel();
            this.tcId = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcDate = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcDétails = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcDébit = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tcCrédit = new My.Forms.Controls.ListViews.XPTable.Models.TextColumn();
            this.tmListe = new My.Forms.Controls.ListViews.XPTable.Models.TableModel();
            this.dtpDateDébut = new System.Windows.Forms.DateTimePicker();
            this.dtpDateFin = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pcListePartenaires = new My.Forms.Controls.Charts.PieChart.PieChart();
            ((System.ComponentModel.ISupportInitialize)(this.tblListe)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // myInfoBar1
            // 
            this.myInfoBar1.BackColor = System.Drawing.Color.Transparent;
            this.myInfoBar1.BackStyle = My.Forms.Controls.Panels.BackStyle.Gradient;
            this.myInfoBar1.BorderSide = System.Windows.Forms.Border3DSide.Bottom;
            this.myInfoBar1.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.myInfoBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.myInfoBar1.GradientEndColor = System.Drawing.Color.Transparent;
            this.myInfoBar1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Horizontal;
            this.myInfoBar1.GradientStartColor = System.Drawing.Color.White;
            this.myInfoBar1.Image = global::MaCompta.Properties.Resources.file_manager_New1_32;
            this.myInfoBar1.ImageAlign = My.Forms.Controls.Panels.ImageAlignment.TopLeft;
            this.myInfoBar1.ImageOffsetX = 2;
            this.myInfoBar1.ImageOffsetY = 10;
            this.myInfoBar1.Location = new System.Drawing.Point(0, 0);
            this.myInfoBar1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.myInfoBar1.Name = "myInfoBar1";
            this.myInfoBar1.Size = new System.Drawing.Size(917, 52);
            this.myInfoBar1.TabIndex = 1;
            this.myInfoBar1.Text1 = "Détails des Partenaires";
            this.myInfoBar1.Text1Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.myInfoBar1.Text1ForeColor = System.Drawing.SystemColors.ControlText;
            this.myInfoBar1.Text1OffsetX = 0;
            this.myInfoBar1.Text1OffsetY = 0;
            this.myInfoBar1.Text2 = "Informations sur les différents partenaires.";
            this.myInfoBar1.Text2Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.myInfoBar1.Text2ForeColor = System.Drawing.SystemColors.ControlText;
            this.myInfoBar1.Text2OffsetX = 20;
            this.myInfoBar1.Text2OffsetY = 0;
            // 
            // tblListe
            // 
            this.tblListe.AlternatingRowColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tblListe.ColumnModel = this.cmListe;
            this.tblListe.CustomEditKey = System.Windows.Forms.Keys.None;
            this.tblListe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblListe.EditStartAction = My.Forms.Controls.ListViews.XPTable.Editors.EditStartAction.CustomKey;
            this.tblListe.EnableHeaderContextMenu = false;
            this.tblListe.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.tblListe.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.tblListe.GridLines = My.Forms.Controls.ListViews.XPTable.Models.GridLines.Columns;
            this.tblListe.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.tblListe.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.tblListe.Location = new System.Drawing.Point(3, 3);
            this.tblListe.Name = "tblListe";
            this.tblListe.NoItemsText = "Aucun partenaire!";
            this.tblListe.Size = new System.Drawing.Size(879, 467);
            this.tblListe.TabIndex = 2;
            this.tblListe.TableModel = this.tmListe;
            this.tblListe.Text = "table1";
            this.tblListe.DoubleClick += new System.EventHandler(this.tblListe_DoubleClick);
            // 
            // cmListe
            // 
            this.cmListe.AutoAdustWidth = true;
            this.cmListe.Columns.AddRange(new My.Forms.Controls.ListViews.XPTable.Models.Column[] {
            this.tcId,
            this.tcDate,
            this.tcDétails,
            this.tcDébit,
            this.tcCrédit});
            this.cmListe.HeaderHeight = 29;
            // 
            // tcId
            // 
            this.tcId.Text = "Id";
            this.tcId.Visible = false;
            this.tcId.Width = 16;
            this.tcId.WidthPercentage = 1D;
            // 
            // tcDate
            // 
            this.tcDate.Text = "Date";
            this.tcDate.Width = 70;
            this.tcDate.WidthPercentage = 8D;
            // 
            // tcDétails
            // 
            this.tcDétails.Text = "Détails";
            this.tcDétails.Width = 641;
            this.tcDétails.WidthPercentage = 73D;
            // 
            // tcDébit
            // 
            this.tcDébit.Alignment = My.Forms.Controls.ListViews.XPTable.Models.ColumnAlignment.Right;
            this.tcDébit.Text = "Débit";
            this.tcDébit.Width = 79;
            this.tcDébit.WidthPercentage = 9D;
            // 
            // tcCrédit
            // 
            this.tcCrédit.Alignment = My.Forms.Controls.ListViews.XPTable.Models.ColumnAlignment.Right;
            this.tcCrédit.Text = "Crédit";
            this.tcCrédit.Width = 79;
            this.tcCrédit.WidthPercentage = 9D;
            // 
            // tmListe
            // 
            this.tmListe.RowHeight = 25;
            // 
            // dtpDateDébut
            // 
            this.dtpDateDébut.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpDateDébut.Checked = false;
            this.dtpDateDébut.Location = new System.Drawing.Point(284, 52);
            this.dtpDateDébut.Name = "dtpDateDébut";
            this.dtpDateDébut.ShowCheckBox = true;
            this.dtpDateDébut.Size = new System.Drawing.Size(268, 24);
            this.dtpDateDébut.TabIndex = 23;
            this.dtpDateDébut.ValueChanged += new System.EventHandler(this.dtpDateDébut_ValueChanged);
            this.dtpDateDébut.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dtpDateDébut_MouseUp);
            // 
            // dtpDateFin
            // 
            this.dtpDateFin.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpDateFin.Location = new System.Drawing.Point(662, 52);
            this.dtpDateFin.Name = "dtpDateFin";
            this.dtpDateFin.Size = new System.Drawing.Size(243, 24);
            this.dtpDateFin.TabIndex = 24;
            this.dtpDateFin.ValueChanged += new System.EventHandler(this.dtpDateFin_ValueChanged);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(172, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 18);
            this.label2.TabIndex = 21;
            this.label2.Text = "Date de début";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(572, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 18);
            this.label1.TabIndex = 22;
            this.label1.Text = "Date de fin";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 52);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(893, 504);
            this.tabControl1.TabIndex = 25;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tblListe);
            this.tabPage1.Location = new System.Drawing.Point(4, 27);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(885, 473);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Liste";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pcListePartenaires);
            this.tabPage2.Location = new System.Drawing.Point(4, 27);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(885, 473);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Graphe";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pcListePartenaires
            // 
            this.pcListePartenaires.AutoSizePie = true;
            this.pcListePartenaires.BackColor = System.Drawing.Color.Transparent;
            this.pcListePartenaires.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pcListePartenaires.Inclination = 1F;
            this.pcListePartenaires.Items.Add(new My.Forms.Controls.Charts.PieChart.PieChartItem(10D, System.Drawing.Color.Gray, "test", "", 0F));
            this.pcListePartenaires.Items.Add(new My.Forms.Controls.Charts.PieChart.PieChartItem(5D, System.Drawing.Color.Red, "test 2", "", 0F));
            this.pcListePartenaires.Location = new System.Drawing.Point(3, 3);
            this.pcListePartenaires.Name = "pcListePartenaires";
            this.pcListePartenaires.Radius = 261.438F;
            this.pcListePartenaires.Size = new System.Drawing.Size(879, 467);
            this.pcListePartenaires.TabIndex = 0;
            this.pcListePartenaires.Text = "Liste des Partenaires";
            this.pcListePartenaires.Thickness = 50F;
            // 
            // ListePartenairesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MaCompta.Properties.Resources.Atra_Dot___White_1920x1200;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(917, 568);
            this.Controls.Add(this.dtpDateDébut);
            this.Controls.Add(this.dtpDateFin);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.myInfoBar1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ListePartenairesForm";
            this.Text = "Liste des Partenaires";
            this.Shown += new System.EventHandler(this.ListeProjetsForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.tblListe)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

		} // End of Private Method InitializeComponent

		#endregion " Private Methods "

		#region " Protected Methods "

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if ( disposing && (components != null) )
			{
			    components.Dispose();
			}
			base.Dispose(disposing);
		}

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private My.Forms.Controls.Charts.PieChart.PieChart pcListePartenaires; // End of Protected Method Dispose

		#endregion " Protected Methods "
	} // End of None Type ListePartenairesForm
}