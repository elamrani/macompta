﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 05/08/1433
 * Time: 19:10
 * 
 * © 1428-1433 (2007-2012) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using System;

namespace Model
{
	/// <summary>
	/// Description of Partenaire.
	/// </summary>
	public class Partenaire : OpérationBaseDonnées
	{
		#region Fields

		string _adresse;
		bool _afficher;
		string _assistant;
		string _email;
		string _fonction;
		long _id;
		string _nom;
		string _téléphone1;
		string _téléphone2;
		string _téléphoneAssistant;
		string _web;

		#endregion Fields

		#region Constructors

		public Partenaire(BaseDonnées bd)
		{
			Connection = bd.Connection;
			Command = bd.Command;
			Initialisation();
		}

		/// <summary>
		/// Permet d'instancier un partenaire dont le nom est précisé
		/// </summary>
		/// <param name="bd">la bd à utiliser</param>
		/// <param name="nomACharger">le nom à charger</param>
		/// <param name="valeurRetournée">null = le chargement des infos s'est bien déroulé; 
		/// sinon l'exception est retournée</param>
		public Partenaire(BaseDonnées bd, string nomACharger, ref Exception valeurRetournée)
		{
			Connection = bd.Connection;
			Command = bd.Command;
			Initialisation();
			valeurRetournée = Charger(nomACharger);
		}

		/// <summary>
		/// Permet d'instancier un partenaire dont le id est précisé
		/// </summary>
		/// <param name="bd">la bd à utiliser</param>
		/// <param name="idACharger">le id à charger</param>
		/// <param name="valeurRetournée">null = le chargement des infos s'est bien déroulé; 
		/// sinon l'exception est retournée</param>
		public Partenaire(BaseDonnées bd, long idACharger, ref Exception valeurRetournée)
		{
			Connection = bd.Connection;
			Command = bd.Command;
			Initialisation();
			valeurRetournée = Charger(idACharger);
		}

		#endregion Constructors

		#region Public Properties

		public string Adresse
		{
			get { return _adresse; }
			set { _adresse = value; }
		}

		public bool Afficher
		{
			get { return _afficher; }
			set { _afficher = value; }
		}

		public string Assistant
		{
			get { return _assistant; }
			set { _assistant = value; }
		}

		public string Email
		{
			get { return _email; }
			set { _email = value; }
		}

		public string Fonction
		{
			get { return _fonction; }
			set { _fonction = value; }
		}

		public long Id
		{
			get { return _id; }
			set { _id = value; }
		}

		public string Nom
		{
			get { return _nom; }
			set { _nom = value; }
		}

		public string Téléphone1
		{
			get { return _téléphone1; }
			set { _téléphone1 = value; }
		}

		public string Téléphone2
		{
			get { return _téléphone2; }
			set { _téléphone2 = value; }
		}

		public string TéléphoneAssistant
		{
			get { return _téléphoneAssistant; }
			set { _téléphoneAssistant = value; }
		}

		public string Web
		{
			get { return _web; }
			set { _web = value; }
		}

		#endregion Public Properties

		#region Private Methods

		private void Initialisation()
		{
			_id = -1;
			_nom = string.Empty;
			_adresse = null;
			_téléphone1 = null;
			_téléphone2 = null;
			_web = null;
			_email = null;
			_assistant = null;
			_fonction = null;
			_téléphoneAssistant = null;
			_afficher = true;
		}

		#endregion Private Methods

		#region Public Methods

		public override Exception Ajouter()
		{
			Exception eValeurRetour = null;
			try {
				string sRequête = "INSERT INTO `Partenaire` (`nom`, `adresse`, `tel1`, " +
			        "`tel2`, `web`, `email`, `assistant`, `fonction`, " +
					"`tel_Assistant`, `afficher`) VALUES (" + Formatter(_nom) +
					", " + Formatter(_adresse) +
					", " + Formatter(_téléphone1) +
					", " + Formatter(_téléphone2) +
					", " + Formatter(_web) +
					", " + Formatter(_email) +
					", " + Formatter(_assistant) +
					", " + Formatter(_fonction) +
					", " + Formatter(_téléphoneAssistant) +
					", " + Formatter(_afficher) +
					")\0";
			    long? lIdRetourné = null;
			    eValeurRetour = ExécuterRequête(sRequête, ref lIdRetourné);
			    if ( eValeurRetour != null )
			    {
			        throw eValeurRetour;
			    }//if
			    else if ( lIdRetourné.HasValue )
			    {
			        _id = lIdRetourné.Value;
			    }//else if
				
			}// try
			catch (Exception ex) {
				eValeurRetour = ex;
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( ex.Message );
			}// catch
			return eValeurRetour;
		}

		/// <summary>
		/// Chargement des informations
		/// </summary>
		/// <param name="id">le id du partenaire à charger</param>
		/// <returns>null si tout est bon, sinon, l'exception de l'erreur</returns>
		public Exception Charger(long id)
		{
			Exception eValeurRetour = null;
			try {
				Command.CommandText = "SELECT * " +
					"FROM `partenaire` " +
					"WHERE `id` = \"" + id + "\"\0";
				Debug();
				Reader = Command.ExecuteReader();
				if ( Reader.Read() ) {
					_id = long.Parse(Reader["id"].ToString());
					_nom = Reader["nom"].ToString();
					_adresse = Reader["adresse"].ToString();
					_téléphone1 = Reader["tel1"].ToString();
					_téléphone2 = Reader["tel2"].ToString();
					_web = Reader["web"].ToString();
					_email = Reader["email"].ToString();
					_assistant = Reader["assistant"].ToString();
					_téléphoneAssistant = Reader["tel_assistant"].ToString();
					_fonction = Reader["fonction"].ToString();
					_afficher = bool.Parse(Reader["afficher"].ToString());
				}//if
				Reader.Close();
			}//try
			catch (Exception e) {
				if ( !Reader.IsClosed ) {
					Reader.Close();
				}//if				
				eValeurRetour = e;
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( e.Message );
			}//catch

			return eValeurRetour;
		}

		/// <summary>
		/// Chargement des informations
		/// </summary>
		/// <param name="nom">le nom du partenaire à charger</param>
		/// <returns>null si tout est bon, sinon, l'exception de l'erreur</returns>
		public Exception Charger(string nom)
		{
			Exception eValeurRetour = null;
			try {
				Command.CommandText = "SELECT * " +
					"FROM `partenaire` " +
					"WHERE `nom` = \"" + nom + "\"\0";
				Debug();
				Reader = Command.ExecuteReader();
				if ( Reader.Read() ) {
					_id = long.Parse(Reader["id"].ToString());
					_nom = nom;
					_adresse = Reader["adresse"].ToString();
					_téléphone1 = Reader["tel1"].ToString();
					_téléphone2 = Reader["tel2"].ToString();
					_web = Reader["web"].ToString();
					_email = Reader["email"].ToString();
					_assistant = Reader["assistant"].ToString();
					_téléphoneAssistant = Reader["tel_assistant"].ToString();
					_fonction = Reader["fonction"].ToString();
					_afficher = bool.Parse(Reader["afficher"].ToString());
				}//if
				Reader.Close();
			}//try
			catch (Exception e) {
				if ( !Reader.IsClosed ) {
					Reader.Close();
				}//if				
				eValeurRetour = e;
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( e.Message );
			}//catch

			return eValeurRetour;
		}

		public override Exception Modifier(long _ancienId)
		{
			Exception eValeurRetour = null;
			try {
				string sRequête = "UPDATE `partenaire` SET " +
			        "`id` = " + Formatter(_id) +
			        ", `nom` = " + Formatter(_nom) +
					", `adresse`= " + Formatter(_adresse) +
					", `tel1` = " + Formatter(_téléphone1) +
					", `tel2` = " + Formatter(_téléphone2) +
					", `web` = " + Formatter(_web) +
					", `email` = " + Formatter(_email) +
					", `assistant` = " + Formatter(_assistant) +
					", `fonction` = " + Formatter(_fonction) +
					", `tel_assistant` = " + Formatter(_téléphoneAssistant) +
					", `afficher` = " + Formatter(_afficher) +
			        " WHERE `id` = " + Formatter(_ancienId) +
					"\0";
			    long? lIdRetourné = null;
			    eValeurRetour = ExécuterRequête(sRequête, ref lIdRetourné);
			    if ( eValeurRetour != null )
			    {
			        throw eValeurRetour;
			    }//if
			}// try
			catch (Exception ex) {
				eValeurRetour = ex;
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( ex.Message );
			}// catch
			return eValeurRetour;
		}

		public override Exception Supprimer()
		{
			return Supprimer(NomsTables.partenaire, _id);
		}

		#endregion Public Methods
	}
}