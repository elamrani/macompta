﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 05/08/1433
 * Time: 20:11
 * 
 * © 1428-1433 (2007-2012) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using System;

namespace Model
{
	/// <summary>
	/// Description of Configuration.
	/// </summary>
	public class Configuration : OpérationBaseDonnées
	{
		#region Fields

		private string _cheminArchive;
		private string _cheminCopieArchive;
		private long _compte_zakat_due;
		private DateTime _date_prix_gramme_or;
		private Exception _exceptionRetournée;
		private double _grammes_or;
		private long _id;
		private bool _maintenirSuppressionsTransactions;
		private string _motPasse;
		private double _nisab;
		private double _prix_gramme_or;

		#endregion Fields

		#region Constructors

		public Configuration(BaseDonnées bd, ref Exception exceptionRetournée)
		{
			Connection = bd.Connection;
			Command = bd.Command;

			Initialisation();
			exceptionRetournée = Charger();
		}

		#endregion Constructors

		#region Public Properties

		public string CheminArchive
		{
			get { return _cheminArchive; }
			set { _cheminArchive = value; }
		}

		public string CheminCopieArchive
		{
			get { return _cheminCopieArchive; }
			set { _cheminCopieArchive = value; }
		}

		public long CompteZakatDue
		{
			get { return _compte_zakat_due; }
			set { _compte_zakat_due = value; }
		}

		public DateTime DatePrixGrammeOr
		{
			get { return _date_prix_gramme_or; }
			set { _date_prix_gramme_or = value; }
		}

		public Exception ExceptionRetournée
		{
			get { return _exceptionRetournée; }
		}

		public double GrammesOr
		{
			get { return _grammes_or; }
			set { _grammes_or = value; }
		}

		public long Id
		{
			get { return _id; }
			set { _id = value; }
		}

		public bool MaintenirSuppressionsTransactions
		{
			get { return _maintenirSuppressionsTransactions; }
			set { _maintenirSuppressionsTransactions = value; }
		}

		public double MontantNisab
		{
			get {
			    return _prix_gramme_or * _grammes_or;
			}
		}

		/// <summary>
		/// Mot de passe choisi par l'usager pour sécuriser les données du logiciel
		/// </summary>
		public string MotPasseLogiciel
		{
			get { return _motPasse; }
			set { _motPasse = value; }
		}

		public double Nisab
		{
			get { return _nisab; }
			set { _nisab = value; }
		}

		public double PrixGrammeOr
		{
			get { return _prix_gramme_or; }
			set { _prix_gramme_or = value; }
		}

		#endregion Public Properties

		#region Private Methods

		private void Initialisation()
		{
			_motPasse = null;
			_id = -1;
			_compte_zakat_due = -1;
			_nisab = 0;
			_grammes_or = 0;
			_prix_gramme_or = 0;
			_cheminArchive = null;
			_cheminCopieArchive = null;
			_exceptionRetournée = null;
			_maintenirSuppressionsTransactions = false;
		}

		#endregion Private Methods

		#region Public Methods

		public override Exception Ajouter()
		{
			return new NotImplementedException("Opération d'ajout non supportée!");
		}

		public Exception Charger()
		{
			Exception eValeurRetour = null;
			try {
			    Command.CommandText = "SELECT * " +
			        "FROM `configuration`\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    if (Reader.Read()) {
			        _id = long.Parse(Reader["id"].ToString());
			        _motPasse = Reader["mot_passe"].ToString();
			        try {
			            _date_prix_gramme_or = DateTime.Parse(Reader["date_prix_gramme_or"].ToString());
			        }//try
			        catch (Exception ex) {
			            System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			            System.Diagnostics.Debug.WriteLine("date_prix_gramme_or MAY BE NULL: " + ex.Message);
			        }//catch
			        _prix_gramme_or = double.Parse(Reader["prix_gramme_or"].ToString());
			        _grammes_or = double.Parse(Reader["grammes_or"].ToString());
			        _nisab = double.Parse(Reader["nisab"].ToString());
			        _cheminArchive = Reader["chemin_archive"].ToString();
			        _cheminCopieArchive = Reader["chemin_copie_archive"].ToString();
			        _maintenirSuppressionsTransactions = bool.Parse(Reader["maintenir_suppressions_transactions"].ToString());
			        try {
			            _compte_zakat_due = long.Parse(Reader["id_compte_zakat_due"].ToString());
			        }//try
			        catch {
			            _compte_zakat_due = -1;
			        }//catch
			    }//if
			    Reader.Close();
			}//try
			catch (Exception e) {
			    if (!Reader.IsClosed) {
			        Reader.Close();
			    }//if
			    eValeurRetour = e;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(e.Message);
			}//catch

			return eValeurRetour;
		}

		public override Exception Modifier(long _ancienId)
		{
			Exception eValeurRetour = null;
			try {
			    string sRequête = "UPDATE `configuration` SET " +
			        "`id` = " + Formatter(_id) +
			        ", `mot_passe` = " + Formatter(_motPasse, false) +
			        ", `date_prix_gramme_or` = " + Formatter(_date_prix_gramme_or) +
			        ", `prix_gramme_or` = " + Formatter(_prix_gramme_or) +
			        ", `grammes_or` = " + Formatter(_grammes_or) +
			        ", `nisab` = " + Formatter(_nisab) +
			        ", `chemin_archive` = " + Formatter(_cheminArchive) +
			        ", `chemin_copie_archive` = " + Formatter(_cheminCopieArchive) +
			        ", `maintenir_suppressions_transactions` = " + Formatter(_maintenirSuppressionsTransactions) +
			        ", `id_compte_zakat_due` = " + (_compte_zakat_due == -1 ? "NULL" : Formatter(_compte_zakat_due)) +
			        " WHERE `id` = " + Formatter(_ancienId) +
			        "\0";
			    long? lIdRetourné = null;
			    eValeurRetour = ExécuterRequête(sRequête, ref lIdRetourné);
			    if (eValeurRetour != null) {
			        throw eValeurRetour;
			    }//if
			}// try
			catch (Exception ex) {
			    eValeurRetour = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}// catch
			return eValeurRetour;
		}

		public override Exception Supprimer()
		{
			return new NotImplementedException("Opération de suppression non permise!");
		}

		#endregion Public Methods
	}
}