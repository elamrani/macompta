﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 09/08/1433
 * Time: 16:19
 * 
 * © 1428-1433 (2007-2012) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using System;
using System.Collections.Generic;

namespace Model
{
	/// <summary>
	/// Description of Transaction.
	/// </summary>
	public class Transaction : FonctionalitéBaseDonnées
	{
		#region Fields

		Conciliation _conciliation;
		List<ContenuTransaction> _contenuTransaction;
		DateTime _dateTransaction;
		long _id;
		string _numéroChèque;
		Partenaire _partenaire;

		private string _description;

		#endregion Fields

		#region Constructors

		public Transaction(BaseDonnées bd)
		{
			Connection = bd.Connection;
			Command = bd.Command;
			Initialisation();
		}

		#endregion Constructors

		#region Public Properties

		public Conciliation Conciliation
		{
			get { return _conciliation; }
			set { _conciliation = value; }
		}

		public List<ContenuTransaction> ContenuTransaction
		{
			get { return _contenuTransaction; }
			set { _contenuTransaction = value; }
		}

		public DateTime DateTransaction
		{
			get { return _dateTransaction; }
			set { _dateTransaction = value; }
		}

		public string Description
		{
			get { return _description; }
			set { _description = value; }
		}

		public long Id
		{
			get { return _id; }
			set { _id = value; }
		}

		public string NuméroChèque
		{
			get { return _numéroChèque; }
			set { _numéroChèque = value; }
		}

		public Partenaire Partenaire
		{
			get { return _partenaire; }
			set { _partenaire = value; }
		}

		#endregion Public Properties

		#region Private Methods

		/// <summary>
		/// initialisation des champs
		/// </summary>
		private void Initialisation()
		{
			_id = -1;
			_dateTransaction = DateTime.Now;
			_partenaire = null;
			_conciliation = null;
			_numéroChèque = null;
			_contenuTransaction = new List<ContenuTransaction>();
			TransactionSeule = true;
		}

		#endregion Private Methods

		#region Public Methods

		/// <summary>
		/// Permet d'ajouter une transaction et son contenu et ce à l'intérieur d'une transaction (BD)
		/// </summary>
		/// <returns>null si tout c bien passé sinon l'exception</returns>
		public Exception Ajouter()
		{
			Exception eValeurRetour = null;
			try {
			    if ( TransactionSeule )
			    {
				    Transaction = Connection.BeginTransaction();
				    System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				    System.Diagnostics.Debug.WriteLine( "--- BeginTransaction ---" );
			    }//if

				string sRequête = "INSERT INTO `transaction` (`date`, " +
			        "`id_partenaire`, `id_conciliation`, `description`, `numero_cheque`) VALUES (" + Formatter(_dateTransaction) +
			        ", " + (_partenaire != null ? _partenaire.Id.ToString() : NULL) +
			        ", " + (_conciliation != null ? _conciliation.Id.ToString() : NULL) +
					", " + Formatter(_description) +
					", " + Formatter(_numéroChèque) +
					")\0";
			    long? lIdRetourné = null;
			    eValeurRetour = ExécuterRequête(sRequête, ref lIdRetourné);
			    if ( eValeurRetour != null )
			    {
			        throw eValeurRetour;
			    }//if
			    else if ( lIdRetourné.HasValue )
			    {
			        _id = lIdRetourné.Value;
			    }//else if
				
				//Ajout du contenu
				foreach ( ContenuTransaction ct in _contenuTransaction ) {
			        ct.Connection = Connection;
			        ct.Command = Command;
			        ct.Transaction = Transaction;
			        ct.Opération.Id = _id;
					eValeurRetour = ct.Ajouter();
					if ( eValeurRetour != null ) {
						throw eValeurRetour;
					}//if
				}//foreach

			    if ( TransactionSeule )
			    {
				    //Valider
				    Transaction.Commit();
				    System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				    System.Diagnostics.Debug.WriteLine( "--- Commit ---" );
			    }//if
			}// try
			catch (Exception ex) {
			    try
			    {
			        Transaction.Rollback();
			        System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine("--- Rollback ---");
			    }//try
			    catch ( Exception e )
			    {
			        System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine(e.Message);
			    }//catch
				eValeurRetour = ex;
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( ex.Message );
			}// catch
			return eValeurRetour;
		}

		/// <summary>
		/// Charge une transaction à partir de la BD
		/// </summary>
		/// <param name="idTransaction">id de la transaction à charger</param>
		/// <returns>null si tout c'est bien déroulé, sinon l'exception est retournée.</returns>
		public Exception Charger(long idTransaction)
		{
			Exception eValeurRetour = null;
			try
			{
			    string sRequête = "SELECT * FROM `charger_transaction` WHERE `id` = " + idTransaction + "\0";
			    Command.CommandText = sRequête;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(Command.CommandText);
			    Reader = Command.ExecuteReader();
			    //transaction
			    if ( Reader.Read() )
			    {
			        _id = idTransaction;
			        if ( !Reader.IsDBNull(Reader.GetOrdinal("id_partenaire")) )
			        {
			            _partenaire = new Model.Partenaire(this);
			            _partenaire.Id = long.Parse(Reader["id_partenaire"].ToString());
			        }//if
			        if ( !Reader.IsDBNull(Reader.GetOrdinal("id_conciliation")) )
			        {
			            _conciliation = new Conciliation(this);
			            _conciliation.Id = long.Parse(Reader["id_conciliation"].ToString());
			        }//if
			        _dateTransaction = DateTime.Parse(Reader["date"].ToString());
			        _description = Reader["description"].ToString();
			        _numéroChèque = Reader["numero_cheque"].ToString();
			        do
			        {
			            ContenuTransaction ctLigne = new ContenuTransaction(this, this);
			            ctLigne.Compte.Id = long.Parse(Reader["id_compte"].ToString());
			            if ( !Reader.IsDBNull(Reader.GetOrdinal("id_projet")) )
			            {
			                ctLigne.Projet = new Projet(this);
			                ctLigne.Projet.Id = long.Parse(Reader["id_projet"].ToString());
			            }//if
			            if ( !Reader.IsDBNull(Reader.GetOrdinal("description")) )
			            {
			                ctLigne.Description = Reader["description"].ToString();
			            }//if
			            if ( !Reader.IsDBNull(Reader.GetOrdinal("debit")) )
			            {
			                ctLigne.Débit = double.Parse(Reader["debit"].ToString());
			            }//if
			            if ( !Reader.IsDBNull(Reader.GetOrdinal("credit")) )
			            {
			                ctLigne.Crédit = double.Parse(Reader["credit"].ToString());
			            }//if
			            _contenuTransaction.Add(ctLigne);
			        } while ( Reader.Read() );
			        Reader.Close();
			        //Chargement des informations associées
			        if ( _partenaire != null )
			        {
			            eValeurRetour = _partenaire.Charger(_partenaire.Id);
			            if ( eValeurRetour != null )
			            {
			                throw eValeurRetour;
			            }//if
			        }//if
			        if ( _conciliation != null )
			        {
			            eValeurRetour = _conciliation.Charger(_conciliation.Id);
			            if ( eValeurRetour != null )
			            {
			                throw eValeurRetour;
			            }//if
			        }//if

			        foreach ( ContenuTransaction ligne in _contenuTransaction )
			        {
			            eValeurRetour = ligne.Compte.Charger(ligne.Compte.Id);
			            if ( eValeurRetour != null )
			            {
			                throw eValeurRetour;
			            }//if

			            if ( ligne.Projet != null )
			            {
			                eValeurRetour = ligne.Projet.Charger(ligne.Projet.Id);
			                if ( eValeurRetour != null )
			                {
			                    throw eValeurRetour;
			                }//if
			            }//if
			        }//foreach

			    }// if
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if
			}// try
			catch ( Exception ex )
			{
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if
			    eValeurRetour = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}// catch
			return eValeurRetour;
		}

		/// <summary>
		/// Permet de mettre à jour l'enregistrement dans la BD
		/// </summary>
		/// <param name="ancienId">L'ancien Id de l'enregistrement à modifier</param>
		/// <param name="maintenirTrace">Doit-on maintenir la trace des modifications?</param>
		/// <returns>null si tout se passe bien, l'exception de l'erreur sinon</returns>
		public Exception Modifier(long ancienId, bool maintenirTrace)
		{
			Exception eValeurRetour = null;
			try
			{
			    Transaction = Connection.BeginTransaction();
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine("--- BeginTransaction ---");
			    if ( maintenirTrace )
			    {
			        #region "Maintenir la trace de modification"
					TransactionSeule = false;
			        Transaction ancienne_transaction = new Transaction(this);
			        ancienne_transaction.TransactionSeule = false;
			        eValeurRetour = ancienne_transaction.Charger(ancienId);
			        if ( eValeurRetour != null )
			        {
			            throw eValeurRetour;
			        }//if
			        eValeurRetour = ancienne_transaction.Supprimer(true);
			        if ( eValeurRetour != null )
			        {
			            throw eValeurRetour;
			        }//if

			        eValeurRetour = Ajouter();
			        if ( eValeurRetour != null )
			        {
			            throw eValeurRetour;
			        }//if
			        #endregion "Maintenir la trace de modification"
			    }//if
			    else
			    {
			        #region "Sans Traces"
			        //Modification de la transaction
			        Command.CommandText = "UPDATE `transaction` SET `date` = " + Formatter(_dateTransaction) +
			            ", `id_partenaire` = " + (_partenaire != null ? _partenaire.Id.ToString() : NULL) +
			            ", `id_conciliation` = " + (_conciliation != null ? _conciliation.Id.ToString() : NULL) +
			            ", `description` = " + Formatter(_description) +
			            ", `numero_cheque`= " + Formatter(_numéroChèque) +
			            " WHERE id = " + Formatter(ancienId) +
			            "\0";
			        Debug();
			        Command.ExecuteNonQuery();

			        //Suppression de l'ancien contenu
			        Command.CommandText = "DELETE FROM `contenu_transaction` WHERE id_transaction = " + Formatter(ancienId) + "\0";
			        Debug();
			        Command.ExecuteNonQuery();

			        //Ajout du contenu
			        foreach ( ContenuTransaction ct in _contenuTransaction )
			        {
			            ct.Connection = Connection;
			            ct.Command = Command;
			            ct.Transaction = Transaction;
			            ct.Opération.Id = ancienId;
			            eValeurRetour = ct.Ajouter();
			            if ( eValeurRetour != null )
			            {
			                throw eValeurRetour;
			            }//if
			        }//foreach
			        #endregion "Sans Traces"
			    }//else

			    //Valider
			    Transaction.Commit();
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine("--- Commit ---");

			}// try
			catch ( Exception ex )
			{
			    try
			    {
			        Transaction.Rollback();
			        System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine("--- Rollback ---");
			    }//try
			    catch ( Exception e )
			    {
			        System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine(e.Message);
			    }//catch
			    eValeurRetour = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}// catch

			//Restauration de la transaction non groupée
			TransactionSeule = true;

			return eValeurRetour;
		}

		/// <summary>Permet de supprimer un enregistrement
		/// </summary>
		/// <param name="maintenirTrace">Doit-on maintenir la trace des modifications?</param>
		/// <returns>null si tout c'est bien déroulé, sinon l'exception est retournée.</returns>
		public Exception Supprimer(bool maintenirTrace)
		{
			Exception eValeurRetour = null;
			try
			{
			    if ( maintenirTrace )
			    {
			        #region "Avec Trace de Suppression"
			        Transaction transaction_inversée = new Transaction(this);
			        transaction_inversée.TransactionSeule = this.TransactionSeule;
			        transaction_inversée.Description = "Suppression de Op#" + _id;
			        if ( !string.IsNullOrEmpty(_description) )
			        {
			            transaction_inversée.Description += " (" + _description + ")";
			        }//if
			        transaction_inversée.DateTransaction = _dateTransaction;
			        transaction_inversée.Partenaire = _partenaire;
			        transaction_inversée.ContenuTransaction.AddRange(_contenuTransaction.ToArray());
			        foreach ( ContenuTransaction ct in transaction_inversée.ContenuTransaction )
			        {
			            double? tmp = ct.Débit;
			            ct.Débit = ct.Crédit;
			            ct.Crédit = tmp;
			            ct.Opération.Id = -1;
			        }//foreach

			        //ajout dans la BD
			        eValeurRetour = transaction_inversée.Ajouter();
			        if ( eValeurRetour != null )
			        {
			            throw eValeurRetour;
			        }//if
			        #endregion "Avec Trace de Suppression"
			    }//if
			    else
			    {
			        #region "Sans Traces de suppression"
			        Transaction = Connection.BeginTransaction();
			        System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine("--- BeginTransaction ---");
			        //Suppression de l'ancien contenu
			        Command.CommandText = "DELETE FROM `contenu_transaction` WHERE id_transaction = " + Formatter(_id) + "\0";
			        Debug();
			        Command.ExecuteNonQuery();

			        //Suppression de l'ancienne transaction
			        Command.CommandText = "DELETE FROM `transaction` WHERE id = " + Formatter(_id) + "\0";
			        Debug();
			        Command.ExecuteNonQuery();

			        //Valider
			        Transaction.Commit();
			        System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine("--- Commit ---");
			        #endregion "Sans Traces de suppression"
			    }//else

			}// try
			catch ( Exception ex )
			{
			    eValeurRetour = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}// catch
			return eValeurRetour;
		}

		#endregion Public Methods
	}
}