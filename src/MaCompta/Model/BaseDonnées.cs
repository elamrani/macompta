﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 16/11/2010
 * Time: 13:53
 * 
 * © 1428-1431 (2007-2010) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using MySql.Data.MySqlClient;

using System;

namespace Model
{
	/// <summary>
	/// Description of BaseDonnées.
	/// </summary>
	public class BaseDonnées
	{
		#region Fields

		MySqlCommand _command;
		MySqlConnection _connection;
		string _motPasse;
		string _nomBaseDonnées;
		string _nomHôte;
		string _nomUsager;
		int _numéroPort;
		MySqlDataReader _reader;
		MySqlTransaction _transaction;

		#endregion Fields

		#region Constructors

		public BaseDonnées()
		{
			Initialisation();
		}

		#endregion Constructors

		#region Public Properties

		public MySqlCommand Command
		{
			get { return _command; }
			set { _command = value; }
		}

		public MySqlConnection Connection
		{
			get { return _connection; }
			set { _connection = value;
				_command.Connection = _connection;
			}
		}

		public string MotPasse
		{
			get { return _motPasse; }
			set { _motPasse = value; }
		}

		public string NomBaseDonnées
		{
			get { return _nomBaseDonnées; }
			set { _nomBaseDonnées = value; }
		}

		public string NomHôte
		{
			get { return _nomHôte; }
			set { _nomHôte = value; }
		}

		public string NomUsager
		{
			get { return _nomUsager; }
			set { _nomUsager = value; }
		}

		public int NuméroPort
		{
			get { return _numéroPort; }
			set { _numéroPort = value; }
		}

		public MySqlDataReader Reader
		{
			get { return _reader; }
			set { _reader = value; }
		}

		public MySqlTransaction Transaction
		{
			get { return _transaction; }
			set { _transaction = value; }
		}

		#endregion Public Properties

		#region Private Methods

		/// <summary>
		/// permet de fermer et de disposer les objets de la BD
		/// </summary>
		private void Fermer()
		{
			try {
				_reader.Close();
				_reader.Dispose();
			}// try
			catch {}// catch rien
			try {
				_command.Dispose();
			}// try
			catch {}// catch rien
			try {
				_connection.Close();
				_connection.Dispose();
			}// try
			catch {}// catch rien
		}

		private void Initialisation()
		{
			_motPasse = "123123";
			_nomBaseDonnées = "macompta";
			_nomHôte = "localhost";
			_nomUsager = "user_macompta";
			_numéroPort = 3306;
			_connection = new MySqlConnection();
			_command = new MySqlCommand();
		}

		#endregion Private Methods

		#region Public Methods

		/// <summary>
		/// Tente d'ouvrir la BD
		/// </summary>
		/// <returns>null si tout c'est bien passé, sinon retourner l'exception.</returns>
		public Exception Ouvrir()
		{
			Exception eValeurRetour = null;
			try {
				_connection.ConnectionString = "Server=" + _nomHôte +
					";Port=" + _numéroPort +
					";User Id=" + _nomUsager +
					";Password=" + _motPasse +
					";Database=" + _nomBaseDonnées;
				_connection.Open();
				_command.Connection = _connection;
			}//try
			catch(Exception e) {
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( e.Message );
				eValeurRetour = e;
			}//catch
			return eValeurRetour;
		}

		#endregion Public Methods
	}
}