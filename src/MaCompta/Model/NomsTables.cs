﻿/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 16/11/2010
 * Time: 19:57
 * 
 * © 1428-1431 (2007-2010) All rights reserved to Mohamed Y. ElAmrani.
 */
namespace Model
{
	#region Enumerations

	public enum NomsTables
	{
		ansiba,
		classe_compte,
		compte,
		conciliation,
		configuration,
		contenu_transaction,
		partenaire,
		projet,
		transaction,
		type_compte
	}

	#endregion Enumerations
}