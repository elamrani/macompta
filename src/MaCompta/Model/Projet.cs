﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 05/08/1433
 * Time: 19:20
 * 
 * © 1428-1433 (2007-2012) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using System;

namespace Model
{
	/// <summary>
	/// Description of Projet.
	/// </summary>
	public class Projet : OpérationBaseDonnées
	{
		#region Fields

		private bool _afficher;
		private Nullable<DateTime> _date_début;
		private Nullable<DateTime> _date_fin;
		private long _id;
		private string _nom;

		#endregion Fields

		#region Constructors

		public Projet(BaseDonnées bd)
		{
			Connection = bd.Connection;
			Command = bd.Command;
			Initialisation();
		}

		/// <summary>
		/// Permet d'instancier un Projet dont le nom est précisé
		/// </summary>
		/// <param name="bd">la bd à utiliser</param>
		/// <param name="nomACharger">le nom à charger</param>
		/// <param name="valeurRetournée">null = le chargement des infos s'est bien déroulé; 
		/// sinon l'exception est retournée</param>
		public Projet(BaseDonnées bd, string nomACharger, ref Exception valeurRetournée)
		{
			Connection = bd.Connection;
			Command = bd.Command;
			Initialisation();
			valeurRetournée = Charger(nomACharger);
		}

		/// <summary>
		/// Permet d'instancier un Projet dont le id est précisé
		/// </summary>
		/// <param name="bd">la bd à utiliser</param>
		/// <param name="idACharger">le id à charger</param>
		/// <param name="valeurRetournée">null = le chargement des infos s'est bien déroulé; 
		/// sinon l'exception est retournée</param>
		public Projet(BaseDonnées bd, long idACharger, ref Exception valeurRetournée)
		{
			Connection = bd.Connection;
			Command = bd.Command;
			Initialisation();
			valeurRetournée = Charger(idACharger);
		}

		#endregion Constructors

		#region Public Properties

		public bool Afficher
		{
			get { return _afficher; }
			set { _afficher = value; }
		}

		public Nullable<DateTime> DateDébut
		{
			get { return _date_début; }
			set { _date_début = value; }
		}

		public Nullable<DateTime> DateFin
		{
			get { return _date_fin; }
			set { _date_fin = value; }
		}

		public long Id
		{
			get { return _id; }
			set { _id = value; }
		}

		public string Nom
		{
			get { return _nom; }
			set { _nom = value; }
		}

		#endregion Public Properties

		#region Private Methods

		private void Initialisation()
		{
			_id = -1;
			_nom = string.Empty;
			_afficher = true;
		}

		#endregion Private Methods

		#region Public Methods

		public override Exception Ajouter()
		{
			Exception eValeurRetour = null;
			try
			{
			    string sRequête = "INSERT INTO `projet` (`nom`, `date_debut`, `date_fin`, `afficher`) VALUES (" + Formatter(_nom) +
			        ", " + Formatter(_date_début) +
			        ", " + Formatter(_date_fin) +
			        ", " + Formatter(_afficher) +
			        ")\0";
			    long? lIdRetourné = null;
			    eValeurRetour = ExécuterRequête(sRequête, ref lIdRetourné);
			    if ( eValeurRetour != null )
			    {
			        throw eValeurRetour;
			    }//if
			    else if ( lIdRetourné.HasValue )
			    {
			        _id = lIdRetourné.Value;
			    }//else if

			}// try
			catch ( Exception ex )
			{
			    eValeurRetour = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}// catch
			return eValeurRetour;
		}

		/// <summary>
		/// Chargement des informations
		/// </summary>
		/// <param name="id">le id du partenaire à charger</param>
		/// <returns>null si tout est bon, sinon, l'exception de l'erreur</returns>
		public Exception Charger(long id)
		{
			Exception eValeurRetour = null;
			try
			{
			    Command.CommandText = "SELECT * " +
			        "FROM `projet` " +
			        "WHERE `id` = \"" + id + "\"\0";
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(Command.CommandText);
			    Reader = Command.ExecuteReader();
			    if ( Reader.Read() )
			    {
			        _id = long.Parse(Reader["id"].ToString());
			        _nom = Reader["nom"].ToString();
			        _afficher = bool.Parse(Reader["afficher"].ToString());
			        if ( string.IsNullOrEmpty(Reader["date_debut"].ToString()) )
			        {
			            _date_début = null;
			        }//if
			        else
			        {
			            _date_début = DateTime.Parse(Reader["date_debut"].ToString());
			        }//else
			        if ( string.IsNullOrEmpty(Reader["date_fin"].ToString()) )
			        {
			            _date_fin = null;
			        }//if
			        else
			        {
			            _date_début = DateTime.Parse(Reader["date_fin"].ToString());
			        }//else
			    }//if
			    Reader.Close();
			}//try
			catch ( Exception e )
			{
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if				
			    eValeurRetour = e;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(e.Message);
			}//catch

			return eValeurRetour;
		}

		/// <summary>
		/// Chargement des informations
		/// </summary>
		/// <param name="nom">le nom du projet à charger</param>
		/// <returns>null si tout est bon, sinon, l'exception de l'erreur</returns>
		public Exception Charger(string nom)
		{
			Exception eValeurRetour = null;
			try
			{
			    Command.CommandText = "SELECT * " +
			        "FROM `projet` " +
			        "WHERE `nom` = \"" + nom + "\"\0";
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(Command.CommandText);
			    Reader = Command.ExecuteReader();
			    if ( Reader.Read() )
			    {
			        _id = long.Parse(Reader["id"].ToString());
			        _nom = nom;
			        if ( string.IsNullOrEmpty(Reader["date_debut"].ToString()) )
			        {
			            _date_début = null;
			        }//if
			        else
			        {
			            _date_début = DateTime.Parse(Reader["date_debut"].ToString());
			        }//else
			        if ( string.IsNullOrEmpty(Reader["date_fin"].ToString()) )
			        {
			            _date_fin = null;
			        }//if
			        else
			        {
			            _date_début = DateTime.Parse(Reader["date_fin"].ToString());
			        }//else
			        _afficher = bool.Parse(Reader["afficher"].ToString());
			    }//if
			    Reader.Close();
			}//try
			catch ( Exception e )
			{
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if				
			    eValeurRetour = e;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(e.Message);
			}//catch

			return eValeurRetour;
		}

		public override Exception Modifier(long _ancienId)
		{
			Exception eValeurRetour = null;
			try
			{
			    string sRequête = "UPDATE `projet` SET " +
			        "`id` = " + Formatter(_id) +
			        ", `nom` = " + Formatter(_nom) +
			        ", `date_debut` = " + Formatter(_date_début) +
			        ", `date_fin` = " + Formatter(_date_fin) +
			        ", `afficher` = " + Formatter(_afficher) +
			        " WHERE `id` = " + Formatter(_ancienId) +
			        "\0";
			    long? lIdRetourné = null;
			    eValeurRetour = ExécuterRequête(sRequête, ref lIdRetourné);
			    if ( eValeurRetour != null )
			    {
			        throw eValeurRetour;
			    }//if
			}// try
			catch ( Exception ex )
			{
			    eValeurRetour = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}// catch
			return eValeurRetour;
		}

		/// <summary>
		/// We cannot delete a project previously used.
		/// </summary>
		/// <returns>null = all is OK, else the exception is returned.</returns>
		public override Exception Supprimer()
		{
			Exception eValeurRetour = null;
			try
			{
			    Command.CommandText = "SELECT * FROM `contenu_transaction` " +
			        " WHERE `id_projet` = " + _id +
			        "\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    if ( Reader.Read() )
			    {
			        Reader.Close();
			        throw new InvalidOperationException("Ce projet contient une ou plusieurs opérations et ne peux donc pas être supprimé.");
			    }//if
			    else
			    {
			        Reader.Close();
			        eValeurRetour = Supprimer(NomsTables.projet, _id);
			        if ( eValeurRetour != null )
			        {
			            throw eValeurRetour;
			        }//if
			    }//else
			}// try
			catch ( Exception ex )
			{
			    eValeurRetour = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}// catch
			return eValeurRetour;
		}

		#endregion Public Methods
	}
}