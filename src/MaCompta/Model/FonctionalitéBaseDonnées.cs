﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 23/11/2010
 * Time: 15:58
 * 
 * © 1428-1431 (2007-2010) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using System;
using System.Globalization;

namespace Model
{
	/// <summary>
	/// Description of FonctionsBaseDonnées.
	/// </summary>
	public class FonctionalitéBaseDonnées : BaseDonnées
	{
		#region Constants

		public const string NULL = "NULL";

		#endregion Constants

		#region Fields

		private CultureInfo _CultureInfo;

		/// <summary>
		/// Is this operation requires a single query or
		/// this is part of a group of queries to be included in one transaction
		/// </summary>
		private bool _transaction_seule = true;

		#endregion Fields

		#region Constructors

		public FonctionalitéBaseDonnées()
		{
		}

		#endregion Constructors

		#region Public Properties

		/// <summary>
		/// Is this operation requires a single query or
		/// this is part of a group of queries to be included in one transaction
		/// </summary>
		public bool TransactionSeule
		{
			get { return _transaction_seule; }
			set { _transaction_seule = value; }
		}

		#endregion Public Properties

		#region Protected Methods

		/// <summary>
		/// Supprime un enregistrement de la classe
		/// </summary>
		/// <param name="nomTable">Nom de la table d'où la suppression sera effectuée</param>
		/// <param name="lId">Id de l'enregistrement à supprimer</param>
		/// <returns>null: tout s'est bien passé, Sinon, l'exception est retournée</returns>
		protected Exception Supprimer(NomsTables nomTable, long lId)
		{
			Exception eValeurRetour = null;
			try {
				// Suppression de l'adresse
				string sRequête =
					"DELETE FROM `" + nomTable + "` WHERE `Id` = " + lId;

			    long? lIdRetourné = null;
			    eValeurRetour = ExécuterRequête(sRequête, ref lIdRetourné);
			    if ( eValeurRetour != null )
			    {
			        throw eValeurRetour;
			    }//if
			}// try
			catch (Exception ex) {
				eValeurRetour = ex;
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( ex.Message );
			}// catch
			return eValeurRetour;
		}

		#endregion Protected Methods

		#region Public Methods

		/// <summary>
		/// Show the query in the Debug mode
		/// </summary>
		public void Debug()
		{
			System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			System.Diagnostics.Debug.WriteLine(Command.CommandText);
		}

		///<summary>
		/// Exécute la requête et retourne l'exception s'il y en a une qui est générée ou null.
		/// Méthode utilisée pour les ajouts et modifications
		/// </summary>
		/// <param name="sRequête">La requête INSERT/UPDATE à executer</param>
		/// <param name="lIdRetourné">Le id du nouvel enregistrement si applicable</param>
		/// <returns>null =tout s'est bien passé, Sinon, l'exception est retournée</returns>
		public Exception ExécuterRequête(string sRequête, ref long? lIdRetourné)
		{
			Exception eValeurRetournée = null;
			lIdRetourné = null;
			try {
				Command.CommandText = sRequête;
				Debug();
				Command.ExecuteNonQuery();
			    lIdRetourné = Command.LastInsertedId;
			}// try
			catch (Exception ex) {
				eValeurRetournée = ex;
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( ex.Message );
			}// catch
			return eValeurRetournée;
		}

		/// <summary>
		/// Permet de formatter la date (sans l'heure) selon le format de la BD MySQL
		/// </summary>
		/// <param name="dtDate">la date à formatter</param>
		/// <returns>Le texte de la date formattée selon YYYY-MM-DD</returns>
		public string Formatter(DateTime dtDate)
		{
			return Formatter(dtDate, false);
		}

		/// <summary>
		/// Permet de formatter la date (sans l'heure) selon le format de la BD MySQL
		/// </summary>
		/// <param name="dtDate">la date à formatter</param>
		/// <param name="bHeure">doit-on aussi formatter l'heure ou non</param>
		/// <returns>Le texte de la date formattée selon YYYY-MM-DD HH:mm:ss</returns>
		public string Formatter(DateTime dtDate, bool bHeure)
		{
			string sValeurRetour = "\"" + dtDate.Year.ToString("D4") + "-" +
			    dtDate.Month.ToString("D2") + "-" + dtDate.Day.ToString("D2");
			if ( bHeure )
			{
			    sValeurRetour += " " + dtDate.Hour.ToString("D2") + ":" +
			        dtDate.Minute.ToString("D2") + ":" + dtDate.Second.ToString("D2");
			}//if
			sValeurRetour += "\"";

			return sValeurRetour;
		}

		/// <summary>
		/// Permet de formatter la date (sans l'heure) selon le format de la BD MySQL
		/// </summary>
		/// <param name="dtDate">la date à formatter</param>
		/// <returns>Le texte de la date formattée selon YYYY-MM-DD</returns>
		public string Formatter(Nullable<DateTime> dtDate)
		{
			return Formatter(dtDate, false);
		}

		/// <summary>
		/// Permet de formatter la date (sans l'heure) selon le format de la BD MySQL
		/// </summary>
		/// <param name="dtDate">la date à formatter</param>
		/// <param name="bHeure">doit-on aussi formatter l'heure ou non</param>
		/// <returns>Le texte de la date formattée selon YYYY-MM-DD HH:mm:ss</returns>
		public string Formatter(Nullable<DateTime> dtDate, bool bHeure)
		{
			string sValeurRetour = NULL;

			if ( dtDate != null )
			{
			    sValeurRetour = "\"" + dtDate.Value.Year.ToString("D4") + "-" + 
			        dtDate.Value.Month.ToString("D2") + "-" + dtDate.Value.Day.ToString("D2");
			    if ( bHeure )
			    {
			        sValeurRetour += " " + dtDate.Value.Hour.ToString("D2") + ":" +
			            dtDate.Value.Minute.ToString("D2") + ":" + dtDate.Value.Second.ToString("D2");
			    }//if
			    sValeurRetour += "\"";
			}//if
			return sValeurRetour;
		}

		/// <summary>
		/// Permet de formatter le texte avec le séparateur décimal = . 
		/// et le sans groupement des chiffres (1234 et non 1 234)
		/// </summary>
		/// <param name="dValeur">la valeur à convertir</param>
		/// <returns>le texte du paramètre</returns>
		public string Formatter(double dValeur)
		{
			_CultureInfo = new CultureInfo("en-US");
			_CultureInfo.NumberFormat.NumberDecimalSeparator = ".";
			_CultureInfo.NumberFormat.NumberGroupSeparator = string.Empty;
			return dValeur.ToString(_CultureInfo);
		}

		/// <summary>
		/// Permet de formatter le texte avec le séparateur décimal = . 
		/// et le sans groupement des chiffres (1234 et non 1 234)
		/// </summary>
		/// <param name="lValeur">la valeur à convertir</param>
		/// <returns>le texte du paramètre</returns>
		public string Formatter(long lValeur)
		{
			_CultureInfo = new CultureInfo("en-US");
			_CultureInfo.NumberFormat.NumberDecimalSeparator = ".";
			_CultureInfo.NumberFormat.NumberGroupSeparator = string.Empty;
			return lValeur.ToString(_CultureInfo);
		}

		/// <summary>
		/// Permet de formatter le texte avec le séparateur décimal = . 
		/// et le sans groupement des chiffres (1234 et non 1 234) en prenant
		/// zn considération le cas ou la valeur est nulle.
		/// </summary>
		/// <param name="lValeur">la valeur à convertir</param>
		/// <returns>le texte du paramètre</returns>
		public string Formatter(long? lValeur)
		{
			string sValeurRetour;
			if ( lValeur.HasValue )
			{
			    sValeurRetour = Formatter(lValeur.Value);
			}//if
			else
			{
			    sValeurRetour = NULL;
			}//else

			return sValeurRetour;
		}

		/// <summary>
		/// Permet de formatter le texte avec le séparateur décimal = . 
		/// et le sans groupement des chiffres (1234 et non 1 234) en prenant
		/// zn considération le cas ou la valeur est nulle.
		/// </summary>
		/// <param name="dValeur">la valeur à convertir</param>
		/// <returns>le texte du paramètre</returns>
		public string Formatter(double? dValeur)
		{
			string sValeurRetour;
			if ( dValeur.HasValue )
			{
			    sValeurRetour = Formatter(dValeur.Value);
			}//if
			else
			{
			    sValeurRetour = NULL;
			}//else

			return sValeurRetour;
		}

		/// <summary>
		/// Permet de formatter les booléens
		/// </summary>
		/// <param name="bValeur">le booléen à formatter</param>
		/// <returns>TRUE ou FALSE</returns>
		public string Formatter(bool bValeur)
		{
			return (bValeur ? "TRUE" : "FALSE");
		}

		/// <summary>
		/// Permet de retourner le texte avec des guillemets doubles (") ou bien NULL lorsque
		/// le texte est nul ou vide.
		/// Si le texte contient les doubles guillemets (") ils sont remplacés par le guillemet simple (').
		/// </summary>
		/// <param name="sValeur">le texte à retourner ou bien une référence nulle</param>
		/// <returns>Le texte avec des guillemets doubles (") ou bien NULL</returns>
		public string Formatter(string sValeur)
		{
			return Formatter(sValeur, true);
		}

		/// <summary>
		/// Permet de retourner le texte avec des guillemets doubles (") ou bien NULL lorsque
		/// le texte est nul ou vide.
		/// Si le texte contient les doubles guillemets (") ils sont remplacés par le guillemet simple (').
		/// </summary>
		/// <param name="sValeur">le texte à retourner ou bien une référence nulle</param>
		/// <returns>Le texte avec des guillemets doubles (") ou bien NULL</returns>
		public string Formatter(string sValeur, bool bRendreNULL)
		{
			string sValeurRetournée = string.Empty;
			if ( string.IsNullOrEmpty(sValeur) && bRendreNULL ) {
				sValeurRetournée = NULL;
			}//if
			else {
				sValeurRetournée = "\"" + sValeur.Replace("\"", "'").Replace("\\", "\\\\") + "\"";
			}//else

			return sValeurRetournée;
		}

		/// <summary>
		/// Mise à jour du prochain ID
		/// </summary>
		/// <param name="eNomTable">Le nom du champ associé à la
		/// table dont le prochain ID sera enregistré</param>
		/// <returns>null = tout c'est bien passé, Sinon, retourner l'exception</returns>
		public Exception IncrémenterProchainID(NomsTables eNomTable)
		{
			Exception eValeurRetour = null;
			try {
				Command.CommandText = "UPDATE `ConfigProchainID` SET `" + eNomTable
				                      + "` = `" + eNomTable + "` + 1\0";
				Debug();
				Command.ExecuteNonQuery();
			}// try
			catch (Exception e) {
				eValeurRetour = e;
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( e.Message );
			}// catch
			return eValeurRetour;
		}

		#endregion Public Methods
	}
}