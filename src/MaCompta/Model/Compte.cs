﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 05/08/1433
 * Time: 19:20
 * 
 * © 1428-1433 (2007-2012) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using System;

namespace Model
{
	/// <summary>
	/// Description of Compte.
	/// </summary>
	public class Compte : OpérationBaseDonnées
	{
		#region Fields

		private bool _afficher;
		private string _agence;
		private double? _budget_quotidien;
		private bool _estUtilisé;
		private long _id;
		private long _id_type_compte;
		private string _institution;
		private string _nom;
		private string _numéro;
		private int? _type_nombre_jours;

		#endregion Fields

		#region Constructors

		public Compte(BaseDonnées bd)
		{
			Connection = bd.Connection;
			Command = bd.Command;
			Initialisation();
		}

		/// <summary>
		/// Permet d'instancier un Compte dont le nom est précisé
		/// </summary>
		/// <param name="bd">la bd à utiliser</param>
		/// <param name="nomACharger">le nom à charger</param>
		/// <param name="valeurRetournée">null = le chargement des infos s'est bien déroulé; 
		/// sinon l'exception est retournée</param>
		public Compte(BaseDonnées bd, string nomACharger, ref Exception valeurRetournée)
		{
			Connection = bd.Connection;
			Command = bd.Command;
			Initialisation();
			valeurRetournée = Charger(nomACharger);
		}

		/// <summary>
		/// Permet d'instancier un Compte dont le id est précisé
		/// </summary>
		/// <param name="bd">la bd à utiliser</param>
		/// <param name="idACharger">le id à charger</param>
		/// <param name="valeurRetournée">null = le chargement des infos s'est bien déroulé; 
		/// sinon l'exception est retournée</param>
		public Compte(BaseDonnées bd, long idACharger, ref Exception valeurRetournée)
		{
			Connection = bd.Connection;
			Command = bd.Command;
			Initialisation();
			valeurRetournée = Charger(idACharger);
		}

		#endregion Constructors

		#region Public Properties

		public bool Afficher
		{
			get { return _afficher; }
			set { _afficher = value; }
		}

		public string Agence
		{
			get { return _agence; }
			set { _agence = value; }
		}

		public double? BudgetQuotidien
		{
			get { return _budget_quotidien; }
			set { _budget_quotidien = value; }
		}

		public bool EstUtilisé
		{
			get { return _estUtilisé; }
		}

		public long Id
		{
			get { return _id; }
			set { _id = value; }
		}

		public long IdTypeCompte
		{
			get { return _id_type_compte; }
			set { _id_type_compte = value; }
		}

		public string Institution
		{
			get { return _institution; }
			set { _institution = value; }
		}

		public string Nom
		{
			get { return _nom; }
			set { _nom = value; }
		}

		public string Numéro
		{
			get { return _numéro; }
			set { _numéro = value; }
		}

		public double? Solde
		{
			get { return CalculerSolde(); }
		}

		public int? TypeNombreJours
		{
			get { return _type_nombre_jours; }
			set { _type_nombre_jours = value; }
		}

		#endregion Public Properties

		#region Private Methods

		/// <summary>
		/// returns the account balance of normal and sub-group accounts:
		///     debit - credit for 2000-2999, 5000-5999.
		///     credit - debit for 1000-1999, 3000-3999, 4000-4999.
		/// </summary>
		/// <returns>the balance</returns>
		private double? CalculerSolde()
		{
			double? solde = null;
			try {
			    if ((_id >= 2000 && _id <= 2999) || (_id >= 5000 && _id <= 5999)) {
			        //debit - credit for 2000-2999, 5000-5999.
			        Command.CommandText = "SELECT SUM( IFNULL(`debit`, 0) - IFNULL(`credit`, 0) ) ";
			    }//if
			    else {
			        //credit - debit for 1000-1999, 3000-3999, 4000-4999.
			        Command.CommandText = "SELECT SUM( IFNULL(`credit`, 0) - IFNULL(`debit`, 0) ) ";
			    }//else
			    Command.CommandText += "FROM `contenu_transaction`, `compte` " +
			        "WHERE (`contenu_transaction`.`id_compte` = `compte`.`id`) AND (`id_compte` = " + _id +
			        ") AND (`id_type_compte` BETWEEN 2 AND 3)\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    if (Reader.Read()) {
			        solde = double.Parse(Reader[0].ToString());
			    }//if
			    Reader.Close();
			}//try
			catch (Exception ex) {
			    if (!Reader.IsClosed) {
			        Reader.Close();
			    }//if
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}//catch

			return solde;
		}

		private void Initialisation()
		{
			_id = -1;
			_id_type_compte = -1;
			_nom = string.Empty;
			_numéro = string.Empty;
			_institution = string.Empty;
			_agence = string.Empty;
			_budget_quotidien = null;
			_type_nombre_jours = null;
			_afficher = true;
			_estUtilisé = false;
		}

		#endregion Private Methods

		#region Public Methods

		public override Exception Ajouter()
		{
			Exception eValeurRetour = null;
			try {
			    string sRequête = "INSERT INTO `compte` (`id`, `id_type_compte`, `nom`, `numero`, `institution`, `agence`, " +
			        "`budget_quotidien`, `type_nombre_jours`, `afficher`) VALUES (" + _id +
			        ", " + Formatter(_id_type_compte) +
			        ", " + Formatter(_nom) +
			        ", " + Formatter(_numéro) +
			        ", " + Formatter(_institution) +
			        ", " + Formatter(_agence) +
			        ", " + Formatter(_budget_quotidien) +
			        ", " + (_budget_quotidien == null ? NULL : Formatter(_type_nombre_jours)) +
			        ", " + Formatter(_afficher) +
			        ")\0";
			    long? lIdRetourné = null;
			    eValeurRetour = ExécuterRequête(sRequête, ref lIdRetourné);
			    if (eValeurRetour != null) {
			        throw eValeurRetour;
			    }//if
			    else if (lIdRetourné.HasValue) {
			        _id = lIdRetourné.Value;
			    }//else if

			}// try
			catch (Exception ex) {
			    eValeurRetour = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}// catch
			return eValeurRetour;
		}

		/// <summary>
		/// Chargement des informations
		/// </summary>
		/// <param name="id">le id du partenaire à charger</param>
		/// <returns>null si tout est bon, sinon, l'exception de l'erreur</returns>
		public Exception Charger(long id)
		{
			Exception eValeurRetour = null;
			try {
			    Command.CommandText = "SELECT * " +
			        "FROM `compte` " +
			        "WHERE `id` = " + id + "\0";
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(Command.CommandText);
			    Reader = Command.ExecuteReader();
			    if (Reader.Read()) {
			        _id = long.Parse(Reader["id"].ToString());
			        _id_type_compte = long.Parse(Reader["id_type_compte"].ToString());
			        _nom = Reader["nom"].ToString();
			        _numéro = Reader["numero"].ToString();
			        _institution = Reader["institution"].ToString();
			        _agence = Reader["agence"].ToString();
			        if (string.IsNullOrEmpty(Reader["budget_quotidien"].ToString())) {
			            _budget_quotidien = null;
			            _type_nombre_jours = null;
			        }//if
			        else {
			            _budget_quotidien = double.Parse(Reader["budget_quotidien"].ToString());
			            if (!string.IsNullOrEmpty(Reader["type_nombre_jours"].ToString())) {
			                _type_nombre_jours = int.Parse(Reader["type_nombre_jours"].ToString());
			            }//if
			        }//else
			        _afficher = bool.Parse(Reader["afficher"].ToString());
			    }//if
			    Reader.Close();

			    //vérifier si le compte est utilisé
			    Command.CommandText = "SELECT * FROM `contenu_transaction` " +
			        " WHERE `id_compte` = " + _id +
			        "\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    if (Reader.Read()) {
			        _estUtilisé = true;
			    }//if
			    else {
			        _estUtilisé = false;
			    }//else
			    Reader.Close();

			}//try
			catch (Exception e) {
			    if (!Reader.IsClosed) {
			        Reader.Close();
			    }//if				
			    eValeurRetour = e;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(e.Message);
			}//catch

			return eValeurRetour;
		}

		/// <summary>
		/// Chargement des informations
		/// </summary>
		/// <param name="nom">le nom du compte à charger</param>
		/// <returns>null si tout est bon, sinon, l'exception de l'erreur</returns>
		public Exception Charger(string nom)
		{
			Exception eValeurRetour = null;
			try {
			    Command.CommandText = "SELECT * " +
			        "FROM `compte` " +
			        "WHERE `nom` = \"" + nom + "\"\0";
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(Command.CommandText);
			    Reader = Command.ExecuteReader();
			    if (Reader.Read()) {
			        _id = long.Parse(Reader["id"].ToString());
			        _id_type_compte = long.Parse(Reader["id_type_compte"].ToString());
			        _nom = nom;
			        _numéro = Reader["numero"].ToString();
			        _institution = Reader["institution"].ToString();
			        _agence = Reader["agence"].ToString();
			        if (string.IsNullOrEmpty(Reader["budget_quotidien"].ToString())) {
			            _budget_quotidien = null;
			            _type_nombre_jours = null;
			        }//if
			        else {
			            _budget_quotidien = double.Parse(Reader["budget_quotidien"].ToString());
			            if (!string.IsNullOrEmpty(Reader["type_nombre_jours"].ToString())) {
			                _type_nombre_jours = int.Parse(Reader["type_nombre_jours"].ToString());
			            }//if
			        }//else
			        _afficher = bool.Parse(Reader["afficher"].ToString());
			    }//if
			    Reader.Close();

			    //vérifier si le compte est utilisé
			    Command.CommandText = "SELECT * FROM `contenu_transaction` " +
			        " WHERE `id_compte` = " + _id +
			        "\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    if (Reader.Read()) {
			        _estUtilisé = true;
			    }//if
			    else {
			        _estUtilisé = false;
			    }//else
			    Reader.Close();

			}//try
			catch (Exception e) {
			    if (!Reader.IsClosed) {
			        Reader.Close();
			    }//if				
			    eValeurRetour = e;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(e.Message);
			}//catch

			return eValeurRetour;
		}

		public override Exception Modifier(long _ancienId)
		{
			Exception eValeurRetour = null;
			try {
			    string sRequête = "UPDATE `compte` SET " +
			        "`id` = " + Formatter(_id) +
			        ", `id_type_compte` = " + Formatter(_id_type_compte) +
			        ", `nom` = " + Formatter(_nom) +
			        ", `numero` = " + Formatter(_numéro) +
			        ", `institution` = " + Formatter(_institution) +
			        ", `agence` = " + Formatter(_agence) +
			        ", `budget_quotidien` = " + Formatter(_budget_quotidien) +
			        ", `type_nombre_jours` = " + (_budget_quotidien == null ? NULL : Formatter(_type_nombre_jours)) +
			        ", `afficher` = " + Formatter(_afficher) +
			        " WHERE `id` = " + Formatter(_ancienId) +
			        "\0";
			    long? lIdRetourné = null;
			    eValeurRetour = ExécuterRequête(sRequête, ref lIdRetourné);
			    if (eValeurRetour != null) {
			        throw eValeurRetour;
			    }//if
			}// try
			catch (Exception ex) {
			    eValeurRetour = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}// catch
			return eValeurRetour;
		}

		/// <summary>
		/// We cannot delete an account previously used.
		/// </summary>
		/// <returns>null = all is OK, else the exception is returned.</returns>
		public override Exception Supprimer()
		{
			Exception eValeurRetour = null;
			try {
			    if (_estUtilisé) {
			        throw new InvalidOperationException("Ce compte contient une ou plusieurs opérations et ne peux donc pas être supprimé.");
			    }//if
			    else {
			        Reader.Close();
			        eValeurRetour = Supprimer(NomsTables.compte, _id);
			        if (eValeurRetour != null) {
			            throw eValeurRetour;
			        }//if
			    }//else
			}// try
			catch (Exception ex) {
			    eValeurRetour = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}// catch
			return eValeurRetour;
		}

		#endregion Public Methods
	}
}