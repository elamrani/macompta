﻿using System;

namespace Model
{
	class Ansiba : OpérationBaseDonnées
	{
		#region Fields

		private DateTime _date;
		private long _id;
		private double _montant_nisab;
		private double _montant_total;
		private double _montant_zakat;
		private bool _supprimee;

		#endregion Fields

		#region Constructors

		public Ansiba(BaseDonnées bd)
		{
			Connection = bd.Connection;
			Command = bd.Command;
			Initialisation();
		}

		#endregion Constructors

		#region Public Properties

		public DateTime Date
		{
			get { return _date; }
			set { _date = value; }
		}

		public bool EstSupprimée
		{
			get { return _supprimee; }
			set { _supprimee = value; }
		}

		public long Id
		{
			get { return _id; }
			set { _id = value; }
		}

		public double MontantNisab
		{
			get { return _montant_nisab; }
			set { _montant_nisab = value; }
		}

		public double MontantTotal
		{
			get { return _montant_total; }
			set { _montant_total = value; }
		}

		public double MontantZakat
		{
			get { return _montant_zakat; }
			set { _montant_zakat = value; }
		}

		#endregion Public Properties

		#region Private Methods

		private void Initialisation()
		{
			_date = DateTime.Now;
			_id = -1;
			_montant_total = 0;
			_montant_nisab = 0;
			_montant_zakat = 0;
			_supprimee = false;
		}

		#endregion Private Methods

		#region Public Methods

		public override Exception Ajouter()
		{
			Exception eValeurRetour = null;
			try {
			    string sRequête = "INSERT INTO `ansiba` (`date`, `montant_total`, `montant_nisab`, `montant_zakat`, `supprimee`) VALUES (" + 
			        ", " + Formatter(_montant_total) +
			        ", " + Formatter(_montant_nisab) +
			        ", " + Formatter(_montant_zakat) +
			        ", " + Formatter(_supprimee) +
			        ")\0";
			    long? lIdRetourné = null;
			    eValeurRetour = ExécuterRequête(sRequête, ref lIdRetourné);
			    if (eValeurRetour != null) {
			        throw eValeurRetour;
			    }//if
			    else if (lIdRetourné.HasValue) {
			        _id = lIdRetourné.Value;
			    }//else if

			}// try
			catch (Exception ex) {
			    eValeurRetour = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}// catch
			return eValeurRetour;
		}

		/// <summary>
		/// Chargement des informations
		/// </summary>
		/// <param name="id">le id du nisab à charger</param>
		/// <returns>null si tout est bon, sinon, l'exception de l'erreur</returns>
		public Exception Charger(long id)
		{
			Exception eValeurRetour = null;
			try {
			    Command.CommandText = "SELECT * " +
			        "FROM `ansiba` " +
			        "WHERE `id` = " + id + "\0";
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(Command.CommandText);
			    Reader = Command.ExecuteReader();
			    if (Reader.Read()) {
			        _id = long.Parse(Reader["id"].ToString());
			        _date = DateTime.Parse(Reader["date"].ToString());
			        _montant_total = double.Parse(Reader["montant_total"].ToString());
			        _montant_nisab = double.Parse(Reader["montant_nisab"].ToString());
			        _montant_zakat = double.Parse(Reader["montant_zakat"].ToString());
			        _supprimee = bool.Parse(Reader["supprimee"].ToString());
			    }//if
			    Reader.Close();
			}//try
			catch (Exception e) {
			    if (!Reader.IsClosed) {
			        Reader.Close();
			    }//if				
			    eValeurRetour = e;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(e.Message);
			}//catch

			return eValeurRetour;
		}

		/// <summary>
		/// Chargement des informations
		/// </summary>
		/// <param name="date">la date du nisab à charger</param>
		/// <returns>null si tout est bon, sinon, l'exception de l'erreur</returns>
		public Exception Charger(DateTime date)
		{
			Exception eValeurRetour = null;
			try {
			    Command.CommandText = "SELECT * " +
			        "FROM `ansiba` " +
			        "WHERE `date` = " + Formatter(date) + "\0";
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(Command.CommandText);
			    Reader = Command.ExecuteReader();
			    if (Reader.Read()) {
			        _id = long.Parse(Reader["id"].ToString());
			        _date = DateTime.Parse(Reader["date"].ToString());
			        _montant_total = double.Parse(Reader["montant_total"].ToString());
			        _montant_nisab = double.Parse(Reader["montant_nisab"].ToString());
			        _montant_zakat = double.Parse(Reader["montant_zakat"].ToString());
			        _supprimee = bool.Parse(Reader["supprimee"].ToString());
			    }//if
			    Reader.Close();
			}//try
			catch (Exception e) {
			    if (!Reader.IsClosed) {
			        Reader.Close();
			    }//if				
			    eValeurRetour = e;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(e.Message);
			}//catch

			return eValeurRetour;
		}

		public override Exception Modifier(long _ancienId)
		{
			Exception eValeurRetour = null;
			try {
			    string sRequête = "UPDATE `ansiba` SET " +
			        ", `date` = " + Formatter(_date) +
			        ", `montant_total` = " + Formatter(_montant_total) +
			        ", `montant_nisab` = " + Formatter(_montant_nisab) +
			        ", `montant_zakat` = " + Formatter(_montant_zakat) +
			        ", `supprimee` = " + Formatter(_supprimee) +
			        " WHERE `id` = " + Formatter(_ancienId) +
			        "\0";
			    long? lIdRetourné = null;
			    eValeurRetour = ExécuterRequête(sRequête, ref lIdRetourné);
			    if (eValeurRetour != null) {
			        throw eValeurRetour;
			    }//if
			}// try
			catch (Exception ex) {
			    eValeurRetour = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}// catch
			return eValeurRetour;
		}

		/// <summary>
		/// We cannot delete an account previously used.
		/// </summary>
		/// <returns>null = all is OK, else the exception is returned.</returns>
		public override Exception Supprimer()
		{
			Exception eValeurRetour = null;
			try {
			        eValeurRetour = Supprimer(NomsTables.ansiba, _id);
			        if (eValeurRetour != null) {
			            throw eValeurRetour;
			        }//if
			}// try
			catch (Exception ex) {
			    eValeurRetour = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}// catch
			return eValeurRetour;
		}

		#endregion Public Methods
	}
}