﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 16/11/2010
 * Time: 10:08
 * 
 * © 1428-1431 (2007-2010) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using System;
using System.Globalization;

namespace Model
{
	/// <summary>
	/// Description of OpérationBaseDonnées.
	/// </summary>
	public abstract class OpérationBaseDonnées : FonctionalitéBaseDonnées
	{
		#region Public Methods

		///<summary>Permet d'ajouter un enregistrement
		/// </summary>
		/// <returns>null si tout c'est bien déroulé, sinon l'exception est retournée.</returns>
		public abstract Exception Ajouter();

		/// <summary>
		/// Permet de mettre à jour l'enregistrement dans la BD
		/// </summary>
		/// <param name="_ancienId">L'ancien Id de l'enregistrement à modifier</param>
		/// <returns>null si tout se passe bien, l'exception de l'erreur sinon</returns>
		public abstract Exception Modifier(long _ancienId);

		///<summary>Permet de supprimer un enregistrement
		/// </summary>
		/// <returns>null si tout c'est bien déroulé, sinon l'exception est retournée.</returns>
		public abstract Exception Supprimer();

		#endregion Public Methods
	}
}