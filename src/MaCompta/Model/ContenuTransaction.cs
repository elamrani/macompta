﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 19/11/2010
 * Time: 17:17
 * 
 * © 1428-1431 (2007-2010) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using System;

namespace Model
{
	/// <summary>
	/// Description of ContenuTransaction.
	/// </summary>
	public class ContenuTransaction : OpérationBaseDonnées
	{
		#region Fields

		private long _id;

		Compte _compte;
		double? _crédit;
		string _description;
		double? _débit;
		Projet _projet;
		Transaction _transaction;

		#endregion Fields

		#region Constructors

		/// <summary>
		/// préparation du contenu d'une transaction
		/// </summary>
		/// <param name="transaction">la transaction associée</param>
		public ContenuTransaction(BaseDonnées bd, Transaction transaction = null)
		{
			Initialisation(transaction);
			Connection = bd.Connection;
			Command = bd.Command;
		}

		/// <summary>
		/// Représente une ligne de transaction.
		/// Initialisation de la classe en utilisant obligatoirement
		/// les informations passées en paramètre
		/// </summary>
		/// <param name="bd">la base de données à utiliser</param>
		/// <param name="transaction">la transaction associée</param>
		/// <param name="description">description de la ligne de transaction</param>
		/// <param name="compte">compte associé à cette ligne de transaction</param>
		/// <param name="projet">projet associé à cette ligne de transaction</param>
		/// <param name="débit">le montant du débit ou null</param>
		/// <param name="crédit">le montant du crédit ou null</param>
		public ContenuTransaction( BaseDonnées bd,
			Transaction transaction,
			string description, Compte compte, Projet projet,
			double? débit, double? crédit)
		{
			Initialisation(transaction);
			Connection = bd.Connection;
			Command = bd.Command;
			_transaction = transaction;
			_description = description;
			_projet = projet;
			_compte = compte;
			_débit = débit;
			_crédit = crédit;
		}

		#endregion Constructors

		#region Public Properties

		public Compte Compte
		{
			get { return _compte; }
			set { _compte = value; }
		}

		public double? Crédit
		{
			get { return _crédit; }
			set { _crédit = value; }
		}

		public string Description
		{
			get { return _description; }
			set { _description = value; }
		}

		public double? Débit
		{
			get { return _débit; }
			set { _débit = value; }
		}

		public long Id
		{
			get { return _id;}
			set { _id = value;}
		}

		/// <summary>
		/// Transaction
		/// </summary>
		public Transaction Opération
		{
			get { return _transaction; }
			set { _transaction = value; }
		}

		public Projet Projet
		{
			get { return _projet; }
			set { _projet = value; }
		}

		#endregion Public Properties

		#region Private Methods

		private void Initialisation(Transaction transaction = null)
		{
			_id = -1;
			_compte = new Compte(this);
			_crédit = null;
			_description = string.Empty;
			_débit = null;
			_projet = null;
			_transaction = transaction;
		}

		#endregion Private Methods

		#region Public Methods

		public override Exception Ajouter()
		{
			Exception eValeurRetour = null;
			try {
				string sRequête = "INSERT INTO `contenu_transaction` (" +
					"`id_transaction`, `id_compte`, `id_projet`, `description`, " +
					"`debit`, `credit`) VALUES (" + _transaction.Id +
			        ", " + _compte.Id +
			        ", " + (_projet != null && _projet.Id != -1 ? _projet.Id.ToString() : NULL) +
			        ", " + Formatter(_description) +
					", " + Formatter(_débit) +
					", " + Formatter(_crédit) +
					")\0";
			    long? lIdRetourné = null;
			    eValeurRetour = ExécuterRequête(sRequête, ref lIdRetourné);
			    if ( eValeurRetour != null )
			    {
			        throw eValeurRetour;
			    }//if
			    else if ( lIdRetourné.HasValue )
			    {
			        _id = lIdRetourné.Value;
			    }//else if
				
			}// try
			catch (Exception ex) {
				eValeurRetour = ex;
				System.Diagnostics.Debug.WriteLine( Environment.NewLine );
				System.Diagnostics.Debug.WriteLine( ex.Message );
			}// catch
			return eValeurRetour;
		}

		public override Exception Modifier(long _ancienId)
		{
			throw new NotImplementedException("Modifier() : Cette fonction n'est pas censée être implémentée " +
			                                  "puisque pour modifier un contenu de transaction, " +
			                                  "il faut annuler l'ancien puis ajouter le nouveau contenu");
		}

		public override Exception Supprimer()
		{
			throw new NotImplementedException("Supprimer() : Cette fonction n'est pas censée être implémentée.");
		}

		#endregion Public Methods
	}
}