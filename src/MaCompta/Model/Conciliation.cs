﻿#region Header

/*
 * Created by SharpDevelop.
 * Author: Mohamed Y. ELAMRANI
 * Date: 05/08/1433
 * Time: 19:20
 * 
 * © 1428-1433 (2007-2012) All rights reserved to Mohamed Y. ElAmrani.
 */

#endregion Header

using System;

namespace Model
{
	/// <summary>
	/// Description of Conciliation.
	/// </summary>
	public class Conciliation : OpérationBaseDonnées
	{
		#region Fields

		string _date;
		long _id;
		string _reference;

		#endregion Fields

		#region Constructors

		public Conciliation(BaseDonnées bd)
		{
			Connection = bd.Connection;
			Command = bd.Command;
			Initialisation();
		}

		/// <summary>
		/// Permet d'instancier un Conciliation dont le nom est précisé
		/// </summary>
		/// <param name="bd">la bd à utiliser</param>
		/// <param name="dateACharger">la date à charger</param>
		/// <param name="valeurRetournée">null = le chargement des infos s'est bien déroulé; 
		/// sinon l'exception est retournée</param>
		public Conciliation(BaseDonnées bd, string dateACharger, ref Exception valeurRetournée)
		{
			Connection = bd.Connection;
			Command = bd.Command;
			Initialisation();
			valeurRetournée = Charger(dateACharger);
		}

		/// <summary>
		/// Permet d'instancier un Conciliation dont le id est précisé
		/// </summary>
		/// <param name="bd">la bd à utiliser</param>
		/// <param name="idACharger">le id à charger</param>
		/// <param name="valeurRetournée">null = le chargement des infos s'est bien déroulé; 
		/// sinon l'exception est retournée</param>
		public Conciliation(BaseDonnées bd, long idACharger, ref Exception valeurRetournée)
		{
			Connection = bd.Connection;
			Command = bd.Command;
			Initialisation();
			valeurRetournée = Charger(idACharger);
		}

		#endregion Constructors

		#region Public Properties

		public string Date
		{
			get { return _date; }
			set { _date = value; }
		}

		public long Id
		{
			get { return _id; }
			set { _id = value; }
		}

		public string Reference
		{
			get { return _reference; }
			set { _reference = value; }
		}

		#endregion Public Properties

		#region Private Methods

		private void Initialisation()
		{
			_id = -1;
			_date = string.Empty;
			_reference = string.Empty;
		}

		#endregion Private Methods

		#region Public Methods

		public override Exception Ajouter()
		{
			Exception eValeurRetour = null;
			try
			{
			    string sRequête = "INSERT INTO `conciliation` (`date`, `reference`) VALUES (" + Formatter(_date) +
			        ", " + Formatter(_reference) +
			        ")\0";
			    long? lIdRetourné = null;
			    eValeurRetour = ExécuterRequête(sRequête, ref lIdRetourné);
			    if ( eValeurRetour != null )
			    {
			        throw eValeurRetour;
			    }//if
			    else if ( lIdRetourné.HasValue )
			    {
			        _id = lIdRetourné.Value;
			    }//else if

			}// try
			catch ( Exception ex )
			{
			    eValeurRetour = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}// catch
			return eValeurRetour;
		}

		/// <summary>
		/// Chargement des informations
		/// </summary>
		/// <param name="id">le id du conciliation à charger</param>
		/// <returns>null si tout est bon, sinon, l'exception de l'erreur</returns>
		public Exception Charger(long id)
		{
			Exception eValeurRetour = null;
			try
			{
			    Command.CommandText = "SELECT * " +
			        "FROM `conciliation` " +
			        "WHERE `id` = \"" + id + "\"\0";
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(Command.CommandText);
			    Reader = Command.ExecuteReader();
			    if ( Reader.Read() )
			    {
			        _id = long.Parse(Reader["id"].ToString());
			        _date = Reader["date"].ToString();
			        _reference = Reader["reference"].ToString();
			    }//if
			    Reader.Close();
			}//try
			catch ( Exception e )
			{
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if				
			    eValeurRetour = e;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(e.Message);
			}//catch

			return eValeurRetour;
		}

		/// <summary>
		/// Chargement des informations
		/// </summary>
		/// <param name="date">le date de la conciliation à charger</param>
		/// <returns>null si tout est bon, sinon, l'exception de l'erreur</returns>
		public Exception Charger(string date)
		{
			Exception eValeurRetour = null;
			try
			{
			    Command.CommandText = "SELECT * " +
			        "FROM `conciliation` " +
			        "WHERE `date` = \"" + date + "\"\0";
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(Command.CommandText);
			    Reader = Command.ExecuteReader();
			    if ( Reader.Read() )
			    {
			        _id = long.Parse(Reader["id"].ToString());
			        _date = date;
			        _reference = Reader["reference"].ToString();
			    }//if
			    Reader.Close();
			}//try
			catch ( Exception e )
			{
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if				
			    eValeurRetour = e;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(e.Message);
			}//catch

			return eValeurRetour;
		}

		public override Exception Modifier(long _ancienId)
		{
			Exception eValeurRetour = null;
			try
			{
			    string sRequête = "UPDATE `conciliation` SET " +
			        "`id` = " + Formatter(_id) +
			        ", `date` = " + Formatter(_date) +
			        ", `reference` = " + Formatter(_reference) +
			        " WHERE `id` = " + Formatter(_ancienId) +
			        "\0";
			    long? lIdRetourné = null;
			    eValeurRetour = ExécuterRequête(sRequête, ref lIdRetourné);
			    if ( eValeurRetour != null )
			    {
			        throw eValeurRetour;
			    }//if
			}// try
			catch ( Exception ex )
			{
			    eValeurRetour = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}// catch
			return eValeurRetour;
		}

		public override Exception Supprimer()
		{
			return Supprimer(NomsTables.conciliation, _id);
		}

		#endregion Public Methods
	}
}