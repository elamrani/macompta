﻿using My.Forms.Controls.ListViews.XPTable.Models;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Text;

namespace Controller
{
	public class ListeProjetsController : Controller
	{
		#region Fields

		private CultureInfo _cultureIG;

		#endregion Fields

		#region Constructors

		public ListeProjetsController(Model.BaseDonnées db, CultureInfo cultureIG)
			: base(db)
		{
			_cultureIG = cultureIG;
		}

		#endregion Constructors

		#region Public Methods

		/// <summary>
		/// Permet d'afficher les totaux pour chaque projet
		/// </summary>
		/// <param name="tblListe">la liste à peupler</param>
		/// <returns>null = tout s'est bien passé, sinon l'exception est retournée.</returns>
		public Exception ChargerListeProjets(Table tblListe)
		{
			Exception eValeurRetournée = null;
			try
			{
			    tblListe.TableModel.Rows.Clear();
			    Command.CommandText = "SELECT * FROM `totaux_projets`\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    double dTotalDébits = 0, dTotalCrédits = 0, dTemp = 0;
			    while ( Reader.Read() )
			    {
			        string sDébit, sCrédit;
			        dTemp = double.Parse(Reader["debit"].ToString());
			        if ( dTemp == 0 )
			        {
			            sDébit = "-";
			        }//if
			        else
			        {
			            dTotalDébits += dTemp;
			            sDébit = dTemp.ToString("N2", _cultureIG);
			        }//else
			        dTemp = double.Parse(Reader["credit"].ToString());
			        if ( dTemp == 0 )
			        {
			            sCrédit = "-";
			        }//if
			        else
			        {
			            dTotalCrédits += dTemp;
			            sCrédit = dTemp.ToString("N2", _cultureIG);
			        }//else
			        tblListe.TableModel.Rows.Add(new Row(new Cell[] { 
			                new Cell(Reader["id_projet"].ToString()), 
			                new Cell(), 
			                new Cell(Reader["nom"].ToString()), 
			                new Cell(sDébit), 
			                new Cell(sCrédit)
			            }));
			    }//while
			    Reader.Close();
			    //Totaux
			    tblListe.TableModel.Rows.Add(
			        new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }
			        ));
			    Row ligneTotaux = new Row(new Cell[] { 
			        new Cell(), 
			        new Cell(), 
			        new Cell("TOTAL :"), 
			        new Cell(dTotalDébits.ToString("N2", _cultureIG)), 
			        new Cell(dTotalCrédits.ToString("N2", _cultureIG)) 
			    });
			    ligneTotaux.Font = new Font(tblListe.Font.FontFamily, tblListe.Font.Size + 1, FontStyle.Bold | FontStyle.Underline);
			    tblListe.TableModel.Rows.Add(ligneTotaux);
			}//try
			catch ( Exception ex )
			{
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    eValeurRetournée = ex;
			}//catch

			return eValeurRetournée;
		}

		/// <summary>
		/// Permet d'afficher les totaux pour chaque projet
		/// </summary>
		/// <param name="tblListe">la liste à peupler</param>
		/// <param name="sNomProjetACharger"></param>
		/// <param name="dtDateDébut"></param>
		/// <param name="dtDateFin"></param>
		/// <returns>null = tout s'est bien passé, sinon l'exception est retournée.</returns>
		public Exception ChargerListeTransactions(Table tblListe, string sNomProjetACharger, DateTime? dtDateDébut, DateTime dtDateFin)
		{
			Exception eValeurRetournée = null;
			try
			{
			    if ( !string.IsNullOrEmpty(sNomProjetACharger) )
			    {
			        tblListe.TableModel.Rows.Clear();
			        Row ligneTitre = new Row(new Cell[] { 
			                    new Cell(), //id projet
			                    new Cell(), //date
			                    new Cell(sNomProjetACharger), //détails
			                    new Cell(), //débit
			                    new Cell()  //crédit
			                });
			        ligneTitre.Font = new Font(tblListe.Font.FontFamily, tblListe.Font.Size + 1, FontStyle.Bold | FontStyle.Underline);
			        tblListe.TableModel.Rows.Add(ligneTitre);
			        tblListe.TableModel.Rows.Add(
			            new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }
			            ));
			    }//if
			    else
			    {
			        //Sans projet, ne rien charger
			        return null;
			    }//else

			    Command.CommandText = "SELECT * FROM `charger_transaction` WHERE `date` <= " + Formatter(dtDateFin);
			    if ( dtDateDébut.HasValue )
			    {
			        Command.CommandText += " AND `date` >= " + Formatter(dtDateDébut);
			    }//if
			    if ( !string.IsNullOrEmpty(sNomProjetACharger) )
			    {
			        try
			        {
			            Command.CommandText += " AND `projet` = " + Formatter(sNomProjetACharger);
			        }//try
			        catch ( Exception ex )
			        {
			            System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			            System.Diagnostics.Debug.WriteLine(ex.Message);
			        }//catch
			    }//if
			    Command.CommandText += "\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    double dTotalDébits = 0, dTotalCrédits = 0, dTemp = 0;
			    while ( Reader.Read() )
			    {
			        string description = string.Empty;
			        if ( !string.IsNullOrEmpty(Reader["id_compte"].ToString()) )
			        {
			            description += Reader["id_compte"].ToString() + "-" + Reader["compte"].ToString();
			        }//if
			        if ( !string.IsNullOrEmpty(Reader["partenaire"].ToString()) )
			        {
			            if ( !string.IsNullOrEmpty(description) )
			            {
			                description += ", ";
			            }//if
			            description += Reader["partenaire"].ToString();
			        }//if
			        if ( !string.IsNullOrEmpty(Reader["description_transaction"].ToString()) )
			        {
			            if ( !string.IsNullOrEmpty(description) )
			            {
			                description += ", ";
			            }//if
			            description += Reader["description_transaction"].ToString();
			        }//if
			        if ( !string.IsNullOrEmpty(Reader["description"].ToString()) )
			        {
			            if ( !string.IsNullOrEmpty(description) )
			            {
			                description += ", ";
			            }//if
			            description += Reader["description"].ToString() + ".";
			        }//if
			        else if ( !string.IsNullOrEmpty(description) )
			        {
			            description += ".";
			        }//else if
			        string sDébit, sCrédit;
			        if ( string.IsNullOrEmpty(Reader["debit"].ToString()) )
			        {
			            sDébit = "-";
			        }//if
			        else
			        {
			            dTemp = double.Parse(Reader["debit"].ToString());
			            dTotalDébits += dTemp;
			            sDébit = dTemp.ToString("N2", _cultureIG);
			        }//else
			        if ( string.IsNullOrEmpty(Reader["credit"].ToString()) )
			        {
			            sCrédit = "-";
			        }//if
			        else
			        {
			            dTemp = double.Parse(Reader["credit"].ToString());
			            dTotalCrédits += dTemp;
			            sCrédit = dTemp.ToString("N2", _cultureIG);
			        }//else
			        tblListe.TableModel.Rows.Add(new Row(new Cell[] { 
			                new Cell(Reader["id"].ToString()), 
			                new Cell(DateTime.Parse(Reader["date"].ToString()).ToShortDateString()), 
			                new Cell(description), 
			                new Cell(sDébit), 
			                new Cell(sCrédit)
			            }));
			    }//while
			    Reader.Close();
			    //Totaux
			    tblListe.TableModel.Rows.Add(
			        new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }
			        ));
			    Row ligneTotaux = new Row(new Cell[] { 
			        new Cell(), 
			        new Cell(), 
			        new Cell("TOTAL :"), 
			        new Cell(dTotalDébits.ToString("N2", _cultureIG)), 
			        new Cell(dTotalCrédits.ToString("N2", _cultureIG)) 
			    });
			    ligneTotaux.Font = new Font(tblListe.Font.FontFamily, tblListe.Font.Size + 1, FontStyle.Bold | FontStyle.Underline);
			    tblListe.TableModel.Rows.Add(ligneTotaux);
			}//try
			catch ( Exception ex )
			{
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    eValeurRetournée = ex;
			}//catch

			return eValeurRetournée;
		}

		#endregion Public Methods
	}
}