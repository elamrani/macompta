﻿using My.Forms.Controls.ListViews.XPTable.Models;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Text;

namespace Controller
{
	public class PlanComptableController : Controller
	{
		#region Fields

		private CultureInfo _cultureIG;

		#endregion Fields

		#region Constructors

		public PlanComptableController(Model.BaseDonnées db, CultureInfo cultureIG)
			: base(db)
		{
			_cultureIG = cultureIG;
		}

		#endregion Constructors

		#region Public Methods

		/// <summary>
		/// Permet de charger le plan comptable.
		/// </summary>
		/// <param name="tblPlanComptable">la table qui contiendra le plan comptable.</param>
		/// <returns>null: tout est OK, sinon l'exception est retournée.</returns>
		public Exception ChargerPlanComptable(Table tblPlanComptable)
		{
			Exception eValeurRetournée = null;

			try
			{
			    Cell cCompte, cType;
			    bool bAfficher;
			    long lIdTypeCompte;
			    long lIdCompte;
			    int iClasseCompte = 1;
			    bool bAjouterClasseCompte = true;
			    string sPréfixe = string.Empty;
			    Command.CommandText = "SELECT * FROM `charger_plan_comptable`\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    while ( Reader.Read() )
			    {
			        lIdCompte = long.Parse(Reader["id"].ToString());

			        if ( !bAjouterClasseCompte && lIdCompte >= ((iClasseCompte + 1) * 1000) )
			        {
			            bAjouterClasseCompte = true;
			            iClasseCompte++;
			        }//if
			        if ( bAjouterClasseCompte )
			        {
			            bAjouterClasseCompte = false;
			            tblPlanComptable.TableModel.Rows.Add(new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }));
			            Cell cell = new Cell();
			            cell.Font = new Font(tblPlanComptable.Font.FontFamily, tblPlanComptable.Font.Size + 1, FontStyle.Bold | FontStyle.Italic);
			            if ( iClasseCompte == 1 )
			            {
			                cell.Text = "ACTIF";
			            }//if
			            else if ( iClasseCompte == 2 )
			            {
			                cell.Text = "PASSIF";
			            }//else if
			            else if ( iClasseCompte == 3 )
			            {
			                cell.Text = "AVOIR";
			            }//else if
			            else if ( iClasseCompte == 4 )
			            {
			                cell.Text = "REVENU";
			            }//else if
			            else if ( iClasseCompte == 5 )
			            {
			                cell.Text = "DEPENSE";
			            }//else if
			            tblPlanComptable.TableModel.Rows.Add(new Row(new Cell[] { new Cell(), new Cell(), cell, new Cell(), new Cell(), new Cell() }));
			            tblPlanComptable.TableModel.Rows.Add(new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }));
			        }//if

			        cCompte = new Cell(Reader["compte"].ToString());
			        cType = new Cell(Reader["type"].ToString());
			        bAfficher = bool.Parse(Reader["afficher"].ToString());
			        lIdTypeCompte = long.Parse(Reader["id_type_compte"].ToString());
			        Row ligne = new Row(new Cell[] { new Cell(lIdCompte.ToString()), new Cell(lIdTypeCompte.ToString()), cCompte, cType, new Cell(), new Cell() });
			        FontStyle fs = FontStyle.Regular;
			        sPréfixe = string.Empty;

			        if (lIdTypeCompte == 1)
			        {
			            fs = FontStyle.Bold;
			        }//if
			        else if (lIdTypeCompte == 2)
			        {
			            fs = FontStyle.Regular;
			            sPréfixe = PREFIXE_COMPTE_NORMAL;
			            ligne.Cells[5].Text = CalculerSoldeCompte(lIdCompte, DateTime.Now, _cultureIG);
			            if ( string.IsNullOrEmpty(ligne.Cells[5].Text) )
			            {
			                ligne.Cells[5].Text = (0).ToString("N2", _cultureIG);
			            }//if
			        }//if
			        else if (lIdTypeCompte == 3)
			        {
			            fs = FontStyle.Italic;
			            sPréfixe = PREFIXE_COMPTE_SOUS_GROUPE;
			            ligne.Cells[4].Text = CalculerSoldeCompte(lIdCompte, DateTime.Now, _cultureIG);
			            if ( string.IsNullOrEmpty(ligne.Cells[4].Text) )
			            {
			                ligne.Cells[4].Text = (0).ToString("N2", _cultureIG);
			            }//if
			        }//if
			        else if (lIdTypeCompte == 4)
			        {
			            fs = FontStyle.Underline;
			            sPréfixe = PREFIXE_COMPTE_NORMAL;
			            ligne.Cells[5].Text = CalculerTotalSousGroupe(tblPlanComptable, 4, _cultureIG);
			            if ( string.IsNullOrEmpty(ligne.Cells[5].Text) )
			            {
			                ligne.Cells[5].Text = (0).ToString("N2", _cultureIG);
			            }//if
			        }//if
			        else if (lIdTypeCompte == 5)
			        {
			            fs = FontStyle.Bold | FontStyle.Underline;
			            ligne.Cells[5].Text = CalculerGrandTotal(tblPlanComptable, 4, _cultureIG);
			            if ( string.IsNullOrEmpty(ligne.Cells[5].Text) )
			            {
			                ligne.Cells[5].Text = (0).ToString("N2", _cultureIG);
			            }//if
			        }//if

			        if (!bAfficher)
			        {
			            fs |= FontStyle.Strikeout;
			        }//if
			        cCompte.Text = sPréfixe + cCompte.Text;
			        ligne.Font = new Font(tblPlanComptable.Font.FontFamily, tblPlanComptable.Font.Size, fs);
			        tblPlanComptable.TableModel.Rows.Add(ligne);
			        if ( lIdTypeCompte == 5 )
			        { //ajouter une ligne vide de séparation
			            tblPlanComptable.TableModel.Rows.Add(new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }));
			        }//if
			    
			    }//while
			    Reader.Close();
			}//try
			catch ( Exception ex )
			{
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    eValeurRetournée = ex;
			}//catch

			return eValeurRetournée;
		}

		#endregion Public Methods
	}
}