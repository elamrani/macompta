﻿using My.Forms.Controls.ListViews.XPTable.Models;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Controller
{
	public class RechercherTransactionController : Controller
	{
		#region Fields

		private CultureInfo _cultureIG;

		#endregion Fields

		#region Constructors

		public RechercherTransactionController(Model.BaseDonnées db, CultureInfo cultureIG)
			: base(db)
		{
			_cultureIG = cultureIG;
		}

		#endregion Constructors

		#region Public Methods

		/// <summary>
		/// Permet de peupler la table avec la liste des transactions respectant les critères sélectionnés
		/// </summary>
		/// <param name="tblListe">la table à peupler</param>
		/// <param name="date_début">la date de début</param>
		/// <param name="date_fin">la date de fin</param>
		/// <param name="partenaire">le partenaire</param>
		/// <param name="montant">le montant de la transaction</param>
		/// <param name="type_montant">1: "Montant Inférieur à", 2: "Montant Egale à", 3: "Montant Supérieur à"</param>
		/// <param name="id_compte">le compte</param>
		/// <returns>null: tout s'est bien passé, sinon, l'exception est retournée.</returns>
		public Exception Rechercher(
			Table tblListe, DateTime date_début, DateTime date_fin, string partenaire, string montant, int type_montant, string id_compte)
		{
			Exception eValeurRetournée = null;
			try
			{
			    tblListe.TableModel.Rows.Clear();
			    Command.CommandText = "SELECT * FROM `charger_transaction` WHERE (`date` BETWEEN " + Formatter(date_début) +
			        " AND " + Formatter(date_fin) + " ) ";
			    if ( !string.IsNullOrEmpty(partenaire) )
			    {
			        Command.CommandText += " AND (`partenaire` = " + Formatter(partenaire) + " ) ";
			    }//if
			    if ( !string.IsNullOrEmpty(montant) )
			    {
			        string sOpération = string.Empty;
			        if ( type_montant == 1 )
			        {
			            sOpération = " <= ";
			        }//if
			        else if ( type_montant == 2 )
			        {
			            sOpération = " = ";
			        }//else if
			        else if ( type_montant == 3 )
			        {
			            sOpération = " >= ";
			        }//else if
			        sOpération += Formatter(double.Parse(montant));
			        Command.CommandText += " AND ((`debit` " + sOpération + " ) OR (`credit` " + sOpération + " ))";
			    }//if
			    if ( !string.IsNullOrEmpty(id_compte) )
			    {
			        int iid_compte = -1;
			        try
			        {
			            iid_compte = int.Parse(id_compte);
			        }//try
			        catch ( Exception ex )
			        {
			            System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			            System.Diagnostics.Debug.WriteLine(ex.Message);
			        }//catch
			        Command.CommandText += " AND (`id_compte` = " + Formatter(iid_compte) + " ) ";
			    }//if
			    Command.CommandText += "\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    long id_ancienne_transaction = -1, id_transaction_courante = -1;
			    string date_transaction = string.Empty;
			    string nom_partenaire = string.Empty;
			    string designation = string.Empty;
			    while ( Reader.Read() )
			    {
			        string sDébit = string.Empty;
			        string sCrédit = string.Empty;
			        id_transaction_courante = long.Parse(Reader["id"].ToString());
			        if ( id_ancienne_transaction != id_transaction_courante )
			        {
			            id_ancienne_transaction = id_transaction_courante;
			            if ( tblListe.TableModel.Rows.Count > 0 )
			            {
			                tblListe.TableModel.Rows.Add(
			                    new Row(new Cell[] { 
			                    new Cell(), //id transaction
			                    new Cell(), //date
			                    new Cell(), //partenaire
			                    new Cell(), //compte
			                    new Cell(), //débit
			                    new Cell()  //crédit
			                }));
			            }//if
			            date_transaction = DateTime.Parse(Reader["date"].ToString()).ToShortDateString();
			            nom_partenaire = Reader["partenaire"].ToString();
			            designation = Reader["description_transaction"].ToString();
			        }//if
			        else
			        {
			            date_transaction = string.Empty;
			            nom_partenaire = string.Empty;
			            designation = string.Empty;
			        }//else
			        if ( !string.IsNullOrEmpty(Reader["debit"].ToString()) )
			        {
			            sDébit = double.Parse(Reader["debit"].ToString()).ToString("N2", _cultureIG);
			        }//if
			        if ( !string.IsNullOrEmpty(Reader["credit"].ToString()) )
			        {
			            sCrédit = double.Parse(Reader["credit"].ToString()).ToString("N2", _cultureIG);
			        }//if
			        tblListe.TableModel.Rows.Add(new Row(new Cell[] { 
			                new Cell(Reader["id"].ToString()), 
			                new Cell(date_transaction), 
			                new Cell(nom_partenaire), 
			                new Cell(designation), 
			                new Cell(Reader["id_compte"].ToString() + "-" + Reader["compte"].ToString()), 
			                new Cell(sDébit),
			                new Cell(sCrédit)
			            }));
			    }//while
			    Reader.Close();
			}//try
			catch ( Exception ex )
			{
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    eValeurRetournée = ex;
			}//catch

			return eValeurRetournée;
		}

		#endregion Public Methods
	}
}