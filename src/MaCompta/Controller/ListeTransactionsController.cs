﻿using My.Forms.Controls.ListViews;
using My.Forms.Controls.ListViews.XPTable.Models;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Text;

namespace Controller
{
	public class ListeTransactionsController : Controller
	{
		#region Fields

		private CultureInfo _cultureIG;

		#endregion Fields

		#region Constructors

		public ListeTransactionsController(Model.BaseDonnées db, CultureInfo cultureIG)
			: base(db)
		{
			_cultureIG = cultureIG;
		}

		#endregion Constructors

		#region Public Methods

		/// <summary>
		/// Permet de préparer la liste des transactions dans la table d'affichage
		/// </summary>
		/// <param name="tblListe">la table d'affichage</param>
		/// <param name="sNomCompteACharger">le id-nom de la transaction à charger</param>
		/// <param name="dtDateDébut">la date de début si disponible, sinon, afficher la liste complète</param>
		/// <param name="dtDateFin">la da te de fin de la liste</param>
		/// <returns>null = tout s'est bien passé, sinon, l'exception est retournée.</returns>
		public Exception ChargerListeTransactions(Table tblListe, string sNomCompteACharger, DateTime? dtDateDébut, DateTime dtDateFin)
		{
			Exception eValeurRetournée = null;
			double dSolde = 0;
			try
			{
			    tblListe.TableModel.Rows.Clear();
			    if ( !string.IsNullOrEmpty(sNomCompteACharger) )
			    {
			        Row ligneTitre = new Row(new Cell[] { 
			                    new Cell(), //id transaction
			                    new Cell(), //id compte
			                    new Cell(), //nom compte
			                    new Cell(), //date
			                    new Cell(sNomCompteACharger), //détails
			                    new Cell(), //débit
			                    new Cell(),  //crédit
			                    new Cell()  //solde
			                });
			        ligneTitre.Font = new Font(tblListe.Font.FontFamily, tblListe.Font.Size + 1, FontStyle.Bold | FontStyle.Underline);
			        tblListe.TableModel.Rows.Add(ligneTitre);
			        tblListe.TableModel.Rows.Add(
			            new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }
			            ));
			        string sDateSolde;
			        if ( dtDateDébut.HasValue )
			        {
			            sDateSolde = "Solde au " + dtDateDébut.Value.AddDays(-1).ToShortDateString() + " :";
			        }//if
			        else
			        {
			            Command.CommandText = "SELECT * FROM `charger_transaction` ORDER BY `date` LIMIT 1";
			            Debug();
			            Reader = Command.ExecuteReader();
			            if ( Reader.Read() )
			            {
			                sDateSolde = "Solde au " + DateTime.Parse(Reader["date"].ToString()).AddDays(-1).ToShortDateString() + " :";
			            }//if
			            else
			            {
			                sDateSolde = "Solde :";
			            }//else
			            Reader.Close();
			        }//else
			        tblListe.TableModel.Rows.Add(
			            new Row(new Cell[] { 
			                    new Cell(), //id transaction
			                    new Cell(), //id compte
			                    new Cell(), //nom compte
			                    new Cell(), //date
			                    new Cell(sDateSolde), //détails
			                    new Cell(), //débit
			                    new Cell(), //crédit
			                    new Cell()  //solde
			                }));
			    }//if
			    Command.CommandText = "SELECT * FROM `charger_transaction` WHERE `date` <= " + Formatter(dtDateFin);
			    if ( dtDateDébut.HasValue )
			    {
			        Command.CommandText += " AND `date` >= " + Formatter(dtDateDébut);
			    }//if
			    if ( !string.IsNullOrEmpty(sNomCompteACharger) )
			    {
			        try
			        {
			            Command.CommandText += " AND `id_compte` = " + Formatter(long.Parse(sNomCompteACharger.Substring(0, 4)));
			        }//try
			        catch ( Exception ex )
			        {
			            System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			            System.Diagnostics.Debug.WriteLine(ex.Message);
			        }//catch
			    }//if
			    Command.CommandText += "\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    double dTotalDébits = 0, dTotalCrédits = 0, dTemp = 0;
			    long id_ancienne_transaction = -1, id_transaction_courante = -1;
			    string description = string.Empty;
			    string date_transaction = string.Empty;
			    while ( Reader.Read() )
			    {
			        id_transaction_courante = long.Parse(Reader["id"].ToString());
			        if ( id_ancienne_transaction != id_transaction_courante )
			        {
			            id_ancienne_transaction = id_transaction_courante;
			            if ( string.IsNullOrEmpty(sNomCompteACharger) )
			            {
			                tblListe.TableModel.Rows.Add(
			                    new Row(new Cell[] { 
			                    new Cell(), //id transaction
			                    new Cell(), //id compte
			                    new Cell(), //nom compte
			                    new Cell(), //date
			                    new Cell(), //détails
			                    new Cell(), //débit
			                    new Cell(), //crédit
			                    new Cell()  //solde
			                }));
			                description = string.Empty;
			                if ( !string.IsNullOrEmpty(Reader["partenaire"].ToString()) )
			                {
			                    description += Reader["partenaire"].ToString();
			                }//if
			                if ( !string.IsNullOrEmpty(Reader["description_transaction"].ToString()) )
			                {
			                    if ( !string.IsNullOrEmpty(description) )
			                    {
			                        description += ", ";
			                    }//if
			                    description += Reader["description_transaction"].ToString();
			                }//if
			                Row ligne = new Row(new Cell[] { 
			                        new Cell(Reader["id"].ToString()), //id transaction
			                        new Cell(), //id compte
			                        new Cell(), //nom compte
			                        new Cell(DateTime.Parse(Reader["date"].ToString()).ToShortDateString()), //date
			                        new Cell(description), //détails
			                        new Cell(), //débit
			                        new Cell(), //crédit
			                        new Cell()  //solde
			                    });
			                ligne.Font = new Font(tblListe.Font.FontFamily, tblListe.Font.Size, FontStyle.Italic | FontStyle.Bold | FontStyle.Underline);
			                tblListe.TableModel.Rows.Add(ligne);
			            }//if
			        }//if
			        description = string.Empty;
			        date_transaction = string.Empty;
			        if ( string.IsNullOrEmpty(sNomCompteACharger) )
			        {
			            description = Reader["id_compte"].ToString() + "-" + Reader["compte"].ToString();
			        }//if
			        else
			        {
			            description = string.Empty;
			            date_transaction = DateTime.Parse(Reader["date"].ToString()).ToShortDateString();

			            if ( !string.IsNullOrEmpty(Reader["partenaire"].ToString()) )
			            {
			                description += Reader["partenaire"].ToString();
			            }//if
			            if ( !string.IsNullOrEmpty(Reader["description_transaction"].ToString()) )
			            {
			                if ( !string.IsNullOrEmpty(description) )
			                {
			                    description += ", ";
			                }//if
			                description += Reader["description_transaction"].ToString();
			            }//if
			        }//else
			        if ( !string.IsNullOrEmpty(Reader["description"].ToString()) )
			        {
			            if ( !string.IsNullOrEmpty(description) )
			            {
			                description += ", ";
			            }//if
			            description += Reader["description"].ToString();
			        }//if
			        if ( !string.IsNullOrEmpty(Reader["projet"].ToString()) )
			        {
			            if ( !string.IsNullOrEmpty(description) )
			            {
			                description += ". ";
			            }//if
			            description += " <" + Reader["projet"].ToString() + ">";
			        }//if
			        else if ( !string.IsNullOrEmpty(description) )
			        {
			            description += ".";
			        }//else if
			        string sDébit, sCrédit;
			        if ( string.IsNullOrEmpty(Reader["debit"].ToString()) )
			        {
			            sDébit =  "-";
			        }//if
			        else
			        {
			            dTemp = double.Parse(Reader["debit"].ToString());
			            dTotalDébits += dTemp;
			            sDébit = dTemp.ToString("N2", _cultureIG);
			        }//else
			        if ( string.IsNullOrEmpty(Reader["credit"].ToString()) )
			        {
			            sCrédit =  "-";
			        }//if
			        else
			        {
			            dTemp = double.Parse(Reader["credit"].ToString());
			            dTotalCrédits += dTemp;
			            sCrédit = dTemp.ToString("N2", _cultureIG);
			        }//else
			        tblListe.TableModel.Rows.Add(new Row(new Cell[] { 
			                new Cell(Reader["id"].ToString()), 
			                new Cell(Reader["id_compte"].ToString()), 
			                new Cell(Reader["compte"].ToString()), 
			                new Cell(date_transaction), 
			                new Cell(description), 
			                new Cell(sDébit), 
			                new Cell(sCrédit), 
			                new Cell()
			            }));
			    }//while
			    Reader.Close();

			    //Calcul du solde
			    if ( !string.IsNullOrEmpty(sNomCompteACharger) && 
			        //!string.IsNullOrEmpty(tblListe.TableModel.Rows[3].Cells[3].Text)) //date de la première transaction
			        tblListe.TableModel.Rows.Count > 3 )
			    {
			        Command.CommandText = "SELECT SUM( IFNULL(`contenu_transaction`.`credit`, 0) - IFNULL(`contenu_transaction`.`debit`, 0) ) AS solde " +
			            "FROM `contenu_transaction`, `transaction`, `compte` " +
			            "WHERE (`contenu_transaction`.`id_compte` = `compte`.`id`) " +
			            "AND (`contenu_transaction`.`id_transaction` = `transaction`.`id`) " +
			            "AND  (`compte`.`id` = " + long.Parse(sNomCompteACharger.Substring(0, 4)).ToString() + ") ";
			        try
			        {
			            DateTime dt = DateTime.Parse(tblListe.TableModel.Rows[3].Cells[3].Text);//1ere transaction
			            Command.CommandText += "AND (`transaction`.`date` < " + Formatter(dt) + ") ";
			        }//try
			        catch ( Exception ex )
			        {
			            System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			            System.Diagnostics.Debug.WriteLine(ex.Message);
			        }//catch
			        Command.CommandText += "\0";
			        Debug();
			        Reader = Command.ExecuteReader();
			        if ( Reader.Read() )
			        {
			            try 
			            {
			                dSolde = double.Parse(Reader["solde"].ToString());
			            }//try
			            catch (Exception ex)
			            {
			                System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			                System.Diagnostics.Debug.WriteLine(ex.Message);
			                dSolde = 0;
			            }//catch
			        }//if
			        Reader.Close();
			        tblListe.TableModel.Rows[2].Cells[7].Text = dSolde.ToString("N2", _cultureIG);
			        for ( int i = 3; i < tblListe.TableModel.Rows.Count; i++ )
			        {
			            Row ligneCourante = tblListe.TableModel.Rows[i];
			            try
			            {
			                dSolde -= double.Parse(ligneCourante.Cells[5].Text);
			            }//try
			            catch ( Exception ex )
			            {
			                System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			                System.Diagnostics.Debug.WriteLine(ex.Message);
			            }//catch

			            try
			            {
			                dSolde += double.Parse(ligneCourante.Cells[6].Text);
			            }//try
			            catch ( Exception ex )
			            {
			                System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			                System.Diagnostics.Debug.WriteLine(ex.Message);
			            }//catch

			            ligneCourante.Cells[7].Text = dSolde.ToString("N2", _cultureIG);
			        }//for i
			    }//if
			    //Totaux
			    tblListe.TableModel.Rows.Add(
			        new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }
			        ));
			    Row ligneTotaux = new Row(new Cell[] { 
			        new Cell(), 
			        new Cell(), 
			        new Cell(), 
			        new Cell(), 
			        new Cell("TOTAUX :"), 
			        new Cell(dTotalDébits.ToString("N2", _cultureIG)), 
			        new Cell(dTotalCrédits.ToString("N2", _cultureIG)),
			        new Cell(dSolde.ToString("N2", _cultureIG))
			    });
			    ligneTotaux.Font = new Font(tblListe.Font.FontFamily, tblListe.Font.Size + 1, FontStyle.Bold |FontStyle.Underline);
			    tblListe.TableModel.Rows.Add(ligneTotaux);
			}//try
			catch ( Exception ex )
			{
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    eValeurRetournée = ex;
			}//catch

			return eValeurRetournée;
		}

		#endregion Public Methods
	}
}