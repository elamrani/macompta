﻿using My.Forms.Controls.ListViews.XPTable.Models;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Controller
{
	public class AnsibaController : Controller
	{
		#region Fields

		private CultureInfo _cultureIG;

		#endregion Fields

		#region Constructors

		public AnsibaController(Model.BaseDonnées db, CultureInfo cultureIG)
			: base(db)
		{
			_cultureIG = cultureIG;
		}

		#endregion Constructors

		#region Public Methods

		/// <summary>
		/// Permet de charger ansiba.
		/// </summary>
		/// <param name="tblListe">la table qui contiendra les ansiba.</param>
		/// <returns>null: tout est OK, sinon l'exception est retournée.</returns>
		public Exception ChargerAnsiba(Table tblListe)
		{
			Exception eValeurRetournée = null;
			try {
			    Command.CommandText = "SELECT * FROM `ansiba` ORDER BY `supprimee`, `date` desc\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    tblListe.TableModel.Rows.Clear();
			    while (Reader.Read()) {
			        Cell cSupprimee = new Cell();
			        cSupprimee.Checked = bool.Parse(Reader["supprimee"].ToString());
			        tblListe.TableModel.Rows.Add(new Row(new Cell[] { 
			                new Cell(Reader["id"].ToString()), 
			                new Cell(DateTime.Parse(Reader["date"].ToString()).ToShortDateString()), 
			                new Cell(double.Parse(Reader["montant_total"].ToString()).ToString("N2", _cultureIG)), 
			                new Cell(double.Parse(Reader["montant_nisab"].ToString()).ToString("N2", _cultureIG)),
			                new Cell(double.Parse(Reader["montant_zakat"].ToString()).ToString("N2", _cultureIG)),
			                cSupprimee
			            }));
			    }//while
			    Reader.Close();
			}//try
			catch (Exception ex) {
			    if (!Reader.IsClosed) {
			        Reader.Close();
			    }//if
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    eValeurRetournée = ex;
			}//catch

			return eValeurRetournée;
		}

		#endregion Public Methods
	}
}