﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Controller
{
	public class MainController : Controller
	{
		#region Constructors

		public MainController(Model.BaseDonnées db)
			: base(db)
		{
		}

		#endregion Constructors

		#region Public Methods

		/// <summary>
		/// Permet de charger le mot de passe
		/// </summary>
		/// <param name="_mot_passe">le mot de passe</param>
		/// <returns>null si tout s'est bien déroulé, sinon, l'exception est retournée</returns>
		public Exception ChargerConfiguration(ref string mot_passe)
		{
			Exception eValeurRetournée = null;
			try {
			    Command.CommandText = "SELECT * FROM `configuration`\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    if (Reader.Read()) {
			        mot_passe = Reader["mot_passe"].ToString();
			    }//if
			    Reader.Close();
			}//try
			catch (Exception ex) {
			    eValeurRetournée = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    try {
			        if (!Reader.IsClosed) {
			            Reader.Close();
			        }//if
			    }//try
			    catch (Exception e) {
			        System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine(e.Message);
			    }//catch
			}//catch

			return eValeurRetournée;
		}

		/// <summary>
		/// Permet de charger le type de suppressions des transactions
		/// </summary>
		/// <param name="configuration">Configuration</param>
		/// <returns>null si tout s'est bien déroulé, sinon, l'exception est retournée</returns>
		public Exception ChargerConfiguration(ref Model.Configuration configuration)
		{
			Exception eValeurRetournée = null;
			try {
			    configuration = new Model.Configuration(this, ref eValeurRetournée);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			    eValeurRetournée = configuration.Charger();
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			}//try
			catch (Exception ex) {
			    eValeurRetournée = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}//catch

			return eValeurRetournée;
		}

		/// <summary>
		/// Permet de charger la date de la dernière opération dans les comptes et de le
		/// retourner dans la variable fournie en paramètre
		/// </summary>
		/// <param name="dtDateSoldeTotal">La date de la dernière transaction ou NULL</param>
		/// <returns>null si tout s'est bien déroulé, sinon l'exception est retournée</returns>
		public Exception ChargerDateSoldeTotal(ref DateTime? dtDateSoldeTotal)
		{
			Exception eValeurRetournée = null;
			dtDateSoldeTotal = null;
			try {
			    Command.CommandText = "SELECT `date` FROM `charger_transaction` " +
			        "ORDER BY `date` DESC LIMIT 1\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    if (Reader.Read()) {
			        if (!string.IsNullOrEmpty(Reader[0].ToString())) {
			            dtDateSoldeTotal = DateTime.Parse(Reader[0].ToString());
			        }//if
			    }//if
			    Reader.Close();
			}//try
			catch (Exception ex) {
			    eValeurRetournée = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    try {
			        if (!Reader.IsClosed) {
			            Reader.Close();
			        }//if
			    }//try
			    catch (Exception e) {
			        System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine(e.Message);
			    }//catch
			}//catch

			return eValeurRetournée;
		}

		/// <summary>
		/// Permet de charger le solde total et de le retourner dans la variable fournie en paramètre
		/// </summary>
		/// <param name="dSoldeTotalChargé">Le solde total ou bien null si une erreur est survenue</param>
		/// <returns>null si tout s'est bien déroulé, sinon l'exception est retournée</returns>
		public Exception ChargerSoldeTotal(ref double? dSoldeTotalChargé)
		{
			Exception eValeurRetournée = null;
			dSoldeTotalChargé = null;
			try {
			    Command.CommandText = "SELECT * FROM `solde_total`\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    if (Reader.Read()) {
			        if (!string.IsNullOrEmpty(Reader[0].ToString())) {
			            dSoldeTotalChargé = double.Parse(Reader[0].ToString());
			        }//if
			    }//if
			    Reader.Close();
			}//try
			catch (Exception ex) {
			    eValeurRetournée = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    try {
			        if (!Reader.IsClosed) {
			            Reader.Close();
			        }//if
			    }//try
			    catch (Exception e) {
			        System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine(e.Message);
			    }//catch
			}//catch

			return eValeurRetournée;
		}

		/// <summary>
		/// Permet de sauvegarder la BD
		/// </summary>
		/// <param name="cheminMySqlBin">Chemin du répertoire bin de MySql. Ex: "C:\Program Files\MySQL\MySQL Server 5.5\bin\"</param>
		/// <param name="chemin1">Chemin d'enregistrement. Ex: "C:\"</param>
		/// <param name="chemin2">2e chemin d'enregistrement. Ex: "Z:\"</param>
		/// <returns>null = tout s'est bien passé, sinon l'exception est retournée</returns>
		public Exception CopieSecours(string cheminMySqlBin, string chemin1, string chemin2)
		{
			Exception eValeurRertournée = null;
			if (!string.IsNullOrEmpty(chemin1) && !string.IsNullOrEmpty(chemin2)) {
			    try {
			        string nomFichier = "macompta.backup." +
			            DateTime.Now.Year.ToString("D4") +
			            DateTime.Now.Month.ToString("D2") +
			            DateTime.Now.Day.ToString("D2") + ".sql";
			        StreamWriter file = new StreamWriter(chemin1 + nomFichier, false, System.Text.Encoding.UTF8);
			        ProcessStartInfo proc = new ProcessStartInfo();
			        string cmd = string.Format(@"--default-character-set=latin1 -u{0} -p{1} -h{2} {3}", NomUsager, MotPasse, NomHôte, NomBaseDonnées);
			        proc.FileName = cheminMySqlBin + "mysqldump";
			        proc.RedirectStandardInput = false;
			        proc.RedirectStandardOutput = true;
			        proc.Arguments = cmd;
			        proc.UseShellExecute = false;
			        proc.WindowStyle = ProcessWindowStyle.Hidden;
			        proc.CreateNoWindow = true;
			        Process p = Process.Start(proc);
			        string res = p.StandardOutput.ReadToEnd();
			        res = res.Replace("/*!40101 SET NAMES latin1", "/*!40101 SET NAMES utf8");
			        file.WriteLine(res);
			        p.WaitForExit();
			        file.Close();
			        //Copie de l'archive
			        try {
			            File.Delete(chemin2 + nomFichier);
			        }//try
			        catch (Exception ex) {
			            System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			            System.Diagnostics.Debug.WriteLine(ex.Message);
			        }//catch

			        try {
			            File.Copy(chemin1 + nomFichier, chemin2 + nomFichier);
			        }//try
			        catch (Exception ex) {
			            System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			            System.Diagnostics.Debug.WriteLine(ex.Message);
			        }//catch

			    }//try
			    catch (Exception ex) {
			        System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine(ex.Message);
			        eValeurRertournée = ex;
			    }//catch
			}//if
			return eValeurRertournée;
		}

		/// <summary>
		/// Verifie si le mot de passe est bon
		/// </summary>
		/// <param name="motPasse">le mot de passe à vérifier</param>
		/// <param name="motPasseOK">true : le mot de passe est bon, false : pas bon</param>
		/// <returns>null si tout s'est bien déroulé, sinon, l'exception est retournée</returns>
		public Exception VérifierMotPasse(string motPasse, ref bool motPasseOK)
		{
			Exception eValeurRetournée = null;
			try {
			    Command.CommandText = "SELECT * FROM `configuration` " +
			        "WHERE `mot_passe` = " + Formatter(motPasse, false) + "\0";
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(Command.CommandText);
			    Reader = Command.ExecuteReader();
			    if (Reader.Read()) {
			        Reader.Close();
			        //Mot de passe correct
			        motPasseOK = true;
			    }//if
			    else {
			        Reader.Close();
			        motPasseOK = false;
			    }//else
			}//try
			catch (Exception e) {
			    eValeurRetournée = e;
			    motPasseOK = false;
			    try {
			        Reader.Close();
			    }//try
			    catch (Exception ex) {
			        System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine(ex.Message);
			    }//catch
			}//catch

			return eValeurRetournée;
		}

		public Exception VérifierNisab(double solde_total, double nisab_actuel)
		{
			Exception eValeurRetournée = null;
			try {
			    double dTotalMontants = 0;
			    Command.CommandText = "SELECT SUM(`montant_total`) FROM `ansiba` " +
			        "WHERE `supprimee` = FALSE\0";
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(Command.CommandText);
			    Reader = Command.ExecuteReader();
			    if (Reader.Read()) {
			        dTotalMontants = double.Parse(Reader[0].ToString());
			    }//if
			    Reader.Close();

			    if (dTotalMontants + nisab_actuel <= solde_total) {
			        //Un nouveau nisab est atteint.
			    }//if
			    else if (dTotalMontants < solde_total) {
			        //Un nisab (ou plusieurs) doivent etre supprimee.
			    }//else if

			}//try
			catch (Exception e) {
			    eValeurRetournée = e;
			    try {
			        Reader.Close();
			    }//try
			    catch (Exception ex) {
			        System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine(ex.Message);
			    }//catch
			}//catch

			return eValeurRetournée;
		}

		#endregion Public Methods
	}
}