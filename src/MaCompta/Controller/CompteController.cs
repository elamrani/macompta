﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Controller
{
	public class CompteController : Controller
	{
		#region Constructors

		public CompteController(Model.BaseDonnées db)
			: base(db)
		{
		}

		#endregion Constructors

		#region Public Methods

		/// <summary>
		/// Permet de charger la date de la dernière opération dans un compte et de le
		/// retourner dans la variable fournie en paramètre
		/// </summary>
		/// <param name="lIdCompte">Le id du compte</param>
		/// <param name="dtDateSoldeTotal">La date de la dernière transaction ou NULL</param>
		/// <returns>null si tout s'est bien déroulé, sinon l'exception est retournée</returns>
		public Exception ChargerDateSoldeCompte(long lIdCompte, ref DateTime? dtDateSoldeTotal)
		{
			Exception eValeurRetournée = null;
			dtDateSoldeTotal = null;
			try
			{
			    Command.CommandText = "SELECT `date` FROM `charger_transaction` WHERE `id_compte` = " +
			        Formatter(lIdCompte) + " ORDER BY `date` DESC LIMIT 1\0";
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(Command.CommandText);
			    Reader = Command.ExecuteReader();
			    if (Reader.Read())
			    {
			        if (!string.IsNullOrEmpty(Reader[0].ToString()))
			        {
			            dtDateSoldeTotal = DateTime.Parse(Reader[0].ToString());
			        }//if
			    }//if
			    else
			    {
			        dtDateSoldeTotal = DateTime.Now;
			    }//else
			    Reader.Close();
			}//try
			catch (Exception ex)
			{
			    eValeurRetournée = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    try
			    {
			        if (!Reader.IsClosed)
			        {
			            Reader.Close();
			        }//if
			    }//try
			    catch (Exception e)
			    {
			        System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine(e.Message);
			    }//catch
			}//catch

			return eValeurRetournée;
		}

		#endregion Public Methods
	}
}