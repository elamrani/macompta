﻿using My.Forms.Controls.Charts.PieChart;
using My.Forms.Controls.ListViews.XPTable.Models;

using MySql.Data.MySqlClient;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Text;

namespace Controller
{
	public class BilanController : Controller
	{
		#region Fields

		private CultureInfo _cultureIG;

		#endregion Fields

		#region Constructors

		public BilanController(Model.BaseDonnées db, CultureInfo cultureIG)
			: base(db)
		{
			_cultureIG = cultureIG;
		}

		#endregion Constructors

		#region Private Methods

		/// <summary>
		/// Ajoute la différence : Revenus - Dépenses à la table
		/// </summary>
		/// <param name="tblPlanComptable">la table</param>
		/// <returns>null: tout est OK, sinon l'exception est retournée.</returns>
		private Exception CalculerBénéficesNet(Table tblPlanComptable)
		{
			Exception eValeurRetournée = null;
			try {
			    int index = tblPlanComptable.TableModel.Rows.Count;
			    double totalRevenus = 0;
			    double totalDépenses = 0;
			    int i;
			    for (i = index - 3; tblPlanComptable.TableModel.Rows[i].Cells[0].Text != STR_TOTAL; i--) {
			        ;
			    }//for i
			    //Revenus
			    if (!string.IsNullOrEmpty(tblPlanComptable.TableModel.Rows[i].Cells[1].Text)) {
			        if (int.Parse(tblPlanComptable.TableModel.Rows[i].Cells[1].Text) == 4
			            && !string.IsNullOrEmpty(tblPlanComptable.TableModel.Rows[i].Cells[4].Text)) {
			            totalRevenus = double.Parse(tblPlanComptable.TableModel.Rows[i].Cells[4].Text);
			        }//if
			    }//if
			    //Dépenses
			    if (!string.IsNullOrEmpty(tblPlanComptable.TableModel.Rows[index - 2].Cells[1].Text)) {
			        if (int.Parse(tblPlanComptable.TableModel.Rows[index - 2].Cells[1].Text) == 5
			            && !string.IsNullOrEmpty(tblPlanComptable.TableModel.Rows[index - 2].Cells[4].Text)) {
			            totalDépenses = double.Parse(tblPlanComptable.TableModel.Rows[index - 2].Cells[4].Text);
			        }//if
			    }//if

			    Row ligne = new Row(new Cell[] { 
			        new Cell(STR_TOTAL), 
			        new Cell(), 
			        new Cell("BENEFICE NET :"), 
			        new Cell(), 
			        new Cell((totalRevenus - totalDépenses).ToString("N2", _cultureIG)) 
			    });
			    ligne.Font = new Font(tblPlanComptable.Font.FontFamily, tblPlanComptable.Font.Size + 2, FontStyle.Bold | FontStyle.Underline | FontStyle.Italic);
			    tblPlanComptable.TableModel.Rows.Add(new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }));
			    tblPlanComptable.TableModel.Rows.Add(ligne);
			    tblPlanComptable.TableModel.Rows.Add(new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }));

			}//try
			catch (Exception ex) {
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    eValeurRetournée = ex;
			}//catch

			return eValeurRetournée;
		}

		/// <summary>
		/// Permet de calculer et d'afficher le total d'une section donnée
		/// </summary>
		/// <param name="tblPlanComptable">la table contenant les comptes</param>
		/// <returns>null: tout est OK, sinon l'exception est retournée.</returns>
		private Exception CalculerTotalSection(Table tblPlanComptable)
		{
			Exception eValeurRetournée = null;
			try {
			    int index = tblPlanComptable.TableModel.Rows.Count;
			    double total = 0;
			    int i;
			    for (i = index - 1; tblPlanComptable.TableModel.Rows[i].Cells[0].Text != "-"; i--) {
			        if (!string.IsNullOrEmpty(tblPlanComptable.TableModel.Rows[i].Cells[1].Text)) {
			            if (int.Parse(tblPlanComptable.TableModel.Rows[i].Cells[1].Text) == 5
			                && !string.IsNullOrEmpty(tblPlanComptable.TableModel.Rows[i].Cells[4].Text)) {
			                total += double.Parse(tblPlanComptable.TableModel.Rows[i].Cells[4].Text);
			            }//if
			        }//if
			    }//for i
			    Row ligne = new Row(new Cell[] { 
			        new Cell(STR_TOTAL), 
			        new Cell(tblPlanComptable.TableModel.Rows[i].Cells[1].Text), 
			        new Cell(STR_TOTAL + " " + tblPlanComptable.TableModel.Rows[i].Cells[2].Text + " :"), 
			        new Cell(), 
			        new Cell(total.ToString("N2", _cultureIG)) 
			    });
			    ligne.Font = new Font(tblPlanComptable.Font.FontFamily, tblPlanComptable.Font.Size + 1, FontStyle.Bold | FontStyle.Underline | FontStyle.Italic);
			    tblPlanComptable.TableModel.Rows.Add(ligne);
			    tblPlanComptable.TableModel.Rows.Add(new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }));

			}//try
			catch (Exception ex) {
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    eValeurRetournée = ex;
			}//catch

			return eValeurRetournée;
		}

		#endregion Private Methods

		#region Public Methods

		/// <summary>
		/// Permet de charger le plan comptable.
		/// </summary>
		/// <param name="dateFin">Date de fin du bilan</param>
		/// <param name="tblListe">la table qui contiendra le plan comptable.</param>
		/// <param name="bMasquerComptesVides">Doit-on afficher les comptes vides ou les masquer?</param>
		/// <param name="bAfficherZéros">Si on affiche les comptes, doit-on afficher 0 ou texte vide en cas de solde nul?</param>
		/// <returns>null: tout est OK, sinon l'exception est retournée.</returns>
		public Exception ChargerBilan(DateTime dateFin, Table tblListe, bool bMasquerComptesVides, bool bAfficherZéros)
		{
			Exception eValeurRetournée = null;
			try {
			    Cell cCompte;
			    long lIdTypeCompte;
			    long lIdCompte;
			    int iClasseCompte = 1;
			    bool bAjouterClasseCompte = true;
			    string sPréfixe = string.Empty;
			    Command.CommandText = "SELECT * FROM `charger_plan_comptable`\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    tblListe.TableModel.Rows.Clear();
			    while (Reader.Read()) {
			        lIdCompte = long.Parse(Reader["id"].ToString());

			        if (!bAjouterClasseCompte && lIdCompte >= ((iClasseCompte + 1) * 1000)) {
			            bAjouterClasseCompte = true;
			            iClasseCompte++;
			            // Calculer le total de la section
			            CalculerTotalSection(tblListe);
			        }//if
			        if (bAjouterClasseCompte) {
			            bAjouterClasseCompte = false;
			            tblListe.TableModel.Rows.Add(new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }));
			            Cell cell = new Cell();
			            cell.Font = new Font(tblListe.Font.FontFamily, tblListe.Font.Size + 1, FontStyle.Bold | FontStyle.Italic);
			            if (iClasseCompte == CLASSE_COMPTE_ACTIF) {
			                cell.Text = "ACTIF";
			            }//if
			            else if (iClasseCompte == CLASSE_COMPTE_PASSIF) {
			                cell.Text = "PASSIF";
			            }//else if
			            else if (iClasseCompte == CLASSE_COMPTE_AVOIRS) {
			                cell.Text = "AVOIR";
			            }//else if
			            else if (iClasseCompte == CLASSE_COMPTE_REVENU) {
			                cell.Text = "REVENU";
			            }//else if
			            else if (iClasseCompte == CLASSE_COMPTE_DEPENSE) {
			                cell.Text = "DEPENSE";
			            }//else if
			            Row ligneTitre = new Row(new Cell[] { new Cell("-"), new Cell(iClasseCompte.ToString()), cell, new Cell(), new Cell() });
			            ligneTitre.BackColor = Color.FromArgb(191, 219, 255);
			            tblListe.TableModel.Rows.Add(ligneTitre);
			            tblListe.TableModel.Rows.Add(new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }));
			        }//if

			        cCompte = new Cell(Reader["compte"].ToString());
			        lIdTypeCompte = long.Parse(Reader["id_type_compte"].ToString());
			        Row ligne = new Row(new Cell[] { 
			            new Cell(lIdCompte.ToString()), 
			            new Cell(lIdTypeCompte.ToString()), 
			            cCompte, 
			            new Cell(), 
			            new Cell() 
			        });
			        FontStyle fs = FontStyle.Regular;
			        sPréfixe = string.Empty;

			        if (lIdTypeCompte == 1) {
			            fs = FontStyle.Bold;
			        }//if
			        else if (lIdTypeCompte == 2) {
			            fs = FontStyle.Regular;
			            sPréfixe = PREFIXE_COMPTE_NORMAL;
			            ligne.Cells[4].Text = CalculerSoldeCompte(lIdCompte, dateFin, _cultureIG);
			            if (bMasquerComptesVides) {
			                if (string.IsNullOrEmpty(ligne.Cells[4].Text)) {
			                    continue; //Ne pas ajouter cette ligne à la table
			                }//if
			            }//if
			            else if (bAfficherZéros) {
			                if (string.IsNullOrEmpty(ligne.Cells[4].Text)) {
			                    ligne.Cells[4].Text = (0).ToString("N2", _cultureIG);
			                }//if
			            }//if
			        }//if
			        else if (lIdTypeCompte == 3) {
			            fs = FontStyle.Italic;
			            sPréfixe = PREFIXE_COMPTE_SOUS_GROUPE;
			            ligne.Cells[3].Text = CalculerSoldeCompte(lIdCompte, dateFin, _cultureIG);
			            if (bMasquerComptesVides) {
			                if (string.IsNullOrEmpty(ligne.Cells[3].Text)) {
			                    continue; //Ne pas ajouter cette ligne à la table
			                }//if
			            }//if
			            else if (bAfficherZéros) {
			                if (string.IsNullOrEmpty(ligne.Cells[3].Text)) {
			                    ligne.Cells[3].Text = (0).ToString("N2", _cultureIG);
			                }//if
			            }//if
			        }//if
			        else if (lIdTypeCompte == 4) {
			            fs = FontStyle.Underline;
			            sPréfixe = PREFIXE_COMPTE_NORMAL;
			            ligne.Cells[4].Text = CalculerTotalSousGroupe(tblListe, 3, _cultureIG);
			            if (bMasquerComptesVides) {
			                if (string.IsNullOrEmpty(ligne.Cells[4].Text)) {
			                    continue; //Ne pas ajouter cette ligne à la table
			                }//if
			            }//if
			            else if (bAfficherZéros) {
			                if (string.IsNullOrEmpty(ligne.Cells[4].Text)) {
			                    ligne.Cells[4].Text = (0).ToString("N2", _cultureIG);
			                }//if
			            }//if
			        }//if
			        else if (lIdTypeCompte == 5) {
			            fs = FontStyle.Bold | FontStyle.Underline;
			            ligne.Cells[4].Text = CalculerGrandTotal(tblListe, 3, _cultureIG);
			            if (!bMasquerComptesVides && bAfficherZéros && string.IsNullOrEmpty(ligne.Cells[4].Text)) {
			                ligne.Cells[4].Text = (0).ToString("N2", _cultureIG);
			            }//if
			        }//if

			        ligne.Font = new Font(tblListe.Font.FontFamily, tblListe.Font.Size, fs);
			        cCompte.Text = sPréfixe + cCompte.Text;
			        tblListe.TableModel.Rows.Add(ligne);
			        if (lIdTypeCompte == 5) { //ajouter une ligne vide de séparation
			            tblListe.TableModel.Rows.Add(new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }));
			        }//if

			    }//while
			    Reader.Close();

			    CalculerTotalSection(tblListe);//Pour les dépenses
			    CalculerBénéficesNet(tblListe);
			}//try
			catch (Exception ex) {
			    if (!Reader.IsClosed) {
			        Reader.Close();
			    }//if
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    eValeurRetournée = ex;
			}//catch

			return eValeurRetournée;
		}

		/// <summary>
		/// Charge un graphe selon le type de comptes et le niveau de details
		/// </summary>
		/// <param name="table">la table contenu les donnees</param>
		/// <param name="graphe">la tarte a afficher</param>
		/// <param name="classe">le type de comptes</param>
		/// <param name="detaillé">true: afficher les totaux des groupes de comptes, false: afficher uniquement le premier groupe</param>
		/// <returns>null: tout est OK, sinon l'exception est retournée.</returns>
		public Exception ChargerGraphe(Table table, PieChart graphe, byte classe, bool detaillé)
		{
			Exception eValeurRetournée = null;
			try {
			    graphe.Items.Clear();
			    int iStartOfClass = 0;
			    int iEndOfClass = table.TableModel.Rows.Count - 1;
			    foreach (Row row in table.TableModel.Rows) {
			        if (!string.IsNullOrEmpty(row.Cells[0].Text) && !string.IsNullOrEmpty(row.Cells[1].Text)) {
			            if (row.Cells[0].Text.Equals("-") && row.Cells[1].Text.Equals(classe.ToString())) {
			                iStartOfClass = row.Index;
			                continue;
			            }//if
			        }//if
			        if (!string.IsNullOrEmpty(row.Cells[0].Text) && !string.IsNullOrEmpty(row.Cells[1].Text)) {
			            if ((row.Cells[0].Text.Equals(STR_TOTAL) && row.Cells[1].Text.Equals(classe.ToString())) 
			                || row.Cells[0].Text.Equals("-") && row.Cells[1].Text.Equals((classe + 1).ToString())) {
			                iEndOfClass = row.Index;
			                break;
			            }//if
			        }//if
			    }//foreach
			    double total = 0;
			    for (int i = iStartOfClass; i < iEndOfClass; i++) {
			        Row row = table.TableModel.Rows[i];
			        double value = 0;
			        PieChartItem pci = new PieChartItem();
			        if (detaillé) {
			            if (!string.IsNullOrEmpty(row.Cells[1].Text)) {
			                if (row.Cells[1].Text.Equals("5")) {
			                    try {
			                        value = double.Parse(row.Cells[4].Text);
			                        pci.Text = row.Cells[2].Text.Trim();
			                        pci.ToolTipText = pci.Text + " - " + value.ToString("N2", _cultureIG);
			                        pci.Weight = value;
			                        pci.Color = RandomColor;
			                        graphe.Items.Add(pci);
			                    }//try
			                    catch (Exception e) {
			                        System.Diagnostics.Debug.WriteLine("BilanController:ChargerGraphe:Exception: " + e.Message);
			                    }//catch
			                }//if
			            }//if
			        }//if
			        else if (!string.IsNullOrEmpty(row.Cells[1].Text)) {
			            if (row.Cells[1].Text.Equals("2") || row.Cells[1].Text.Equals("4")) {
			                try {
			                    value = double.Parse(row.Cells[4].Text);
			                    pci.Text = row.Cells[2].Text.Trim();
			                    pci.ToolTipText = pci.Text + " - " + value.ToString("N2", _cultureIG);
			                    pci.Weight = value;
			                    pci.Color = RandomColor;
			                    graphe.Items.Add(pci);
			                }//try
			                catch (Exception e) {
			                    System.Diagnostics.Debug.WriteLine("BilanController:ChargerGraphe:Exception: " + e.Message);
			                }//catch
			            }//if
			        }//else if
			        total += value;
			    }//for i
			    //Add percentage
			    foreach (PieChartItem item in graphe.Items) {
			        item.ToolTipText += " (" + (100 * item.Weight / total).ToString("N2", _cultureIG) + "%)";
			    }//foreach PieChartItem
			}//try
			catch (Exception e) {
			    eValeurRetournée = e;
			}//catch

			return eValeurRetournée;
		}

		/// <summary>
		/// Permet de charger tous les graphes
		/// </summary>
		/// <param name="table">la table contenant les donnees</param>
		/// <param name="pcActifs">la tarte des actifs</param>
		/// <param name="pcPassifs">la tarte des passifs</param>
		/// <param name="pcRevenus">la tarte des revenus</param>
		/// <param name="pcDepenses">la tarte des depenses</param>
		/// <returns>null: tout est OK, sinon l'exception est retournée.</returns>
		public Exception ChargerGraphes(Table table, PieChart pcActifs, PieChart pcPassifs, PieChart pcRevenus, PieChart pcDepenses)
		{
			Exception eValeurRetournée = null;

			try {
			    eValeurRetournée = ChargerGraphe(table, pcActifs, CLASSE_COMPTE_ACTIF, false);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if

			    eValeurRetournée = ChargerGraphe(table, pcPassifs, CLASSE_COMPTE_PASSIF, false);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if

			    eValeurRetournée = ChargerGraphe(table, pcRevenus, CLASSE_COMPTE_REVENU, false);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if

			    eValeurRetournée = ChargerGraphe(table, pcDepenses, CLASSE_COMPTE_DEPENSE, true);
			    if (eValeurRetournée != null) {
			        throw eValeurRetournée;
			    }//if
			}//try
			catch (Exception e) {
			    eValeurRetournée = e;
			}//catch

			return eValeurRetournée;
		}

		#endregion Public Methods
	}
}