﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Controller
{
	public class ConfigurationController : Controller
	{
		#region Constants

		private const string STR_TEMP_FILENAME = "temp.myc";
		private const string STR_URL_GOLD_SITE = "http://www.24hgold.com/english/gold_silver_prices_charts.aspx?money=MAD";

		#endregion Constants

		#region Constructors

		public ConfigurationController(Model.BaseDonnées db)
			: base(db)
		{
		}

		#endregion Constructors

		#region Public Methods

		/// <summary>
		/// Obtention du prix du gramme d'or en utilisant le site 24hgold.com
		/// </summary>
		/// <param name="dPrixGrammeOr">le prix extrait en MAD</param>
		/// <param name="dateMAJ">date de la mise à jour du prix du gramme d'or</param>
		/// <returns>null = OK, sinon, l'exception est retournée</returns>
		public Exception ObtenirPrixGrammeOr(ref double dPrixGrammeOr, ref string dateMAJ)
		{
			Exception eValeurRetournée = null;
			try {
			    WebClient webClient = new WebClient();
			    webClient.DownloadFile(STR_URL_GOLD_SITE, STR_TEMP_FILENAME);
			    StreamReader file = new StreamReader(STR_TEMP_FILENAME);
			    string line;
			    while ((line = file.ReadLine()) != null) {
			        if (line.Contains("lbGoldGramEurValue")) {
			            int iPosEnd;
			            int iPosStart;
			            if (line.Contains("&nbsp;</b></span>")) {
			                iPosEnd = line.IndexOf("&nbsp;</b></span>");
			            }//if
			            else {
			                line = file.ReadLine();
			                iPosEnd = line.IndexOf("&nbsp;</span>");
			            }//else
			            iPosStart = line.LastIndexOf("<b>") + 3;
			            try {
			                string temp = line.Substring(iPosStart, iPosEnd - iPosStart);
			                dPrixGrammeOr = double.Parse(temp);
			                dateMAJ = DateTime.Now.ToShortDateString();
			                break;
			            }//try
			            catch (Exception ex) {
			                eValeurRetournée = ex;
			                System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			                System.Diagnostics.Debug.WriteLine(ex.Message);
			            }//catch	
			        }//if
			    }//while
			    file.Close();
			    try {
			        File.Delete(STR_TEMP_FILENAME);
			    }//try
			    catch (Exception ex) {
			        System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			        System.Diagnostics.Debug.WriteLine(ex.Message);
			    }//catch
			}//try
			catch (Exception e) {
			    eValeurRetournée = e;
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(e.Message);
			}//catch
			return eValeurRetournée;
		}

		#endregion Public Methods
	}
}