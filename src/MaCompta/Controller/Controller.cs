﻿using My.Forms.Controls.Charts.PieChart;
using My.Forms.Controls.ComboBoxes;
using My.Forms.Controls.ListViews.XPTable.Models;

using MySql.Data.MySqlClient;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace Controller
{
	public class Controller : Model.FonctionalitéBaseDonnées
	{
		#region Constants

		public const byte CLASSE_COMPTE_ACTIF = 1;
		public const byte CLASSE_COMPTE_AVOIRS = 3;
		public const byte CLASSE_COMPTE_DEPENSE = 5;
		public const byte CLASSE_COMPTE_PASSIF = 2;
		public const byte CLASSE_COMPTE_REVENU = 4;
		public const string PREFIXE_COMPTE_NORMAL = "   ";
		public const string PREFIXE_COMPTE_SOUS_GROUPE = "       ";
		public const string STR_TOTAL = "TOTAL";

		#endregion Constants

		#region Fields

		// A set of colors to select a random one from these.
		protected Color[] _colors = new Color[] {
			Color.FromArgb(255, 200, 255, 255),
			Color.FromArgb(255, 150, 200, 255),
			Color.FromArgb(255, 100, 100, 200),
			Color.FromArgb(255, 255, 60, 130),
			Color.FromArgb(255, 250, 200, 255),
			Color.FromArgb(255, 255, 255, 0),
			Color.FromArgb(255, 255, 155, 55),
			Color.FromArgb(255, 150, 200, 155),
			Color.FromArgb(255, 255, 255, 200),
			Color.FromArgb(255, 100, 150, 200),
			Color.FromArgb(255, 130, 235, 250),
			Color.FromArgb(255, 150, 240, 80),
			Color.FromArgb(155, 200, 255, 255),
			Color.FromArgb(155, 150, 200, 255),
			Color.FromArgb(155, 100, 100, 200),
			Color.FromArgb(155, 255, 60, 130),
			Color.FromArgb(155, 250, 200, 255),
			Color.FromArgb(155, 255, 255, 0),
			Color.FromArgb(155, 255, 155, 55),
			Color.FromArgb(155, 150, 200, 155),
			Color.FromArgb(155, 255, 255, 200),
			Color.FromArgb(155, 100, 150, 200),
			Color.FromArgb(155, 130, 235, 250),
			Color.FromArgb(155, 150, 240, 80)
		};

		private Random _rnd = new Random();

		#endregion Fields

		#region Constructors

		public Controller()
		{
		}

		public Controller(Model.BaseDonnées db)
		{
			Connection = db.Connection;
			Command = db.Command;
		}

		#endregion Constructors

		#region Public Properties

		/// <summary>
		/// Retourne aléatoirement une couleur parmi ceux dans la liste m_colors
		/// </summary>
		public Color RandomColor
		{
			get {
			    return _colors[_rnd.Next(0, _colors.Length - 1)];
			}
		}

		#endregion Public Properties

		#region Private Methods

		/// <summary>
		/// Charge le contenu d'une table dans un combobox
		/// </summary>
		/// <param name="cbListe">le combo box à remplir</param>
		/// <param name="bTous">true: tous les comptes seront chargés, 
		/// false: Seulement les comptes normaux et de sous-groupes</param>
		/// <param name="typeCompte">1 = actif, 2 = passif, 3 = avoir, 4 = revenu, 5 = dépense</param>
		/// <returns>null si tout c'est bien passé; sinon l'exception est retournée</returns>
		private Exception ChargerComptes(PowerComboBox pcbListe, bool bTous, int typeCompte)
		{
			Exception eValeurRetournée = null;
			try
			{
			    string nom_compte;
			    int id_type_compte = 0;
			    Command.CommandText = "SELECT CONCAT(`id`, \"-\", `nom`) AS `id_nom`, `id_type_compte` FROM `compte` " +
			        "WHERE (`Afficher` = true) ";
			    Command.CommandText += "AND (`id` BETWEEN " + (typeCompte * 1000).ToString() + 
			        " AND " + (typeCompte * 1000 + 999).ToString() + ")\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    while ( Reader.Read() )
			    {
			        nom_compte = Reader[0].ToString();
			        if ( !bTous )
			        {
			            id_type_compte = int.Parse(Reader["id_type_compte"].ToString());
			            if ( id_type_compte == 1 )
			            {
			                nom_compte = pcbListe.DividerFormat + " <<<<< " + nom_compte + pcbListe.DividerFormat;
			            }//if
			            else if ( id_type_compte == 4 )
			            {
			                nom_compte = pcbListe.DividerFormat + nom_compte + pcbListe.DividerFormat;
			            }//else
			            else if ( id_type_compte == 5 )
			            {
			                nom_compte = pcbListe.DividerFormat + nom_compte + " >>>>> " + pcbListe.DividerFormat;
			            }//else
			        }//if
			        pcbListe.Items.Add(nom_compte);
			    }//while
			    Reader.Close();
			}//try
			catch ( Exception ex )
			{
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if
			    eValeurRetournée = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}//catch

			return eValeurRetournée;
		}

		#endregion Private Methods

		#region Public Methods

		/// <summary>
		/// Calculer le grand total des comptes
		/// </summary>
		/// <param name="tblListe">la table contenant la liste des comptes</param>
		/// <param name="indexMontantSousGroupe">index du montant dans la table</param>
		/// <param name="cultureIG">Le type d'affichage du total</param>
		/// <returns>Le total converti en texte ou texte vide/zéro en cas de problèmes</returns>
		public string CalculerGrandTotal(Table tblListe, int indexMontantSousGroupe, CultureInfo cultureIG)
		{
			string sValeurRetournée = string.Empty;
			try
			{
			    int index = tblListe.TableModel.Rows.Count;
			    double total = 0;
			    for ( int i = index - 1; int.Parse(tblListe.TableModel.Rows[i].Cells[1].Text) != 1; i-- )
			    {
			        int idTypeCompte = int.Parse(tblListe.TableModel.Rows[i].Cells[1].Text);
			        if ( idTypeCompte == 4 )
			        {
			            continue;
			        }//if
			        else if ( !string.IsNullOrEmpty(tblListe.TableModel.Rows[i].Cells[indexMontantSousGroupe].Text) )
			        {
			            total += double.Parse(tblListe.TableModel.Rows[i].Cells[indexMontantSousGroupe].Text);
			        }//else if
			        else if ( !string.IsNullOrEmpty(tblListe.TableModel.Rows[i].Cells[indexMontantSousGroupe + 1].Text) )
			        {
			            total += double.Parse(tblListe.TableModel.Rows[i].Cells[indexMontantSousGroupe + 1].Text);
			        }//else if
			    }//for i
			    if ( total != 0 )
			    {
			        sValeurRetournée = total.ToString("N2", cultureIG);
			    }//if
			}//try
			catch ( Exception ex )
			{
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}//catch

			return sValeurRetournée;
		}

		/// <summary>
		/// Calcul du solde des comptes normaux et des comptes de sous-groupes
		/// </summary>
		/// <param name="lIdCompte">Numéro du compte</param>
		/// <param name="dateFin">La da te de fin du calcul</param>
		/// <param name="cultureIG">Le style d'affichage</param>
		/// <returns>Le solde converti en texte ou texte vide/zéro en cas de problèmes</returns>
		public string CalculerSoldeCompte(long lIdCompte, DateTime dateFin, CultureInfo cultureIG)
		{
			return CalculerSoldeCompte(lIdCompte, dateFin).ToString("N2", cultureIG);
		}

		/// <summary>
		/// Calcul du solde des comptes normaux et des comptes de sous-groupes
		/// </summary>
		/// <param name="lIdCompte">Numéro du compte</param>
		/// <param name="dateFin">La da te de fin du calcul</param>
		/// <returns>Le solde</returns>
		public double CalculerSoldeCompte(long lIdCompte, DateTime dateFin)
		{
			double dValeurRetournée = 0;
			try
			{
			    MySqlConnection cn = new MySqlConnection();
			    cn.ConnectionString = "Server=" + NomHôte +
			        ";Port=" + NuméroPort +
			        ";User Id=" + NomUsager +
			        ";Password=" + MotPasse +
			        ";Database=" + NomBaseDonnées;
			    cn.Open();
			    MySqlCommand cmd = new MySqlCommand();
			    cmd.Connection = cn;
			    cmd.CommandText = "SELECT SUM(`solde`) AS `solde` " +
			        "FROM `charger_solde_compte` " +
			        "WHERE `id_compte` = " + lIdCompte +
			        " AND `date` <= " + Formatter(dateFin, true) + "\0";
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(cmd.CommandText);
			    MySqlDataReader reader = cmd.ExecuteReader();
			    if (reader.Read())
			    {
			        if (!string.IsNullOrEmpty(reader["solde"].ToString()))
			        {
			            dValeurRetournée = double.Parse(reader["solde"].ToString());
			            if ((lIdCompte >= 2000 && lIdCompte <= 2999) || (lIdCompte >= 4000 && lIdCompte <= 4999))
			            {
			                //Passif et Revenu
			                dValeurRetournée *= -1;
			            }//if
			        }//if
			    }//if
			    reader.Close();
			    cn.Close();
			}//try
			catch (Exception ex)
			{
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}//catch

			return dValeurRetournée;
		}

		/// <summary>
		/// Calculer le total des comptes de sous-groupes
		/// </summary>
		/// <param name="tblListe">la table contenant la liste des comptes</param>
		/// <param name="indexMontantSousGroupe">index du montant dans la table</param>
		/// <param name="cultureIG">Le style d'affichage</param>
		/// <returns>Le total converti en texte ou texte vide/zéro en cas de problèmes</returns>
		public string CalculerTotalSousGroupe(Table tblListe, int indexMontantSousGroupe, CultureInfo cultureIG)
		{
			string sValeurRetournée = string.Empty;
			try
			{
			    int index = tblListe.TableModel.Rows.Count;
			    double total = 0;
			    for ( int i = index - 1; int.Parse(tblListe.TableModel.Rows[i].Cells[1].Text) == 3; i-- )
			    {
			        if ( !string.IsNullOrEmpty(tblListe.TableModel.Rows[i].Cells[indexMontantSousGroupe].Text) )
			        {
			            total += double.Parse(tblListe.TableModel.Rows[i].Cells[indexMontantSousGroupe].Text);
			        }//if
			    }//for i
			    if ( total != 0 )
			    {
			        sValeurRetournée = total.ToString("N2", cultureIG);
			    }//if
			}//try
			catch ( Exception ex )
			{
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}//catch

			return sValeurRetournée;
		}

		/// <summary>
		/// Charge le contenu d'une table dans un combobox
		/// </summary>
		/// <param name="cbListe">le combo box à remplir</param>
		/// <param name="bTous">true: tous les comptes seront chargés, 
		/// false: Seulement les comptes normaux et de sous-groupes</param>
		/// <param name="typeCompte">1 = actif, 2 = passif, 3 = avoir, 4 = revenu, 5 = dépense</param>
		/// <returns>null si tout c'est bien passé; sinon l'exception est retournée</returns>
		public Exception ChargerComptes(ComboBox cbListe, int typeCompte)
		{
			Exception eValeurRetournée = null;
			try {
			    string nom_compte;
			    Command.CommandText = "SELECT CONCAT(`id`, \"-\", `nom`) AS `id_nom` FROM `compte` " +
			        "WHERE (`Afficher` = true) AND (`id` BETWEEN " + (typeCompte * 1000).ToString() +
			        " AND " + (typeCompte * 1000 + 999).ToString() +
			        ") AND (`id_type_compte` = 2 OR `id_type_compte` = 3)\0"; //Comptes normaux et de sous-groupes
			    Debug();
			    Reader = Command.ExecuteReader();
			    while (Reader.Read()) {
			        nom_compte = Reader[0].ToString();
			        cbListe.Items.Add(nom_compte);
			    }//while
			    Reader.Close();
			}//try
			catch (Exception ex) {
			    if (!Reader.IsClosed) {
			        Reader.Close();
			    }//if
			    eValeurRetournée = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}//catch

			return eValeurRetournée;
		}

		/// <summary>
		/// Charge le contenu d'une table dans un combobox
		/// </summary>
		/// <param name="cbListe">le combo box à remplir</param>
		/// <param name="nomTable">la table source</param>
		/// <param name="nomChamp">la colonne à extraire</param>
		/// <returns>null si tout c'est bien passé; sinon l'exception est retournée</returns>
		public Exception ChargerListe(ComboBox cbListe,
			string nomTable, string nomChamp)
		{
			return ChargerListe(cbListe, nomTable, nomChamp, null);
		}

		/// <summary>
		/// Charge le contenu d'une table dans un combobox
		/// </summary>
		/// <param name="cbListe">le combo box à remplir</param>
		/// <param name="effacerListe"> effacer le combo box avant de le remplir?</param>
		/// <param name="nomTable">la table source</param>
		/// <param name="nomChamp1">la colonne à extraire</param>
		/// <param name="condition">Contenu de la condition sans la clause WHERE</param>
		/// <returns>null si tout c'est bien passé; sinon l'exception est retournée</returns>
		public Exception ChargerListe(PowerComboBox pcbListe, bool effacerListe,
			string nomTable, string nomChamp, string condition)
		{
			Exception eValeurRetournée = null;
			try
			{
			    Command.CommandText = "SELECT `" + nomChamp +
			        "` FROM `" + nomTable + "`";
			    if ( !string.IsNullOrEmpty(condition) )
			    {
			        Command.CommandText += " WHERE " +
			            condition;
			    }//if
			    Command.CommandText += "\0";

			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(Command.CommandText);
			    Reader = Command.ExecuteReader();
			    if ( effacerListe )
			    {
			        pcbListe.Items.Clear();
			    }//if
			    while ( Reader.Read() )
			    {
			        pcbListe.Items.Add(Reader[nomChamp].ToString());
			    }//while
			    Reader.Close();
			}//try
			catch ( Exception ex )
			{
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if
			    eValeurRetournée = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}//catch

			return eValeurRetournée;
		}

		/// <summary>
		/// Charge le contenu d'une table dans un combobox
		/// </summary>
		/// <param name="cbListe">le combo box à remplir</param>
		/// <param name="nomTable">la table source</param>
		/// <param name="nomChamp">la colonne à extraire</param>
		/// <param name="condition">Contenu de la condition sans la clause WHERE</param>
		/// <returns>null si tout c'est bien passé; sinon l'exception est retournée</returns>
		public Exception ChargerListe(ComboBox cbListe,
			string nomTable, string nomChamp, string condition)
		{
			Exception eValeurRetournée = null;
			try
			{
			    Command.CommandText = "SELECT `" + nomChamp +
			        "` FROM `" + nomTable + "`";
			    if ( !string.IsNullOrEmpty(condition) )
			    {
			        Command.CommandText += " WHERE " +
			            condition;
			    }//if
			    Command.CommandText += "\0";

			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(Command.CommandText);
			    Reader = Command.ExecuteReader();
			    List<string> items = new List<string>();
			    while ( Reader.Read() )
			    {
			        items.Add(Reader[nomChamp].ToString());
			    }//while
			    Reader.Close();

			    cbListe.Items.Clear();
			    cbListe.Items.AddRange(items.ToArray());
			}//try
			catch ( Exception ex )
			{
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if
			    eValeurRetournée = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}//catch

			return eValeurRetournée;
		}

		/// <summary>
		/// Chargement de la liste des comptes
		/// </summary>
		/// <param name="pcbCompte">le combo box spécial pour les comptes</param>
		/// <param name="bTous">true: tous les comptes seront chargés, 
		/// false: Seulement les comptes normaux et de sous-groupes</param>
		/// <returns>null: tout s'est bien passé. Sinon, l'exception est retournée.</returns>
		public Exception ChargerListeComptes(My.Forms.Controls.ComboBoxes.PowerComboBox pcbCompte, bool bTous = false)
		{
			Exception eValeurRetournée;
			try
			{
			    pcbCompte.Items.Clear();
			    pcbCompte.Items.Add("--- ========== ACTIF ========== ---");
			    eValeurRetournée = ChargerComptes(pcbCompte, bTous, 1);
			    if ( eValeurRetournée != null )
			    {
			        throw eValeurRetournée;
			    }//if
			    pcbCompte.Items.Add("--- ========== PASSIF ========== ---");
			    eValeurRetournée = ChargerComptes(pcbCompte, bTous, 2);
			    if ( eValeurRetournée != null )
			    {
			        throw eValeurRetournée;
			    }//if
			    pcbCompte.Items.Add("--- ========== AVOIR ========== ---");
			    eValeurRetournée = ChargerComptes(pcbCompte, bTous, 3);
			    if ( eValeurRetournée != null )
			    {
			        throw eValeurRetournée;
			    }//if
			    pcbCompte.Items.Add("--- ========== REVENU ========== ---");
			    eValeurRetournée = ChargerComptes(pcbCompte, bTous, 4);
			    if ( eValeurRetournée != null )
			    {
			        throw eValeurRetournée;
			    }//if
			    pcbCompte.Items.Add("--- ========== DEPENSE ========== ---");
			    eValeurRetournée = ChargerComptes(pcbCompte, bTous, 5);
			    if ( eValeurRetournée != null )
			    {
			        throw eValeurRetournée;
			    }//if
			}//try
			catch ( Exception ex)
			{
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    eValeurRetournée = ex;
			}//catch

			return eValeurRetournée;
		}

		/// <summary>
		/// Permet de charger la liste des clés et valeurs dans une liste
		/// </summary>
		/// <param name="cbListe">le combobox à remplir.</param>
		/// <param name="sNomTable">le nom de la table qui contient les données.</param>
		/// <param name="sClé">la clé.</param>
		/// <param name="sValeur">la valeur associée à la clé.</param>
		/// <param name="sCondition">la condition sur les clés et valeurs.</param>
		/// <returns>null si tout c'est bien passé; sinon l'exception est retournée</returns>
		public Exception ChargerListeValeurs(ComboBox cbListe, string sNomTable, string sClé, string sValeur, string sCondition)
		{
			Exception eValeurRetournée = null;
			try
			{
			    Command.CommandText = "SELECT `" + sClé +
			        "`, `"+ sValeur +
			        "` FROM `" + sNomTable + "`";
			    if (!string.IsNullOrEmpty(sCondition))
			    {
			        Command.CommandText += " WHERE " +
			            sCondition;
			    }//if
			    Command.CommandText += "\0";

			    Debug();
			    Reader = Command.ExecuteReader();
			    cbListe.Items.Clear();
			    Dictionary<long, string> dic = new Dictionary<long, string>();
			    while (Reader.Read())
			    {
			        dic.Add(long.Parse(Reader[0].ToString()), Reader[1].ToString());
			    }//while
			    Reader.Close();

			    if ( dic.Count > 0 )
			    {
			        cbListe.DataSource = new BindingSource(dic, null);
			        cbListe.DisplayMember = "Value";
			        cbListe.ValueMember = "Key";
			    }//if

			}//try
			catch (Exception ex)
			{
			    if (!Reader.IsClosed)
			    {
			        Reader.Close();
			    }//if
			    eValeurRetournée = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}//catch

			return eValeurRetournée;
		}

		#endregion Public Methods
	}
}