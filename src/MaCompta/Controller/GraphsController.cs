﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

using My.Forms.Controls.Charts.PieChart;
using My.Forms.Controls.ComboBoxes;

namespace Controller
{
	public class GraphesController : Controller
	{
		#region Fields

		private CultureInfo _cultureIG;

		#endregion Fields

		#region Constructors

		public GraphesController(Model.BaseDonnées db, CultureInfo cultureIG)
			: base(db)
		{
			_cultureIG = cultureIG;
		}

		#endregion Constructors

		#region Public Methods

		/// <summary>
		/// Charge les comptes de groupes dans un PowerComboBox
		/// </summary>
		/// <param name="pcbListe">le combo box à remplir</param>
		/// <param name="typeCompte">1 = actif, 2 = passif, 3 = avoir, 4 = revenu, 5 = dépense</param>
		/// <returns>null si tout c'est bien passé; sinon l'exception est retournée</returns>
		public Exception ChargerComptesGroupes(PowerComboBox pcbListe, int typeCompte)
		{
			Exception eValeurRetournée = null;
			try
			{
			    string nom_compte;
			    int id_type_compte = 0;
			    Command.CommandText = "SELECT CONCAT(`id`, \"-\", `nom`) AS `id_nom`, `id_type_compte` FROM `compte` " +
			        "WHERE (`Afficher` = true) ";
			    Command.CommandText += "AND (`id` BETWEEN " + (typeCompte * 1000).ToString() +
			        " AND " + (typeCompte * 1000 + 999).ToString() + ")\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    while (Reader.Read())
			    {
			        nom_compte = Reader[0].ToString();
			        id_type_compte = int.Parse(Reader["id_type_compte"].ToString());
			        if (id_type_compte == 1)
			        {
			            pcbListe.Items.Add(nom_compte);
			        }//if
			    }//while
			    Reader.Close();
			}//try
			catch (Exception ex)
			{
			    if (!Reader.IsClosed)
			    {
			        Reader.Close();
			    }//if
			    eValeurRetournée = ex;
			    System.Diagnostics.Debug.WriteLine(Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			}//catch

			return eValeurRetournée;
		}

		public Exception ChargerGraphe(PieChart graphe, long lIdCompteGroupe)
		{
			Exception eValeurRetournée = null;
			try {
			    graphe.Items.Clear();
			    Command.CommandText = "SELECT `id`, `nom`, `id_type_compte` FROM `compte` " +
			        "WHERE (`Afficher` = true) AND (`id` > " + lIdCompteGroupe + ")\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    long lIdCompte = lIdCompteGroupe, lIdTypeCompte = 1;
			    double value = 0, total = 0;
			    while (Reader.Read() && lIdTypeCompte < 5)
			    {
			        lIdTypeCompte = long.Parse(Reader["id_type_compte"].ToString());
			        if (lIdTypeCompte == 2 || lIdTypeCompte == 3)
			        {
			            lIdCompte = long.Parse(Reader["id"].ToString());
			            try
			            {
			                PieChartItem pci = new PieChartItem();
			                value = CalculerSoldeCompte(lIdCompte, DateTime.Now);
			                total += value;
			                pci.Text = lIdCompte + "-" + Reader["nom"].ToString();
			                pci.ToolTipText = pci.Text + ": " + value.ToString("N2", _cultureIG);
			                pci.Weight = value;
			                pci.Color = RandomColor;
			                graphe.Items.Add(pci);
			            }//try
			            catch (Exception e)
			            {
			                System.Diagnostics.Debug.WriteLine("GraphesController:ChargerGraphe:Exception: " + e.Message);
			            }//catch
			        }//if
			    }//while
			    Reader.Close();
			    //Add percentage
			    foreach (PieChartItem item in graphe.Items)
			    {
			        item.ToolTipText += " (" + (100 * item.Weight / total).ToString("N2", _cultureIG) + "%)";
			    }//foreach PieChartItem
			}//try
			catch (Exception e)
			{
			    eValeurRetournée = e;
			}//catch

			return eValeurRetournée;
		}

		#endregion Public Methods
	}
}