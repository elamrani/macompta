﻿using My.Forms.Controls.Charts.PieChart;
using My.Forms.Controls.ListViews.XPTable.Models;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Text;

namespace Controller
{
	public class ListePartenairesController : Controller
	{
		#region Fields

		private CultureInfo _cultureIG;

		#endregion Fields

		#region Constructors

		public ListePartenairesController(Model.BaseDonnées db, CultureInfo cultureIG)
			: base(db)
		{
			_cultureIG = cultureIG;
		}

		#endregion Constructors

		#region Public Methods

		/// <summary>
		/// Permet de charger la liste des totaux de tous les partenaires dans une tarte
		/// </summary>
		/// <param name="graphe">la tarte à peupler</param>
		/// <param name="table">la table contenant les donnees</param>
		/// <returns>null: tout est OK, sinon l'exception est retournée.</returns>
		public Exception ChargerGraphePartenaires(PieChart graphe, Table table)
		{
			Exception eValeurRetournée = null;
			try {
			    graphe.Items.Clear();
			    double total = 0;
			    foreach (Row row in table.TableModel.Rows) {
			        if (! string.IsNullOrEmpty(row.Cells[2].Text) && ! row.Cells[2].Text.Equals(STR_TOTAL + " :")) {
			            double value = double.Parse(row.Cells[3].Text);
			            PieChartItem pci = new PieChartItem();
			            pci.Text = row.Cells[2].Text;
			            pci.ToolTipText = pci.Text + " - " + value.ToString("N2", _cultureIG);
			            pci.Weight = value;
			            pci.Color = RandomColor;
			            graphe.Items.Add(pci);
			            total += value;
			        }//if
			    }//foreach
			    //Add percentage
			    foreach (PieChartItem item in graphe.Items) {
			        item.ToolTipText += " (" + (100 * item.Weight / total).ToString("N2", _cultureIG) + "%)";
			    }//foreach PieChartItem
			}//try
			catch (Exception ex) {
			    if (!Reader.IsClosed) {
			        Reader.Close();
			    }//if
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    eValeurRetournée = ex;
			}//catch

			return eValeurRetournée;
		}

		/// <summary>
		/// Permet de charger la liste des totaux de tous les partenaires
		/// </summary>
		/// <param name="tblListe">la liste à peupler</param>
		/// <returns>null: tout est OK, sinon l'exception est retournée.</returns>
		public Exception ChargerListePartenaires(Table tblListe)
		{
			Exception eValeurRetournée = null;
			try
			{
			    tblListe.TableModel.Rows.Clear();
			    Command.CommandText = "SELECT * FROM `totaux_partenaires`\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    double dTotalDébits = 0, dTotalCrédits = 0, dTemp = 0;
			    while ( Reader.Read() )
			    {
			        string sDébit, sCrédit;
			        dTemp = double.Parse(Reader["debit"].ToString());
			        if ( dTemp == 0 )
			        {
			            sDébit = "-";
			        }//if
			        else
			        {
			            dTotalDébits += dTemp;
			            sDébit = dTemp.ToString("N2", _cultureIG);
			        }//else
			        dTemp = double.Parse(Reader["credit"].ToString());
			        if ( dTemp == 0 )
			        {
			            sCrédit = "-";
			        }//if
			        else
			        {
			            dTotalCrédits += dTemp;
			            sCrédit = dTemp.ToString("N2", _cultureIG);
			        }//else
			        tblListe.TableModel.Rows.Add(new Row(new Cell[] { 
			                new Cell(Reader["id_partenaire"].ToString()), 
			                new Cell(), 
			                new Cell(Reader["nom"].ToString()), 
			                new Cell(sDébit), 
			                new Cell(sCrédit)
			            }));
			    }//while
			    Reader.Close();
			    //Totaux
			    tblListe.TableModel.Rows.Add(
			        new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }
			        ));
			    Row ligneTotaux = new Row(new Cell[] { 
			        new Cell(), 
			        new Cell(), 
			        new Cell(STR_TOTAL + " :"), 
			        new Cell(dTotalDébits.ToString("N2", _cultureIG)), 
			        new Cell(dTotalCrédits.ToString("N2", _cultureIG)) 
			    });
			    ligneTotaux.Font = new Font(tblListe.Font.FontFamily, tblListe.Font.Size + 1, FontStyle.Bold | FontStyle.Underline);
			    tblListe.TableModel.Rows.Add(ligneTotaux);
			}//try
			catch ( Exception ex )
			{
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    eValeurRetournée = ex;
			}//catch

			return eValeurRetournée;
		}

		/// <summary>
		/// Permet d'afficher les totaux d'un partenaire particulier
		/// </summary>
		/// <param name="tblListe">la liste à peupler</param>
		/// <param name="sNomPartenaireACharger"></param>
		/// <param name="dtDateDébut"></param>
		/// <param name="dtDateFin"></param>
		/// <returns>null: tout est OK, sinon l'exception est retournée.</returns>
		public Exception ChargerListeTransactions(Table tblListe, string sNomPartenaireACharger, DateTime? dtDateDébut, DateTime dtDateFin)
		{
			Exception eValeurRetournée = null;
			try
			{
			    if ( !string.IsNullOrEmpty(sNomPartenaireACharger) )
			    {
			        tblListe.TableModel.Rows.Clear();
			        Row ligneTitre = new Row(new Cell[] { 
			                    new Cell(), //id projet
			                    new Cell(), //date
			                    new Cell(sNomPartenaireACharger), //détails
			                    new Cell(), //débit
			                    new Cell()  //crédit
			                });
			        ligneTitre.Font = new Font(tblListe.Font.FontFamily, tblListe.Font.Size + 1, FontStyle.Bold | FontStyle.Underline);
			        tblListe.TableModel.Rows.Add(ligneTitre);
			        tblListe.TableModel.Rows.Add(
			            new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }
			            ));
			    }//if
			    else
			    {
			        //Sans projet, ne rien charger
			        return null;
			    }//else

			    Command.CommandText = "SELECT * FROM `charger_transaction` WHERE `date` <= " + Formatter(dtDateFin);
			    if ( dtDateDébut.HasValue )
			    {
			        Command.CommandText += " AND `date` >= " + Formatter(dtDateDébut);
			    }//if
			    if ( !string.IsNullOrEmpty(sNomPartenaireACharger) )
			    {
			        try
			        {
			            Command.CommandText += " AND `partenaire` = " + Formatter(sNomPartenaireACharger);
			        }//try
			        catch ( Exception ex )
			        {
			            System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			            System.Diagnostics.Debug.WriteLine(ex.Message);
			        }//catch
			    }//if
			    Command.CommandText += "\0";
			    Debug();
			    Reader = Command.ExecuteReader();
			    double dTotalDébits = 0, dTotalCrédits = 0, dTemp = 0;
			    long id_ancienne_transaction = -1, id_transaction_courante = -1;
			    string date_transaction = string.Empty;
			    while ( Reader.Read() )
			    {
			        id_transaction_courante = long.Parse(Reader["id"].ToString());
			        if ( id_ancienne_transaction != id_transaction_courante )
			        {
			            id_ancienne_transaction = id_transaction_courante;
			            if ( tblListe.TableModel.Rows.Count > 3 )
			            {
			                tblListe.TableModel.Rows.Add(
			                    new Row(new Cell[] { 
			                    new Cell(), //id transaction
			                    new Cell(), //date
			                    new Cell(), //détails
			                    new Cell(), //débit
			                    new Cell() //crédit
			                }));
			            }//if
			            date_transaction = DateTime.Parse(Reader["date"].ToString()).ToShortDateString();
			        }//if
			        else
			        {
			            date_transaction = string.Empty;
			        }//else
			        string description = string.Empty;
			        if ( !string.IsNullOrEmpty(Reader["id_compte"].ToString()) )
			        {
			            description += Reader["id_compte"].ToString() + "-" + Reader["compte"].ToString();
			        }//if
			        if ( !string.IsNullOrEmpty(Reader["description_transaction"].ToString()) )
			        {
			            if ( !string.IsNullOrEmpty(description) )
			            {
			                description += ", ";
			            }//if
			            description += Reader["description_transaction"].ToString();
			        }//if
			        if ( !string.IsNullOrEmpty(Reader["description"].ToString()) )
			        {
			            if ( !string.IsNullOrEmpty(description) )
			            {
			                description += ", ";
			            }//if
			            description += Reader["description"].ToString() + ".";
			        }//if
			        if ( !string.IsNullOrEmpty(Reader["projet"].ToString()) )
			        {
			            description += " <" + Reader["projet"].ToString() + ">";
			        }//if
			        else if ( !string.IsNullOrEmpty(description) )
			        {
			            description += ".";
			        }//else if
			        string sDébit, sCrédit;
			        if ( string.IsNullOrEmpty(Reader["debit"].ToString()) )
			        {
			            sDébit = "-";
			        }//if
			        else
			        {
			            dTemp = double.Parse(Reader["debit"].ToString());
			            dTotalDébits += dTemp;
			            sDébit = dTemp.ToString("N2", _cultureIG);
			        }//else
			        if ( string.IsNullOrEmpty(Reader["credit"].ToString()) )
			        {
			            sCrédit = "-";
			        }//if
			        else
			        {
			            dTemp = double.Parse(Reader["credit"].ToString());
			            dTotalCrédits += dTemp;
			            sCrédit = dTemp.ToString("N2", _cultureIG);
			        }//else
			        tblListe.TableModel.Rows.Add(new Row(new Cell[] { 
			                new Cell(Reader["id"].ToString()), 
			                new Cell(date_transaction), 
			                new Cell(description), 
			                new Cell(sDébit), 
			                new Cell(sCrédit)
			            }));
			    }//while
			    Reader.Close();
			    //Totaux
			    tblListe.TableModel.Rows.Add(
			        new Row(new Cell[] { new Cell(), new Cell(), new Cell(), new Cell(), new Cell() }
			        ));
			    Row ligneTotaux = new Row(new Cell[] { 
			        new Cell(), 
			        new Cell(), 
			        new Cell("TOTAL :"), 
			        new Cell(dTotalDébits.ToString("N2", _cultureIG)), 
			        new Cell(dTotalCrédits.ToString("N2", _cultureIG)) 
			    });
			    ligneTotaux.Font = new Font(tblListe.Font.FontFamily, tblListe.Font.Size + 1, FontStyle.Bold | FontStyle.Underline);
			    tblListe.TableModel.Rows.Add(ligneTotaux);
			}//try
			catch ( Exception ex )
			{
			    if ( !Reader.IsClosed )
			    {
			        Reader.Close();
			    }//if
			    System.Diagnostics.Debug.WriteLine(System.Environment.NewLine);
			    System.Diagnostics.Debug.WriteLine(ex.Message);
			    eValeurRetournée = ex;
			}//catch

			return eValeurRetournée;
		}

		#endregion Public Methods
	}
}