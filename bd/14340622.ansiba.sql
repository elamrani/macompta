USE `macompta` ;

-- -----------------------------------------------------

-- Table `macompta`.`ansiba`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `macompta`.`ansiba`;

CREATE  TABLE IF NOT EXISTS `macompta`.`ansiba` (

  `id` INT NOT NULL AUTO_INCREMENT ,

  `date` DATE NOT NULL ,

  `montant_total` DOUBLE NOT NULL DEFAULT 0 ,

  `montant_nisab` DOUBLE NOT NULL DEFAULT 0 ,

  `montant_zakat` DOUBLE NOT NULL DEFAULT 0 ,

  `supprimee` TINYINT(1) NOT NULL DEFAULT FALSE ,

  PRIMARY KEY (`id`) )

ENGINE = InnoDB;


GRANT INSERT, UPDATE, SELECT, DELETE ON TABLE `macompta`.`ansiba` TO 'user_macompta';
