CREATE DATABASE  IF NOT EXISTS `macompta` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `macompta`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: macompta
-- ------------------------------------------------------
-- Server version	5.5.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contenu_transaction`
--

DROP TABLE IF EXISTS `contenu_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contenu_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_transaction` int(11) NOT NULL,
  `id_compte` int(4) NOT NULL,
  `id_projet` int(11) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `debit` double DEFAULT NULL,
  `credit` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_transaction_has_compte_compte1` (`id_compte`),
  KEY `fk_transaction_has_compte_transaction1` (`id_transaction`),
  KEY `fk_transaction_has_compte_projet1` (`id_projet`),
  CONSTRAINT `fk_transaction_has_compte_compte1` FOREIGN KEY (`id_compte`) REFERENCES `compte` (`id`),
  CONSTRAINT `fk_transaction_has_compte_projet1` FOREIGN KEY (`id_projet`) REFERENCES `projet` (`id`) ON DELETE SET NULL,
  CONSTRAINT `fk_transaction_has_compte_transaction1` FOREIGN KEY (`id_transaction`) REFERENCES `transaction` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contenu_transaction`
--

LOCK TABLES `contenu_transaction` WRITE;
/*!40000 ALTER TABLE `contenu_transaction` DISABLE KEYS */;
INSERT INTO `contenu_transaction` VALUES (1,1,1020,NULL,NULL,299.5,NULL),(2,1,5320,NULL,NULL,NULL,299.5),(3,2,1020,NULL,NULL,56,NULL),(4,2,5380,NULL,NULL,NULL,56),(5,3,1020,NULL,NULL,25,NULL),(6,3,5330,NULL,NULL,NULL,25),(7,4,1030,NULL,NULL,100,NULL),(8,4,5420,NULL,NULL,NULL,100),(9,5,1020,NULL,NULL,480,NULL),(10,5,5930,NULL,NULL,NULL,480),(11,6,1030,NULL,NULL,796,NULL),(12,6,5520,NULL,NULL,NULL,796),(13,7,1030,NULL,NULL,50,NULL),(14,7,5560,NULL,NULL,NULL,50),(15,8,1020,NULL,NULL,20,NULL),(16,8,5380,NULL,NULL,NULL,20),(17,9,1020,NULL,NULL,110.6,NULL),(18,9,5320,NULL,NULL,NULL,110.6),(19,10,1040,NULL,NULL,30,NULL),(20,10,5920,1,NULL,NULL,30),(21,11,1040,NULL,NULL,16,NULL),(22,11,5370,1,NULL,NULL,16),(23,12,1040,NULL,NULL,40,NULL),(24,12,5060,1,NULL,NULL,40),(25,13,1030,NULL,NULL,55,NULL),(26,13,5150,NULL,'3 piles',NULL,55),(27,14,1040,NULL,NULL,50,NULL),(28,14,5060,1,NULL,NULL,50),(29,15,1040,NULL,NULL,100,NULL),(30,15,5260,1,NULL,NULL,100),(31,16,1030,NULL,NULL,20,NULL),(32,16,5460,NULL,NULL,NULL,20),(33,17,1030,NULL,NULL,15,NULL),(34,17,5460,NULL,NULL,NULL,15),(35,18,1030,NULL,NULL,5,NULL),(36,18,5460,NULL,NULL,NULL,5),(37,19,1030,NULL,NULL,35,NULL),(38,19,5150,NULL,NULL,NULL,35),(39,20,1030,NULL,NULL,15,NULL),(40,20,5150,NULL,NULL,NULL,15),(41,21,1010,NULL,NULL,30000,NULL),(42,21,1040,NULL,NULL,NULL,30000),(43,22,1040,NULL,NULL,16,NULL),(44,22,5370,NULL,NULL,NULL,16),(45,23,1020,NULL,NULL,463.6,NULL),(46,23,5320,NULL,NULL,NULL,463.6),(47,24,1020,NULL,NULL,24,NULL),(48,24,5370,NULL,NULL,NULL,24),(49,25,1020,NULL,NULL,31,NULL),(50,25,5330,NULL,NULL,NULL,31),(51,26,1030,NULL,NULL,6,NULL),(52,26,5405,NULL,NULL,NULL,6),(53,27,1020,NULL,NULL,151,NULL),(54,27,5320,NULL,NULL,NULL,151),(55,28,1020,NULL,NULL,9,NULL),(56,28,5350,NULL,NULL,NULL,5),(57,28,5370,NULL,NULL,NULL,4),(58,29,1030,NULL,NULL,100,NULL),(59,29,5940,2,NULL,NULL,100),(60,30,1030,NULL,NULL,NULL,2500),(61,30,4030,NULL,NULL,2500,NULL),(62,31,1030,NULL,NULL,40,NULL),(63,31,5910,NULL,NULL,NULL,40),(64,32,1040,NULL,NULL,30000,NULL),(65,32,5040,1,NULL,NULL,30000),(66,33,1040,NULL,NULL,50,NULL),(67,33,5060,1,NULL,NULL,50),(68,34,1030,NULL,NULL,34,NULL),(69,34,5840,2,'5kg',NULL,34),(70,35,1010,NULL,NULL,183,NULL),(71,35,5260,2,NULL,NULL,183),(72,36,1010,NULL,NULL,364.53,NULL),(73,36,5280,NULL,'Au titre du mois de Mai 2012',NULL,72.83),(74,36,5290,1,NULL,NULL,291.7),(75,37,1020,NULL,NULL,NULL,3300),(76,37,4030,NULL,NULL,3300,NULL),(77,38,1020,NULL,NULL,400.1,NULL),(78,38,5320,NULL,NULL,NULL,400.1),(79,39,1020,NULL,NULL,50,NULL),(80,39,5380,NULL,NULL,NULL,50),(81,40,1020,NULL,NULL,28,NULL),(82,40,5330,NULL,NULL,NULL,28),(83,41,1010,NULL,NULL,406.25,NULL),(84,41,5270,2,NULL,NULL,406.25),(85,42,1040,NULL,NULL,16,NULL),(86,42,5370,1,NULL,NULL,16),(87,43,1020,NULL,NULL,67.2,NULL),(88,43,5320,NULL,'eau minérale et 8 yoghourts',NULL,67.2),(89,44,1020,NULL,NULL,30,NULL),(90,44,5930,NULL,NULL,NULL,11),(91,44,5320,NULL,NULL,NULL,19),(92,45,1040,NULL,NULL,18,NULL),(93,45,5370,1,NULL,NULL,8),(94,45,5350,1,NULL,NULL,10),(95,46,2050,1,NULL,60000,NULL),(96,46,1040,NULL,NULL,NULL,60000),(97,47,1040,NULL,NULL,50,NULL),(98,47,5060,1,NULL,NULL,50),(99,48,1040,NULL,NULL,17,NULL),(100,48,5350,1,NULL,NULL,5),(101,48,5370,1,NULL,NULL,12),(102,49,1040,NULL,NULL,NULL,3000),(103,49,2050,1,NULL,3000,NULL),(104,50,1020,NULL,NULL,196,NULL),(105,50,5320,NULL,NULL,NULL,196),(106,51,1020,NULL,NULL,20,NULL),(107,51,5350,NULL,NULL,NULL,20),(108,52,1020,NULL,NULL,357.1,NULL),(109,52,5320,NULL,NULL,NULL,357.1),(110,53,1030,NULL,NULL,34.2,NULL),(111,53,5670,NULL,NULL,NULL,20.2),(112,53,5650,NULL,NULL,NULL,14),(113,54,1040,NULL,NULL,17,NULL),(114,54,5370,1,NULL,NULL,5),(115,54,5370,1,NULL,NULL,12),(116,55,1020,NULL,NULL,7,NULL),(117,55,5330,NULL,NULL,NULL,7),(118,56,1030,NULL,NULL,30,NULL),(119,56,5510,NULL,NULL,NULL,30),(120,57,1020,NULL,NULL,17,NULL),(121,57,5330,NULL,NULL,NULL,17),(122,58,1040,NULL,NULL,30000,NULL),(123,58,5040,1,NULL,NULL,30000),(124,59,1020,NULL,NULL,31.5,NULL),(125,59,5320,NULL,NULL,NULL,31.5),(126,60,1040,NULL,NULL,1450,NULL),(127,60,5010,1,NULL,NULL,1450),(128,61,1040,NULL,NULL,13,NULL),(129,61,5350,NULL,NULL,NULL,5),(130,61,5370,NULL,NULL,NULL,8),(131,62,1020,NULL,NULL,NULL,3531.2),(132,62,1030,NULL,NULL,NULL,1230.4),(133,62,1040,NULL,NULL,NULL,17815.9),(134,62,1010,NULL,NULL,NULL,61062.65),(135,62,2030,NULL,NULL,50000,NULL),(136,62,2040,NULL,NULL,30000,NULL),(137,62,2010,NULL,NULL,35000,NULL),(138,62,2020,NULL,NULL,20000,NULL),(139,62,3020,NULL,NULL,NULL,51359.85);
/*!40000 ALTER TABLE `contenu_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classe_compte`
--

DROP TABLE IF EXISTS `classe_compte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classe_compte` (
  `numero_debut` int(4) NOT NULL,
  `numero_fin` int(4) NOT NULL,
  `nom` varchar(50) NOT NULL,
  PRIMARY KEY (`numero_debut`,`numero_fin`),
  UNIQUE KEY `nom_UNIQUE` (`nom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classe_compte`
--

LOCK TABLES `classe_compte` WRITE;
/*!40000 ALTER TABLE `classe_compte` DISABLE KEYS */;
INSERT INTO `classe_compte` VALUES (1000,1999,'Actif'),(3000,3999,'Avoir'),(5000,5999,'Dépense'),(2000,2999,'Passif'),(4000,4999,'Revenu');
/*!40000 ALTER TABLE `classe_compte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `solde_total`
--

DROP TABLE IF EXISTS `solde_total`;
/*!50001 DROP VIEW IF EXISTS `solde_total`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `solde_total` (
  `solde` double
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `conciliation`
--

DROP TABLE IF EXISTS `conciliation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conciliation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `reference` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conciliation`
--

LOCK TABLES `conciliation` WRITE;
/*!40000 ALTER TABLE `conciliation` DISABLE KEYS */;
/*!40000 ALTER TABLE `conciliation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `charger_solde_compte`
--

DROP TABLE IF EXISTS `charger_solde_compte`;
/*!50001 DROP VIEW IF EXISTS `charger_solde_compte`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `charger_solde_compte` (
  `id_compte` int(4),
  `date` datetime,
  `solde` double
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `charger_transaction`
--

DROP TABLE IF EXISTS `charger_transaction`;
/*!50001 DROP VIEW IF EXISTS `charger_transaction`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `charger_transaction` (
  `id` int(11),
  `partenaire` varchar(150),
  `id_conciliation` int(11),
  `date` datetime,
  `description_transaction` varchar(250),
  `numero_cheque` varchar(150),
  `id_contenu_transaction` int(11),
  `id_compte` int(4),
  `compte` varchar(150),
  `projet` varchar(150),
  `description` varchar(250),
  `debit` double,
  `credit` double
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `charger_plan_comptable`
--

DROP TABLE IF EXISTS `charger_plan_comptable`;
/*!50001 DROP VIEW IF EXISTS `charger_plan_comptable`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `charger_plan_comptable` (
  `id` int(4),
  `compte` varchar(155),
  `id_type_compte` int(11),
  `type` varchar(50),
  `afficher` tinyint(1)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `projet`
--

DROP TABLE IF EXISTS `projet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(150) NOT NULL,
  `date_debut` datetime DEFAULT NULL,
  `date_fin` datetime DEFAULT NULL,
  `afficher` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom_UNIQUE` (`nom`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projet`
--

LOCK TABLES `projet` WRITE;
/*!40000 ALTER TABLE `projet` DISABLE KEYS */;
INSERT INTO `projet` VALUES (1,'Hay Riyad - Phase V - Gros-Oeuvres',NULL,NULL,1),(2,'Harhoura',NULL,NULL,1);
/*!40000 ALTER TABLE `projet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partenaire`
--

DROP TABLE IF EXISTS `partenaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partenaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(150) NOT NULL,
  `adresse` varchar(250) DEFAULT NULL,
  `tel1` varchar(150) DEFAULT NULL,
  `tel2` varchar(150) DEFAULT NULL,
  `web` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `assistant` varchar(150) DEFAULT NULL,
  `fonction` varchar(150) DEFAULT NULL,
  `tel_assistant` varchar(150) DEFAULT NULL,
  `afficher` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom_UNIQUE` (`nom`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partenaire`
--

LOCK TABLES `partenaire` WRITE;
/*!40000 ALTER TABLE `partenaire` DISABLE KEYS */;
INSERT INTO `partenaire` VALUES (1,'Aswak Salam',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(2,'Vendeur de Journaux',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(3,'Magasin Divers',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(4,'Vendeur de Propane',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(5,'Divers',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(6,'Famille',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(7,'Divers Transporteurs',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(8,'Autobus',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(9,'Electricien',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(10,'Ouvriers',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(11,'Acima',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(12,'Blanchisserie',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(13,'Gardiennage Harhoura',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(14,'Coiffeur',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(15,'Abderrahman Ettaleb',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(16,'Ittisalat Al Maghrib',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(17,'Redal',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(18,'Mohamed Y.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(19,'Grand Taxi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(20,'Pharmacie',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `partenaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_partenaire` int(11) DEFAULT NULL,
  `id_conciliation` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `numero_cheque` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_transaction_partenaire` (`id_partenaire`),
  KEY `fk_transaction_conciliation1` (`id_conciliation`),
  CONSTRAINT `fk_transaction_conciliation1` FOREIGN KEY (`id_conciliation`) REFERENCES `conciliation` (`id`),
  CONSTRAINT `fk_transaction_partenaire` FOREIGN KEY (`id_partenaire`) REFERENCES `partenaire` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES (1,1,NULL,'2012-06-01 08:51:51','Alimentation Générale',NULL),(2,NULL,NULL,'2012-06-01 08:53:47','Déplacement de Rabat, Salé, Rabat et retour',NULL),(3,2,NULL,'2012-06-01 08:56:09','4 journaux et 1 hebdomadaire',NULL),(4,3,NULL,'2012-06-01 08:58:15','Achat d\'une paire de chaussure pour Amina',NULL),(5,4,NULL,'2012-06-02 09:00:01','bouteille de propane 35kg',NULL),(6,5,NULL,'2012-06-02 09:02:35','Zakat du 01/03/2011 au 31/05/2011',NULL),(7,6,NULL,'2012-06-03 09:06:09','Somme servie à ma soeur maternelle',NULL),(8,7,NULL,'2012-06-03 09:43:45','Déplacement de Amina à Rabat et retour',NULL),(9,1,NULL,'2012-06-04 09:53:20',NULL,NULL),(10,NULL,NULL,'2012-06-04 09:54:45','3 copies du plan de la villa de hay riyad',NULL),(11,8,NULL,'2012-06-04 09:58:51','Déplacement à hay riyad et retour',NULL),(12,9,NULL,'2012-06-04 10:00:49','Montant versé à l\'électricien et à son aide',NULL),(13,5,NULL,'2012-06-05 10:03:28','3 piles pour montres à bracelet',NULL),(14,10,NULL,'2012-06-06 10:05:45','Somme servie aux ouvriers du chantier',NULL),(15,NULL,NULL,'2012-06-05 10:09:28','Recharge',NULL),(16,NULL,NULL,'2012-06-06 10:13:09','Bain Maure pour Amina',NULL),(17,NULL,NULL,'2012-06-06 10:17:54','Paire de ciseaux pour barbe',NULL),(18,NULL,NULL,'2012-06-06 10:20:04','Bâtonnets',NULL),(19,NULL,NULL,'2012-06-06 10:22:34','Tendeur pour butane',NULL),(20,5,NULL,'2012-06-06 10:23:50','3 piles',NULL),(21,NULL,NULL,'2012-06-06 10:24:52','Retraits en espèces',NULL),(22,8,NULL,'2012-06-07 10:25:55','Hay riyad et retour',NULL),(23,11,NULL,'2012-06-08 10:30:26','Alimentation générale',NULL),(24,8,NULL,'2012-06-06 10:32:36','Rabat - Salé et retour',NULL),(25,2,NULL,'2012-06-08 10:37:45','4 journaux et 2 hebdo.',NULL),(26,12,NULL,'2012-06-08 10:51:12','Repassage de la djellaba de Amina',NULL),(27,NULL,NULL,'2012-06-09 10:56:04','Fruits et légumes',NULL),(28,7,NULL,'2012-06-06 10:58:06','Déplacement à Témara et retour',NULL),(29,13,NULL,'2012-06-10 10:59:39','Juin 2012',NULL),(30,6,NULL,'2012-06-11 11:03:15',NULL,NULL),(31,14,NULL,'2012-06-12 11:05:09',NULL,NULL),(32,15,NULL,'2012-06-12 11:06:07','11e versement en main propre','espèces'),(33,10,NULL,'2012-06-12 11:08:05','pourboires hebdomadaires',NULL),(34,3,NULL,'2012-06-13 11:09:05','Engrais',NULL),(35,16,NULL,'2012-06-13 11:10:26','Factures de téléphone et Internet',NULL),(36,17,NULL,'2012-06-13 11:11:55','Règlement de factures',NULL),(37,6,NULL,'2012-06-15 11:15:41','Contribution au titre du mois de juillet 2012',NULL),(38,1,NULL,'2012-06-15 11:16:51',NULL,NULL),(39,7,NULL,'2012-06-15 11:22:07','inconnu',NULL),(40,2,NULL,'2012-06-15 11:34:12','5 journaux et 1 hebdo.',NULL),(41,17,NULL,'2012-06-15 11:41:40','Mai 2012',NULL),(42,8,NULL,'2012-06-15 11:52:27',NULL,NULL),(43,3,NULL,'2012-06-16 11:53:21',NULL,NULL),(44,3,NULL,'2012-06-16 11:55:26','Bouteille de gaz PM et produits alimentaires',NULL),(45,7,NULL,'2012-06-16 11:56:58','Rabat - hay riyad et retour',NULL),(46,18,NULL,'2012-06-16 11:58:50','Prêt accordé en espèces',NULL),(47,10,NULL,'2012-06-19 12:00:32','pourboire hebdomadaire',NULL),(48,7,NULL,'2012-06-19 12:01:28','Rabat - hey riyad et retour',NULL),(49,18,NULL,'2012-06-20 12:02:46','Prêt accordé en espèces',NULL),(50,11,NULL,'2012-06-20 12:04:20',NULL,NULL),(51,19,NULL,'2012-06-20 12:04:57','Temara',NULL),(52,3,NULL,'2012-06-21 12:05:39',NULL,NULL),(53,20,NULL,'2012-06-21 12:12:58','Renforcer les gencives',NULL),(54,7,NULL,'2012-06-21 12:14:20','Rabat-Hay riyad et retour',NULL),(55,2,NULL,'2012-06-21 12:18:30','2 journaux',NULL),(56,5,NULL,'2012-08-06 12:19:52',NULL,NULL),(57,2,NULL,'2012-06-22 12:21:11','2 journaux et 1 hebdo.',NULL),(58,15,NULL,'2012-06-22 12:22:18','13e versement en espèces',NULL),(59,11,NULL,'2012-06-22 12:23:05',NULL,NULL),(60,3,NULL,'2012-06-23 12:23:44','Achat d\'article d\'électricité',NULL),(61,7,NULL,'2012-06-23 12:25:06','Rabat-hay riyad et retour',NULL),(62,NULL,NULL,'2012-05-31 12:26:11','Ecriture d\'ouverture',NULL);
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `totaux_projets`
--

DROP TABLE IF EXISTS `totaux_projets`;
/*!50001 DROP VIEW IF EXISTS `totaux_projets`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `totaux_projets` (
  `id_projet` int(11),
  `nom` varchar(150),
  `debit` double,
  `credit` double
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `type_compte`
--

DROP TABLE IF EXISTS `type_compte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_compte` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom_UNIQUE` (`nom`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_compte`
--

LOCK TABLES `type_compte` WRITE;
/*!40000 ALTER TABLE `type_compte` DISABLE KEYS */;
INSERT INTO `type_compte` VALUES (3,'Compte de Sous-Groupe'),(2,'Compte Normal'),(1,'Titre de Compte'),(5,'Total de Compte'),(4,'Total de Sous-Groupe');
/*!40000 ALTER TABLE `type_compte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compte`
--

DROP TABLE IF EXISTS `compte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compte` (
  `id` int(4) NOT NULL,
  `id_type_compte` int(11) NOT NULL DEFAULT '2',
  `nom` varchar(150) NOT NULL,
  `numero` varchar(150) DEFAULT NULL,
  `institution` varchar(150) DEFAULT NULL,
  `agence` varchar(150) DEFAULT NULL,
  `budget_quotidien` double DEFAULT NULL,
  `type_nombre_jours` int(11) DEFAULT NULL,
  `afficher` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_compte_type_compte1` (`id_type_compte`),
  CONSTRAINT `fk_compte_type_compte1` FOREIGN KEY (`id_type_compte`) REFERENCES `type_compte` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compte`
--

LOCK TABLES `compte` WRITE;
/*!40000 ALTER TABLE `compte` DISABLE KEYS */;
INSERT INTO `compte` VALUES (1000,1,'Comptes d\'Administration',NULL,NULL,NULL,NULL,NULL,1),(1010,2,'SGMB','000 050 00 106084 77','Société Générale Marocaine de Banques','Moulay Youssef - Rabat',NULL,NULL,1),(1020,3,'Alimentation & Produits rattachés','Affectation des fonds de dépenses','Gestion du Budget Familial ( G B F )','Harhoura',110,0,1),(1030,3,'Fonds Propres','Fonds destinés à couvrir la totalité des dépenses à l\'\'exception de celles de l\'\'alimentation et des produits rattachés','Gestion du Budget Familial ( GBF )','Harhoura',NULL,NULL,1),(1040,3,'Gestion des ressources financières ( Espèces )',NULL,NULL,NULL,NULL,NULL,1),(1050,4,'Sous Total des Fonds',NULL,NULL,NULL,NULL,NULL,1),(1099,5,'Total des Comptes d\'Administration',NULL,NULL,NULL,NULL,NULL,1),(2000,1,'Emprunts',NULL,NULL,NULL,NULL,NULL,1),(2010,2,'Hajja Amina Houir Alami',NULL,NULL,NULL,NULL,NULL,1),(2020,2,'Haj Mohamed Houir Alami',NULL,NULL,NULL,NULL,NULL,1),(2030,2,'Haj Abdelouahed Houir Alami',NULL,NULL,NULL,NULL,NULL,1),(2040,2,'Hajja Houir Alami Laïla',NULL,NULL,NULL,NULL,NULL,1),(2050,2,'Mohamed Yassine Elamrani',NULL,NULL,NULL,NULL,NULL,1),(2099,5,'Total des Emprunts',NULL,NULL,NULL,NULL,NULL,1),(3000,1,'Comptes d\'Administration',NULL,NULL,NULL,NULL,NULL,1),(3010,2,'Bénéfices',NULL,NULL,NULL,NULL,NULL,1),(3020,2,'Avoir initial',NULL,NULL,NULL,NULL,NULL,1),(3099,5,'Total du Compte d\'Administration',NULL,NULL,NULL,NULL,NULL,1),(4000,1,'Comptes des Revenus',NULL,NULL,NULL,NULL,NULL,1),(4010,2,'Mutuelle Générale des PTT',NULL,NULL,NULL,NULL,NULL,1),(4020,2,'Caisse Marocaine de Retraite',NULL,NULL,NULL,NULL,NULL,1),(4030,2,'H. A. AMINA',NULL,NULL,NULL,NULL,NULL,1),(4099,5,'Total des Revenus',NULL,NULL,NULL,NULL,NULL,1),(5000,1,'Frais de Construction',NULL,NULL,NULL,NULL,NULL,1),(5010,2,'Electricité',NULL,NULL,NULL,NULL,NULL,1),(5020,2,'Plomberie',NULL,NULL,NULL,NULL,NULL,1),(5025,2,'Equipements et Accessoires',NULL,NULL,NULL,NULL,NULL,1),(5030,3,'Architecte',NULL,NULL,NULL,NULL,NULL,1),(5040,3,'Entrepreneur',NULL,NULL,NULL,NULL,NULL,1),(5050,3,'Autres Corps de métiers',NULL,NULL,NULL,NULL,NULL,1),(5060,3,'Pourboires',NULL,NULL,NULL,NULL,NULL,1),(5070,3,'Taxes',NULL,NULL,NULL,NULL,NULL,1),(5080,3,'Divers',NULL,NULL,NULL,NULL,NULL,1),(5090,4,'Total des Frais des Professionnels',NULL,NULL,NULL,NULL,NULL,1),(5099,5,'Total Frais de Construction',NULL,NULL,NULL,NULL,NULL,1),(5100,1,'Dépenses d\'Administration',NULL,NULL,NULL,NULL,NULL,1),(5110,3,'Articles de cuisine',NULL,NULL,NULL,NULL,NULL,1),(5130,3,'Articles pour téléphone',NULL,NULL,NULL,NULL,NULL,1),(5140,3,'Articles informatique',NULL,NULL,NULL,NULL,NULL,1),(5150,3,'Articles divers',NULL,NULL,NULL,NULL,NULL,1),(5190,4,'Total Equipements et Articles divers',NULL,NULL,NULL,NULL,NULL,1),(5210,3,'Frais de tenue de compte',NULL,NULL,NULL,NULL,NULL,1),(5220,3,'Relevés d\'opérations',NULL,NULL,NULL,NULL,NULL,1),(5230,3,'Autres frais bancaires',NULL,NULL,NULL,NULL,NULL,1),(5240,4,'Total des Frais Bancaires',NULL,NULL,NULL,NULL,NULL,1),(5250,2,'Cabinet Immobilier Benomar',NULL,NULL,NULL,NULL,NULL,1),(5260,2,'Ittisalat Al Maghrib',NULL,NULL,NULL,NULL,NULL,1),(5270,3,'REDAL Harhoura',NULL,NULL,NULL,NULL,NULL,1),(5280,3,'REDAL Rabat-Hassan',NULL,NULL,NULL,NULL,NULL,1),(5290,3,'REDAL Rabat-Hay Riyad',NULL,NULL,NULL,NULL,NULL,1),(5310,4,'Total des Réglements des créances dûes de Redal',NULL,NULL,NULL,NULL,NULL,1),(5320,3,'Frais d\'alimentation',NULL,NULL,NULL,NULL,NULL,1),(5330,3,'Journaux et Ecrits périodiques',NULL,NULL,NULL,NULL,NULL,1),(5340,4,'Total des Produits alimentaires et articles rattachés',NULL,NULL,NULL,NULL,NULL,1),(5350,3,'Frais de Grand-Taxi',NULL,NULL,NULL,NULL,NULL,1),(5360,3,'Frais de Petit-Taxi',NULL,NULL,NULL,NULL,NULL,1),(5370,3,'Frais d\'Autobus',NULL,NULL,NULL,NULL,NULL,1),(5380,3,'Autres Frais de transport',NULL,NULL,NULL,NULL,NULL,1),(5390,4,'Total des Frais de Transport',NULL,NULL,NULL,NULL,NULL,1),(5400,3,'Tissus et Accessoires',NULL,NULL,NULL,NULL,NULL,1),(5405,3,'Frais de Nettoyage',NULL,NULL,NULL,NULL,NULL,1),(5410,3,'Vêtements',NULL,NULL,NULL,NULL,NULL,1),(5420,3,'Chaussures et sandales',NULL,NULL,NULL,NULL,NULL,1),(5430,3,'Articles de sport',NULL,NULL,NULL,NULL,NULL,1),(5440,3,'Frais de confection (Khyata)',NULL,NULL,NULL,NULL,NULL,1),(5450,4,'Total des Frais d\'Habillement, chaussures et articles similaires',NULL,NULL,NULL,NULL,NULL,1),(5460,2,'Soins Personnels',NULL,NULL,NULL,NULL,NULL,1),(5499,5,'Total des Dépenses d\'Administration',NULL,NULL,NULL,NULL,NULL,1),(5500,1,'Dons, Cadeaux, Souab',NULL,NULL,NULL,NULL,NULL,1),(5510,2,'Souab attaazyate',NULL,NULL,NULL,NULL,NULL,1),(5520,2,'Azzakat',NULL,NULL,NULL,NULL,NULL,1),(5530,2,'Zyarate Almaride',NULL,NULL,NULL,NULL,NULL,1),(5540,2,'Cadeaux Divers',NULL,NULL,NULL,NULL,NULL,1),(5550,2,'fi sabil Allah',NULL,NULL,NULL,NULL,NULL,1),(5560,2,'Cadeau en espèces',NULL,NULL,NULL,NULL,NULL,1),(5570,2,'Cadeau à mon père',NULL,NULL,NULL,NULL,NULL,1),(5599,5,'Total Dons , Cadeaux, Souab',NULL,NULL,NULL,NULL,NULL,1),(5600,1,'Dépenses de santé',NULL,NULL,NULL,NULL,NULL,1),(5610,3,'Frais d\'\'Ophtalmologiste',NULL,NULL,NULL,NULL,NULL,1),(5620,3,'Frais de Dentiste',NULL,NULL,NULL,NULL,NULL,1),(5630,3,'Frais de Médecin - Autres',NULL,NULL,NULL,NULL,NULL,1),(5640,4,'Total des Médecins',NULL,NULL,NULL,NULL,NULL,1),(5650,3,'Dolipranne',NULL,NULL,NULL,NULL,NULL,1),(5660,3,'Alcool à 70°',NULL,NULL,NULL,NULL,NULL,1),(5670,3,'Arthrodont Enoxolone 1%',NULL,NULL,NULL,NULL,NULL,1),(5680,3,'Autres Médicaments',NULL,NULL,NULL,NULL,NULL,1),(5690,4,'Total des Médicaments',NULL,NULL,NULL,NULL,NULL,1),(5700,2,'Analyses médicales',NULL,NULL,NULL,NULL,NULL,1),(5710,2,'Autres Frais de santé',NULL,NULL,NULL,NULL,NULL,1),(5799,5,'Total des Dépenses de santé',NULL,NULL,NULL,NULL,NULL,1),(5800,1,'Produits et articles de jardinage',NULL,NULL,NULL,NULL,NULL,1),(5810,2,'Insecticide',NULL,NULL,NULL,NULL,NULL,1),(5820,3,'Engrais granulés',NULL,NULL,NULL,NULL,NULL,1),(5830,3,'Engrais liquide',NULL,NULL,NULL,NULL,NULL,1),(5840,3,'Engrais composé Organo-Chimique',NULL,NULL,NULL,NULL,NULL,1),(5850,4,'Total Engrais',NULL,NULL,NULL,NULL,NULL,1),(5860,2,'Articles pour jardinage',NULL,NULL,NULL,NULL,NULL,1),(5899,5,'Total Produits et articles de jardinage',NULL,NULL,NULL,NULL,NULL,1),(5900,1,'Produits et Services',NULL,NULL,NULL,NULL,NULL,1),(5910,2,'Coiffeur pour homme',NULL,NULL,NULL,NULL,NULL,1),(5920,2,'Photocopies',NULL,NULL,NULL,NULL,NULL,1),(5930,2,'Frais de Gaz',NULL,NULL,NULL,NULL,NULL,1),(5940,2,'Frais d\'entretient divers',NULL,NULL,NULL,NULL,NULL,1),(5999,5,'Total des Produits et Services',NULL,NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `compte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configuration`
--

DROP TABLE IF EXISTS `configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mot_passe` varchar(50) NOT NULL DEFAULT '',
  `nisab` double NOT NULL DEFAULT '0',
  `chemin_archive` varchar(250) NOT NULL,
  `chemin_copie_archive` varchar(250) NOT NULL,
  `id_compte_zakat_due` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_configuration_compte1` (`id_compte_zakat_due`),
  CONSTRAINT `fk_configuration_compte1` FOREIGN KEY (`id_compte_zakat_due`) REFERENCES `compte` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuration`
--

LOCK TABLES `configuration` WRITE;
/*!40000 ALTER TABLE `configuration` DISABLE KEYS */;
INSERT INTO `configuration` VALUES (1,'',0,'','',NULL);
/*!40000 ALTER TABLE `configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `solde_total`
--

/*!50001 DROP TABLE IF EXISTS `solde_total`*/;
/*!50001 DROP VIEW IF EXISTS `solde_total`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `solde_total` AS select sum((ifnull(`contenu_transaction`.`credit`,0) - ifnull(`contenu_transaction`.`debit`,0))) AS `solde` from (`contenu_transaction` join `compte`) where ((`contenu_transaction`.`id_compte` = `compte`.`id`) and (`compte`.`id_type_compte` between 2 and 3) and (`compte`.`id` between 1000 and 1999)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `charger_solde_compte`
--

/*!50001 DROP TABLE IF EXISTS `charger_solde_compte`*/;
/*!50001 DROP VIEW IF EXISTS `charger_solde_compte`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `charger_solde_compte` AS select `contenu_transaction`.`id_compte` AS `id_compte`,`transaction`.`date` AS `date`,sum((ifnull(`contenu_transaction`.`credit`,0) - ifnull(`contenu_transaction`.`debit`,0))) AS `solde` from (`contenu_transaction` join `transaction`) where (`contenu_transaction`.`id_transaction` = `transaction`.`id`) group by `contenu_transaction`.`id_compte`,`transaction`.`date` order by `transaction`.`date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `charger_transaction`
--

/*!50001 DROP TABLE IF EXISTS `charger_transaction`*/;
/*!50001 DROP VIEW IF EXISTS `charger_transaction`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `charger_transaction` AS select `transaction`.`id` AS `id`,`partenaire`.`nom` AS `partenaire`,`transaction`.`id_conciliation` AS `id_conciliation`,`transaction`.`date` AS `date`,`transaction`.`description` AS `description_transaction`,`transaction`.`numero_cheque` AS `numero_cheque`,`contenu_transaction`.`id` AS `id_contenu_transaction`,`contenu_transaction`.`id_compte` AS `id_compte`,`compte`.`nom` AS `compte`,`projet`.`nom` AS `projet`,`contenu_transaction`.`description` AS `description`,`contenu_transaction`.`debit` AS `debit`,`contenu_transaction`.`credit` AS `credit` from ((`compte` join (`contenu_transaction` left join `projet` on((`contenu_transaction`.`id_projet` = `projet`.`id`)))) join (`transaction` left join `partenaire` on((`transaction`.`id_partenaire` = `partenaire`.`id`)))) where ((`contenu_transaction`.`id_transaction` = `transaction`.`id`) and (`contenu_transaction`.`id_compte` = `compte`.`id`)) order by `transaction`.`date` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `charger_plan_comptable`
--

/*!50001 DROP TABLE IF EXISTS `charger_plan_comptable`*/;
/*!50001 DROP VIEW IF EXISTS `charger_plan_comptable`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `charger_plan_comptable` AS select `compte`.`id` AS `id`,concat(`compte`.`id`,'-',`compte`.`nom`) AS `compte`,`compte`.`id_type_compte` AS `id_type_compte`,`type_compte`.`nom` AS `type`,`compte`.`afficher` AS `afficher` from (`compte` join `type_compte`) where (`compte`.`id_type_compte` = `type_compte`.`id`) order by `compte`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `totaux_projets`
--

/*!50001 DROP TABLE IF EXISTS `totaux_projets`*/;
/*!50001 DROP VIEW IF EXISTS `totaux_projets`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `totaux_projets` AS select `contenu_transaction`.`id_projet` AS `id_projet`,`projet`.`nom` AS `nom`,sum(ifnull(`contenu_transaction`.`debit`,0)) AS `debit`,sum(ifnull(`contenu_transaction`.`credit`,0)) AS `credit` from (`contenu_transaction` join `projet` on((`contenu_transaction`.`id_projet` = `projet`.`id`))) group by `contenu_transaction`.`id_projet`,`projet`.`nom` order by `projet`.`nom` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-08-06 12:38:41
