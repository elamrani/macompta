use macompta;

select * from configuration;

CREATE  OR REPLACE VIEW `macompta`.`solde_total` AS
SELECT SUM( IFNULL(`contenu_transaction`.`credit`, 0) - IFNULL(`contenu_transaction`.`debit`, 0) ) AS solde
FROM `contenu_transaction`, `compte`
WHERE (`contenu_transaction`.`id_compte` = `compte`.`id`) 
        AND (`compte`.`id_type_compte` BETWEEN 2 AND 3) 
        AND (`compte`.`id` BETWEEN 1000 AND 1999);

GRANT INSERT, UPDATE, SELECT, DELETE ON TABLE `macompta`.`solde_total` TO 'user_macompta';

CREATE  OR REPLACE VIEW `macompta`.`charger_transaction` AS 
SELECT `transaction`.`id`, `id_partenaire`, `partenaire`.`nom` AS `partenaire`, `id_conciliation`, `date`, 
`transaction`.`description` AS `description_transaction`, `numero_cheque`, 
`contenu_transaction`.`id` AS `id_contenu_transaction`, `id_compte`, `compte`.`nom` AS `compte`, `id_projet`, 
`projet`.`nom` AS`projet`, `contenu_transaction`.`description`, `debit`, `credit` 
FROM  `compte`, `contenu_transaction` LEFT JOIN `projet` ON `contenu_transaction`.`id_projet` = `projet`.`id`, `transaction` 
LEFT JOIN `partenaire` ON  `transaction`.`id_partenaire` = `partenaire`.`id` 
WHERE `contenu_transaction`.`id_transaction` = `transaction`.`id` AND `contenu_transaction`.`id_compte` = `compte`.`id`
ORDER BY `date`;

GRANT INSERT, UPDATE, SELECT, DELETE ON TABLE `macompta`.`charger_transaction` TO 'user_macompta';

