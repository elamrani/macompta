USE `macompta` ;


ALTER TABLE `configuration`
ADD `date_prix_gramme_or` DATE AFTER `mot_passe`,
ADD `prix_gramme_or` DOUBLE NOT NULL DEFAULT 0 AFTER `date_prix_gramme_or`,
ADD `grammes_or` DOUBLE NOT NULL DEFAULT 85 AFTER `prix_gramme_or`,
ADD `nb_fichiers_archives` INT NOT NULL DEFAULT 0 AFTER `chemin_copie_archive`;


ALTER TABLE `configuration`
MODIFY `chemin_archive` VARCHAR(250) NULL DEFAULT NULL,
MODIFY `chemin_copie_archive` VARCHAR(250) NULL DEFAULT NULL;