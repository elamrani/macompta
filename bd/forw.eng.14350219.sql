SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `macompta` ;
CREATE SCHEMA IF NOT EXISTS `macompta` DEFAULT CHARACTER SET utf8 ;
USE `macompta` ;

-- -----------------------------------------------------
-- Table `macompta`.`type_compte`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`type_compte` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `nom_UNIQUE` (`nom` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `macompta`.`compte`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`compte` (
  `id` INT(4) NOT NULL,
  `id_type_compte` INT NOT NULL DEFAULT 2,
  `nom` VARCHAR(150) NOT NULL,
  `numero` VARCHAR(150) NULL DEFAULT NULL,
  `institution` VARCHAR(150) NULL DEFAULT NULL,
  `agence` VARCHAR(150) NULL DEFAULT NULL,
  `budget_quotidien` DOUBLE NULL DEFAULT NULL,
  `type_nombre_jours` INT NULL DEFAULT NULL,
  `afficher` TINYINT(1) NOT NULL DEFAULT TRUE,
  PRIMARY KEY (`id`),
  INDEX `fk_compte_type_compte1_idx` (`id_type_compte` ASC),
  CONSTRAINT `fk_compte_type_compte1`
    FOREIGN KEY (`id_type_compte`)
    REFERENCES `macompta`.`type_compte` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `macompta`.`classe_compte`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`classe_compte` (
  `numero_debut` INT(4) NOT NULL,
  `numero_fin` INT(4) NOT NULL,
  `nom` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`numero_debut`, `numero_fin`),
  UNIQUE INDEX `nom_UNIQUE` (`nom` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `macompta`.`partenaire`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`partenaire` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(150) NOT NULL,
  `adresse` VARCHAR(250) NULL,
  `tel1` VARCHAR(150) NULL,
  `tel2` VARCHAR(150) NULL,
  `web` VARCHAR(250) NULL,
  `email` VARCHAR(250) NULL,
  `assistant` VARCHAR(150) NULL,
  `fonction` VARCHAR(150) NULL,
  `tel_assistant` VARCHAR(150) NULL,
  `afficher` TINYINT(1) NOT NULL DEFAULT TRUE,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `nom_UNIQUE` (`nom` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `macompta`.`conciliation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`conciliation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` DATETIME NOT NULL,
  `reference` VARCHAR(50) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `macompta`.`transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`transaction` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_partenaire` INT NULL,
  `id_conciliation` INT NULL,
  `date` DATE NOT NULL,
  `description` VARCHAR(250) NULL,
  `numero_cheque` VARCHAR(150) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_transaction_partenaire_idx` (`id_partenaire` ASC),
  INDEX `fk_transaction_conciliation1_idx` (`id_conciliation` ASC),
  CONSTRAINT `fk_transaction_partenaire`
    FOREIGN KEY (`id_partenaire`)
    REFERENCES `macompta`.`partenaire` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_transaction_conciliation1`
    FOREIGN KEY (`id_conciliation`)
    REFERENCES `macompta`.`conciliation` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `macompta`.`projet`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`projet` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(150) NOT NULL,
  `date_debut` DATETIME NULL,
  `date_fin` DATETIME NULL,
  `afficher` TINYINT(1) NOT NULL DEFAULT true,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `nom_UNIQUE` (`nom` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `macompta`.`contenu_transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`contenu_transaction` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_transaction` INT NOT NULL,
  `id_compte` INT(4) NOT NULL,
  `id_projet` INT NULL,
  `description` VARCHAR(250) NULL DEFAULT NULL,
  `debit` DOUBLE NULL DEFAULT NULL,
  `credit` DOUBLE NULL DEFAULT NULL,
  INDEX `fk_transaction_has_compte_compte1_idx` (`id_compte` ASC),
  INDEX `fk_transaction_has_compte_transaction1_idx` (`id_transaction` ASC),
  INDEX `fk_transaction_has_compte_projet1_idx` (`id_projet` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_transaction_has_compte_transaction1`
    FOREIGN KEY (`id_transaction`)
    REFERENCES `macompta`.`transaction` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_transaction_has_compte_compte1`
    FOREIGN KEY (`id_compte`)
    REFERENCES `macompta`.`compte` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT,
  CONSTRAINT `fk_transaction_has_compte_projet1`
    FOREIGN KEY (`id_projet`)
    REFERENCES `macompta`.`projet` (`id`)
    ON DELETE SET NULL
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `macompta`.`configuration`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`configuration` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `mot_passe` VARCHAR(50) NOT NULL DEFAULT '',
  `date_prix_gramme_or` DATE NULL,
  `prix_gramme_or` DOUBLE NOT NULL DEFAULT 0,
  `grammes_or` DOUBLE NOT NULL DEFAULT 85,
  `nisab` DOUBLE NOT NULL DEFAULT 0,
  `chemin_archive` VARCHAR(250) NULL,
  `chemin_copie_archive` VARCHAR(250) NULL,
  `nb_fichiers_archives` INT NOT NULL DEFAULT 0,
  `maintenir_suppressions_transactions` TINYINT(1) NOT NULL DEFAULT FALSE,
  `id_compte_zakat_due` INT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_configuration_compte1_idx` (`id_compte_zakat_due` ASC),
  CONSTRAINT `fk_configuration_compte1`
    FOREIGN KEY (`id_compte_zakat_due`)
    REFERENCES `macompta`.`compte` (`id`)
    ON DELETE SET NULL
    ON UPDATE RESTRICT)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `macompta`.`ansiba`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`ansiba` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `montant_total` DOUBLE NOT NULL DEFAULT 0,
  `montant_nisab` DOUBLE NOT NULL DEFAULT 0,
  `montant_zakat` DOUBLE NOT NULL DEFAULT 0,
  `supprimee` TINYINT(1) NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

USE `macompta` ;

-- -----------------------------------------------------
-- Placeholder table for view `macompta`.`charger_transaction`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`charger_transaction` (`id` INT, `id_partenaire` INT, `partenaire` INT, `id_conciliation` INT, `date` INT, `description_transaction` INT, `numero_cheque` INT, `id_contenu_transaction` INT, `id_compte` INT, `compte` INT, `id_projet` INT, `projet` INT, `description` INT, `debit` INT, `credit` INT);

-- -----------------------------------------------------
-- Placeholder table for view `macompta`.`solde_total`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`solde_total` (`solde` INT);

-- -----------------------------------------------------
-- Placeholder table for view `macompta`.`charger_plan_comptable`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`charger_plan_comptable` (`id` INT, `compte` INT, `id_type_compte` INT, `type` INT, `afficher` INT);

-- -----------------------------------------------------
-- Placeholder table for view `macompta`.`charger_solde_compte`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`charger_solde_compte` (`id_compte` INT, `date` INT, `solde` INT);

-- -----------------------------------------------------
-- Placeholder table for view `macompta`.`totaux_projets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`totaux_projets` (`id_projet` INT, `nom` INT, `debit` INT, `credit` INT);

-- -----------------------------------------------------
-- Placeholder table for view `macompta`.`totaux_partenaires`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `macompta`.`totaux_partenaires` (`id_partenaire` INT, `nom` INT, `debit` INT, `credit` INT);

-- -----------------------------------------------------
-- View `macompta`.`charger_transaction`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `macompta`.`charger_transaction`;
USE `macompta`;
CREATE  OR REPLACE VIEW `macompta`.`charger_transaction` AS 
SELECT `transaction`.`id`, `id_partenaire`, `partenaire`.`nom` AS `partenaire`, `id_conciliation`, `date`, 
`transaction`.`description` AS `description_transaction`, `numero_cheque`, 
`contenu_transaction`.`id` AS `id_contenu_transaction`, `id_compte`, `compte`.`nom` AS `compte`, `id_projet`, 
`projet`.`nom` AS`projet`, `contenu_transaction`.`description`, `debit`, `credit` 
FROM  `compte`, `contenu_transaction` LEFT JOIN `projet` ON `contenu_transaction`.`id_projet` = `projet`.`id`, `transaction` 
LEFT JOIN `partenaire` ON  `transaction`.`id_partenaire` = `partenaire`.`id` 
WHERE `contenu_transaction`.`id_transaction` = `transaction`.`id` AND `contenu_transaction`.`id_compte` = `compte`.`id`
ORDER BY `date`;


-- -----------------------------------------------------
-- View `macompta`.`solde_total`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `macompta`.`solde_total`;
USE `macompta`;
CREATE  OR REPLACE VIEW `macompta`.`solde_total` AS
SELECT SUM( IFNULL(`contenu_transaction`.`credit`, 0) - IFNULL(`contenu_transaction`.`debit`, 0) ) AS solde
FROM `contenu_transaction`, `compte`
WHERE (`contenu_transaction`.`id_compte` = `compte`.`id`) 
        AND (`compte`.`id_type_compte` BETWEEN 2 AND 3) 
        AND (`compte`.`id` BETWEEN 1000 AND 1999);

-- -----------------------------------------------------
-- View `macompta`.`charger_plan_comptable`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `macompta`.`charger_plan_comptable`;
USE `macompta`;
CREATE  OR REPLACE VIEW `macompta`.`charger_plan_comptable` AS
SELECT `compte`.`id`, CONCAT(`compte`.`id`, '-', `compte`.`nom`) AS `compte`,  `id_type_compte`, `type_compte`.`nom` AS `type`, `compte`.`afficher` 
FROM `compte`, `type_compte` 
WHERE `compte`.`id_type_compte` = `type_compte`.`id` 
ORDER BY `compte`.`id` ASC;

-- -----------------------------------------------------
-- View `macompta`.`charger_solde_compte`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `macompta`.`charger_solde_compte`;
USE `macompta`;
CREATE  OR REPLACE VIEW `macompta`.`charger_solde_compte` AS
SELECT `id_compte`, `date`, SUM( IFNULL(`contenu_transaction`.`credit`, 0) - IFNULL(`contenu_transaction`.`debit`, 0) ) AS `solde`
FROM `contenu_transaction`, `transaction`
WHERE `contenu_transaction`.`id_transaction` = `transaction`.`id`
GROUP BY `id_compte`, `date`
ORDER BY `date`;

-- -----------------------------------------------------
-- View `macompta`.`totaux_projets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `macompta`.`totaux_projets`;
USE `macompta`;
CREATE  OR REPLACE VIEW `macompta`.`totaux_projets` AS
SELECT `id_projet`, `nom`, SUM( IFNULL(`debit`, 0)) AS `debit`, SUM( IFNULL(`credit`, 0)) AS `credit`
FROM `contenu_transaction` INNER JOIN `projet` ON `contenu_transaction`.`id_projet` = `projet`.`id`
GROUP BY `id_projet`, `nom`
ORDER BY `nom`;

-- -----------------------------------------------------
-- View `macompta`.`totaux_partenaires`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `macompta`.`totaux_partenaires`;
USE `macompta`;
CREATE  OR REPLACE VIEW `macompta`.`totaux_partenaires` AS
SELECT `id_partenaire`, `nom`, SUM( IFNULL(`debit`, 0)) AS `debit`, SUM( IFNULL(`credit`, 0)) AS `credit`
FROM `contenu_transaction`, `transaction`, `partenaire`
WHERE `transaction`.`id` = `contenu_transaction`.`id_transaction` AND `transaction`.`id_partenaire` = `partenaire`.`id`
GROUP BY `id_partenaire`, `nom`
ORDER BY `nom`;
CREATE USER 'user_macompta' IDENTIFIED BY '123123';

GRANT SELECT ON TABLE `macompta`.`charger_transaction` TO 'user_macompta';
GRANT SELECT ON TABLE `macompta`.`solde_total` TO 'user_macompta';
GRANT SELECT ON TABLE `macompta`.`classe_compte` TO 'user_macompta';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `macompta`.`compte` TO 'user_macompta';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `macompta`.`conciliation` TO 'user_macompta';
GRANT UPDATE, SELECT ON TABLE `macompta`.`configuration` TO 'user_macompta';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `macompta`.`contenu_transaction` TO 'user_macompta';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `macompta`.`partenaire` TO 'user_macompta';
GRANT DELETE, INSERT, SELECT, UPDATE ON TABLE `macompta`.`projet` TO 'user_macompta';
GRANT DELETE, INSERT, UPDATE, SELECT ON TABLE `macompta`.`transaction` TO 'user_macompta';
GRANT SELECT ON TABLE `macompta`.`type_compte` TO 'user_macompta';
GRANT SELECT ON TABLE `macompta`.`charger_plan_comptable` TO 'user_macompta';
GRANT SELECT ON TABLE `macompta`.`charger_solde_compte` TO 'user_macompta';
GRANT INSERT, UPDATE, SELECT, DELETE ON TABLE `macompta`.`ansiba` TO 'user_macompta';
GRANT SELECT ON TABLE `macompta`.`totaux_partenaires` TO 'user_macompta';
GRANT SELECT ON TABLE `macompta`.`totaux_projets` TO 'user_macompta';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `macompta`.`type_compte`
-- -----------------------------------------------------
START TRANSACTION;
USE `macompta`;
INSERT INTO `macompta`.`type_compte` (`id`, `nom`) VALUES (1, 'Titre de Groupe');
INSERT INTO `macompta`.`type_compte` (`id`, `nom`) VALUES (2, 'Compte de Groupe');
INSERT INTO `macompta`.`type_compte` (`id`, `nom`) VALUES (3, 'Compte de Sous-Groupe');
INSERT INTO `macompta`.`type_compte` (`id`, `nom`) VALUES (4, 'Total de Sous-Groupe');
INSERT INTO `macompta`.`type_compte` (`id`, `nom`) VALUES (5, 'Total de Groupe');

COMMIT;


-- -----------------------------------------------------
-- Data for table `macompta`.`classe_compte`
-- -----------------------------------------------------
START TRANSACTION;
USE `macompta`;
INSERT INTO `macompta`.`classe_compte` (`numero_debut`, `numero_fin`, `nom`) VALUES (1000, 1999, 'Actif');
INSERT INTO `macompta`.`classe_compte` (`numero_debut`, `numero_fin`, `nom`) VALUES (2000, 2999, 'Passif');
INSERT INTO `macompta`.`classe_compte` (`numero_debut`, `numero_fin`, `nom`) VALUES (3000, 3999, 'Avoir');
INSERT INTO `macompta`.`classe_compte` (`numero_debut`, `numero_fin`, `nom`) VALUES (4000, 4999, 'Revenu');
INSERT INTO `macompta`.`classe_compte` (`numero_debut`, `numero_fin`, `nom`) VALUES (5000, 5999, 'Dépense');

COMMIT;


-- -----------------------------------------------------
-- Data for table `macompta`.`configuration`
-- -----------------------------------------------------
START TRANSACTION;
USE `macompta`;
INSERT INTO `macompta`.`configuration` (`id`, `mot_passe`, `date_prix_gramme_or`, `prix_gramme_or`, `grammes_or`, `nisab`, `chemin_archive`, `chemin_copie_archive`, `nb_fichiers_archives`, `maintenir_suppressions_transactions`, `id_compte_zakat_due`) VALUES (1, '', NULL, 0, 85, 0, '', '', 0, 0, NULL);

COMMIT;

